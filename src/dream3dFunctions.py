#A module for specifically dream3d manipulation and generation of pipelines
import subprocess
import json
import numbers
import random
import math
import os
import h5py
import numpy as np
from scipy.stats import lognorm
import string

if 'D3_PIPELINE_DIR' in os.environ: pathToPipelineRunner=os.environ['D3_PIPELINE_DIR']+'/PipelineRunner'
else: pathToPipelineRunner='~/bin/dream3d/bin/PipelineRunner'
pathToPipelineRunner = os.path.expandvars(os.path.expanduser(pathToPipelineRunner))

if not os.path.lexists(pathToPipelineRunner):
    raise ValueError('dream3dFunctions: PipelineRunner cannot be found in {}.'.format(pathToPipelineRunner)) 
    pathToPipelineRunner=None

def set_D3_PIPELINE_DIR(dirPath):
    global pathToPipelineRunner
    print('D3_PIPELINE_DIR set to {}'.format(dirPath))
    pathToPipelineRunner = os.path.expandvars(os.path.expanduser(dirPath))


#Each pipeline module is keyed as 0 : {Module}
def checkKwargs(kwargs, varName, default, verbose=False, dictToModify = None):
    if varName in kwargs:
        if verbose:
            print('{}: {}'.format(varName, kwargs[varName]))
        if isinstance(dictToModify, dict):
            dictToModify[varName] = kwargs[varName]
        return kwargs[varName], False
    else:
        if verbose:
            print('{}: {} (default)'.format(varName, default))
        if isinstance(dictToModify, dict):
            dictToModify[varName] = default
        return default, True

def writePipelines(outFilePath, modulesList):
    #Write a pipeline file
    name = outFilePath.split('/')[-1].split('.')[0]
    jsonDict = {"PipelineBuilder": {"Name": name, "Number_Filters": len(modulesList), "Version":6}}
    for i in range(len(modulesList)):
        module = modulesList[i]
        #print(type(module), module)
        jsonDict[str(i)] = module
    jsonStr = json.dumps(jsonDict, sort_keys=True, indent = 4)
    jsonFile = open(outFilePath, 'w')
    jsonFile.write(jsonStr)
    jsonFile.close()

def genBetaConstants(aspectRatio = 'equiaxed', addAlphRand = True, addBetaRand = True, alphA = 1.1, alphB = 28.9, alphMult = 1, betaA = 30, betaB = 28.9, betaMult = -1, alphRandMult = 1, betaRandMult = 1):
    #Creates the Beta Constants based on this formula:
    #const = (A + Mult*(B* 1/aspectRatio) + randMult*rand())
    if aspectRatio == 'equiaxed':
        alphA = 15
        alphB = 0
        alphMult = 0
        alphRandMult = 1
        betaA = 1.25
        betaB = 0
        betaMult = 0
        betaRandMult = 0.5
        aspectRatio = 1
    elif aspectRatio == 'omega3':
        alphA = 10
        alphB = 0
        alphMult = 0
        alphRandMult = 1
        betaA = 1.5
        betaB = 0
        betaMult = 0
        betaRandMult = 0.5
        aspectRatio = 1

    elif isinstance(aspectRatio, numbers.Number):
        if aspectRatio == 0:
            return
    else:
        return


    if addAlphRand == True:
        alphRand = alphRandMult*random.random()
    elif isinstance(addAlphRand, numbers.Number):
        alphRand = addAlphRand
    else:
        alphRand = 0
    
    alpha = alphA + alphMult*(alphB*(1/aspectRatio)) + alphRand
    
    if addBetaRand == True:
        betaRand = betaRandMult*random.random()
    elif isinstance(addBetaRand, numbers.Number):
        betaRand = addBetaRand
    else:
        betaRand = 0
    
    beta = betaA + betaMult*(betaB*(1/aspectRatio)) + betaRand
    return (alpha, beta)

def genNeighborConst(maxBins, A = 14.0, B = 2.0, C = 0.3):
    #Generates mu and sigma from the number of bins
    #mu = ln(A + B * (binNum - middleBin)
    #sigma = 0.3 + ((middleBin - binNum) / (middleBin * 10)
    if A == 'equiaxed':
        A = 14.0
        B = 2.0
        C = 0.3
    elif A == 'rolled':
        A = 8.0
        B = 1
        C = 0.3
    middleBin = maxBins / 2
    mus = []
    sigmas = []
    for i in range(maxBins):
        try:
            #print(A, B, A+B*(i-middleBin))
            mus.append(math.log(A + B * (i - middleBin)))
        except:
            mus.append(np.nan)
        sigmas.append(float(C + ((middleBin - i) / (middleBin * 10))))
    return (mus, sigmas)

def logNorm (x, mu, sigma):
    #I hope this is correct
    val = (1 / (x*sigma*math.sqrt(2*math.pi)))*math.exp(-1*((math.log(x) - mu)**2)/(2*(sigma**2)))
    return val

def genBinStats(mu, sigma, maxCut = 5, minCut = 5, binStep = 0.5):
     
    #Get the Cutoffs
    minCutx = math.exp(mu - minCut*sigma)
    maxCutx = math.exp(mu + maxCut*sigma)
    #Create equally spaced bins. For n bins, n + 1 points.
    numOfBins = math.floor(((maxCutx - minCutx) / binStep) + 1);
    #print(mu, sigma, maxCut, minCut, maxCutx, minCutx)
    #The center point is going to be half the binStep
    binNums = [0 for x in range(numOfBins)]
    for i in range(numOfBins):
        binNums[i] = minCutx + (i *binStep) #logNorm(minCutx + (i * binStep), mu, sigma)
        #print(i, minCutx + (i* binStep), binNums[i])

    return binNums


def genAxisODFweight(euler1, euler2, euler3, sigma=0, weight=0):
    #All values are either values or lists
    if not isinstance(euler1, list):
        euler1 = [euler1]
    if not isinstance(euler2, list):
        euler2 = [euler2]
    if not isinstance(euler3, list):
        euler3 = [euler3]
    if not isinstance(sigma, list):
        sigma = [sigma]
    if not isinstance(weight, list):
        weight = [weight]

    odfDict = {'Euler 1':euler1,
            'Euler 2':euler2,
            'Euler 3':euler3,
            'Sigma':sigma,
            'weight':weight}
    print('odfDict', odfDict)
    return odfDict

def readODFFile(fn, compress=True, rounding=8, delim = ',', thresh=0):
    #Compress adds up the weights of duplicate ODFs. No idea if that actually works but *lets find out*
    odfFile = open(fn, 'r')

    data = []
    for line in odfFile: #Apparently that's legal
        if line.startswith('#'):
            continue
        elif line.startswith('Angle Count'):
            continue
        else:
            e1, e2, e3, w, sig = line.strip().split(delim)
            data.append([float(e1), float(e2), float(e3), float(w), float(sig)])
    data = np.round(np.array(data), rounding)
    if compress:
        uniqueODFs, counts = np.unique(data, axis=0, return_counts=True)
        uniqueODFs[:,3] = counts
        uniqueODFs = np.delete(uniqueODFs, np.nonzero(counts <= thresh), axis = 0)
        return uniqueODFs
    else:
        return data

    

def genStatsData(**kwargs):
    """Mimics the StatsGenerator of DREAM3D and default parameters.
Configurable Parameters. Parameters marked with (*) are used to calculate pre-fitted distributions. 
PhaseType
Name
PhaseFraction
BoundaryArea
CrystalSymmetry

binMu*
binSigma*
maxCut*
minCut*
binStep*
binNumber
FeatureSize_Distribution
Feature_Diameter_Info

A*
B*
C*
baRand*
caRand*

FeatureSize_Vs_B_Over_A_Distributions
FeatureSize_Vs_C_Over_A_Distributions
FeatureSize_Vs_Omega3_Distributions
FeatureSize_Vs_Neighbors_Distributions

euler1*
euler2*
euler3*
eulerSigma*
weight*
AxisODF-Weights
ODF-Weights
MDF-Weights
ODF-Filepath
ODF-Compress
ODF-Delim
ODF-Thresh

If PhaseType = "Precipitate"
Percipitate_Boundary_Function
Radial_Distribution_Function

"""
              
    #Generates a single primary phase StatsData. We'll deal with percipitates later.

    #AxisODF-Weights {Euler 1, Euler 2, Euler 3, Sigma, Weight}. How aligned are the grains?
    #Bin Count
    #BinNumber, aka the ESD of each bin.
    #BoundaryArea = 0
    #Crystal Symmetry = 1
    #FeatureSize Distribution {Average aka Mu, Stndard Deviation aka Sigma}
    #FeatureSize Vs B Over A Distribution { Alpha: {}, Beta: {}

    #Step One, Bin Sizes.

    #Feature Diameter Info is [stepSize, LargestBinNum, SmallestBinNum]
    
    #Phase Type
    PhaseType,default       =checkKwargs(kwargs, "PhaseType", "Primary") #Raw
    Name,default            =checkKwargs(kwargs, "Name", "Primary Phase") #Raw
    PhaseFraction,default   =checkKwargs(kwargs, "PhaseFraction", 1) #Raw
    BoundaryArea,default    =checkKwargs(kwargs, 'BoundaryArea', 0) #Raw
    CrystalSymmetry,default =checkKwargs(kwargs, 'CrystalSymmetry', 1) #Raw
               
    #Size Distribution
    binMu,default           =checkKwargs(kwargs, "mu", 1, verbose=False)
    binSigma,default        =checkKwargs(kwargs, "sigma", 0.1, verbose=False)
    maxCut,default          =checkKwargs(kwargs, "maxCut", 5, verbose=False) 
    minCut,default          =checkKwargs(kwargs, "minCut", 5, verbose=False)
    binStep,default         =checkKwargs(kwargs, "binStep", 0.5, verbose=False)
    forceBinCount,default         =checkKwargs(kwargs, "forceBinCount", None, verbose=False)
    if isinstance(forceBinCount, int): #scales binstep s.t. there are always a certain number of bins.
        minCutx = math.exp(binMu - minCut*binSigma)
        maxCutx = math.exp(binMu + maxCut*binSigma)
        binStep = (maxCutx - minCutx) / forceBinCount
    
    binNums,default         =checkKwargs(kwargs, "BinNumber", genBinStats(binMu, binSigma, maxCut, minCut, binStep), verbose=False) #list Raw
    binNums = [binNum for binNum in binNums if binNum > 0] #Prevents the log calcs from calling inf or nan 
    
    default=True #If your values 
    if default: 
        FeatureSizeDistrib={"Average":binMu, "Standard Deviation":binSigma}
    else:
        FeatureSizeDistrib={"Average":np.mean(np.log(binNums)), "Standard Deviation":np.std(np.log(binNums))}
        #Raw
    FeatureSizeDistrib,default          =checkKwargs(kwargs, "FeatureSize Distribution", FeatureSizeDistrib) 
    binCount = len(binNums)#int Raw
    FeatureDiameterInfo,default =checkKwargs(kwargs, "Feature_Diameter_Info", [binStep, max(binNums), min(binNums)]) #dict Raw

    #Aspect Ratios
    baRatio,default               =checkKwargs(kwargs, "baRatio", 1) 
    caRatio,default               =checkKwargs(kwargs, "caRatio", 1) 
    bacaRatio,default               =checkKwargs(kwargs, "bacaRatio", None) 
    if not isinstance(bacaRatio, type(None)):
        caRatio = baRatio/bacaRatio

    #PUT IN ROTATION CODE HERE
    bcRotate = False
    if caRatio > baRatio:
        temp = baRatio
        baRatio = caRatio
        caRatio = temp 
        bcRotate = True

    A,default               =checkKwargs(kwargs, "A", 1) 
    B,default               =checkKwargs(kwargs, "B", 1*baRatio)
    C,default               =checkKwargs(kwargs, "C", 1*caRatio)
    baRand,default          =checkKwargs(kwargs, "baRand", True) 
    caRand,default          =checkKwargs(kwargs, "caRand", True)

    if A == B == C:
        baAspectRatio = 'equiaxed'
        caAspectRatio = 'equiaxed'
        neighborType = 'equiaxed'
    else:
        baAspectRatio = A / B
        caAspectRatio = A / C
        neighborType = 'rolled'

    baAlpha = []
    baBeta = []
    caAlpha = []
    caBeta = []
    omega3Alpha = []
    omega3Beta = []
     #Same for both equiaxed and rolled
    for i in range(binCount):
        baTuple = genBetaConstants(baAspectRatio, addAlphRand = baRand, addBetaRand = baRand)
        baAlpha.append(baTuple[0])
        baBeta.append(baTuple[1])
        caTuple = genBetaConstants(caAspectRatio, addAlphRand = caRand, addBetaRand = caRand)
        caAlpha.append(caTuple[0])
        caBeta.append(caTuple[1])
        omega3Tuple = genBetaConstants('omega3', addAlphRand = baRand, addBetaRand = caRand)
        omega3Alpha.append(omega3Tuple[0])
        omega3Beta.append(omega3Tuple[1])
    FeatureSizeOmega3 = {"Alpha":omega3Alpha, "Beta":omega3Beta, "Distribution Type":"Beta Distribution"} #FeatureSize Vs B Over A Distributions
    FeatureSizeBoA = {"Alpha":baAlpha, "Beta":baBeta, "Distribution Type":"Beta Distribution"} #FeatureSize Vs B Over A Distributions
    FeatureSizeCoA = {"Alpha":caAlpha, "Beta":caBeta, "Distribution Type": "Beta Distribution"} #FeatureSize Vs B Over A Distributions

    FeatureSizeBoA,default          = checkKwargs(kwargs, "FeatureSize Vs B Over A Distributions", FeatureSizeBoA) #dict Raw
    FeatureSizeCoA,default          = checkKwargs(kwargs, "FeatureSize Vs C Over A Distributions", FeatureSizeCoA) #dict Raw
    
    #Omega3s          
    omegaRand,default               = checkKwargs(kwargs, "omegaRand", True) 
    FeatureSizeOmega3,default       = checkKwargs(kwargs, "FeatureSize Vs Omega3 Distributions", FeatureSizeOmega3) #dict Raw
    
    #Neighbors
    neighborTuple = genNeighborConst(binCount, neighborType) 
    FeatureSizeNeighbor,default= checkKwargs(kwargs, "FeatureSize Vs Neighbors Distributions", {
                                                    "Average":neighborTuple[0], 
                                                    "Standard Deviation":neighborTuple[1], 
                                                    "Distribution Type": "Log Normal Distribution"
                                                    }, verbose=False)
    #Clean up nans and infinities
    FeatureSizeNeighbor["Average"] = [val for val in FeatureSizeNeighbor["Average"] if not np.isinf(val) and not np.isnan(val)]
    FeatureSizeNeighbor["Standard Deviation"] = [val for val in FeatureSizeNeighbor["Standard Deviation"] if not np.isinf(val) and not np.isnan(val)]
    #ODF            
    euler1,default          =checkKwargs(kwargs, "euler1", [0]) 
    euler2,default          =checkKwargs(kwargs, "euler2", [0]) 
    euler3,default          =checkKwargs(kwargs, "euler3", [0]) 
    eulerSigma,default      =checkKwargs(kwargs, "eulerSigma", [0]) 
    weight,default          =checkKwargs(kwargs, "weight", [0])
    ODFFilepath,default =checkKwargs(kwargs, "ODF-Filepath", None) #dict Raw
    ODFCompress,default = checkKwargs(kwargs, 'ODF-Compress', False)
    ODFDelim,default = checkKwargs(kwargs, 'ODF-Delim', ',')
    ODFThresh,default = checkKwargs(kwargs, 'ODF-Thresh', 0)
    #Euler values are a single list. GUI input is in degrees, pipelines is radians. 180 = pi, 90 = pi/2
    
    if not isinstance(euler1, list):
        euler1= [euler1]
    if not isinstance(euler2, list):
        euler2= [euler2]
    if not isinstance(euler3, list):
        euler3= [euler3]
    if not isinstance(eulerSigma, list):
        eulerSigma= [eulerSigma]
    if not isinstance(weight, list):
        weight= [weight]
    if isinstance(ODFFilepath, str):
        loadODFs = readODFFile(ODFFilepath, compress=ODFCompress, delim=ODFDelim, thresh=ODFThresh)
        euler1.extend(loadODFs[:,0].tolist())
        euler2.extend(loadODFs[:,1].tolist())
        euler3.extend(loadODFs[:,2].tolist())
        weight.extend(loadODFs[:,3].tolist())
        eulerSigma.extend(loadODFs[:,4].tolist())

    if bcRotate:
        euler2[0] += math.pi/2
    #print('euler1', euler1)
    #print('euler2', euler2)
    #print('euler3', euler3)
    #print('eulerSigma', eulerSigma)
    #print('weight', weight)
    if all([euler1[0] == 0, euler2[0] == 0, euler3[0] == 0, eulerSigma[0] == 0, weight[0] == 0]):
        axisODFweights = {}
    else:
        axisODFweights = genAxisODFweight(euler1, euler2, euler3, eulerSigma, weight)
    #Add loaded ODF in.

    axisODFweights,default =checkKwargs(kwargs, "AxisODF-Weights", axisODFweights) #dict Raw
    for odfkey, odfval in axisODFweights.items():
        if not isinstance(odfval, list):
            axisODFweights[odfkey]=[odfval]
    ODFweights,default     =checkKwargs(kwargs, 'ODF-Weights', {}) #dict Raw
    for odfkey, odfval in ODFweights.items():
        if not isinstance(odfval, list):
            ODFweights[odfkey]=[odfval]
    MDFweights,default     =checkKwargs(kwargs, 'MDF-Weights', {}) #dict Raw
    for odfkey, odfval in MDFweights.items():
        if not isinstance(odfval, list):
            MDFweights[odfkey]=[odfval]

    

    StatsDataArray = {"AxisODF-Weights": axisODFweights, #dict
            "Bin Count": binCount, #int
            "BinNumber": binNums, #list
            "BoundaryArea": BoundaryArea, #int
            "Crystal Symmetry": CrystalSymmetry, #int
            "FeatureSize Distribution": FeatureSizeDistrib, #int
            "FeatureSize Vs B Over A Distributions": FeatureSizeBoA, #dict
            "FeatureSize Vs C Over A Distributions": FeatureSizeCoA, #dict
            "FeatureSize Vs Neighbors Distributions": FeatureSizeNeighbor, #dict
            "FeatureSize Vs Omega3 Distributions": FeatureSizeOmega3, #dict
            "Feature_Diameter_Info": FeatureDiameterInfo, #dict
            "MDF-Weights": MDFweights, #list
            "Name": Name, #str
            "ODF-Weights": ODFweights, #dict
            "PhaseFraction": PhaseFraction, #int
            "PhaseType": PhaseType} #str
       
    if PhaseType == 'Precipitate':
        RadialDistFunc = {"Bin Count":50, "BoxDims": [100,100,100], "BoxRes": [0.1,0.1,0.1], "Max": 80, "Min":10}
        RadialDistFunc,default          = checkKwargs(kwargs, "FeatureSize Vs B Over A Distributions", RadialDistFunc) #dict Raw
        StatsDataArray["Radial Distribution Function"] = RadialDistFunc
    

    return StatsDataArray

def genStatsGeneratorModule(phaseKwargs):
    """phaseKwargs can be either a dict or a list/tuple of dicts"""
    CellEnsembleAttributeMatrixName = "CellEnsembleData"
    CrystalStructureArrayName = "CrystalStructures"
    Filter_Human_Label = "StatsGenerator"
    PhaseNamesArrayName = "PhaseName" 
    PhaseTypesArrayName = "PhaseTypes" 
    StatsDataArrayName ="Statistics" 
    StatsGeneratorDataContainerName = "StatsGeneratorDataContainer"
    Filter_Enabled = True
    Filter_Name = "StatsGeneratorFilter"
    Filter_Uuid = "{f642e217-4722-5dd8-9df9-cee71e7b26ba}"
    #StatsDataArray={1:{...}, 2:{...}} of each phase
    #Also needs Phase Count
    if isinstance(phaseKwargs, dict):
        StatsDataArray = {
                "1":genStatsData(**phaseKwargs),
                "Name": "Statistics",
                "Phase Count" : 2
                }
    elif isinstance(phaseKwargs, (tuple, list)):
        StatsDataArray = {"Name": "Statistics", "Phase Count" : len(phaseKwargs)+1}
        for i in range(1, len(phaseKwargs)+1):
            phaseDict = phaseKwargs[i-1]
            StatsDataArray[str(i)] = genStatsData(**phaseDict)
    else:
        raise ValueError('genStatsGeneratorModule(): phaseKwargs is either a dict, or list/tuple of dicts') 
    StatsGenArray = {
        "CellEnsembleAttributeMatrixName": CellEnsembleAttributeMatrixName, 
        "CrystalStructureArrayName":CrystalStructureArrayName, 
        "Filter_Enabled":Filter_Enabled, 
        "Filter_Human_Label":Filter_Human_Label, 
        "Filter_Name":Filter_Name, 
        "Filter_Uuid":Filter_Uuid, 
        "PhaseNamesArrayName":PhaseNamesArrayName, 
        "PhaseTypesArrayName": PhaseTypesArrayName, 
        "StatsDataArrayName":StatsDataArrayName, 
        "StatsGeneratorDataContainerName":StatsGeneratorDataContainerName, 
        "StatsDataArray":StatsDataArray
    }

    return StatsGenArray
    #Just for nowS

    #A module designed to rederive and calculate the statsgenerator module (or at least some of it) in python
    #Step 1: Bin Sizes based on a Log Normal Distribution
    #mu is the log(ESD), where ESD is the equivalent sphere diameter (in voxels)
    #sigma is the standard deviation
    #cut off values are calculated by minCutoff = exp(mu - min*sigma) and maxCutoff = exp(mu + max *sigma)
    #Bin Size is calculated by: numBins = (maxCutoff - minCutoff / binStepSize) + 1
    #Aspect Ratio of B/A see SyntheticBuildingFilters/Presets/PrimaryRolledPresets.cpp
    #Under initializeBOverATableModel
    #Effectively, it is a beta distribution with
    #Equiaxed:
    #alpha = 15 + randDouble
    #beta = 1.25 + (0.5 * randDouble)
    #Rolled:
    #alpha = (1.1 + (28.9 * (1/AspectRatio))) + randDouble (random.cpp)
    #beta = (30 - (28.9 * (1/AspectRatio))) + randDouble (random.cpp)
    #With the aspect ratio = A/B or A/C, A >= B >= C, and randDouble from [0, 1)

    #Omega3 values for both use the equillized axis equation

    #Neighbors work on a similar log-normal distribution
    #Equiaxed:
    #In a loop by i, and middlebin being the num-of-bins / 2
    #mu = ln(14.0 + (2.0 * (i - middlebin))
    #sigma = 0.3 + ((middlebin - i) / (middlebin * 10))
    #Rolled:
    #mu = ln(8.0 + (1.0 * (i - middlebin))
    #sigma = 0.3 + ((middlebin - i) / (middlebin * 10))


def genSyntheticVolumeModule(axisLength = 32, resolution = 0.25, **kwargs):
    dimLength = math.ceil(axisLength/resolution)
    trueLength = dimLength*resolution
    
    boxDimensions = "X Range: 0 to {xdel} (Delta: {xdel})\nY Range: 0 to {ydel} (Delta: {ydel})\nZ Range: 0 to {zdel} (Delta: {zdel})".format(xdel = trueLength, ydel = trueLength, zdel = trueLength) 
    CellAttributeMatrixName = "CellData"
    DataContainerName = "SyntheticVolumeDataContainer"
    Dimensions = {'x': dimLength, 'y': dimLength, 'z': dimLength}
    EnsambleAttributeMatrixName = 'CellEnsembleData'
    EstimateNumberOfFeatures = 0
    EstimatedPrimaryFeatures = ''
    FilterVersion = '6.5.128'
    Filter_Enabled = True
    Filter_Human_Label = "Initialize Synthetic Volume"
    Filter_Name = "InitializeSyntheticVolume"
    Filter_Uuid = "{c2ae366b-251f-5dbd-9d70-d790376c0c0d}"
    InputPhaseTypesArrayPath = {
        "Attribute Matrix Name": "CellEnsembleData",
        "Data Array Name": "PhaseTypes",
        "Data Container Name": "StatsGeneratorDataContainer"
    }
    InputStatsArrayPath = {
        "Attribute Matrix Name": "CellEnsembleData",
        "Data Array Name": "Statistics",
        "Data Container Name": "StatsGeneratorDataContainer"
    }
    Origin = {'x': 0, 'y': 0, 'z': 0}
    Resolution = {'x': resolution, 'y': resolution, 'z' : resolution}

    synthVolData = {
            "boxDimensions" : boxDimensions,
            "CellAttributeMatrixName" : CellAttributeMatrixName,
            "DataContainerName" : DataContainerName,
            "Dimensions" : Dimensions,
            "EnsambleAttributeMatrixName" :EnsambleAttributeMatrixName,
            "EstimateNumberOfFeatures" :EstimateNumberOfFeatures,
            "EstimatedPrimaryFeatures" :EstimatedPrimaryFeatures,
            "FilterVersion" : FilterVersion,
            "Filter_Enabled" : Filter_Enabled,
            "Filter_Human_Label" : Filter_Human_Label,
            "Filter_Name" : Filter_Name,
            "Filter_Uuid" : Filter_Uuid,
            "InputPhaseTypesArrayPath" : InputPhaseTypesArrayPath,
            "InputStatsArrayPath" : InputStatsArrayPath,
            "Origin" : Origin,
            "Resolution" : Resolution
            }
    return synthVolData

def genEstablishShapeTypesModule():
    estShapeData={
    "FilterVersion": "6.5.128",
    "Filter_Enabled": True,
    "Filter_Human_Label": "Establish Shape Types",
    "Filter_Name": "EstablishShapeTypes",
    "Filter_Uuid": "{4edbbd35-a96b-5ff1-984a-153d733e2abb}",
    "InputPhaseTypesArrayPath": {
        "Attribute Matrix Name": "CellEnsembleData",
        "Data Array Name": "PhaseTypes",
        "Data Container Name": "StatsGeneratorDataContainer"
    },
    "ShapeTypeData": [
        999,
        0
    ],
    "ShapeTypesArrayName": "ShapeTypes"
    }
    return estShapeData

def genPrimaryPackingModule():
    

    packingModuleData = {
        "CellPhasesArrayName": "Phases",
        "FeatureGeneration": 0,
        "FeatureIdsArrayName": "FeatureIds",
        "FeatureInputFile": "/home/ethan",
        "FeaturePhasesArrayName": "Phases",
        "FilterVersion": "6.5.128",
        "Filter_Enabled": True,
        "Filter_Human_Label": "Pack Primary Phases",
        "Filter_Name": "PackPrimaryPhases",
        "Filter_Uuid": "{84305312-0d10-50ca-b89a-fda17a353cc9}",
        "InputPhaseNamesArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "PhaseName",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "InputPhaseTypesArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "PhaseTypes",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "InputShapeTypesArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "ShapeTypes",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "InputStatsArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "Statistics",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "MaskArrayPath": {
            "Attribute Matrix Name": "CellData",
            "Data Array Name": "Mask",
            "Data Container Name": "ImageDataContainer"
        },
        "NewAttributeMatrixPath": {
            "Attribute Matrix Name": "Synthetic Shape Parameters (Primary Phase)",
            "Data Array Name": "",
            "Data Container Name": "SyntheticVolumeDataContainer"
        },
        "NumFeaturesArrayName": "NumFeatures",
        "OutputCellAttributeMatrixPath": {
            "Attribute Matrix Name": "CellData",
            "Data Array Name": "",
            "Data Container Name": "SyntheticVolumeDataContainer"
        },
        "OutputCellEnsembleAttributeMatrixName": "CellEnsembleData",
        "OutputCellFeatureAttributeMatrixName": "CellFeatureData",
        "PeriodicBoundaries": 0,
        "SaveGeometricDescriptions": 0,
        "SelectedAttributeMatrixPath": {
            "Attribute Matrix Name": "",
            "Data Array Name": "",
            "Data Container Name": ""
        },
        "UseMask": 0
    }
    return packingModuleData

def genWriteModule(saveFilePath):
    cwd = os.getcwd()
    sd,part,fn = saveFilePath.rpartition('/')
    if sd == '': sd = '.'
    if part == '': part = '/'
    os.chdir(sd)
    fullfn = os.getcwd() + part + fn
    os.chdir(cwd)
    writeModuleData =  {
        "FilterVersion": "1.2.809",
        "Filter_Enabled": True,
        "Filter_Human_Label": "Write DREAM.3D Data File",
        "Filter_Name": "DataContainerWriter",
        "Filter_Uuid": "{3fcd4c43-9d75-5b86-aad4-4441bc914f37}",
        "OutputFile": fullfn,
        "WriteTimeSeries": 0,
        "WriteXdmfFile": 1
    }
    return writeModuleData

def genBoundaryModule():
    BoundaryCellsArrayName= "BoundaryCells"
    FeatureIdsArrayPath= {
            "Attribute Matrix Name": "CellData",
            "Data Array Name": "FeatureIds",
            "Data Container Name": "SyntheticVolumeDataContainer"
    }
    FilterVersion= "6.5.141"
    Filter_Enabled= True
    Filter_Human_Label= "Find Boundary Cells (Image)"
    Filter_Name= "FindBoundaryCells"
    Filter_Uuid= "{8a1106d4-c67f-5e09-a02a-b2e9b99d031e}"
    IgnoreFeatureZero= 1
    IncludeVolumeBoundary=0
    writeModuleData = {
        "BoundaryCellsArrayName":  BoundaryCellsArrayName,
        "FeatureIdsArrayPath":     FeatureIdsArrayPath,
        "FilterVersion":           FilterVersion,
        "Filter_Enabled":          Filter_Enabled,
        "Filter_Human_Label":      Filter_Human_Label,
        "Filter_Name":             Filter_Name,
        "Filter_Uuid":             Filter_Uuid,
        "IgnoreFeatureZero":       IgnoreFeatureZero,
        "IncludeVolumeBoundary":   IncludeVolumeBoundary,
    }
    return writeModuleData

def genInsertPrecipitateModule(pbc=False, matchRDF=False, **kwargs):
    moduleData = {
            "BoundaryCellsArrayPath": {
                "Attribute Matrix Name": "CellData",
                "Data Array Name": "BoundaryCells",
                "Data Container Name": "SyntheticVolumeDataContainer"
            },
            "CellPhasesArrayPath": {
                "Attribute Matrix Name": "CellData",
                "Data Array Name": "Phases",
                "Data Container Name": "SyntheticVolumeDataContainer"
            },
            "FeatureGeneration": 0,
            "FeatureIdsArrayPath": {
                "Attribute Matrix Name": "CellData",
                "Data Array Name": "FeatureIds",
                "Data Container Name": "SyntheticVolumeDataContainer"
            },
            "FeaturePhasesArrayPath": {
                "Attribute Matrix Name": "CellFeatureData",
                "Data Array Name": "Phases",
                "Data Container Name": "SyntheticVolumeDataContainer"
            },
            "FilterVersion": "6.5.141",
            "Filter_Enabled": True,
            "Filter_Human_Label": "Insert Precipitate Phases",
            "Filter_Name": "InsertPrecipitatePhases",
            "Filter_Uuid": "{1e552e0c-53bb-5ae1-bd1c-c7a6590f9328}",
            "InputPhaseTypesArrayPath": {
                "Attribute Matrix Name": "CellEnsembleData",
                "Data Array Name": "PhaseTypes",
                "Data Container Name": "StatsGeneratorDataContainer"
            },
            "InputShapeTypesArrayPath": {
                "Attribute Matrix Name": "CellEnsembleData",
                "Data Array Name": "ShapeTypes",
                "Data Container Name": "StatsGeneratorDataContainer"
            },
            "InputStatsArrayPath": {
                "Attribute Matrix Name": "CellEnsembleData",
                "Data Array Name": "Statistics",
                "Data Container Name": "StatsGeneratorDataContainer"
            },
            "MaskArrayPath": {
                "Attribute Matrix Name": "CellData",
                "Data Array Name": "Mask",
                "Data Container Name": "ImageDataContainer"
            },
            "MatchRDF": int(matchRDF),
            "NewAttributeMatrixPath": {
                "Attribute Matrix Name": "Synthetic Shape Parameters (Precipitate)",
                "Data Array Name": "",
                "Data Container Name": "SyntheticVolumeDataContainer"
            },
            "NumFeaturesArrayPath": {
                "Attribute Matrix Name": "CellEnsembleData",
                "Data Array Name": "NumFeatures",
                "Data Container Name": "SyntheticVolumeDataContainer"
            },
            "PeriodicBoundaries": int(pbc),
            "PrecipInputFile": "/home/ethan",
            "SaveGeometricDescriptions": 0,
            "SelectedAttributeMatrixPath": {
                "Attribute Matrix Name": "",
                "Data Array Name": "",
                "Data Container Name": ""
            },
            "UseMask": 0
        }
    return moduleData


def runDream3d(jsonFilePath, verbose=False, stderrVerbose=True):
    cwd = os.getcwd()
    sd,part,fn = jsonFilePath.rpartition('/')
    if sd == '': sd = '.'
    if part == '': part = '/'
    os.chdir(sd)
    fullfn = os.getcwd() + part + fn
    os.chdir(cwd)
    if verbose: stdout = None
    else: stdout = subprocess.DEVNULL
    if stderrVerbose: stderr = None
    else: stderr = subprocess.DEVNULL
    subprocess.run("{} -p {}".format(pathToPipelineRunner, fullfn), shell = True, stdout=stdout, stderr = stderr)

def runSynthPipeline(jsonFilePath='synthetic.json', dream3dFilePath='synthetic.dream3d', verbose=True, phaseKwargs=None, **kwargs):
    """Creates a synthetic microstructure based on kwargs parameters
    
    jsonFilePath=synthetic.json : str
        File path to create the Dream3d pipeline json file.

    dream3dFilePath=synthetic.dream3d : str
        File path to the Dream3d file.
    
    phaseKwargs=None : None or Dict or List/Tuple of Dicts
        Keyword arguements passed to genStatsData(). If phaseKwargs=None, then parameters passed through **kwargs will be used instead for a single phase material.

    verbose=True
        
    """
    if verbose:
        print('RUNNING SYNTH PIPELINE')
    for key, val in kwargs.items():
        if isinstance(val, list) and len(val) > 50:
            continue
        if verbose:
            print('{}: {}'.format(key, val))
    pipelineList = []
    multiphase=False
    if isinstance(phaseKwargs, type(None)):
        phaseKwargs=kwargs
    if isinstance(phaseKwargs, (list, tuple)) and len(phaseKwargs) > 1:
        multiphase=True
    pipelineList.append(genStatsGeneratorModule(phaseKwargs))
    pipelineList.append(genSyntheticVolumeModule(**kwargs))
    pipelineList.append(genEstablishShapeTypesModule())
    pipelineList.append(genPrimaryPackingModule())
    if multiphase:
        pipelineList.append(genBoundaryModule())
        pipelineList.append(genInsertPrecipitateModule(**kwargs))

    pipelineList.append(genWriteModule(dream3dFilePath))
    writePipelines(jsonFilePath, pipelineList)
   
    if verbose:
        print(jsonFilePath)
        print(dream3dFilePath)
        print('Running Dream3d?')
    runDream3d(jsonFilePath, verbose=verbose)
    

def getPipeline(d3filePath):
    #returns a dictionary of the exported dream3d pipeline. Use json.dumps to write it to json string again.
    f1d3 = h5py.File(d3filePath, 'r')
    d3pipestr = str(f1d3['Pipeline']['Pipeline'].value)
    lines = d3pipestr.strip().split("\\n")
    d3formatstr = ''
    d3jsonstr = ''
    for line in lines:
        d3formatstr = d3formatstr + line.replace('\\', '",')+'\n'
        d3jsonstr = d3jsonstr + line.strip().replace('\\', ',')
    d3jsonstr = d3jsonstr[1:].strip("'").strip()
    jsonPipeDict = json.loads(d3jsonstr)
    return(jsonPipeDict)

def writePipeFromDict(outFilePath, jsonDict):
    moduleList = []
    #print(jsonDict.keys())
    #for i in range(len(jsonDict)-1):
    for key in sorted(list(jsonDict.keys())):
        if any(num in list(string.digits) for num in list(key)):
            moduleList.append(jsonDict[key])
    writePipelines(outFilePath, moduleList)

def dream3dSearch(d3File, searchKey, splitChar='/'):
    #Search of a h5py file until it finds 'key'
    if isinstance(d3File, str):
        d3File = h5py.File(d3File)
    pathQueue = list(d3File.keys()) #"root/node1/node2"
    notFound = True
    while notFound:
        curPath = pathQueue.pop()
        curContainer = d3File
        for pathKey in curPath.split(splitChar): #Load currentContainer
            curContainer = curContainer[pathKey]
        if curPath.endswith(searchKey):
            return curPath, curContainer
        for key in [key for key in curContainer.keys() if isinstance(curContainer[key], h5py.Group)]: 
            pathQueue.append('{}{}{}'.format(curPath,splitChar,key))
        notFound = len(pathQueue) > 0 #Keep running until exhausted
    #If not found, raise Error
    raise KeyError('Key {} not found in dream3d file'.format(searchKey))

