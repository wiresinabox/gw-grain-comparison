#From Jeremy Mason's example. All rights to him.
#Wrappers and such.


import ctypes as ct
import numpy as np
import numpy.ctypeslib as ctl
import time
import sys
import os
import subprocess as sub
import warnings
np.seterr(divide='raise')

if 'GW_LIB_DIR' in os.environ: GW_PATH=os.environ['GW_LIB_DIR']
else: GW_PATH='/usr/local/lib'
GW_PATH = os.path.expandvars(os.path.expanduser(GW_PATH))
if not os.path.lexists(GW_PATH + '/libgwdist.so'):
    warnings.warn('gwFunctions: libgwdist.so cannot be found in {}. Defaulting to python backup.'.format(GW_PATH))
    GW_PATH=None

if 'SOLVER2D_LIB_DIR' in os.environ: SOLVER2D_LIB_DIR=os.environ['SOLVER2D_LIB_DIR']
else: SOLVER2D_LIB_DIR='/usr/local/lib'
SOLVER2D_LIB_DIR = os.path.expandvars(os.path.expanduser(SOLVER2D_LIB_DIR))
if not os.path.lexists(SOLVER2D_LIB_DIR + '/solver.so'):
    warnings.warn('gwFunctions: solver.so cannot be found in {}. Defaulting to python backup.'.format(SOLVER2D_LIB_DIR))
    SOLVER2D_LIB_DIR=None

def set_GW_LIB_DIR(dirPath):
    global GW_PATH
    GW_PATH =  os.path.expandvars(os.path.expanduser(dirPath))
    print('GW_LIB_DIR set to {}'.format(GW_PATH))

def set_SOLVER2D_LIB_DIR(dirPath):
    global SOLVER2D_LIB_DIR
    SOLVER2D_LIB_DIR =  os.path.expandvars(os.path.expanduser(dirPath))
    print('SOLVER2D_LIB_DIR set to {}'.format(SOLVER2D_LIB_DIR))


class gw_gromov:
    autoReg = False
    def __init__(self, libname='libgwdist.so', libdir=None, autoReg = False):
        if isinstance(GW_PATH, type(None)):
            libdir = GW_PATH
            if isinstance(GW_PATH, type(None)):
                raise ValueError('gw_gromov: Path to library does not exist {}/{}'.format(libdir, libname))
        self.c_gw_gromov = ctl.load_library(libname, libdir).gw_gromov
        self.c_gw_gromov.restype = ct.c_double
        self.autoReg = autoReg

    
    def __call__(self, m1in, m2in, D1in, D2in, initial = 'o', t1=False, t2=False, regularM=True):
        '''
        A wrapper function to call gw_gromov from the Wasserstein library by Jeremy Mason. These are entropic regulation approximations.
        m1in,m2in : Mass vectors. numpy vectors of size N and M respectively.
        D1in,D2in : Cost matrix. numpy arrays of NxN and MxM
        t1,t2 = False. numpy arrays of type np.float32 and order='F'. Controls typing. If False assumes all are the same type.
        regularM = True. Controls whether m1in ad m2in are normalized s.t. they sum to 1.
        '''
        
        
        if printTime:
            startTime = time.time()
        if type(m1in) == type([]):
            m1in = np.array(m1in)
        if type(m2in) == type([]):
            m2in = np.array(m2in)


        n1t = m1in.shape
        n2t = m2in.shape
        n1 = n1t[0]
        n2 = n2t[0]

        if len(n1t) < 2:
            m1in = np.expand_dims(m1in, axis = 1)
        if len(n2t) < 2:
            m2in = np.expand_dims(m2in, axis = 1)

        #check types
        if t1 == False:
            t1 = np.zeros(n1, dtype = np.uint32, order = 'F')
        if t2 == False:
            t2 = np.zeros(n2, dtype = np.uint32, order = 'F')

        #Check if m1 and m2 are regularized
        m1Sum = np.sum(m1in)
        m2Sum = np.sum(m2in)
        if m1Sum != 1 and regularM:
            m1 = np.array(m1in/m1Sum, dtype = np.float64, order = 'F')
        else:
            m1 = np.array(m1in, dtype = np.float64, order = 'F')
        if m2Sum != 1 and regularM:
            m2 = np.array(m2in/m1Sum, dtype = np.float64, order = 'F')
        else:
            m2 = np.array(m2in, dtype = np.float64, order = 'F')
        
        #Convert D1 and D2
        D1 = np.array(D1in, dtype = np.float64, order = 'F')
        D2 = np.array(D2in, dtype = np.float64, order = 'F')
        #G is then gamma, the optimal coupling

        G = np.empty((n1, n2), order = 'F')
        
        dist = self.c_gw_gromov(
            G.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m1.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m2.ctypes.data_as(ct.POINTER(ct.c_double)), \
            t1.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            t2.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            D1.ctypes.data_as(ct.POINTER(ct.c_double)), \
            D2.ctypes.data_as(ct.POINTER(ct.c_double)), \
            ct.c_uint32(n1),        \
            ct.c_uint32(n2),        \
            ord(initial))
        return (dist, G)
        


class gw_wass:
    def __init__(self, libname='libgwdist.so', libdir=GW_PATH):
        if isinstance(GW_PATH, type(None)):
            libdir = GW_PATH
            if isinstance(GW_PATH, type(None)):
                raise ValueError('gw_wass: Path to library does not exist {}/{}'.format(libdir, libname))
        self.c_gw_wasser = ctl.load_library(libname, libdir).gw_wasser
        self.c_gw_wasser.restype = ct.c_double

    def __call__(self, Cin, m1in, m2in, Gin = None, t1 = False, t2 = False, regularM=True):
        '''
        A wrapper function to call gw_wass from the Wasserstein library by Jeremy Mason. These are entropic regulation approximations.
        Cin : Cost matrix. numpy arrays of NxM
        m1in,m2in : Mass vectors. numpy vectors of size N and M
        Gin =False : NxM matrix which the values are placed into. By default will create a new array
        t1,t2 = False. numpy arrays of type np.float32 and order='F'. Controls typing. If False assumes all are the same type.
        regularM = True. Controls whether m1in ad m2in are normalized s.t. they sum to 1.
        '''

        #THIS NEEDS TO BE NORMALIZED. S.T THE MASS STAYS EQUAL TO 1
        if isinstance(m1in, int):
            m1 = np.ones(m1in, dtype = np.float64, order = 'F')
        else:
            m1 = m1in
        if isinstance(m2in, int):
            m2 = np.ones(m2in, dtype = np.float64, order = 'F')
        else:
            m2 = m2in
        if regularM:
            m1 = np.array(m1/np.sum(m1), dtype = np.float64, order = 'F')
            m2 = np.array(m2/np.sum(m2), dtype = np.float64, order = 'F')
        else:
            m1 = np.array(m1, dtype = np.float64, order = 'F')
            m2 = np.array(m2, dtype = np.float64, order = 'F')
            
        n1 = m1.size
        n2 = m2.size
        

        if t1 == False:
            t1 = np.zeros(n1, dtype = np.float64, order = 'F')
        if t2 == False:
            t2 = np.zeros(n2, dtype = np.float64, order = 'F')
        C = np.array(Cin.flatten(), dtype = np.float64, order = 'F')
        if isinstance(Gin, type(None)):
            G = np.zeros(m1.size * m2.size, order = 'F')
        else:
            G = Gin
        

        dist = self.c_gw_wasser(
            G.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m1.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m2.ctypes.data_as(ct.POINTER(ct.c_double)), \
            t1.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            t2.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            C.ctypes.data_as(ct.POINTER(ct.c_double)), \
            ct.c_uint32(n1),        \
            ct.c_uint32(n2))

        G = G.reshape((m1.size, m2.size))
        return dist, G

def rtsafe(x, WACC=0.01, MAXIT=100, SMALL_NUM=2e-16):
    """Finds the maximum values of x s.t. phi(w|x) = x(1-x^{-w})-wlog(x) is not less than zero
    Described in A. Thibault. L.Chizat, C. Dossal, and N.Papadakis, Overrelaxed
    Sinkhorn--Knopp Algorithem for Regularixed Optimal Transport
    Via standard newton's method.
    This one is a modified version based on pg. 366 of W.H. Press S. A. Teukolsky, W. T. Vetterling, B. P. Flannery Numerical Recipies in C. Second Ed.
    Originally implimented by Jeremy Mason in wasserstein3.m
    """

    #Definitions

    if x >=1.:
        w=2.
        return w

    logx = np.log(x)
    #Initial Search Interval
    wl = 2.
    wh = 1.

    #Initial Guess
    w = 1. + 1./np.sqrt(1.-np.log(x))
    dtold=1.
    dt=dtold

    xow=np.power(x, 1.-w)
    f=x-xow-w*logx
    df=(xow-1.)*logx


    for a in range(0, MAXIT):
        if ((w-wh)*df-f)*((w-wl)*df-f)>0. or np.abs(2.*f) > np.abs(dtold*df):
            dtold=dt
            dt=(wh-wl)/2.
            w=wl+dt
        else:
            dtold=dt
            dt=f/df
            w=w-dt
        if np.abs(dt) < WACC:
            return w
        xow=np.power(x, 1.-w)
        f=x-xow-w*logx
        df=(xow-1.)*logx
        if f < 0.:
            wl=w
        else:
            wh=w
    raise RuntimeError("rtsafe: failed to converge")


def newton(x, WACC = 0.01, MAXIT = 100, SMALL_NUM=2e-16):
    #Originally implemented by Jeremy Mason in newton.c
    if np.any(not np.isfinite(x)):
        raise RuntimeError('newton: argument not finite')
    if x >= 1. - SMALL_NUM:
        return 2.
    log_x = np.log(x)
    #initial search interval
    wl = 1.
    wh = 2.
    #initial guess
    w = 1. + 1./ np.sqrt(1. - log_x)
    xow = np.power(x, 1.-w)
    f = x - xow - w*log_x
    df = (xow-1.)*log_x
    dw_old = 1.
    dw = -f / df
    
    for a in range(0, MAXIT):
        if (wh-w) < dw or dw < (wl - w) or np.abs(dw) > np.abs(dw_old/2.):
            dw = (wh - wl)/2. - w
        w= w + dw
        if (np.abs(dw) < WACC):
            return w
        xow = np.power(x, 1.-w)
        f = x - xow - w * log_x
        df = (xow-1.) * log_x

        dw_old = dw
        dw = -f/df

        if dw > 0.:
            wl = w
        else:
            wh = w
    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=8)
    print('x:\n', x)
    raise RuntimeError('newton: failed to converge')

def _solverHardW(s1, s2):
    '''where s1 and s2 are 2d ndarrays'''
    m1 = s1.reshape(int(s1.shape[0]*s1.shape[1]))
    m2 = s2.reshape(int(s2.shape[0]*s2.shape[1]))
    C = np.zeros((m1.size, m2.size))
    for i in range(m1.size):
        for j in range(m2.size):
            #L1
            x1 = int(np.floor(i/s1.shape[1]))
            y1 = i%s1.shape[1]
            x2 = int(np.floor(j/s2.shape[1]))
            y2 = j%s2.shape[1]
            C[i,j] = abs(x1 - x2) + abs(y1-y2)
    return C, m1, m2

def hardW(C, m1, m2, t1=False, t2=False, normGamma=False, normMass=True, etaFMult = 1e-2, **kwargs):
    """
    A python transliteration of the balanced Wasserstein Metric (by Entropic Regularization) by Jeremy Mason originally in Matlab.
    C : nxm cost matrix. Numpy Array
    m1, m2 : n and m long numpy vectors. Numpy Array
    t1, t2 = False : n and m long vectors that define the type of elements in m1 and m2.
    normGamma = False : If True then G is normalized by n
    etaFMult = 1e-2 : defines etaF = etaFMult*min(min(m1)/len(m1), min(m2)/len(m2))
    """
    if isinstance(m1, int):
        m1 = np.ones(m1, dtype = np.float64, order = 'F')
    if isinstance(m2, int):
        m2 = np.ones(m2, dtype = np.float64, order = 'F')

    if normMass:
        m1 = m1/np.sum(m1)
        m2 = m2/np.sum(m2)

    #NORMALIZE m1, and m2


    tau = 1e3
    delc = 0.02
    MAX_IT = 1e4
    theta = 1.75
    etaF = etaFMult*min(np.min(m1) / m1.size, np.min(m2) / m2.size)
    etaS = max(np.median(C), 4*etaF)
    tolF = 1e-4* etaF

    #print('etaF:', etaF, 'etaS:', etaS, 'tolF:', tolF) 
    n1 = m1.size
    n2 = m2.size

    G = np.zeros((n1, n2))
    
    if isinstance(t1, type(False)) and t1 == False:
        t1 = np.zeros(n1)
    if isinstance(t2, type(False)) and t2 == False:
        t2 = np.zeros(n2)
    
    mask = np.zeros(C.shape)
    for a in range(0, n1):
        for b in range(0, n2):
            if t1[a] != t2[b]:
                mask[a,b] = -3.403e+38
    
    R = np.exp(mask) *np.matmul(m1, m2.transpose())
    u = np.ones(n1)
    v = np.ones(n2)
    Rv = np.sum(R, 1)
    while max(abs(u*Rv-m1)) > tolF:
        u=m1/Rv
        v=m2/np.matmul(R.transpose(), u)
        Rv = np.matmul(R, v)
    R = np.matmul(np.matmul(np.diag(u), R), np.diag(v))
    
    etaRange = etaS/etaF
    k=int(np.ceil(np.log(etaRange)/np.log(4.)))
    l=etaRange**(-1./k)
    
    E = np.append(etaS* np.power(l, np.arange(0, k)), etaF)
    T = np.append(np.fmax(E[0:k] / 100., tolF), tolF)
    T=T*min(n1, n2)
    
    alph = np.zeros(n1)
    beta=np.zeros(n2)
    K = -C
    for a in range(0, k+1):
        iterc=0
        not_converged=True
        while not_converged:
            #print('NEXT')
            u=np.ones(n1)
            v=np.ones(n2)
            #print('C:', C[380, :])
            K=np.round(np.exp(K/E[a] + mask), 8) * R
            #print('K:', K[380, :], np.sum(K[380,:]), K[380,240], C[380, 240])
            Kv=np.sum(K,1)
            while np.max(np.abs(u)) < tau and np.max(np.abs(v)) < tau and not_converged:
                try:
                    ur = u / (m1 / Kv) #Newton will fail to converge if divide by zero is encountered here.
                except FloatingPointError as e:
                    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=5)
                    print('C:\n', C)
                    print('Kv:\n', Kv)
                    print('K:\n', K)
                    print('m1:\n', m1)
                    print('u:\n', u)
                    #print('m1/Kv:', m1/Kv)
                    infInd = np.nonzero(Kv == 0)
                    print('Zero at:', infInd)
                    print('Kv at zero:', Kv[infInd])
                    print('m1 at zero:', m1[infInd])
                    print('K at zero:',  K[infInd, :])
                    print('C at zero:', C [infInd, :])
                    #print(Kv[380])
                    print(e)
                    quit()
                w = np.fmin(np.fmax(1., newton(np.min(ur)) - delc), theta)
                u = u / (ur**w)

                try:
                    vr = v / (m2 / np.matmul(K.transpose(), u))
                except FloatingPointError as e:
                    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=8)
                    #print('C:\n', C)
                    print('K:\n', K)
                    print('m2:\n', m2)
                    print('v:\n', v)
                    print('u:\n', u)
                    print(e)
                    quit()
                w = np.fmin(np.fmax(1., newton(np.min(vr)) - delc), theta)
                v = v / (vr**w)

                Kv = np.matmul(K, v)
                iterc += 1
                not_converged = np.max(np.abs(u * Kv - m1)) > T[a] and iterc < MAX_IT
                
            alph = alph + E[a]*np.log(u)
            beta = beta + E[a]*np.log(v)
            
            for b in range(0,n2):
                K[:, b] = -C[:, b] + alph + beta[b]


    G = np.round(np.exp((K/etaF) + mask), 8) * R
    
    #Normalize Gamma
    if normGamma:
        G = G/n1

    
    score = np.sum(G*C)
        
    #print(G)
    #print('Sum G x:', np.sum(G,0))
    #print('Sum G y:', np.sum(G,1))
    #print('Score:', score)
    return score, G
                



def softW(c,m1,m2,l1in,l2in,lmult = 1,K0=False, t1=False,t2=False, usePenaltyFinal = False, endlmult=1, swapMass = False, safeC=True):

    """
    A python transliteration of the unbalanced Wasserstein Metric (by Entropic Regularization) by Jeremy Mason originally in Matlab. NOTE: This function is unstable currently, use at your own risk.
    c : nxm cost matrix. Numpy Array
    m1, m2 : n and m long numpy vectors. Numpy Array
    l1in,l2in: n and m long penalties. Numpy Array
    lmult: a multplier for penalties used during calculation. As lmult -> infinity then it should converge to the balanced wasserstein problem as much as it possibly can.
    t1, t2 = False : n and m long vectors that define the type of elements in m1 and m2.
    usePenaltyFinal=False : If False will set penalties to unity AFTER gamma has been calculated.
    endlmult=1 : multiplies l by a set amount AFTER gamma has been calculated.
    swapMass = False : Swaps m1 and m2. For some reason this matteers.
    safeC = True: Scales up c before scaling it down after gamma has been calculated. Avoids some underflow issues.
    normGamma = False : If True then G is normalized by n
    etaFMult = 1e-2 : defines etaF = etaFMult*min(min(m1)/len(m1), min(m2)/len(m2))
    """
    

    #pass
    #usePenaltyFinal is for the final calculation. If it is false, the final one will be set to 1 else a number.
    #constants
    #lmult multiplies at the beginning, lmult multiplies at the end.
    #safeC scales the cost matrix down before scaling back up the score at the end. Ensure that underflow doesn't happen.
    n1 = m1.size
    n2 = m2.size
    eps = np.finfo(np.float64).eps
    realmax = np.finfo(np.float64).max
    stime= time.time()

    #Making things default
    if isinstance(K0, type(False)) and K0 == False:
        #K0 = (np.ones((n1,n2)) / (n1*n2)) * np.mean([sum(m1), sum(m2)]) 
        K0 = np.outer(m1, m2) / np.mean([sum(m1), sum(m2)]) #np.ones((n1,n2)) / (n1 * n2)
    if isinstance(t1, type(False)) and t1 == False:
        t1 = np.ones(n1)
    if isinstance(t2, type(False)) and t2 == False:
        t2 = np.ones(n2)

    #Setting lmult to a window dependant constant
        
        

    l1 = l1in * lmult 
    l2 = l2in * lmult 

    #It seems like if m2 has a HIGHER MAXIMUM VALUE, then it reaches a lower minimum because
    #it seems like rs1-rs0 converges first always.
    if swapMass:
        if np.max(m2) > np.max(m1):
            c = c.transpose()
            tempm = m1
            templ = l1
            m1 = m2
            l1 = l2
            m2 = tempm
            l2 =templ

    if safeC: #keeps the coupling score dominant
        print('SAFEING C')
        maxC = np.max(c)
        c = c/maxC


    #Adjustable
    EPS_F = 1e-4 #CHANGE THIS VALUE IF THINGS START BREAKING
    TOL_F = 1e-8
    #TOL_D = 100. * eps
    THRES = 1000.
    M_MAX_IT = 1e4
    #K_MAX_IT = max(n1, n2);
    
    # Segregates different types of materials
    mask = np.zeros([n1,n2])
    for a in range(0, n1):
        for b in range(0, n2):
            if t1[a] != t2[b]:
                mask[a,b] = -realmax

    #K0 = K0 * np.exp(mask)
    arg = -c #Make C approximately one

    rs1 = np.zeros([n1])
    cs1 = np.zeros([n2])

    u = np.zeros([n1])
    v = np.zeros([n2])

    eps_s = np.fmax(np.max(c), 4. * EPS_F)
    eps_range = eps_s / EPS_F
    #print(np.ceil(np.log(eps_range)/np.log(4.)))



    k = int(np.ceil(np.log(eps_range) / np.log(4.)))
    l = eps_range**(-1. / k)
    E = np.append(eps_s * np.power(l, np.arange(0, k)), EPS_F)
    T = np.append(np.fmax(E[0:k] / 100., TOL_F), TOL_F)
   
    iterList = [0,0,0,0,0]

    for a in range(E.size):
        m_iter = 0
        m_not_converged = True
        while m_not_converged:
            rm = np.ones([n1,1]) #a
            cm = np.ones([n2,1]) #b
            K1 = np.exp(arg / E[a])
            Kc = np.sum(K1, 1) #column sums
            Kr = np.sum(K1, 0) #row sums
            while m_not_converged and (np.abs(rm) < THRES).all() and (np.abs(cm) < THRES).all():
                #HERE, corresponds with line 298 to 351 c code.
                #54 to 74, matlab
                
                rm = m1 / Kc #row multiplier
                rmr = np.exp(-(l1 + u) / E[a]) 
                rm = np.fmax(rm, rmr)
                rmr = np.exp((l1 - u) / E[a])
                #print(np.max(rmr), np.min(rmr), 'fmin:', np.min(rm))
                #if np.max(rmr) == np.inf:
                #    quit()
                #lambda Total Variation, proxdivF(s, u, e)
                rm = np.fmin(rm, rmr)
                #Kr = K1.transpose().dot(rm)
                Kr = np.matmul(K1.transpose(), rm)

                #rs0 = rs1
                #cs0 = cs1
                #print('rm:',rm)
                #print('Kc:',Kc)
                #rs0 = rm * Kc
                #cs0 = cm * Kr

                cm = m2 / Kr
                cmr = np.exp(-(l2 + v) / E[a])
                cm = np.fmax(cm, cmr)
                cmr = np.exp((l2 - v) / E[a])
                cm = np.fmin(cm, cmr)
                Kc = np.matmul(K1,cm)
                #Kc = K1.dot(cm)

                #NOTES: Imbalance on the final coupling matrix comes from the algorithem scaling
                #the matrix overall by a constant to which ever mass it hits last, aka m2.

                rs0 = rs1
                cs0 = cs1
                #print('rm:',rm)
                #print('Kc:',Kc)
                rs1 = rm * Kc
                cs1 = cm * Kr
                #print(np.abs(rs1 - rs0), np.abs(cs1-cs0), T[a]) 
                threshConv1 =(np.abs(rs1 - rs0) > T[a] * n2).any()
                threshConv2=(np.abs(cs1-cs0) > T[a] * n1).any()
                iterConv=(m_iter < M_MAX_IT)
                threshCheck1 = (np.abs(rm) < THRES).all()
                threshCheck2 = (np.abs(cm) < THRES).all()
                #m_not_converged = ((rs1 - rs0) > T[a] * n2).any() or (np.abs(cs1-cs0) > T[a] * n1).any() and (m_iter < M_MAX_IT)
                m_not_converged=(threshConv1 or threshConv2) and iterConv
                #TO HERE
                m_iter += 1
                #print('E:', E[a])
                #print('iter:', m_iter)
                #print('rm:',rm) 
                #print('cm:',cm) 
                #print('u:',u) 
                #print('v:',v) 
            #print(threshConv1, threshConv2, iterConv, m_not_converged, '|', threshCheck1, threshCheck2)
            iterList[0] += int(not threshConv1)#Checks for convergence to an answer
            iterList[1] += int(not threshConv2)#Checks for convergence to an answer.
            iterList[2] += int(not iterConv) #Sets maximum iteration number
            iterList[3] += int(not threshCheck1) #Checks if row multipliers are too large
            iterList[4] += int(not threshCheck2) #Checks if col multipliers are too large

            u = u + E[a] * np.log(rm)
            v = v + E[a] * np.log(cm)
            
            for b in range(0, n2):
                arg[:, b] = -c[:, b] + u.transpose() + v[b]# + mask[:, b]
        #print(K1)

    K1 = np.exp(arg / E[a])
    K = K1
    #print(m_iter)
    Csum =  np.sum(K*c)
    if safeC:
        Csum = Csum*maxC
    if not isinstance(usePenaltyFinal, bool) and np.isreal(usePenaltyFinal):
        l2 = usePenaltyFinal
        l1 = usePenaltyFinal
    elif isinstance(usePenaltyFinal, bool) and usePenaltyFinal == False:
        l2 = 1 
        l1 = 1 
    elif isinstance(usePenaltyFinal, str) and usePenaltyFinal == 'frac': #fraction of the original
        l1 = l1in*endlmult
        l2 = l2in*endlmult
    else:
        l1 = l1*endlmult
        l2 = l2*endlmult

    Psum2 = np.sum(l2*np.abs(np.sum(K,0).transpose() - m2)) 
    Psum1 = np.sum(l1 * np.abs(np.sum(K,1) - m1))
    etime = time.time()

    #print(K)
    #print(iterList)
    print('lmult: {}, swapMass: {}, usePenaltyFinal: {}, endlmult: {}, Time: {:.3f} s'.format(lmult, swapMass, usePenaltyFinal, endlmult, (etime-stime))) 
    print('Coupling Term : ', Csum)
    print('Penalty Term 1: ', Psum1)
    print('Penalty Term 2: ', Psum2)
    #print('l1:', l1)
    #print('l2:', l2)
    print('(M_MAX_IT:{}, THRES:{})\nEnd Reasons: convergence=[uv1:{}, uv2:{}, m_iter:{}], mult_scale=[uv1:{}, uv2:{}]'.format(M_MAX_IT, THRES, iterList[0], iterList[1], iterList[2], iterList[3], iterList[4]))

    D = Csum + Psum2 + Psum1
    return D, K, Csum, Psum1, Psum2

def _hash(s):
    h=0
    for ch in s:
        h = (h*503 ^ 521*ord(ch)) & 0xFFFFFFF
    return h 

def solverEqualize(m1in, roundMult=5e4, seed='unique', **kwargs):
        if seed == 'unique':
            rng1 = np.random.RandomState(_hash(repr(m1in)))
            
        else:
            rng1 = np.random.RandomState(seed)
            
        if len(m1in.shape) == 1:
            m1=m1in.reshape((int(np.ceil(np.sqrt(m1in.size))),int(np.ceil(np.sqrt(m1in.size)))))
        else:
            m1 = m1in.copy()
        if np.sum(m1) == 0:
            m1sum = 1
        else:
            m1sum = np.sum(m1in)
       
        #Matches the mass of each histogram by adding a 'floor' to each
        m1 = m1 + np.ceil((roundMult - m1sum)/m1.size)
        m1over = int(np.sum(m1) - roundMult)
        
        #Scatters mass balancing noise to get exact mass ratio. This means there is a slight stochastic noise!
        for i in np.arange(0, abs(m1over)):
            if m1over >0:
                m1[rng1.randint(0, m1.shape[0]), rng1.randint(0, m1.shape[1])]-=1
            else: 
                m1[rng1.randint(0, m1.shape[0]), rng1.randint(0, m1.shape[1])]+=1
        
        return m1

def solver2dPrewrite(m1s, m2s, tempDir='./dottemp', idStr='', roundMult=5e4, seed='unique', verbose=False, **kwargs):
    '''Writes SDF histograms to a csv file for easy comparison later. Note! Remember to clean out your tempDir from time to time. Interrupted runs may not clean up these files.

        m1s, m2s : lists of 2D numpy arrays. Numpy array elements must be integers.
        tempDir='./dottemp' : Directory to store temporary CSV files
        idStr='' : Files are named win<1 or 2>-<date>-<idStr>-<window number>.csv. Use idStr to seperate two simultaneous runs
        roundMult = 5e4 : Sum value which each window is equalized to. Equalization happens in two steps:
                1. A 'floor' is built s.t. m1 = m1 + (roundMult - m1)*n1 where n1 is the size of m1
                2. Random noise is added to equalize the remaining mass. The amount of noise required is minimized by step 1.
        seed = 'unique' : Seed for where noise is randomly placed. By default this is hash(repr(m))%(2**32-1). This ensures that comparing the same SDF histogram results in the same unique placement of noise for repeatability. Setting seed=None will use the default numpy seeding.


        returns fp1List, fp2List : lists of strings with the filepaths to each CSV
    '''

    #takes in a list of windows, does the mass blanacing, and then writes them all to a location
    if verbose:
        print('Prewriting 2dhist windows to', tempDir)
    try:
        os.mkdir(tempDir)
    except FileExistsError:
        pass
    fpList1=[]
    fpList2=[]
    t = '{}-{}-{}'.format(time.localtime()[4], time.localtime()[5], time.localtime()[6])
    for j in range(len(m1s)):
        m1in=m1s[j]
        m2in=m2s[j]
           
        m1 = solverEqualize(m1in, roundMult, seed=seed)
        m2 = solverEqualize(m2in, roundMult, seed=seed)


        fp1 = '{}/win1-{}-{}-{}.csv'.format(tempDir,t, idStr, j)
        fp2 = '{}/win2-{}-{}-{}.csv'.format(tempDir,t, idStr, j)
        fpList1.append(fp1)
        fpList2.append(fp2)
        np.savetxt(fp1, np.ceil(m1), delimiter=',', fmt='%1i')
        np.savetxt(fp2, np.ceil(m2), delimiter=',', fmt='%1i')
    
    return fpList1, fpList2 
        
def solver2dCleanup(fpList1, fpList2):
    #Removes windows after a run
    for i in range(len(fpList1)):
        try:
            os.remove(fpList1[i])
        except FileNotFoundError:
            print('File not Found for Deletion:', fpList1[i])
            pass
        try:
            os.remove(fpList2[i])
        except FileNotFoundError:
            print('File not Found for Deletion:', fpList2[i])
            pass

def solver2d(m1, m2, tempDir='dottemp', idStr='', libdir = None, libname='solver.so', roundMult=5e4, timeout=120, **kwargs):
    """Calls the Discrete Optimal Transport (DOT) library for 2D histograms
    Bassetti F., Gualandi S., Veneroni M. On the Computation of Kantorovich-Wasserstein Distances between 2D-Histograms by Uncapacitated Minimum Cost Flows. Available on arXiv. Submitted on April, 2nd, 2018.

    m1 and m2 can either be filepaths to a CSV file or a numpy array.
    Assumes that the distances between each cell are equal and laid out in a grid (L1 distance)
    roundMult is the total mass that m1 and m2 are equalized to.

    returns the normalized distance dist/(n1*n2)

    m1 and m2 are either 2d histograms or not. They will get balanced and multiplied by roundMult
    Since they need to be the same mass and integers.
    Incredible hacky method.
    """
    
    if isinstance(libdir, type(None)):
        libdir = SOLVER2D_LIB_DIR
        if isinstance(libdir, type(None)):
            raise ValueError('solver2d: Path to library does not exist {}/{}'.format(libdir, libname))
    libPath = libdir + '/' + libname
    if not os.path.lexists(libPath):
        raise ValueError('solver2d: Path to library does not exist {}/{}'.format(libdir, libname))

    try:
        os.mkdir(tempDir)
    except FileExistsError:
        pass
    t = '{}-{}-{}'.format(time.localtime()[4], time.localtime()[5], time.localtime()[6])
    if isinstance(m1, str):
        fp1 = m1
        csv1 = open(fp1, 'r')
        n1 = len(csv1.readline().split(','))
        csv1.close()
    else:
        if len(m1.shape) == 1:
            m1=m1.reshape((int(np.ceil(np.sqrt(m1.size))),int(np.ceil(np.sqrt(m1.size)))))
        m1 = np.ceil(m1*(roundMult/np.sum(m1)))
        n1 = m1.shape[0]
        
        m1 = solverEqualize(m1, roundMult, seed=seed)
        
        i=0
        fileExists=True
        while fileExists:
            fp1 = '{}/win1-{}-{}-{}.csv'.format(tempDir,t, idStr, i)
            if i > 1:
                print('Searching for:', fp1)
            fileExists = os.path.lexists(fp1)
        np.savetxt(fp1, np.ceil(m1), delimiter=',', fmt='%1i')

    if isinstance(m2, str):
        fp2 = m2
        csv2 = open(fp2, 'r')
        n2 = len(csv2.readline().split(','))
        csv2.close()
    else:
        if len(m2.shape) == 1:
            m2=m2.reshape((int(np.ceil(np.sqrt(m2.size))),int(np.ceil(np.sqrt(m2.size))))) #1d to 2d, assumes square
            n2 = m2.shape[0]
            
            m2 = solverEqualize(m2, roundMult, seed=seed)
            
            i=0
            fileExists=True
            while fileExists:
                fp1 = '{}/win2-{}-{}-{}.csv'.format(tempDir,t, idStr, i)
                if i > 1:
                    print('Searching for:', fp1)
                fileExists = os.path.lexists(fp1)
            np.savetxt(fp1, np.ceil(m2), delimiter=',', fmt='%1i')


    try:
        out = sub.run([libPath, '-sa', '--h1={}'.format(fp1), '--h2={}'.format(fp2)],stdout=sub.PIPE, timeout=timeout)
        dist = int(out.stdout.split()[19].decode().strip(','))
    except sub.TimeoutExpired:
        print('Reached Timeout of {}s'.format(timeout))
        dist = 0
    

    if not isinstance(m1, str):
        os.remove(fp1)
    if not isinstance(m2,str):
        os.remove(fp2) #cleanup

    return dist/(n1*n2)




