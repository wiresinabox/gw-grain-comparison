import numpy as np
from pixelMethod import *
import gc
#gc.set_debug(gc.DEBUG_LEAK)
#gc.set_debug(gc.DEBUG_STATS)
#TODO: move simplex, pdfo, and evaluate function to here for organizations sake.






def evaluateParameters(inputDict, staticDict = {}, **kwargs):

    #If mu and sigma are dictionaries instead they are inputDict and staticDict, set those values last. They are priority.
    kwargs.update(staticDict) 
    kwargs.update(inputDict) 
    mu =checkKwargs(kwargs,'mu'    , 1)
    sigma=checkKwargs(kwargs,'sigma'    , 0.1) 
    baRatio=checkKwargs(kwargs,'baRatio'    , 1) 
    caRatio=checkKwargs(kwargs,'caRatio'    , 1)
    bacaRatio=checkKwargs(kwargs,'bacaRatio' , None)

    saveFilePath        =checkKwargs(kwargs,'saveFilePath'    , False, verbose=False)
    compareFilePath     =checkKwargs(kwargs,'compareFilePath' , False, verbose=False)
    saveFolder          =checkKwargs(kwargs,'saveFolder'      , './simplex-out', verbose=False )
    jsonFolder          =checkKwargs(kwargs,'jsonFolder'      , './parameter-json', verbose=False )
    dream3dFolder       =checkKwargs(kwargs,'dream3dFolder'   , './dream3d-data', verbose=False )
    microPicFolder      =checkKwargs(kwargs,'microPicFolder'  , './micro-png', verbose=False )
    saveFileName        =checkKwargs(kwargs,'saveFileName'    , '', verbose=False )
    distType            =checkKwargs(kwargs,'distType'        , 'sdf', verbose=False )
    startWindow         =checkKwargs(kwargs,'startWindow'     , 5, verbose=False )
    stepWindow          =checkKwargs(kwargs,'stepWindow'      , 5, verbose=False )
    maxWindow           =checkKwargs(kwargs,'maxWindow'       , 10, verbose=False )
    deleteAfter         =checkKwargs(kwargs,'deleteAfter'     , True, verbose=False)
    microPlot           =checkKwargs(kwargs,'microPlot'       , False, verbose=False )
    iteration           =checkKwargs(kwargs,'iteration'       , 'unknown', verbose=False )
    stepType            =checkKwargs(kwargs,'stepType'        , 'unknown', verbose=False )
    nameList            =checkKwargs(kwargs,'nameList'        , ['xx', 'yy', 'zz'], verbose=False)
    avgNum              =checkKwargs(kwargs,'avgNum'          , 1, verbose=False )
    regen               =checkKwargs(kwargs,'regen'           , True, verbose=False )
    refEval             =checkKwargs(kwargs,'refEval'         , False, verbose=False )
    baseScore           =checkKwargs(kwargs,'baseScore'       , None, verbose=False)
    optionsDict         =checkKwargs(kwargs,'optionsDict'     , None, verbose=False )
    hardW               =checkKwargs(kwargs,'hardW'           , '2dhist', verbose=False )#Depreciated
    lmult               =checkKwargs(kwargs,'lmult'           , 10, verbose=False )#Depreciated
    Cscale              =checkKwargs(kwargs,'Cscale'          , 1, verbose=False)#Depreciated
    endlmult            =checkKwargs(kwargs,'endlmult'        , 1, verbose=False)#Depreciated
    usePenaltyFinal     =checkKwargs(kwargs,'optionsDict'     , False,verbose=False) #Depreciated
    multiproc           =checkKwargs(kwargs,'multiproc'       , False,verbose=False)
    cores               =checkKwargs(kwargs,'cores'           , 1,verbose=False)
    forceWinSym         =checkKwargs(kwargs,'forceWinSym'           , False,verbose=False)
    scoreMult         =checkKwargs(kwargs,'scoreMult'           , 1,verbose=False)
    verbose               =checkKwargs(kwargs,'verbose'           , True,verbose=False)
    roundMult         =checkKwargs(kwargs,'roundMult'           , 1e6,verbose=False)
    addNorm         =checkKwargs(kwargs,'addNorm'           , False,verbose=False)

   
    if optionsDict == None:
        print("Overwriting with default OptionsDict")
        optionsDict = {
                'windowNum':100,
                'axis':'y',
                'slice': 'all', #np.random.randint(0, 128, 5),
                'relativeScale':False,
                'totalAreaHold' : 1,
                'noGrainInWindow':4,
                'forceWindowLen':40, #Overrides noGrainInWindow
                'winSizeDetermine':'max',
                'distMultiplier':1,
                'centerOnRandGrain': False,
                'distType' : 'sdf',
                'sdfDistMin': 0,
                'sdfPenaltyType':'constesd',
                'ignoreBoundaryEdges':True,
                'verbose':False,
                'forceCellLength':True,
                'sdfNormalize':False, #Forces each window to have the same amount of mass
                'sdfReverse': True,
                'sdfRoot':False,
                'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
                'sdfFloorNorm':False,
                'sdfThresh':0
        }
        

    #Hard Limits on BA/CA ratio:
    if not isinstance(bacaRatio, type(None)):
        caRatio = baRatio/bacaRatio

    if jsonFolder.startswith('./'):
        jsonFolder = jsonFolder.replace('./', os.getcwd() + '/')
    if dream3dFolder.startswith('./'):
        dream3dFolder = dream3dFolder.replace('./', os.getcwd() + '/')
    #if saveFolder.startswith('./'):
    #    saveFolder = saveFolder.replace('./', os.getcwd() + '/')
    
    if not os.path.isdir(jsonFolder):
        os.mkdir(jsonFolder)
    if not os.path.isdir(dream3dFolder):
        os.mkdir(dream3dFolder)
    #if not os.path.isdir(saveFolder):
    #    os.mkdir(saveFolder)
    
    picCompareFilePath = microPicFolder+'/'+'ref-micro.png'
    saveFilePath = saveFolder +'/scores.txt'
    if not refEval:
        if saveFileName == '':
            print(mu, sigma, baRatio, caRatio, iteration, stepType)
            saveFileName = 'synthetic-{}-{}-{}-{}-{}-{}'.format('{:.2f}'.format(mu).replace('.','_'), '{:.2f}'.format(sigma).replace('.','_'), '{:.4f}'.format(baRatio).replace('.','_'), '{:.4f}'.format(caRatio).replace('.','_'), iteration, stepType)
        
        jsonFilePath = jsonFolder+'/'+saveFileName+'.json'
        dream3dFilePath = dream3dFolder+'/'+saveFileName+'.dream3d'
        xdmfFilePath = dream3dFolder+'/'+saveFileName+'.xdmf'
        picFilePath = microPicFolder+'/'+saveFileName+'.png'

        #REGEN STUFF GOES HERE TO CHANGE RESOLUTION
        d3f.runSynthPipeline(jsonFilePath, dream3dFilePath,**kwargs) 

        if regen:
            pass
            jsonFilePathRegen = jsonFolder+'/'+saveFileName+'-regen.json'
            dream3dFilePathRegen = dream3dFolder+'/'+saveFileName+'-regen.dream3d'
            resRegen(compareFilePath, dream3dFilePath, jsonFilePathRegen, dream3dFilePathRegen, axis = nameList[0][1], sliceNum = optionsDict['slice'], searchWindowLen = 128, windowSize = 128, distMultiplier = optionsDict['distMultiplier'], sdfDistMin=optionsDict['distMultiplier'])
            if deleteAfter:
                os.remove(dream3dFilePath)
            dream3dFilePath = dream3dFilePathRegen
            jsonFilePath = jsonFilePathRegen
        #Create xx, yy, zz, xy, xz, yz slices

        #if sliceNum == 'half':
        #    #fix this later if compareFilePath points somewhere else
        #    sliceNum = int((axisLength/resolution)/2)

        if compareFilePath == False:
            #By default, it will compare itself
            compareFilePath = dream3dFilePath 
    else:
        if saveFileName == '':
            print('SELF-EVAL')
            saveFileName = 'ref-self-compare'
        
        jsonFilePath = jsonFolder+'/'+saveFileName+'.json'
        dream3dFilePath = compareFilePath 
        xdmfFilePath = dream3dFolder+'/'+saveFileName+'.xdmf'
        picFilePath = microPicFolder+'/'+saveFileName+'.png'

    print(dream3dFilePath)


    if microPlot:
        #try:
            from plotUtil import plotMicroStruct
            plt.ioff()
            axis = [a[1] for a in nameList]
            micros = [dream3dFilePath for i in range(len(axis))]
            fig, axs= plotMicroStruct(micros, titles=axis, subplotStyle='row', keepValues='random', axis=axis, sliceNum= optionsDict['slice'])
            plt.savefig(picFilePath, dpi=300, bbox_inches='tight')
            plt.clf()
            plt.close('all')
            if not os.path.exists(picCompareFilePath):
                axis = [a[0] for a in nameList] 
                micros = [compareFilePath for i in range(len(axis))]
                fig, axs = plotMicroStruct(micros, titles=axis, subplotStyle='row', keepValues='random', axis=axis, sliceNum= optionsDict['slice'])
                plt.savefig(picCompareFilePath, dpi=300, bbox_inches='tight')
                plt.clf()
                plt.close('all')
                
        #except:
        #    print("Couldn't Plot! Continuing")
        #    pass
    scoresSDF = []
    resSDF = []
    for i in range(len(nameList)):
        print(nameList[i])
        resList = []
        for j in range(avgNum):
            finalScoreSDF = 0
            
            optionsDict.update({'axis1':nameList[i][0], 'axis2':nameList[i][1]})
            dataDict = generateWindowData(dream3dFilePath, compareFilePath, optionsDict1=optionsDict, optionsDict2 = optionsDict, axis1=nameList[i][0], axis2=nameList[i][1])
           

            Cs = dataDict['CostMatSDF']
            uv1s = dataDict['uv1SDF']
            uv2s = dataDict['uv2SDF']
            l1 = dataDict['penaEdge1SDF']
            l2 = dataDict['penaEdge2SDF']
            
            resultTup = runWindowsSDFWass(uv1s, uv2s, Cs, l1, l2, lmult = lmult, Cscale = Cscale, multiproc = multiproc, availProcs = cores, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult, hardW='2dhist', forceWinSym=forceWinSym, verbose=verbose, scoreMult=scoreMult, roundMult = roundMult, addNorm = addNorm)
            finalScoreSDF = resultTup[0]
            resList.append(finalScoreSDF)

        scoresSDF.append(np.mean(resList))
        resSDF.append(resList)
             

    finalScore = sum(scoresSDF)
    if not isinstance(baseScore, type(None)):
        finalScore = np.abs(finalScore - baseScore) + baseScore #This sets the lowest score/objective to be the baseScore
    print("Final Score: {}".format(finalScore))

    scoreFile = open(saveFilePath, 'a')
    outStr = 'Iteration: {}'.format(iteration)
    for item in inputDict.items():
        outStr = outStr + ', {}: {}'.format(item[0], item[1])
    outStr = outStr +'\n'
    outStr = outStr +'Scores: '
    for i in range(len(nameList)):
        dimname = nameList[i]
        dimscore = scoresSDF[i]
        dimres=resSDF[i]
        outStr = outStr + '{}: {} ({}), '.format(dimname, dimscore, dimres)
    outStr = outStr + 'Final: {}, '.format(finalScore)
    outStr = outStr + '\n\n'
    scoreFile.write(outStr)
    scoreFile.close()

    
    saveFileName = '{}-{}'.format(saveFileName,'{:.2f}'.format(finalScore).replace('.', '_'))
    newPicFilePath = microPicFolder+'/'+saveFileName+'.png'
    try:
        os.rename(picFilePath, newPicFilePath)
    except:
        pass
    if deleteAfter and not refEval:
        #cleanup to save space. If it crashes beforehand it will leave the thing alone
        try:
            os.remove(dream3dFilePath)
        except FileNotFoundError:
            pass
        try:
            os.remove(xdmfFilePath)
        except FileNotFoundError:
            pass


    return finalScore


class evalParamClass:
    #This class is set up before hand s.t. static inputs are always kept between runs since pdfo only takes in variables and non statics.
    staticInputs = None
    inputVarNameList = None #Used to construct the input dict
    inputVarList=None #Starting values corresponding with inputVarNameList
    evalFunc = None
    bounds = None
    def __init__(self, startInputs, startParameters=dict(), staticInputs=dict(), evalFunc=evaluateParameters):
        self.inputVarNameList = []
        self.inputVarList = []
        
        self.staticInputs = staticInputs #dictionary
        for var, val in startInputs.items():
            self.inputVarNameList.append(var)
            self.inputVarList.append(val)
        self.bounds = []
        ub = []
        lb = []
        for var in self.inputVarNameList:
            if var in startParameters.keys():
                valTup = startParameters[var]
                lb.append(valTup[1])
                ub.append(valTup[2]) #delta, min, maox
            else:
                lb.append(-np.inf)
                ub.append(np.inf)
        self.setBounds(lb, ub)

        self.evalFunc = evalFunc


        pass
    def getInitialValues(self):
        return self.inputVarList
    def getBounds(self):
        return self.bounds

    def setBounds(self, minBounds, maxBounds):
        self.bounds = (pdfo.Bounds(minBounds,maxBounds))
    
    def selfEvaluate(self, setBaseScore=False):
        #Set setBaseScore to True if you want the pdfo evaluation to use the absolute value with respect to the base score.
        tempStaticDict = self.staticInputs.copy()
        tempStaticDict['refEval']=True
        baseScore = self.evalFunc(dict(), tempStaticDict) 
        if setBaseScore:
            print('Setting baseScore={}'.format(baseScore))
            self.staticInputs['baseScore'] = baseScore
        return baseScore
    def evaluate(self, *inputArray):
        inputDict = dict(zip(self.inputVarNameList, inputArray[0]))
        return self.evalFunc(inputDict, staticDict = self.staticInputs)

def pdfoGeometric(startInputs, startParameters=None, evalFunc = evaluateParameters, bounds=[], constraints=[], **kwargs):
    if not isinstance(startInputs, np.ndarray):
        startInputs = np.array(startInputs)
    res = pdfo.pdfo(evalFunc, startInputs, bounds=bounds, constraints = constraints, **kwargs)
    return res.x, res

def simplexGeometric(startInputs, staticInputs = {}, startParameters = None, startDelta = 1, minLimit=None, maxLimit=None, evalFunc = evaluateParameters, stopScoreThresh = 0.1, stopInputThresh = 0.01,  stopIterThresh = 1000, verbose = True, reflS = 1, expaS = 2, contS = 0.25, shrkS = 1):
    #startInputs is a dictionary of 'var':value
    #staticInputs is the same, but do not have anything.
    #startDelta is how far each of the other points want to vary.
    
    #SCALING FACTORS
    #reflS = 1 #Alpha, scaling factor, a > 0
    #expaS = 2 #Gamma, Expansion, gamma > 1
    #contS = 0.25 #Rho, Contraction, 0 < rho < 0.5
    #shrkS = 1 #Shrink

    #For an 9 dimensional problem (Our 3 Axis (x, y, z) and their combination (xy, yz, ...etc)
    #We use 10 points (N+1)
    #QUESTION: are xy and yx axis symmetrical? I think they should be.
    #First: Select a starting point (sample) Po = <b/a, c/a, mu, std>
    #Then make 4 more points (samples) s.t. each one of the axis is varied by a little bit.
    #Each of these points is then evaluated on the xx, yy, zz, xy, xz, yz plane to plane comparison
    #And then the scores are summed and given pack as the point's score
    #Then, when you move a point, you can do a recalculation for THE HIGHEST point given the criteria:
    #REFLECT: If the reflected point score across the plane of the loweset is between the two lowest point scores
    # ---> Keep
    #EXPAND: If the reflected point score is better than both lowest point scores
    #CONTRACT: If the reflect point score is still not better than either.
    #SHRINK: If the reflected point score is worsee than the score of all other points, then intersting values lie inside.
    #

    #Stopping criteria
    #stopScoreThresh: Either a value or a vector or a list. compares all points and if the difference between the largest
                         #and the smallest points are good. then yeah!
    #stopIterThesh: How many iterations before stopping


    #Select Starting values
    numVars = len(startInputs) #There will always be n+1 points for n variables.
    varNames = list(startInputs.keys())
    pointInputs = np.zeros([numVars+1, numVars])  #List of lists, each list contains the inputs of the function.
    startDeltas = []
    minLimits = []
    maxLimits = []

    for i in range(len(varNames)):
        varName = varNames[i]
        try:
            varTup = startParameters[varName]
            sd = varTup[0]
            minv=varTup[1]
            maxv=varTup[2]
        except:
            sd = startDelta
            minv = minLimit
            maxv = maxLimit
        if isinstance(sd, type(None)):
            sd = startDelta
        if isinstance(minv, type(None)):
            minv = minLimits
        if isinstance(maxv, type(None)):
            maxv = maxLimits
        print("Inputs for {}: {}, {}, {}".format(varName, sd, minv, maxv))
        startDeltas.append(sd)
        minLimits.append(minv)
        maxLimits.append(maxv)
        


    
    for i in range(numVars+1):
        j = 0
        for varName in varNames:
            #Set starting pointInputs
            if i == j+1 and i != 0:
                pointInputs[i, j] = startInputs[varName] + startDeltas[j]
            else:
                pointInputs[i, j] = startInputs[varName]
            
            if not isinstance(maxLimits, type(None)) and pointInputs[i,j] > maxLimits[j]:
                pointInputs[i,j] = maxLimits[j]
            if not isinstance(minLimits, type(None)) and pointInputs[i,j] < minLimits[j]:
                pointInputs[i,j] = minLimits[j]

            j+=1

    currentScores = np.zeros(numVars+1) 


    numIter = 0
    while True:
        staticInputs.update({'iteration':numIter})
        if verbose:
            print('Iteration: {}'.format(numIter)) 
            print('Iteration Inputs:', pointInputs)
        for i in range(numVars+1):
            currentScores[i] = evalFunc(dict(zip(varNames, list(pointInputs[i, :]))), staticInputs)
            #NOTE TO SELF. LET POINTINPUT TAKE IN A DICTIONARY OR STRING.
            if verbose:
                simplexPointPrint(i, varNames, pointInputs[i,:], currentScores[i])
                
        
        #Get best:
        bestScore = currentScores.min()
        bestInd = np.nonzero(bestScore == currentScores)[0][0]
        bestInputs = pointInputs[bestInd, :]

        #Get worst:
        worstScore = currentScores.max()
        worstInd = np.nonzero(worstScore == currentScores)[0][0]
        worstInputs = pointInputs[worstInd, :]
        
        if verbose:
            print('Best: {} at {}. Worst: {} at {}.'.format(bestScore, bestInd, worstScore, worstInd))

        #Stopping Check
        numIter +=1
        deltaInputs = np.abs(bestInputs - worstInputs)
        if (np.all(deltaInputs < stopInputThresh) and np.abs(bestScore-worstScore) < stopScoreThresh) or numIter >= stopIterThresh:
            break


        sortedScores = np.sort(currentScores)

        #Get centroid except for the worst ones:
        tempPoints = np.zeros([numVars, numVars])
        print(pointInputs)
        tempPoints[0:worstInd, :] = pointInputs[0:worstInd, :]
        if worstInd < numVars:
            tempPoints[worstInd:, :] = pointInputs[worstInd+1, :]
        centroid = np.sum(tempPoints, 1)/numVars
        print('Centroid Inputs:', centroid)
        #REFLECT
        reflInputs = centroid + reflS*(centroid - pointInputs[worstInd, :])
        if not isinstance(maxLimits, type(None))and np.any(reflInputs > maxLimits):
            highInd = np.nonzero(reflInputs > maxLimits)[0][0]
            reflInputs[highInd] = maxLimits[highInd]
        if not isinstance(minLimits, type(None))and np.any(reflInputs < minLimits):
            lowInd = np.nonzero(reflInputs < minLimits)[0][0]
            reflInputs[lowInd] = minLimits[lowInd]

        print('Reflection Inputs:', reflInputs)
        reflScore = evalFunc(dict(zip(varNames, list(reflInputs))), staticInputs)
        if verbose:
            simplexPointPrint('Reflect', varNames, reflInputs, reflScore)
        
        #If the reflected point is between the best and the second worst.
        if reflScore > sortedScores[0] and reflScore < sortedScores[numVars]:
            pointInputs[worstInd, :] = reflInputs
            print('Reflecting, Between')
        elif reflScore < sortedScores[0]:
            #EXPAND
            expaInputs = centroid + expaS*(reflInputs - centroid)
            if not isinstance(maxLimits, type(None)) and np.any(expaInputs > maxLimits):
                highInd = np.nonzero(expaInputs > maxLimits)
                expaInputs[highInd] = maxLimits[highInd]
            if not isinstance(minLimits, type(None)) and np.any(expaInputs < minLimits):
                lowInd = np.nonzero(expaInputs < minLimits)
                expaInputs[lowInd] = minLimits[lowInd]
            print('Expanding Inputs:', expaInputs)
            expaScore = evalFunc(dict(zip(varNames, list(expaInputs))), staticInputs)
            if verbose:
                simplexPointPrint('Expand', varNames, expaInputs, expaScore)
            if expaScore < reflScore:
                print('Expanding')
                pointInputs[worstInd, :] = expaInputs
            else:
                print('Reflecting, Worst')
                pointInputs[worstInd, :] = reflInputs
        else:
            #CONTRACT
            contInputs = centroid + contS*(pointInputs[worstInd, :]-centroid)
            if not isinstance(maxLimits, type(None))and np.any(contInputs > maxLimits):
                highInd = np.nonzero(contInputs > maxLimits)
                contInputs[highInd] = maxLimits[highInd]
            if not isinstance(minLimits, type(None)) and np.any(contInputs < minLimits):
                lowInd = np.nonzero(contInputs < minLimits)
                contInputs[lowInd] = minLimits[lowInd]
            print('Contraction Inputs:', contInputs)
            contScore = evalFunc(dict(zip(varNames, list(contInputs))), staticInputs)
            if verbose:
                simplexPointPrint('Contract', varNames, contInputs, contScore)
            if contScore < worstScore:
                print('Contracting')
                pointInputs[worstInd, :] = contInputs
            else:
                #SHRINK
                print('Shrinking')
                for i in range(numVars):
                    if i != bestInd:
                        pointInputs[i, :] = pointInputs[bestInd, :] + shrkS*(pointInputs[i, :] - pointInputs[bestInd, :])
                        if not isinstance(maxLimits, type(None))and np.any(pointInputs[i,:] > maxLimits):
                            highInd = np.nonzero(pointInputs[i, :] > maxLimits)
                            pointInputs[i, highInd] = maxLimits[highInd]
                        if not isinstance(minLimits, type(None)) and np.any(pointInputs[i,:] < minLimits):
                            lowInd = np.nonzero(pointInputs[i,:] < minLimits)
                            pointInputs[i, lowInd] = minLimits[lowInd]
        
    return (pointInputs, currentScores)


#Simulated Annealing, based on Metropolis et. al.
#General idea:  Sample new states with control parameter T varying how wild and random the guesses can be.
#               Randomly select new state depending on a boltzmann probability function:
#               P(E, Enew, T) = exp(-(Enew - E) / kT) > rand(0, 1)
#               Generation of new parameters is where the magic happens.

def boltzmannProb(Ecur, Etest, T):
    k=1
    if T == 0:
        prob = 1 - (Etest - Ecur) #as long as Etest is less than Ecurr, will move to steepest descent
    else:
        prob = np.exp(-(Etest-Ecur)/(k*T))
    val = np.random.random()
    return prob >= val 

def __selfReturn(*args, **kwargs):
    return list(args).extend(list(kwargs.values()))
            

def simAnneal(inputDictInit, staticDict={}, bounds={}, **kwargs):
    """
    Implementation of Simulated Annealing specifically for the microstructure evalutation
    inputDictInit -> Initial State
    evaluateFunc -> Pointer to objective function
    paramFunc -> pointer to function that gives new paramters
    probFunc -> generates probability of passing.
    TstepNum -> Maximum number of Temperature Steps
    inputDictNum -> Maximum number of State Iterations per Temperature Step
 
    """
    evaluateFunc        =checkKwargs(kwargs,'evaluateFunc', evaluateParameters, verbose=False)  
    paramFunc=checkKwargs(kwargs,'paramFunc', windowAnnealParamGen, verbose=False)
    probFunc=checkKwargs(kwargs, 'probFunc', boltzmannProb, verbose=False)
    tempUpdateFunc=checkKwargs(kwargs, 'tempUpdateFunc', __selfReturn, verbose=False)
    Tmax=checkKwargs(kwargs, 'Tmax', 100, verbose=False)
    Tsteps=checkKwargs(kwargs, 'Tsteps', 100, verbose=False) #Also takes in a list
    stateSteps=checkKwargs(kwargs, 'stateSteps', 100, verbose=False) #Also takes a list same size as Tsteps
    passThresh=checkKwargs(kwargs, 'passThresh', 10, verbose=False)
    passBreak=checkKwargs(kwargs, 'passThresh', True, verbose=False)
    TBestReset=checkKwargs(kwargs, 'TBestReset', True, verbose=False) #If true, then replaces the current with that T loop's best

    if isinstance(Tsteps, int): 
        Tsteps = np.linspace(Tmax, 0, Tsteps) #Replace later with a more sophisticated schedule'
    Tlim = [np.min(Tsteps), np.max(Tsteps)]
    EBest = np.inf
    inputDictCur = inputDictInit
    inputDictBest = inputDictInit
    for t in range(len(Tsteps)):
        T = Tsteps[t]
        passNum = 0 
        
        tempUpdateFunc(inputDict=inputDictCur, staticDict=staticDict, T=T, Tlim=Tlim) #Do stuff inline, try and prevent a memory leak
        
        if TBestReset:
            inputDictCur = inputDictBest
            print('Resetting Cur to Best, Cur: {}'.format(inputDictCur))


        newStateCur=True
        if isinstance(stateSteps, list):
            numStateSteps = stateSteps[t]
        else:
            numStateSteps = stateSteps
        for s in range(numStateSteps):
            tempStr = 'T{:.2f}'.format(T).replace('.', '_')
            winStr = 'w{}'.format(staticDict['optionsDict']['windowNum'])
            if newStateCur:
                Ecur = evaluateFunc(inputDictCur, staticDict=staticDict, iteration=tempStr, stepType=winStr)
                newStateCur = False #do not calculate Ecur if already calculated. 
            inputDictTest = paramFunc(inputDictCur, staticDict=staticDict, bounds=bounds, T=T, Tlim=Tlim)
            Etest = evaluateFunc(inputDictTest, staticDict=staticDict, iteration=tempStr, stepType=winStr)
            inputDictPass = probFunc(Ecur, Etest, T=T)
            print('T:', T, 's:', s)
            print(tempStr, winStr, staticDict['optionsDict']['windowNum']) 
            print('Ecur:', Ecur, 'inputDictCur:', inputDictCur)
            print('Etest:', Etest, 'inputDictTest:', inputDictTest)
            print('Ebest:', EBest, 'inputDictCur:', inputDictBest)
            print('Pass?', inputDictPass)
            if inputDictPass:
                inputDictCur = inputDictTest
                ECur = Etest
                passNum +=1
                newStateCur=True
            if Ecur < EBest:
                inputDictBest = inputDictCur
                EBest = Ecur
            if isinstance(passThresh,list):
                thresh = passThresh[t]
            else:
                thresh = passThresh
            if passNum >= thresh: #If enough sucesses, continue to next temperature step
                break
            
            #garbage collection?
            print('Performing Collection')
            gc.collect()
            print('Uncollectable:', len(gc.garbage), gc.garbage)

        if passNum == 0 and passBreak: #If found early, break out of loop.
            break

    return inputDictCur.values(), Ecur, inputDictCur, EBest, inputDictBest 
 

def randomGen(inputDict, staticDict={}, **kwargs):
    #Generates a random delta x to add to x.

    returnDict = inputDict.copy()
    scale = checkKwargs(kwargs, 'scale', 1)
    T = checkKwargs(kwargs, 'T', 1)
    scale = np.log(T+1)*scale
    for key in inputDict.keys():
        returnDict[key] = returnDict[key] + (np.random.random()*2*scale - scale) #allows of negative numbers
    return returnDict 

def windowAnnealParamGen(inputDict, staticDict={}, bounds={}, **kwargs):
    #Scale search area based on temperature.
    returnDict = {} 
    T = checkKwargs(kwargs, 'T', 1, verbose=False)
    Tmin, Tmax = checkKwargs(kwargs, 'Tlim', [0,1], verbose=False)
    
    for var, val in inputDict.items():
        if var in bounds.keys():
            varDelta, varMin, varMax = bounds[var]
        else:
            varDelta = 1
            varMin = 0
            varMax = 1
        varRange = varMax - varMin
        TFrac = (T-Tmin)/(Tmax-Tmin)
        testMax = min(val + varRange*0.5*TFrac, varMax) 
        testMin = max(val - varRange*0.5*TFrac, varMin)
        
        returnDict[var] = np.random.random() * (testMax - testMin) + testMin #random location within test bracket
        #print('min: {} max: {}, val: {}, TFrac: {}'.format(testMin, testMax, returnDict[var], TFrac))
    return returnDict

def windowAnnealTempUpdate(inputDict={}, staticDict={} , **kwargs):
    #Update windowNum to scale, TODO: implement nonlinear scaling
    optionsDict = checkKwargs(staticDict, 'optionsDict', {}, verbose=False)
    if 'windowNumMax' not in optionsDict and 'windowNum' in optionsDict:
        optionsDict['windowNumMax'] = optionsDict['windowNum']
    elif 'windowNum' not in optionsDict:
        raise NameError('optionsDict requires variable "windowNum":<int>')
    winNumMax = checkKwargs(optionsDict, 'windowNumMax', 100, verbose=False)
    winNumMin = checkKwargs(optionsDict, 'windowNumMin', 10, verbose=False)
    T = checkKwargs(kwargs, 'T', 1)
    Tmin, Tmax = checkKwargs(kwargs, 'Tlim', [0,1])
    winNum = int(np.ceil(winNumMin + ((winNumMax-winNumMin)*(1-(T-Tmin)/(Tmax-Tmin)))))
    print(winNumMin, (winNumMax-winNumMin), 1-(T-Tmin)/(Tmax-Tmin))
    print('New Window Number: {}, T: {}'.format(winNum, T))
    staticDict['optionsDict']['windowNum'] = winNum

def randomEval(inputDict, staticDict={}, **kwargs):
    rng = np.random.RandomState(hash(str(inputDict))%(2**32-1))
    val = rng.randint(0, 10000)
    return val

def bruteLandscape(inputRangeDict, staticDict={}, evaluateFunc=evaluateParameters, dependencyDict={}):
    #inputRangeDict is {'varname':(start, stop, stepNum)}
    #dependencyDict is {'depvarname':('indvarname', relation} where relation is '>' or 'gt', '>=' or 'ge', or '<=' or 'le', '<' or 'lt', '=' or 'eq'
    varNameList = list(inputRangeDict.keys())
    rangeList = [np.linspace(inputRangeDict[varName][0], inputRangeDict[varName][1], inputRangeDict[varName][2]) for varName in varNameList]
    scoreArray = np.ndarray([len(r) for r in rangeList])
    scoreArray[:] = np.nan 
    combos=list(np.array(np.meshgrid(*rangeList)).T.reshape(-1,len(varNameList)))
    inputDictList = []
    #dependancies
    for combo in combos:
        inputDict = {}
        indCombo = []
        for varInd in range(len(varNameList)):
            varName = varNameList[varInd]
            inputDict[varName] = combo[varInd]
            indCombo.append(np.nonzero(rangeList[varInd]==combo[varInd])[0][0])

        if len(dependencyDict) > 0:
            passList = [False for i in range(len(dependencyDict))]
            depList = list(dependencyDict.items())
            for i in range(len(dependencyDict)):
                depVar, depTup = depList[i]
                indVar, rel = depTup
                if depVar in varNameList and indVar in varNameList:
                    if rel in ['>', 'gt'] and inputDict[depVar] > inputDict[indVar]: passList[i] = True
                    if rel in ['>=', 'ge'] and inputDict[depVar] >= inputDict[indVar]: passList[i] = True
                    if rel in ['<', 'lt'] and inputDict[depVar] < inputDict[indVar]: passList[i] = True
                    if rel in ['<=', 'le'] and inputDict[depVar] <= inputDict[indVar]: passList[i] = True
                    if rel in ['=', 'eq'] and inputDict[depVar] == inputDict[indVar]: passList[i] = True
            if all(passList):
                inputDictList.append((tuple(combo), tuple(indCombo), inputDict))
        else:
            inputDictList.append((tuple(combo), tuple(indCombo), inputDict))
    scoreDict = {}
    for combo, indCombo, inputDict in inputDictList:
        score = evaluateFunc(inputDict, staticDict=staticDict) #iteration=tempStr, stepType=winStr)
        print(indCombo, combo, score)
        scoreArray[indCombo] = score
        scoreDict[indCombo] = (combo, inputDict, float(score))
    return scoreArray, scoreDict, varNameList, rangeList

        
        


##===TEST FUNCTIONS===
def sphereFunc(inputDict, staticDict = {}):
    #Test function for simplex. n-dimensional
    #input dict is a dictionary of 'var':value. Values to be varied
    #staticDict is a dictionary of 'var':value. Values kept constant.
    #where variable will, in this case, be anything
    val = np.sum(np.power(np.array(list(inputDict.values()) + list(staticDict.values())), 2))
    #print(val, type(val), inputDict)
    return val 

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    #inputDict = {'x':50, 'y':-50, 'z':50}
    #stateFinal = simAnneal(inputDict, sphereFunc, randomGen)
    #print('Final State:', stateFinal)
    inputRangeDict = {'x':(0, 50,1), 'y':(0,50,1)}
    dependencyDict = {'y':('x', 'lt')}
    landArray, landDict = bruteLandscape(inputRangeDict, evaluateFunc=sphereFunc, dependencyDict=dependencyDict)
    print(landArray)
    plt.pcolor(landArray)
    plt.show()
