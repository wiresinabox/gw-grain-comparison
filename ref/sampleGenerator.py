#For procedurally generated samples.
import numpy as np
import matplotlib.pyplot as plt
import sys

def part(fullLen, partLen, style = 'center'):
    #gives back the indicies associated with the partition, ceil.
    if isinstance(fullLen, tuple):
        startInd = fullLen[0]
        endInd = fullLen[1]
        totLen = abs(fullLen[1] - fullLen[0])
    else:
        startInd = 0
        endInd = fullLen
        totLen = fullLen 
    print(startInd, endInd, totLen)
    numPart = totLen/partLen
    if style == 'center':
        #split the difference at the ends.
        extraLen =  totLen - np.floor(numPart)*partLen
        if extraLen != 0:
            indicies = np.concatenate([np.array([startInd]), np.arange(startInd+(extraLen/2), endInd, partLen),  np.array([endInd])])
        else:
            indicies = np.concatenate([np.arange(startInd, endInd, partLen), np.array([endInd])])
    if style == 'series':
            indicies = np.concatenate([np.arange(startInd, endInd, partLen), np.array([endInd])])

    
    return indicies
        
        

def brickTile(sampleSize, brickxSize, brickySize = False, tileType = 'straight', multLarge =2, fracLarge = 0.2, offset=(0,0), multPeriod = 'rand'):
    if isinstance(sampleSize, int):
        sampleSize = (sampleSize, sampleSize)
   
    if brickySize == False:
        brickySize = brickxSize
    offset = list(offset) 
    offset[0] = offset[0]%brickxSize
    offset[1] = offset[1]%brickySize

    X = np.zeros(sampleSize)
    if tileType == 'straight':
        X = np.zeros((sampleSize[0]+2*brickxSize, sampleSize[1]+2*brickySize))
        xinds = part(X.shape[0], brickxSize)
        yinds = part(X.shape[1], brickySize)
        currentId = 1
        for i in range(len(xinds) - 1):
            lowx = int(xinds[i])
            highx = int(xinds[i+1])
            for j in range(len(yinds) - 1):
                lowy = int(yinds[j])
                highy =int(yinds[j+1])
                X[lowx:highx, lowy:highy] = currentId
                currentId += 1
        X = X[brickxSize+offset[0]:brickxSize+sampleSize[0]+offset[0], brickySize+offset[1]:brickySize+sampleSize[1]+offset[1]]
    elif tileType == 'herringbone':
        #partition the partitions
        #does not accept y size.
        tempSizeX = int(brickxSize*2*(np.floor(sampleSize[0]/(brickxSize*2))+1))
        tempSizeY = int(brickxSize*2*(np.floor(sampleSize[1]/(brickxSize*2))+1))
        numPart = np.floor(sampleSize[1]/(brickxSize*2))+1
        tempX = np.zeros((tempSizeX, tempSizeY))
        xinds = part(tempSizeX, brickxSize*2)
        yinds = part(tempSizeY, brickxSize*2)
        currentId = 1
        partId = 1
        for i in range(len(xinds) - 1):
            lowx = int(xinds[i])
            highx = int(xinds[i+1])
            diffx = int(np.ceil((highx - lowx) / 2))
            for j in range(len(yinds) - 1):
                
                lowy = int(yinds[j])
                highy =int(yinds[j+1])
                diffy = int(np.ceil((highy - lowy) / 2))
                if partId % 2 == 0:
                    tempX[lowx:lowx+diffx, lowy:highy] = currentId
                    tempX[lowx+diffx:highx, lowy:highy] = currentId + 1
                else:  
                    tempX[lowx:highx,lowy:lowy+diffy] = currentId
                    tempX[lowx:highx, lowy+diffy:highy] = currentId+1
                currentId += 2
                partId += 1
            if numPart % 2 == 0:
                partId += 1
        extraX = int((tempX.shape[0] - X.shape[0])/2)
        extraY = int((tempX.shape[1] - X.shape[1])/2)
        X = tempX[extraX:tempX.shape[0] - extraX, extraY:tempX.shape[1]-extraY]
    elif tileType == 'multiple':
        #replaces some smaller tiles with larger ones, an integer multiplier of the small brick size. Straight style.
        #frac large from 0 to 1, will try its best.
        if fracLarge > 1:
            fracLarge = 1
        elif fracLarge < 0:
            fracLarge = 0
        xinds = part(sampleSize[0], brickxSize*multLarge)
        yinds = part(sampleSize[1], brickySize*multLarge)
        xmesh, ymesh = np.meshgrid(xinds[:-1], yinds[:-1])
        xtemp = list(xmesh.flatten())
        ytemp = list(ymesh.flatten())
        largeInds = []
        if multPeriod == 'rand':
            for i in range(int(np.floor(xinds.size*yinds.size*fracLarge))):
                xt = xtemp.pop(np.random.randint(0, len(xtemp)))
                yt = ytemp.pop(np.random.randint(0, len(ytemp)))
                largeInds.append([xt, yt])

        elif isinstance(multPeriod, int):
            for i in range(0, len(xtemp), multPeriod):
                xt = xtemp.pop(i)
                yt = ytemp.pop(i)
                largeInds.append([xt, yt])

        currentId = 1

        for i in range(len(xinds) - 1):
            lowx = int(xinds[i])
            highx = int(xinds[i+1])
            for j in range(len(yinds) - 1):
                lowy = int(yinds[j])
                highy =int(yinds[j+1])
                if [lowx, lowy] not in largeInds:
                    #partition again with ~recursion~
                    mini =brickTile((highx-lowx, highy-lowy), brickxSize, brickySize)+currentId
                    currentId = np.max(mini)
                    X[lowx:highx, lowy:highy] = mini 
                else:
                    X[lowx:highx, lowy:highy] = currentId
                currentId += 1
    return X

def gradient(sampleSize, slope=1, direction=0):
    if isinstance(sampleSize, int):
        sampleSize = (sampleSize, sampleSize)
    xlin = np.linspace(0, sampleSize[0]*slope)
    ylin = np.linspace(0, sampleSize[1]*slope)
    xMesh, yMesh = np.meshgrid(xlin, ylin)
    if direction in [0, 'r', 'right', 'east']:
        return xMesh
    elif direction in [1, 'u', 'up', 'north']:
        return yMesh
    elif direction in [2, 'l', 'left', 'west']:
        return np.flip(xMesh,1)
    elif direction in [3, 'd', 'down', 'south']:
        return np.flip(yMesh,0)
    


def hexagonTile(sampleSize, sideLen = 5, bufferLen=None):
    if isinstance(bufferLen, type(None)):
        bufferLen = int(sideLen*2) #2 hex buffer)
    if isinstance(sampleSize, int):
        sampleSize += int(2*bufferLen)
        sampleSize = (sampleSize, sampleSize)
    else:
        for i in range(len(sampleSize)):
            sampleSize[i] += bufferLen
    X = np.zeros(sampleSize)

    #Seed points every 2*s*cos(30) across and  1.5 s down.
    xSeedSpace = int(np.ceil(2*sideLen*np.cos(np.deg2rad(30))))
    ySeedSpace = int(np.ceil(1.5*sideLen))
    print(xSeedSpace, ySeedSpace) 
    ySeedCoords = np.arange(0, sampleSize[1], ySeedSpace, dtype=int)
    xSeedCoords = np.arange(0, sampleSize[0], xSeedSpace, dtype=int)
    xSeedCoordsOffset = np.arange(0+int(xSeedSpace/2), sampleSize[0], xSeedSpace, dtype=int)
    
    seedCoordsArray = [[],[]]
    seedNum = 0
    for i in range(len(ySeedCoords)): #all rows
        yCoord = ySeedCoords[i]
        if i%2 == 0:
            seedCoordsArray[1].extend(np.array(np.zeros(len(xSeedCoords)) + yCoord, dtype=int))
            seedCoordsArray[0].extend(list(xSeedCoords))
            seedNum += len(xSeedCoords)
        else:
            seedCoordsArray[1].extend(np.array(np.zeros(len(xSeedCoordsOffset)) + yCoord, dtype=int))
            seedCoordsArray[0].extend(list(xSeedCoordsOffset))
            seedNum += len(xSeedCoordsOffset)
    print(seedCoordsArray) 
    seedCoordsArray = np.array(seedCoordsArray, dtype=int)
    
    xCoordOffsets = [0, 0, int(xSeedSpace/2), int(xSeedSpace/2), -int(xSeedSpace/2), -int(xSeedSpace/2)]
    yCoordOffsets = [0, int(sideLen*2), int(sideLen*0.5), int(sideLen*1.5), int(sideLen*0.5), int(sideLen*1.5)]
    for i in range(seedNum):
        xCoord = seedCoordsArray[0][i]
        yCoord = seedCoordsArray[1][i]
        
        X[xCoord, yCoord] = 1
        #row by row.
        xOffs = np.array([int(xoff) for xoff in range(0,int(xSeedSpace/2)+1) if xCoord + xoff < sampleSize[0]])
        for j in range(len(xOffs)):
            xOff = xOffs[j]
            yOffLo = int(np.ceil((j+1)*(1/2)))-1 #slope
            yOffHi = int(sideLen*2 - yOffLo)
            if yOffHi > sampleSize[1]:
                yOffHi = sampleSize[1] - yCoord
            #print(yCoord+ yOffHi, yOffLo, yOffHi)
            #print(xCoord+ xOffs, sampleSize)
            print(xCoord + xOffs, yCoord + yOffLo,  yCoord+yOffHi)
            X[xCoord + xOff, yCoord + yOffLo:yCoord+yOffHi] = i+1
            X[xCoord - xOff, yCoord + yOffLo:yCoord+yOffHi] = i+1


    print(bufferLen)
    return X[bufferLen:sampleSize[0]-bufferLen, bufferLen:sampleSize[1]-bufferLen]
    #return X

def offsetQuad(sampleSize, xoffset = 0, yoffset=0):
    #4 grains, partition into quadrents
    if isinstance(sampleSize, int):
        sampleSize=(sampleSize,sampleSize)

    midx = int(np.ceil((sampleSize[0]/2) + xoffset))
    midy = int(np.ceil((sampleSize[1]/2) + yoffset))
    X = np.zeros(sampleSize)
    X[0:midx, 0:midy] = 1
    X[midx:sampleSize[0], 0:midy] = 2
    X[midx:sampleSize[0], midy:sampleSize[1]] =3
    X[0:midx, midy:sampleSize[1]] = 4
    return X

def offsetSquare(sampleSize, grainSize=6, xoffset=0, yoffset=0):
    if isinstance(sampleSize, int):
        sampleSize=(sampleSize,sampleSize)
    if isinstance(grainSize, int):
        grainSize=(grainSize,grainSize)

    midgx = int(np.ceil((grainSize[0]/2)))
    midgy = int(np.ceil((grainSize[1]/2)))
    
    midx = int(np.ceil((sampleSize[0]/2) + xoffset))
    midy = int(np.ceil((sampleSize[1]/2) + yoffset))
    
    X = np.zeros(sampleSize)+1
    X[midx-midgx:midx+midgx, midy-midgy:midy+midgy]=np.zeros(grainSize)+2
    print(midx-midgx, midx+midgx, midy+midgy, midy+midgy)
    return X



if __name__ == '__main__':
    import plotUtil
    try:
        size = int(sys.argv[1])
    except:
        size = 5
    #Xstack = brickTile(50, 5, 10, tileType = 'herringbone')
    #Xstack = hexagonTile(50, size) 
    #Xstack = gradient(50, slope=1, direction=3) 
    Xstack = brickTile(24, 12, 12, tileType='straight') 
    XstackOff = brickTile(24, 12, 12, offset=(15,15), tileType='straight') 
    fig, axs = plotUtil.plotMicroStruct([Xstack, XstackOff], sdf=True, verbose=False)
    #print(Xstack)
    #plt.axis('equal')
    #plt.pcolor(Xstack)
    #fig.colorbar()
    plt.show()
