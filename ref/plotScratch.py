import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.image as mpimg
import matplotlib.cm as cm
import matplotlib.colors as colors
from plotUtil import rasterPlot, crossPlot, linPlot, plotMicroStruct, getImage
import os

#    ccmat = np.array([[ 55.42481591, 584.63087909,  60.37825013,  54.43385176,  76.91017833,   88.21227023,  199.08284981],
# [223.43454909, 201.54401046, 223.23471621, 185.55201273, 306.68844924,  304.66807605,  371.79265804],
# [ 55.75455075, 284.99233422,  51.13200057,  57.12638921,  77.68809466,   94.46167715,  189.10238604],
# [ 53.24339936, 486.93177621,  56.58781026,  26.55907391,  80.38158913,   76.21439824,  136.72595393],
# [ 75.1324595 , 588.3870984 ,  94.51231982,  70.00561033,  84.50420085,  107.60877084,  302.77450034],
# [113.81696769, 726.88094067, 136.65368061,  83.06740245, 144.08526998,   57.67916644,  121.5222185 ],
# [149.79547743, 658.82592844, 168.79835066, 115.94023527, 229.82958605,  151.34046148,   45.98515086]])
#    sampleList = []
#    nameList = []
#    sampleList.append('../dream3d-samples/eq1-matrix.dream3d') #equiaxed
#    nameList.append('Equiaxed')
#    sampleList.append('../dream3d-samples/eq2x-matrix.dream3d') #twice as big
#    nameList.append('Equiaxed, 2x ESD')
#    sampleList.append('../dream3d-samples/eq-bimodal-2x-matrix.dream3d') #two sizes
#    nameList.append('Bimodal, 0.5 vol. frac. 2x ESD')
#    sampleList.append('../dream3d-samples/eq-bimodal-05x-matrix.dream3d') #two sizes, but small
#    nameList.append('Bimodal, 0.5 vol. frac. 0.5x ESD')
#    sampleList.append('../dream3d-samples/r-2-1-05-orient-matrix.dream3d') #rolled
#    nameList.append('Rolled 2:1:0.5')
#    sampleList.append(sg.brickTile(256, 8, 8, tileType = 'straight')) #straight
#    nameList.append('Bricks, 8x8 pixels')
#    sampleList.append(sg.brickTile(256, 6, 10, tileType = 'straight')) #straight
#    nameList.append('Bricks, 6x10 pixels')
#    
#    
#    plotTitle='15 Windows - upf: frac - lmult: 10 & 1 - Cscale: 1 - penType: overlay'
#    fig = crossPlot(ccmat, nameList, plotTitle)
#    plt.ioff()
#    #plt.show()
#    plt.savefig('{}/{}-ccmat.png'.format(outDir, jobNumber), dpi = 300, bbox_inches='tight')
#    plt.close(fig)

def linErrESD2():
    #A1 = np.array([[2269.9142412,1885.21808737,2061.35763754,1931.18510229,2096.36430443,1943.92820494,1869.44969905],
    #[3141.56027974,2412.25302033,2642.38603057,2371.92973368,2384.31114831,2411.62871567,2266.26163893],
    #[2520.29019625,2311.03933763,2316.11630748,2098.7866652,2108.87074988,2051.1402094,2209.94758064]])
    #A2 = np.array([[2673.91373372,2380.74723461,2501.94798259,2000.06050867,2007.87064798,1989.93860714,2088.33249311],
    #[2893.65451971,2677.55428661,2515.07076452,2275.68530597,2323.99411376,2439.56602918,2315.69916447],
    #[1893.60788589,2279.41145792,2224.27065523,2121.78536393,2160.10697449,2128.9273141,2113.62819802]])
    #A3 = np.array([[2211.93227016,2296.17445369,2064.12238258,1858.17471386,1998.33603632,1945.97398136,1912.85784656],
    #[2891.12355438,2563.80857605,2465.47112634,2464.25097794,2382.74410854,2350.69701114,2343.75710429],
    #[2465.06026195,2497.83068793,2200.4612554,2126.01349813,2036.64406336,2125.81790684,2035.92843796]])
    #A4 = np.array([[2393.47734169,2449.62887668,2244.41324914,1979.14122789,1992.83320486,1904.72738768,1994.00050584],
    #[2525.14217601,2346.98143068,2283.26117227,2249.29929218,2478.83164708,2526.64469054,2300.4056968,],
    #[2335.97117775,1689.95877109,2056.88484878,2160.48073304,2058.79539086,2176.02630177,2192.87828657]])
    #A5 = np.array([[2592.60948043,1978.92100208,2032.90770299,2243.13770855,2019.95442881,1860.03444324,1979.78740133],
    #[2209.6289068,2870.72930138,2323.81820311,2290.80626975,2282.23552178,2392.98022536,2341.31899932],
    #[2162.92473243,2450.71408808,2092.4446503,2181.64955473,1975.14878829,2022.01426957,2079.61460963]])
    #A6 = np.array([[1839.62453732,2270.67044865,2037.41344305,1955.90620894,2114.94293023,2049.55906739,1930.83602522],
    #[2364.51174686,2372.24963682,2481.09434446,2322.96368056,2274.26059172,2275.04530324,2308.34029229],
    #[2890.29985474,2361.46321302,2114.6332354,2068.28249346,2083.88441757,2092.9111564,2142.18395961]])
    #Regen Method, First pass

    As = [A1]# A2, A3, A4, A5, A6]
    varRange=np.array([1,3,5,10,12,15,20])
    varName = 'windowNum'
    titles=[]
    titles.append('Rolled 2:1:0.5 v. Rolled (ESD 17.07, 17.07)')
    titles.append('Rolled 2:1:0.5 v. Equiaxed (ESD 17.07, 16.67)')
    titles.append('Rolled 2:1:0.5 v. Bimodal 0.5x (ESD 17.07, 12.31)')
    plotTitle='Windows 1 - 2 ESD - Wass Score Penalty: frac- lmults: 10 & 1 - Cscale: 1 - constESD. Old Regen'
    plotStyles = []
    multiStyle = 'std'
    linPlot(As, varRange=varRange, varName=varName, titles=titles, plotTitle=plotTitle, plotStyles=plotStyles, multiStyle=multiStyle)
    plt.show()

def linErrESD3():
    A1 = np.array([[5064.29253085,4227.29862821,4954.08836014,4877.59715175,4205.49785679,4094.26172361,4763.60649228],
    [5798.16042151,4927.5599688,5021.95347301,4729.21001594,4866.72490847,4811.42097869,4946.14493661],
    [4879.25709202,5850.07777797,5073.65401744,4561.1562049,4682.89507647,4522.89703694,4584.98527644]])
    A2 = np.array([[3886.54978141,4453.65529178,4421.0472749,4312.64437046,4548.4777806,4371.17547008,4534.02890617],
    [4874.58140123,5485.64083334,4835.19388282,5037.54568844,4953.79099643,4758.85605987,4711.29943694],
    [6516.24569793,4618.93961487,4513.43363487,4644.40305776,4489.91795065,4563.69306951,4679.48579786]])
    A3 = np.array([[5820.98074606,4240.15743989,5627.31853888,4500.12757894,4121.1149276,4490.50305089,4323.46166447],
    [5726.25242778,4905.39238086,5270.82125178,5312.09403287,4849.65981482,5041.15137904,4889.51737525],
    [6549.80329815,5323.06344284,4967.73775524,4610.26030313,4731.72994759,4437.50566811,4761.57321955]])
    A4 = np.array([[4758.59222356,4432.25521302,4651.90249481,4123.94260321,4241.61924979,4229.2408898,4465.00489668],
    [4831.18971973,5505.2789221,5202.6356878,4904.46364257,4852.13669674,4850.17854902,4937.3294221],
    [4940.59556418,4752.98025304,4738.77433981,4670.06150496,4436.86396259,4502.72603462,4364.66303676]])
    A5 = np.array([[4164.88510928,5091.7426585,5037.99801028,4694.36944445,4244.54958682,4393.99374026,4424.02053349],
    [5540.59081941,5258.59564682,5132.63025558,4577.65220591,4866.26041731,4859.45076047,4944.00719175],
    [5150.4086755,5063.7115906,5079.89884353,4492.64649456,4636.35348952,4538.55021869,4516.55264265]])
    A6 = np.array([[4456.39948941,4673.88146761,4315.3233635,4256.56697181,4338.2810946,4286.44190966,4262.86518571],
    [4931.87348353,5314.0985028,4867.79756627,5047.86033292,4831.919695,5008.74717429,4989.33920282],
    [5385.55127214,4795.84547753,4534.61360814,4407.94216304,4632.77422502,4459.17448825,4315.7071221]])

    As = [A1, A2, A3, A4, A5, A6]
    varRange=np.array([1,3,5,10,12,15,20])
    varName = 'windowNum'
    titles=[]
    titles.append('Rolled 2:1:0.5 v. Rolled (ESD 17.07, 17.07)')
    titles.append('Rolled 2:1:0.5 v. Equiaxed (ESD 17.07, 16.67)')
    titles.append('Rolled 2:1:0.5 v. Bimodal 0.5x (ESD 17.07, 12.31)')
    plotTitle='Windows 1 - 3 ESD - Wass Score Penalty: frac- lmults: 10 & 1 - Cscale: 1 - constESD'
    plotStyles = []
    multiStyle = 'std'
    linPlot(As, varRange=varRange, varName=varName, titles=titles, plotTitle=plotTitle, plotStyles=plotStyles, multiStyle=multiStyle)
    plt.show()

def linErrWin():
    #A1 = np.array([[4204.61181703,3471.8702816,3076.2216961,2091.81734769,1655.69319691,1220.11385551,774.58993274,496.77194742,225.0395076,103.32228386],
    #[4759.80291514,4080.59402189,3368.02742349,2655.80568503,2292.04059473,1502.70629693,913.14036281,599.90087447,291.90996561,97.7671037],
    #[4663.6539186,3754.66388645,3041.55043245,2405.25216233,1824.10296643,1264.00059623,822.85468203,546.06072791,272.29231813,96.70116649]])
    #A2 = np.array([[4389.66893361,3686.96028906,2823.04075023,2242.50769169,1800.60937656,1211.52395289,809.36647357,476.81882543,284.69817801,71.69130977],
    #[4776.58817603,4204.9940986,3399.37024965,2654.66108467,2206.3893573,1562.29782014,1040.08271581,596.73382769,335.92358583,84.97630122],
    #[4634.3514138,3658.1253834,3113.35185619,2553.12838803,1914.93713396,1327.00474981,844.74755708,574.90520876,269.0967566,85.2928764]])
    #A3 = np.array([[4371.05767889,3475.07073455,2723.16554231,2139.6011431,1685.26153569,1190.00717289,878.77578468,536.668883,264.80525424,77.49623563],
    #[4851.25719853,4286.40934408,3239.83203507,2721.94795123,1941.31403862,1475.81004278,992.95729542,731.30027871,308.71433017,85.56821288],
    #[4314.87933229,3641.73687941,3046.94193045,2487.2880254,1788.33400735,1275.56700087,925.14046243,599.26767362,292.78879663,79.05052026]])
    #A4 = np.array([[4585.20166405,3843.33912044,2955.53545127,2228.04766033,1862.78179656,1270.33759677,837.07636302,538.87570443,254.14442029,96.85593661],
    #[4954.96269,4102.12479106,3424.03398162,2679.24191054,2124.98963007,1575.66790871,935.41456541,704.3620735,310.6738255,87.53570946],
    #[4420.3849179,4075.26769301,2994.17899955,2474.79383435,1782.95124069,1284.24815717,846.17732801,506.67554246,262.21762921,139.43251747]])
    #A5 = np.array([[4689.55504359,3704.16494536,2891.92317874,2186.08848301,1698.50814123,1277.72633933,828.95676573,492.51177313,235.4789288,90.43154465],
    #[5084.10734587,4142.0337794,3402.40823799,2624.98554266,2178.18291378,1475.52928515,1023.51669076,611.23046551,318.72445393,97.70486842],
    #[4434.33132315,3958.66011078,3087.45142712,2341.75788651,1875.8259957,1289.55515409,828.74072112,549.07254895,248.25808097,86.55005729]])
    #varRange = np.linspace(3, 0.5, 10)

#s/  / /g
#s/ \]/\]/g
#s/ /,/g

    #A1 = np.array([[2400.58623478,1968.99595797,1495.11590996,1142.58645714,960.28473691,661.42097217,448.94320819,274.54809738,128.78165933,35.8206837],
    #[3209.56554738,2318.49415815,1803.35038125,1429.15551728,1122.03464577,806.96126067,523.76425904,388.63769137,174.4370717,55.04744832],
    #[2320.82288053,2138.84544412,1500.87968735,1248.54609017,951.24571266,686.88899645,444.05567087,271.08199208,149.03580899,54.96400536]])
    #A2 = np.array([[2365.05924333,1926.91247901,1570.35231872,1103.25099748,859.77111662,626.40665853,435.49672072,268.98237286,141.86546716,55.90091107],
    #[2992.12544326,2357.95475873,1715.46939545,1411.28598319,1080.38822473,766.58622617,515.83902729,428.47701444,202.98100177,47.03824856],
    #[2455.75733614,2084.08629937,1665.9939732,1243.46475741,966.16480425,649.72472397,442.76499885,290.52497748,139.27000347,45.98736888]])
    #A3 = np.array([[2472.58906498,1842.53519884,1545.22564711,1149.97590564,932.60677765,673.61167953,410.31384841,276.30326231,123.47548735,39.06321836],
    #[3279.08156564,2244.46346283,1804.70840928,1513.21631302,1130.57871893,820.87210406,540.22579194,417.18204409,174.44545081,62.80892538],
    #[2247.00132745,2001.14661348,1626.29564506,1223.67462076,939.83764065,660.98198191,449.97437369,274.48399385,136.96172433,55.1733156]])
    #A4 = np.array([[2613.04465256,1931.9152854,1585.23372128,1224.88487509,924.99330289,642.5552719,418.42521275,266.63527239,133.47170408,41.3254278],
    #[3183.83685118,2390.12853029,1784.76708502,1378.18616319,1161.92719135,771.79964443,528.38372031,336.8090227,186.65197192,45.81698338],
    #[2310.20756118,1998.5130809,1618.09321327,1198.63364648,961.63459991,642.60202737,465.59530142,269.08930094,140.90243761,54.45794052]])
    #A5 = np.array([[2481.94683533,2066.5151648,1499.82789979,1158.57000669,1055.08524925,634.85321987,439.79230353,284.44771977,127.69736496,40.66920153],
    #[3026.17348017,2403.95132123,1830.4909458,1374.51087577,1077.66014534,765.72386203,533.32659964,401.39549373,169.28420049,66.62378504],
    #[2248.83177188,1995.80467621,1608.99349382,1250.97230161,1010.53712306,678.57293827,434.16174118,273.84019116,127.97913195,46.13787475]])
    #Old Regen Method
    #A1 = np.array([[2445.79994114,2009.06756646,1529.53063061,1180.97615559,913.80397947,652.4817483,420.62866698,275.29793389,141.53173841,37.07779067],
    #[2634.84709603,2267.0567125,1724.23791541,1362.22813809,1071.75969283,721.92163863,445.54407372,327.63172198,167.5182364,40.34439882],
    #[2631.67816394,2142.36711856,1693.18912324,1369.06691128,945.5191701,638.06930855,457.30781729,272.9096527,142.83405903,64.63971256]])
    #Regen Method:
    A1 = np.array([[6069.13863822,5151.13194709,4553.93637046,3713.74863869,3001.04712751,2305.49625063,1877.62988451,1239.36758451,914.28650706,562.65826155],
    [6190.84097323,5464.83545563,4957.24455618,3993.09612817,3345.45048155,2534.82183356,1924.57811143,1418.58868234,1052.29039887,656.05505042],
    [10136.98971248,8502.50556825,7586.66732176,6393.79901376,5141.1402599,3903.49872538,3103.54775204,2072.79442571,1208.21401654,667.23727897]])

    A2 = np.array([[6062.09372755,5034.15138319,4553.65248388,3531.78610471,3085.9245224,2272.63975605,1808.16448039,1238.06384571,921.34534042,554.72190758],
    [6311.27713807,5635.86766109,4795.68005177,3866.00841107,3260.92455287,2447.53020263,1861.41460041,1314.57058486,956.04050991,684.6918165],
    [10442.90976592,8868.26682052,7353.13950424,6161.60615483,5063.96890869,3832.3659183,2945.4413904,2406.81151841,1398.01053612,776.61876952]])

    A3 = np.array([[6031.12154414,4980.17078403,4405.68689284,3504.30570475,2980.13281924,2308.30944529,1825.62275914,1253.94993208,882.7996218,571.68155445],
    [6333.25350392,5405.09835591,4717.61289694,3845.70022452,3123.54542486,2443.25669483,2065.62420766,1355.27174137,1087.27293076,616.99413491],
    [10477.84618274,8888.69215532,7689.46002268,5828.30974483,5214.17507125,3814.68564041,2980.57541925,2139.0838531,1188.83909717,672.37109552]])

    A4 = np.array([[5696.74226149,4789.08586323,4264.3624745,3712.94841202,2925.67972185,2222.1119249,1717.72242874,1202.12584211,890.65585987,563.45985374],
    [6028.14756567,5782.61186179,4627.66076922,3945.32073942,3291.32056591,2410.01121151,1912.69613867,1419.67807752,998.97713255,629.37910089],
    [10237.16587741,8250.36687381,7005.25114472,5889.39169954,5059.55057478,3784.65877245,3021.7166524,2125.86731022,1420.40712785,648.22673329]])

    A5 = np.array([[5617.93133218,4998.83053021,4229.03851727,3429.67999305,2964.74810983,2268.96837466,1650.94855952,1190.22717694,876.45902052,544.80703732],
    [6447.79609031,5435.37494934,4702.41658571,3779.00577829,3208.46302422,2412.44840472,1982.93007003,1371.11624689,990.21532075,677.36519688],
    [10131.44882272,8622.42592322,7625.70593382,6025.4082108,4969.32468457,3876.47036772,3126.6981374,2229.59833251,1435.43752897,685.36214513]])

    A6 = np.array([[6037.36051324,5279.30602517,4508.6972669,3585.81191639,3099.11146135,2273.53006356,1811.9252213,1233.79406667,863.26165403,567.24414144],
    [6236.57066954,5639.38531383,4863.25867613,4054.20409084,3349.83771698,2715.43126215,1951.77098326,1385.25175017,1036.33643171,696.01076558],
    [10053.75441355,8760.12782146,7501.88751656,6290.18757863,5120.53508896,4040.95040081,3156.46758579,2116.85453828,1272.53483059,697.04456092]])

    A7 = np.array([[6111.66165446,5158.07646874,4418.91671488,3557.49767815,3093.10254685,2358.32278742,1773.26599885,1250.32524076,858.37561525,550.73419931],
    [6491.95836202,5458.0063192,5026.48168126,3980.32671771,3373.46055061,2486.30174224,2043.94945,1488.88878005,1032.80824842,634.12759416],
    [10957.06709411,8740.02658055,7917.7192169,6081.62585535,5180.66989529,3719.38213898,3118.02925963,2247.06725348,1170.71070125,691.23059543]])

    varRange = np.linspace(6, 1, 10)
    As = [A1, A2, A3, A4, A5, A6, A7]
    varName = 'noGrainInWindow'
    titles=[]
    titles.append('Rolled 2:1:0.5 v. Rolled ')
    titles.append('Rolled 2:1:0.5 v. Equiaxed ')
    titles.append('Rolled 2:1:0.5 v. Bimodal 0.5x ')
    plotTitle='Windows 20 - Variable ESD - Wass Score Penalty: frac- lmults: 10 & 1 - Cscale: 1 - constESD, New Regen'
    plotStyles = []
    multiStyle = 'std'
    linPlot(As, varRange=varRange, varName=varName, titles=titles, plotTitle=plotTitle, plotStyles=plotStyles, multiStyle=multiStyle)
    plt.show()



def fig1():
    #Window-Window scores
    fig = plt.figure(figsize=(7,2.8))
    #data=np.array([0.00025, 0.877, 0.9673, 1.276, 1.620]) #ref: 8x8 vs 6x10 bricks, 6x10 herringbone, 7 px side hexagons
    #data = np.array([1.08222157, 2.61110899, 3.00666385, 3.89444179, 4.0111083])

    data = np.array([0.,         2.60666455, 2.86666411, 3.93777506, 3.94555283, ])
    imagesList=[
            '../paper-figures/tiling-micro-add/square0.png', 
            '../paper-figures/tiling-micro-add/square0.png', 
            '../paper-figures/tiling-micro-add/square1.png',
            '../paper-figures/tiling-micro-add/square4.png',
            '../paper-figures/tiling-micro-add/square2.png',
            '../paper-figures/tiling-micro-add/square3.png',
            ]
    gs = gridspec.GridSpec(2, len(data)+1)
    axs = [] 
    

    ax=fig.add_subplot(gs[1, 1:])
    axs.append(ax)
    textKwarg = {
        'fontsize':12,
        'fontfamily':'serif',
            }
    crossPlot(data, fig=fig, ax=ax, textKwarg=textKwarg)
    for i in range(1, len(imagesList)):
        imgfp = imagesList[i]
        ax=fig.add_subplot(gs[0, i])
        axs.append(ax)
        if isinstance(imgfp, type(None)):
            continue
        getImage(imgfp, fig = fig, ax = ax)

    imgfp = imagesList[0]
    ax=fig.add_subplot(gs[1,0])
    axs.append(ax)
    getImage(imgfp, fig = fig, ax = ax)
    plt.savefig('fig1-add.png') 
    #plt.show()

def fig2():
    from crossSample import resMain

    reffpList = []
    #reffpList.append(             '../dream3d-samples/eq1-matrix.dream3d') #equiaxed
    #reffpList.append( '../dream3d-samples/1040-AR-40x.dream3d') #rolled
    #reffpList.append( '../dream3d-samples/r-2-1-05-orient-matrix.dream3d') #rolled
    
    #reffpList.append(  '../dream3d-samples/eq1-matrix-res2x.dream3d') #two sizes, but small
    #reffpList.append(  '../dream3d-samples/eq2x-matrix-res2x.dream3d') #two sizes, but small
    #reffpList.append(  '../dream3d-samples/eq-bimodal-2x-matrix-res2x.dream3d') #two sizes, but small
    #reffpList.append(  '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
    reffpList.append(  '../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
    
    #reffpList.append(   '../dream3d-samples/eq-bimodal-2x-matrix.dream3d') #two sizes, but small
    #reffpList.append(            '../dream3d-samples/eq2x-matrix.dream3d') #two sizes, but small
    #reffpList.append(sg.brickTile(256, 12, 12, tileType = 'straight')) #straight
    #reffpList.append(sg.brickTile(256, 8, 18, tileType = 'straight')) #straight
    #reffpList.append(sg.brickTile(256, 8, 18, tileType = 'herringbone')) #straight
    #reffpList.append(sg.hexagonTile(256, 7)) #straight
    
    optionsDict = {
            'windowNum':1,
            'axis':'z',
            'slice': 'half', #DON'T FORGET TO CHANGE THIS.
            'relativeScale':False, #Depreciated, Probably
            'totalAreaHold' : 1, #Depreciated 
            'noGrainInWindow':4, #Depreciated
            'forceWindowLen':128, #Overrides noGrainInWindow
            'winSizeDetermine':'max', #Depreciated
            'distMultiplier':1, #Depreciated
            'centerOnRandGrain': 'center', 
            'distType' : 'sdf', 
            'sdfDistMin': 0, 
            'sdfPenaltyType':'constesd', #Depreciated 
            'ignoreBoundaryEdges':True,
            'verbose':False,
            'forceCellLength':True,
            'sdfNormalize':True, #Forces each window to have the same amount of mass
            'sdfReverse': True,
            'sdfRoot':False,
            'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
            'sdfFloorNorm':False,
            'sdfThresh':0
        }
    loopType='micro' #allmicro (all windows on one), micro (seperate images), regen (regenerate structure w/ hardcoded options)
    
    resMain(reffpList=reffpList, optionsDict=optionsDict, loopType=loopType, plot3d=False, sdf=False, sharedcm=False)




def fig3():
    #Cross plot 800 windows, 2x resolution JOBID: 5301245

    fig = plt.figure(figsize=(7.95,6))
    gs = gridspec.GridSpec(6,8)
    axs = []
    
    ax=fig.add_subplot(gs[1:6, 0:5])
    axs.append(ax)

    scoreArray = []
    #scoreArray.append([51088.52, 104164.7,  56111.82, 60494.21,71804.54])
    #scoreArray.append([104164.7, 47229.04,  117553.3, 84134.65,119654.6])
    #scoreArray.append([56111.82, 117553.3,  47672.25, 69366.22,61461.74])
    #scoreArray.append([60494.21, 84134.65,  69366.22, 51329.78,78871.12])
    #scoreArray.append([71804.54, 119654.6,  61461.74, 78871.12,51718.05])
    #scoreArray.append([3.386111, 6.514927, 3.699375, 3.954667,  4.48409])
    #scoreArray.append([6.514927, 3.199845, 7.264314, 5.42789,   7.658776])
    #scoreArray.append([3.699375, 7.264314, 3.127419, 4.674071,  3.951459])
    #scoreArray.append([3.954667, 5.42789,  4.674071, 3.516407,  5.056013])
    #scoreArray.append([4.48409,  7.658776, 3.951459, 5.506013,  3.414262])
    #scoreArray.append([0.698588,  1.278556,  0.7570247, 0.8439245, 0.9075777 ])
    #scoreArray.append([1.278556,  0.6886992, 1.477971,  1.035973,  1.170892  ])
    #scoreArray.append([0.7570247, 1.477971,  0.6139589, 0.910904,  0.928542  ])
    #scoreArray.append([0.8439245, 1.035973,  0.910904,  0.7150051, 0.9024799 ])
    #scoreArray.append([0.9075777, 1.170892,  0.928542,  0.9024799, 0.7179412 ])
    scoreArray.append([4.35883283,13.44881579,4.76405922, 6.00041535,  5.44093848])
    scoreArray.append([13.44881579,5.69129431,15.70237895,11.04461335, 16.9436016])
    scoreArray.append([4.76405922,15.70237895,3.78664681, 7.50526506,  4.09520609])
    scoreArray.append([6.00041535,11.04461335,7.50526506, 5.00818779,  7.87270508])
    scoreArray.append([5.44093848,16.9436016,  4.09520609,7.87270508,  3.08223071])


    scoreArray = np.array(scoreArray)
    print(scoreArray)
    #titles = ['EI', 'EII', 'BI', 'BII', 'R']
    titles = ['Equiaxed\n(EI)', 'Equiaxed\n(Large) (EII)', 'Bimodal\n(Small) (BI)', 'Bimodal\n(Large)(BII)', 'Rolled\n(R)']
    strArray = np.ndarray(scoreArray.shape, dtype=np.object)
    for i in range(scoreArray.shape[0]):
        for j in range(scoreArray.shape[1]):
            print(i,j)
            strArray[i,j] = '{:}'.format(np.round(scoreArray[i,j], 3))
    print(strArray)
    textKwarg = {
        'fontsize':10,
        'fontfamily':'serif',
            }
    titleKwarg = {
        'fontsize':12,
        'fontfamily':'serif',
            }
    #fig, axs = plt.subplots(1,1)
    cmap ='viridis'
    crossPlot(scoreArray, titles, celltitles=strArray, fig= fig, ax = ax, titleKwarg = titleKwarg, textKwarg = textKwarg, cmap=cmap, colorbar = False)
    ax=fig.add_subplot(gs[:,6])
    axs.append(ax)
    fig.colorbar(cm.ScalarMappable(norm=colors.Normalize(vmin=np.min(scoreArray), vmax = np.max(scoreArray)), cmap=cmap), cax=ax)

    #imagesList=[
    #        '../paper-figures/window-micro-20px/eq1-matrix-res2x-sdf.png', #two sizes, 
    #        '../paper-figures/window-micro-20px/eq2x-matrix-res2x-sdf.png', #two sizes',
    #        '../paper-figures/window-micro-20px/eq-bimodal-05x-matrix-res2x-sdf.png', 
    #        '../paper-figures/window-micro-20px/eq-bimodal-2x-matrix-res2x-sdf.png', #',
    #        '../paper-figures/window-micro-20px/r-2-1-05-orient-matrix-res2x-sdf.png',
    #        ]
    imagesList=[
            '../paper-figures/window-micro-128px/eq1-matrix-res2x-sdf.png', #two sizes, 
            '../paper-figures/window-micro-128px/eq2x-matrix-res2x-sdf.png', #two sizes',
            '../paper-figures/window-micro-128px/eq-bimodal-05x-matrix-res2x-sdf.png', 
            '../paper-figures/window-micro-128px/eq-bimodal-2x-matrix-res2x-sdf.png', #',
            '../paper-figures/window-micro-128px/r-2-1-05-orient-matrix-res2x-sdf.png',
            ]
    for i in range(0, len(imagesList)):
        imgfp = imagesList[i]
        ax=fig.add_subplot(gs[0, i])
        axs.append(ax)
        if isinstance(imgfp, type(None)):
            continue
        getImage(imgfp, fig = fig, ax = ax)
        
        imgfp = imagesList[i]
        ax=fig.add_subplot(gs[i+1, 5])
        axs.append(ax)
        if isinstance(imgfp, type(None)):
            continue
        getImage(imgfp, fig = fig, ax = ax)

    #plt.show()
    plt.savefig('500window-2xres-crossplot-add-pres.png')


def cutSimplexWindows(simplexDir = '../paper-figures/simplex-5302144', xlims = [(175, 520), (700, 1045), (1226, 1571)], ylims=[(475,820)], addlabels=['x', 'y', 'z'], fnFilter='synthetic'):
    #this will make a duplicate of thing it's cutting.
    cutDir = simplexDir + '-cutouts'
    fnList = os.listdir(simplexDir)
    try:
        os.mkdir(cutDir)
    except FileExistsError:
        pass
    selfRefScore = 0
    synthScores = []
    synthDataList = []


    synthDataList = []
    for fn in fnList:
        fnstat = os.stat(simplexDir + '/' + fn)
        if fn.startswith(fnFilter):
            date = fnstat.st_ctime 
            synthDataList.append([date, fn])
    print(synthDataList)
    synthDataList.sort(key=lambda x: x[0], reverse=True)
    for k in range(len(synthDataList)):
        fn = synthDataList[k][1]
        img = mpimg.imread(simplexDir + '/' + fn)
        #print(img.shape, type(img))
        #plt.imshow(img)
        #plt.show()
        
        for i in range(len(xlims)):
            for j in range(len(ylims)):
         
                xlo, xhi = xlims[i]
                ylo, yhi = ylims[j]
                print(xlo, xhi, ylo, yhi, xhi-xlo, yhi-ylo)
                cropimg = img[ylo:yhi, xlo:xhi]
                plt.imsave('{}/{}-{}-{}'.format(cutDir, k+1, addlabels[j*len(xlims)+i],fn), cropimg)
    return cutDir


def fig4():

    #simplexDir = '../paper-figures/simplex-5302144'
    simplexDir = '../paper-figures/simplex-450win-30px'
    cutDir = cutSimplexWindows(simplexDir=simplexDir)
    fnList = os.listdir(simplexDir)
    
    selfRefScore = 0
    synthScores = []
    synthDataList = []
    for fn in fnList:
        fnstat = os.stat(simplexDir + '/' + fn)
        fnParts = fn.replace('.png', '').split('-')
        if fn.startswith('ref-self-compare'):
            fnParts = fn.replace('.png', '').split('-')
            selfRefScore = float(fnParts[-1].replace('_', '.'))
        elif fn.startswith('synthetic'):
            mu =  float(fnParts[1].replace('_', '.'))
            std = float(fnParts[2].replace('_', '.'))
            ba =  float(fnParts[3].replace('_', '.'))
            ca =  float(fnParts[4].replace('_', '.'))
            score =  float(fnParts[-1].replace('_', '.'))
            date = fnstat.st_mtime
            synthDataList.append([date, score, mu, std, ba, ca])
            print(fn, date)

    synthDataList.sort(key=lambda x: x[0])
    print(synthDataList)

    #fig = plt.figure(figsize = (8,6))
    fig = plt.figure(figsize=(5.95,6))
    gs = gridspec.GridSpec(6,6)
    axs = []
    
    ax1=fig.add_subplot(gs[:, 0:5])
    axs.append(ax1)
    
    #fig, ax1 = plt.subplots(1,1, figsize=(6,6))
    iterNum = np.arange(0, len(synthDataList))+1
    scoreVec = np.array([tup[1] for tup in synthDataList])
    baVec = np.array([tup[4] for tup in synthDataList])
    caVec = np.array([tup[5] for tup in synthDataList])
    bcVec=baVec/caVec
    
    ax1.set_xlim(iterNum[0], iterNum[-1])
    ax1.set_ylabel('Score')
    ax1.set_xlabel('Iteration')
    ax2 = ax1.twinx()
    ax2.set_ylim(0, 2.1)
    ax2.set_ylabel('Ratio')
    
    lineScore = ax1.plot(iterNum, scoreVec, linewidth=3, color='black', label=r'$S_{ref, synth}$')
    ax1.axhline(selfRefScore, color='black', linestyle='dotted', linewidth=3)
    ax1.text(1.1,selfRefScore-200, 'Self-Ref. Score', verticalalignment='top', fontfamily='serif', fontsize='medium')

    lineBA= ax2.plot(iterNum, baVec, linestyle='dashed',color='tab:blue', linewidth=2, label='B/A Ratio')
    ax2.axhline(0.5, color='tab:blue', linestyle='dotted', linewidth=2)
    ax2.text(28, 0.5, 'Target\nB/A', verticalalignment='bottom', fontfamily='serif', fontsize='medium', color='tab:blue')
   
    lineCA = ax2.plot(iterNum, caVec, linestyle='dashdot',color='tab:red', linewidth=2, label='C/A Ratio')
    ax2.axhline(0.25, color='tab:red', linestyle='dotted', linewidth=2)
    ax2.text(28, 0.25, 'Target\nC/A', verticalalignment='bottom', fontfamily='serif', fontsize='medium', color='tab:red')
    
    lineBC = ax2.plot(iterNum, bcVec, linestyle='solid',color='tab:orange', linewidth=2, label='B/C Ratio')
    ax2.axhline(2, color='tab:orange', linestyle='dotted', linewidth=2)
    ax2.text(28, 1.95, 'Target\nB/C', verticalalignment='top', fontfamily='serif', fontsize='medium', color='tab:orange')

    ax1.legend(handles=[lineScore[0], lineBA[0], lineCA[0], lineBC[0]], loc='center right')

    print(bcVec) 
    #plt.show()
    plt.savefig('simplexfig-450win-30px.png', dpi=300)

            
def fig5():
#experimental
    from multiLinPlot import parseOutput
    import glob
    varRange = np.array([60, 49, 38, 27, 16, 5])
    scoresArray = []
    #scoresArray.append([1.09807261,1.05148571,0.96730206,0.81495141,0.59933786,0.21876952])
    #scoresArray.append([1.17136232,1.10412902,0.99086622,0.83885419,0.61672286,0.21331501])
    #scoresArray.append([1.22312254,1.1468853,0.99976499,0.8637101,0.63580354,0.21428311])
    for fn in glob.glob('../multi-results/winSize-exp/*.txt'):
        print(fn)
        scoresArray.append(parseOutput(fn)['scoreArray'])    


    #scoresArray = np.array(scoresArray)
    labels=['Experimental vs. Experimental', 'Experimental vs. Reconstruction 1', 'Experimental vs. Reconstruction 2']
    styles = ['b-', 'g:', 'go:']
    fig, axs = plt.subplots(1,1, figsize=(6,4.5))
    linPlot(scoresArray, varRange=varRange, varName = 'Window Size (px)', titles=labels, plotStyles=styles, multiStyle='std', fig=fig, ax=axs)
    #for i in range(scoresArray.shape[0]):
    #    axs.plot(varRange, scoresArray[i, :], styles[i], linewidth=3, label=labels[i])
    axs.legend(loc='lower right')
    #axs.set_xlabel('Window Size (px)')
    axs.set_ylabel('Score')
    plt.savefig('ws-exp.png', dpi=600, bbox_inches='tight')

def landscape():
    import matplotlib.tri as tri
    scoreList = []
    scoreDir = '../opt-results/scores'
    fnList = os.listdir(scoreDir)
   
    for fn in fnList:
        scoreFile = open(scoreDir +'/'+fn, 'r')
        lines = scoreFile.readlines()
        stripFunc = lambda s: float(s.split()[1].strip(','))
        for i in range(int(len(lines)/3)):
            l1 = lines[3*i].strip().split(',')
            l2 = lines[3*i+1].strip().strip('Scores: ').split(',')
            l3 = lines[3*i+2].strip().split(',')
            #print(i)
            #print(l1)
            #print(l2)
            #print(l3)
            score = stripFunc(l2[3])
            scorex = stripFunc(l2[0])
            scorey = stripFunc(l2[1])
            scorez = stripFunc(l2[2])
            if i == 0 and len(l1) < 3:
                #self ref
                selfRefScore = score
            else:
                ba = stripFunc(l1[1])
                baca = stripFunc(l1[2])
                ca = ba/baca
                scoreList.append((ba, baca, score) )
    
        
    C = [] 
    xpts = [] 
    ypts = [] 
    yptsconv = []
    xmin=np.inf
    xmax=-np.inf
    ymin=np.inf
    ymax=-np.inf
    ngrid = 100

    for x,y,val in scoreList:
        
        C.append(val)
        xpts.append(x)
        ypts.append(y)
        yptsconv.append(x/y)
        if xmin > x: xmin=x
        if xmax < x: xmax=x
        if ymin > y: ymin=y
        if ymax < y: ymax=y
    #xgrid = np.linspace(xmin, xmax, ngrid)
    #ygrid = np.linspace(ymin, ymax, ngrid)
    C=np.array(C)
    xpts=np.array(xpts)
    ypts = np.array(ypts)
    yptsconv=np.array(yptsconv)


    fig,axs = plt.subplots(1,1)
    if not isinstance(axs, np.ndarray):
        axs = np.array([axs])
    im = axs[0].tricontourf(xpts, ypts, C, levels=30)
    axs[0].tricontour(xpts, ypts, C, levels=30, colors='k', linewidths=0.5,linestyles='dashed')
    axs[0].scatter([0.5],[0.25], color='white', marker='x')
    axs[0].annotate('Target\nB/A: 0.5\nC/A: 0.25\nSelf-Ref: {:.2f}'.format(selfRefScore), (0.5,0.25), xytext=(0.45, 0.55), color='white', horizontalalignment='right', verticalalignment='center', fontsize='small', arrowprops={'arrowstyle':'->', 'color':'white',})
    axs[0].set_xlabel('B/A')
    axs[0].set_ylabel('C/A')

    #im = axs[1].tricontourf(xpts, ypts, C, levels=30)
    #axs[1].tricontour(xpts, ypts, C, levels=30, colors='k', linewidths=0.5,linestyles='dashed')
    #axs[1].scatter([0.5],[2], color='white', marker='x')

    #axs[1].set_xlabel('B/A')
    #axs[1].set_ylabel('(B/A) / (C/A) - The value PDFO manipulates')
    #Get the Descent of some of them and plot them
    #dirList = os.listdir('../opt-results')
    dirList =[
            #'09-27-2021-5311658-si', 
            #'09-30-2021-5311911-si', 
            #'09-27-2021-5311657-si', 
            #'09-28-2021-5311706-si', 
            #'09-21-2021-5311416-si', 
            #'05-27-2021-5302144-si'
            '10-09-2021-5312454-si',
            ]

    #[
    #        '../opt-results/09-21-2021-5311416-si',
    #        '../opt-results/09-27-2021-5311657-si',
    #        '../opt-results/09-27-2021-5311658-si',
    #        '../opt-results/09-28-2021-5311706-si',
    #        '../opt-results/05-27-2021-5302144-si',
    #        ]
    #cutDir = cutSimplexWindows(simplexDir=simplexDir)
    for simplexDir in dirList:
        fnList = os.listdir('../opt-results/'+simplexDir+'/micro-png')
        run = simplexDir.split('/')[-1]
        run = run.rpartition('-')[0]
        selfRefScore = 0
        synthScores = []
        synthDataList = []

        scoreFile = open('../opt-results/'+simplexDir+'/scores.txt', 'r')
        lines = scoreFile.readlines()
        entries = []
        stripFunc = lambda s: float(s.split()[1].strip(','))
        for i in range(int(len(lines)/3)):
            l1 = lines[3*i].strip().split(',')
            l2 = lines[3*i+1].strip().strip('Scores: ').split(',')
            l3 = lines[3*i+2].strip().split(',')
            #print(i)
            #print(l1)
            #print(l2)
            #print(l3)
            score = stripFunc(l2[3])
            scorex = stripFunc(l2[0])
            scorey = stripFunc(l2[1])
            scorez = stripFunc(l2[2])
            if i == 0 and len(l1) < 3:
                #self ref
                selfRefScore = score
            else:
                ba = stripFunc(l1[1])
                baca = stripFunc(l1[2])
                ca = ba/baca
                synthDataList.append([None, score, None, None, ba, ca])
                if i ==1:
                    run = run + ' B/A: {:.2f}, C/A: {:.2f}'.format(ba, ca)
                
            

        #for fn in fnList:
        #    fnstat = os.stat(simplexDir + '/' + fn)
        #    fnParts = fn.replace('.png', '').split('-')
        #    if fn.startswith('ref-self-compare'):
        #        fnParts = fn.replace('.png', '').split('-')
        #        selfRefScore = float(fnParts[-1].replace('_', '.'))
        #    elif fn.startswith('synthetic'):
        #        mu =  float(fnParts[1].replace('_', '.'))
        #        std = float(fnParts[2].replace('_', '.'))
        #        ba =  float(fnParts[3].replace('_', '.'))
        #        ca =  float(fnParts[4].replace('_', '.'))
        #        score =  float(fnParts[-1].replace('_', '.'))
        #        date = fnstat.st_mtime
        #        synthDataList.append([date, score, mu, std, ba, ca])
        #synthDataList.sort(key=lambda x: x[0])

        xdes = []
        ydes = []
        ydesconv=[]
        scoreDes = []
        for date, score, mu, std, ba, ca in synthDataList:
            xdes.append(ba)
            ydes.append(ba/ca)
            ydesconv.append(ca)
            scoreDes.append(score)
            #print(mu, std, ba, ba/ca, ca, score)

        #print(xdes,ydes)
        axs[0].scatter(xdes,ydes, c=scoreDes, zorder=3)
        p = axs[0].plot(xdes,ydes, label=run, linewidth=2, zorder=2, color='white') 
        
        #for date, score, mu, std, ba, ca in synthDataList[0:8]:
        #    axs[0].annotate('({},{}):{}'.format(ba,ca, score), (ba, ca), fontsize='x-small', color='red', horizontalalignment='right')


        #axs[1].scatter(xdes,ydes, c=scoreDes, zorder=3) 
        #axs[1].plot(xdes,ydes, color=p[0].get_color(), linewidth=2, zorder=2) 
    
    xlim = np.linspace(xmin, xmax, 100) 
    ylim = np.linspace(ymin, ymax, 100) 
    ylimconv = xlim/ylim 
    
    yboundslo = np.zeros(len(xlim))+1 
    yboundsloconv = xlim/yboundslo

    yboundshi = np.zeros(len(xlim))+3 
    yboundshiconv = xlim/yboundshi
    #axs[0].axvline(0.25, linestyle='dashed', color='blue')
    #axs[1].axvline(0.25, linestyle = 'dashed', color='blue')
    #axs[0].plot(xlim, yboundslo, linestyle='dashed', color='red')
    #axs[1].plot(xlim, yboundslo, linestyle='dashed', color='red')
    #axs[0].plot(xlim, yboundshi, linestyle='dashed', color='red')
    #axs[1].plot(xlim, yboundshi, linestyle='dashed', color='red')

    #print(xlim)
    #print(ylim)
    #print(ylimconv)

    print(dirList)
    #axs[0].legend()
    fig.colorbar(im)
    plt.savefig('landscape.png', dpi=600)
    #plt.show()
    
if __name__ == '__main__':
    #fig1()
    #fig3()
    #fig3()
    #fig4()
    #fig5()
    #cutSimplexWindows()
    #cutSimplexWindows(simplexDir='../paper-figures/window-micro-40px', ylims=[(50,1100)], xlims=[(156,1206)],addlabels=['cut'], fnFilter='')
    #linErrWin()
    #linErrESD2()
    landscape()
