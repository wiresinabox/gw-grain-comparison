import os
import sys
import h5py
import marchingSquare as ms
import numpy as np
import scipy.io as sio
import random as r
import dream3dFunctions as d3f
import time
import sampleGenerator as sg
import multiprocessing as mpx
from matplotlib.image import imread
mp = mpx.get_context('spawn')
import queue
sys.path.append("/home/esuwandi/bin/python-libs")
import pdfo

try:
    import matplotlib.pyplot as plt
except:
    print(repr(e))
    print('Unable to load matplotlib module! Will continue.')
    pltOK = False

import gwFunctions as gwF
softGW = gwF.softGW
softW = gwF.softW
if os.path.isdir('/home/esuwandi/bin'):
    libDir = '/home/esuwandi/bin'
    libDirSolver = '/home/esuwandi/bin/solver.so'
else:
    libDir = '/usr/local/lib'
    libDirSolver = '../dotlib-master/solver.so'
try:
    gwDist = gwF.gw_gromov('libgwdist.so', libDir) 
    gwWass = gwF.gw_wass('libgwdist.so', libDir)
    gwOK = True
except Exception as e:
    #print(repr(e))
    #print('Unable to load GW module! Will continue to make the windows.')
    gwOK = False
#np.set_printoptions(threshold=sys.maxsize)

#memory limit

dataMan = None
jobQueue = None
completeQueue = None
dataListShared = None
mpUp=False
def startMP(processes=1):
    global dataMan
    global jobQueue
    global completeQueue
    global dataListShared
    global mpUp
    print('Starting MP Managers and Shared Objects')
    dataMan = mp.Manager()
    jobQueue = dataMan.Queue()
    completeQueue = dataMan.Queue()
    dataListShared = dataMan.list()
    mpUp=True

def dream3dSearch(d3File, searchKey, splitChar='/'):
    #Search of a h5py file until it finds 'key'
    treeDict = dict() #{key:{left:{}, right:{}, prev:root}}
    pathQueue = list(d3File.keys()) #"root/node1/node2"
    notFound = True
    while notFound:
        curPath = pathQueue.pop()
        if curPath.endswith(searchKey):
            return curPath
        curContainer = d3File
        for pathKey in curPath.split(splitChar): #Load currentContainer
            curContainer = curContainer[pathKey]

        for key in [key for key in curContainer.keys() if isinstance(curContainer[key], h5py.Group)]: 
            pathQueue.append('{}{}{}'.format(curPath,splitChar,key))
        notFound = len(pathQueue) > 0 #Keep running until exhausted
    #If not found, raise Error
    raise KeyError('Key {} not found in dream3d file'.format(key))

def getInitialStats(inFilePath, sliceNumX = 'half', sliceNumY = 'half', sliceNumZ = 'half', forceCellLength=False, dream3dContainer='StatsDataArray/1'):
    #dream3dContainerPath is formatted one/two/three/four
    try:
        f1d3 = h5py.File(inFilePath, 'r')
    except OSError:
        print("Unable to open file: {}".format(inFilePath))
        quit()

    #using the values from the pipeline instead of calculated 
    mu='unknown'
    std='unknown'
    dims='unknown'
    res='unknown'
    #altPipelineDict = d3f.getPipeline(inFilePath)
    #for key in altPipelineDict.keys():

    #    if 'StatsDataArray' in altPipelineDict[key].keys():
    #        mu = altPipelineDict[key]['StatsDataArray']['1']['FeatureSize Distribution']['Average']
    #        std = altPipelineDict[key]['StatsDataArray']['1']['FeatureSize Distribution']['Standard Deviation']

    #    if 'Dimensions' in altPipelineDict[key].keys():
    #        dims = altPipelineDict[key]['Dimensions']
    #    if 'Resolution' in altPipelineDict[key].keys():
    #        res = altPipelineDict[key]['Resolution']
    statsPath, statsContainer = d3f.dream3dSearch(inFilePath, dream3dContainer)
    d3Stats = {}
    for key in statsContainer.keys():
        val = statsContainer[key]
        if isinstance(val, h5py.Dataset):
            val = np.array(val).flatten()

            if key == 'Name':
                val = str(val)

            elif isinstance(val, np.ndarray) and val.size == 1:
                val = float(val[0])
            else:
                val = val.tolist()
            
            d3Stats[key] = val
                
        elif isinstance(val, h5py.Group):
            subDict = {}
            for subkey in val.keys():
                subval = val[subkey]
                subval = np.array(subval).flatten()
                if subval.size == 1:
                    subval = float(subval[0])
                else:
                    subval = subval.tolist() 
                    #print(key, subkey, subval, type(subval), type(subval[0]))
                subDict[subkey] = subval
            d3Stats[key] = subDict

                        
    targetSliceX, perCellLengthX = getSlice(inFilePath, 'x', sliceNumX) 
    targetSliceY, perCellLengthY = getSlice(inFilePath, 'y', sliceNumY) 
    targetSliceZ, perCellLengthZ = getSlice(inFilePath, 'z', sliceNumZ) 
    targetSliceF, perCellLengthF, cellIds = getSlice(inFilePath, 'z', sliceNumZ, returnFull=True)
    restupX = ms.sliceAnalysis(targetSliceX, perCellLengthX, forceCellLength)
    restupY = ms.sliceAnalysis(targetSliceY, perCellLengthY, forceCellLength)
    restupZ = ms.sliceAnalysis(targetSliceZ, perCellLengthZ, forceCellLength)
    restupF = ms.sliceAnalysis(cellIds, perCellLengthF, forceCellLength, volume=True)
    sliceStatsDict = {'x':restupX, 'y':restupY, 'z':restupZ, 'vol':restupF}
    binNums =d3Stats['BinNumber']
    print(binNums)
    binNums = [binNum for binNum in binNums if binNum > 0] #Prevents the log calcs from calling inf or nan
    if len(binNums) > 2:
        binStep = binNums[1] - binNums[0]
    else:
        maxCut = d3Stats['Feature_Diameter_Info'][1] #sigma
    mu = d3Stats['FeatureSize Distribution']['Average'] 
    std = d3Stats['FeatureSize Distribution']['Standard Deviation'] #sigma
    minCutx = d3Stats['Feature_Diameter_Info'][2] #sigma
    maxCutx = d3Stats['Feature_Diameter_Info'][1] #sigma
    minCut = (mu - np.log(minCutx))/std
    maxCut = (np.log(maxCutx)- mu)/std
    
    d3DerivedStats = {
        'mu':mu,
        'sigma':std,
        'minCut':minCut,
        'maxCut':maxCut,
        'binStep':binStep
    }


    return sliceStatsDict, d3Stats, d3DerivedStats 



def getImage(inFilePath, lumCoeffs=[1,1,1], gradSteps=256):
    img = imread(inFilePath)
    #Three cases: RGB, RGBA, Grayscale
    if len(img.shape) == 2: #Grayscale
        return img.astype(int)
    elif len(img.shape) ==3 and img.shape[2] == 3: #RGB
        return ((img[:,:,0]*lumCoeffs[0] + img[:,:,1]*lumCoeffs[1]+ img[:,:2]*lumCoeffs[2])*gradSteps).astype(int)
    elif len(img.shape) ==3 and img.shape[2] == 4: #RGBA
        return ((img[:,:,0]*lumCoeffs[0] + img[:,:,1]*lumCoeffs[1] + img[:,:2]*lumCoeffs[2])*img[:,:,3]*gradSteps).astype(int)
    else:
        raise ValueError('Image shape is unrecongized. Only takes Grayscale, RGB, and RGBA. Shape is {}'.format(img.shape()))

def getSlice(inFilePath, axis, sliceNum='half', dream3dContainer = None, returnFull = False, maxAttempts=5):
    #Where infilepath is the filepath of the dream3d microstructure
    #axis is either 0 for x, 1 for y, and 2 for z or 'x', 'y', 'z'
    #sliceNum is the slice index from 0 to n-1, where n is the length of the axis or a list or a numpy array or half or all.
    #dream3dContainer is a string that points to where FeatureIds is located.
    f1d3 = h5py.File(inFilePath, 'r')
    #How many voxels per axis
    #cellIdsDim = list(f1d3['DataContainers']['SyntheticVolumeDataContainer']['_SIMPL_GEOMETRY']['DIMENSIONS'])
    #Resolution, efffectively
    for attempt in range(maxAttempts):
        try:
            if 'SyntheticVolumeDataContainer' in f1d3['DataContainers'].keys():
                cellIdsRes = list(f1d3['DataContainers']['SyntheticVolumeDataContainer']['_SIMPL_GEOMETRY']['SPACING'])
                cellIds = np.array(list(f1d3['DataContainers']['SyntheticVolumeDataContainer']['CellData']['FeatureIds']))
            elif 'ImageDataContainer' in f1d3['DataContainers'].keys():
                cellIdsRes = [1,1,1] 
                cellIds = np.array(list(f1d3['DataContainers']['ImageDataContainer']['CellData']['FeatureIds']))
            elif 'Small IN100' in  f1d3['DataContainers'].keys():
                cellIdsRes = [1,1,1] 
                print(list(f1d3['DataContainers']['Small IN100']))
                cellIds = np.array(list(f1d3['DataContainers']['Small IN100']['EBSD Scan Data']['FeatureIds']))
            elif not isinstance(dream3dContainer, type(None)) and dream3dContainer in f1d3['DataContainers'].keys():
                cellIdsRes = [1,1,1] 
                cellIds = np.array(list(f1d3['DataContainers'][dream3dContainer]['CellData']['FeatureIds']))

            else:
                print('In Container:', list(f1d3['DataContainers']))
                raise Exception('Unknown Data in Dream3d File')
        except OSError:
            if attempt <= maxAttempts:
                waitTime = np.random.randint(1, 10)
                print('OSError, Failed to read. Attempt {}. Retrying in {}s'.format(attempt, waitTime))
                time.sleep(waitTime)
            else:
                raise #raises the last captured error.
        except:
            raise
        break
    
    #Axis
    if axis == 0 or axis.strip() == 'x':
        axisSlice = 0
    if axis == 1 or axis.strip() == 'y':
        axisSlice = 1
    if axis == 2 or axis.strip() == 'z':
        axisSlice = 2
    else:
        print('Unknown Axis: {}'.format(axis))

    #Slice or Slices
    if sliceNum=='half':
        sliceNum=int(cellIds.shape[axisSlice]/2)
    elif isinstance(sliceNum, str) and sliceNum == 'all':
        sliceNum = np.arange(cellIds.shape[0])
    
    #Get Slice
    if isinstance(sliceNum, int):
        if axis == 0 or axis == 'x':
            targetSliceIds = cellIds[sliceNum, :, :].squeeze()
        if axis == 1 or axis == 'y':
            targetSliceIds = cellIds[:, sliceNum, :].squeeze()
        if axis == 2 or axis == 'z':
            targetSliceIds = cellIds[:, :, sliceNum].squeeze()
    else:
        targetSliceIds = []
        for i in range(0, len(sliceNum)):
            if axis == 0 or axis == 'x':
                targetSliceIds.append(cellIds[sliceNum[i],:,:].squeeze())
            if axis == 1 or axis == 'y':
                targetSliceIds.append(cellIds[:,sliceNum[i],:].squeeze())
            if axis == 2 or axis == 'z':
                targetSliceIds.append(cellIds[:,:,sliceNum[i]].squeeze())

    if returnFull:
        return (targetSliceIds, cellIdsRes[0], cellIds)
    else:
        return (targetSliceIds, cellIdsRes[0])

def checkKwargs(kwargs, varName, default, verbose=True, dictToModify = None):
    if varName in kwargs:
        if verbose:
            print('{}: {}'.format(varName, kwargs[varName]))
        if isinstance(dictToModify, dict):
            dictToModify[varName] = kwargs[varName]
        return kwargs[varName]
    else:
        if verbose:
            print('{}: {} (default)'.format(varName, default))
        if isinstance(dictToModify, dict):
            dictToModify[varName] = default
        return default

def queueSDFPrecalc(jobQueue, completeQueue, dataList, timeout=5, verbose=True):
    #support function for precalcing slices
    while True:
        try:
            if jobQueue.empty():
                break
            else:
                index=jobQueue.get(block=True,timeout=5)
        except queue.Empty:
            continue
        if index == 'END':
            if verbose:
                print(mp.current_process().name, "ENDING")
            break
        else:
            sdfType = dataList[0]
            setInd = index[0] #1 or 2
            sliceInd = index[1] #the slice from there.
        
            if sdfType == 'overlap':
                sdfSlice=ms.overlapSDF(dataList[setInd][sliceInd], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1', verbose=verbose)
            elif sdfType == 'sweep':
                sdfSlice=ms.fastSweepSDF(dataList[setInd][sliceInd], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1', binaryFilter=dataList[setInd+2]) 
            if verbose:
                print('Finished {}-{}, {} at {}'.format(sliceInd, setInd, mp.current_process().name, time.asctime()))
            completeQueue.put((setInd, sliceInd, sdfSlice))
        
    return True

def generateWindowData (inFilePath1, inFilePath2,
        optionsDict1={},
        optionsDict2 = {},
        **kwargs
        ):
    #Where inFilePath points to either .dream3d files or .mat files
    #saveFilePath points to a .mat file
    #windowNum is the number of windows made
    #axis1, axis2 are either 'x', 'y', or 'z' depending on the slice. x refers to a y-z slice
    #slice is which slice in that axis do you want. If slice is a list or numpy array, windows will be taken randomly from them. If the slice
    #is 'and' then all slices will be considered.
    #relativeScale is a boolean. If true, it will resize the windows s.t. they both contain number of grain diameters. 
    #totalAreaHold and totalMassHold are either False or a number. If they are a number the microstructure will
    #be scaled s.t. both windows contain the same area, scaling both distances and masses, regardless of how big the
    #window is.
    #such that both windows are 3 grain sizes each.
    #noGrainInWindow scales the size of the window to be the specified number of grain diameters wide.
    #forceWindowLen, if set to an integer, sets the window to be a definate size.
    #microCrop takes either an integer or length two thing of integers and pre-cuts the microstructure if it's too large.
    #winSizeDetermine is only used if relativeScale is False. Max uses the 'max' ESD of the two, 'min' likewise, or 'first'
    #or 'second' to force one or the other.
    #distMultiplier is a straight multiplier to bring out distance differences. In SDF this increases the distance.
    #since it seems like the relatively small distance changes are makingi
    #it hell on the algorithem
    #centerOnRandGrain is a boolean. If True, then it will center the window
    #on a random grain, instead of choosing a random location.
    #distType is a space seperated string of 'standard inverse kagome'. Remove the ones you don't want.
    #for standard and inverse, ignoreBoundaryEdges ignores edges with the boundary, if not its the first entry
    #likewise, for kagome, ignoreBoundaryTripleKagome ignores triple points formed onthe boundary
    #sdfDistMin adds a constant to the 'masses' of each window since each instants needs to be a nonzero number for the wasserstein algorithem
    #sdfNormalize normalizes the masses s.t. each window is forced to have a total mass of 1. 
    #sdfReverse sets the boundaries as the maximum
    #sdfOnlyMass doesn't calculate C and l to save memory
    #sdfFloorNorm sets a universal floor to the max value in the windows.
    #sdfThresh takes the number of steps from lowest. E.g. sdfThresh = -2 means the highest and second highest. Negative
    #numbers take from highest. sdfThresh = 0 means take full sample`
    #sdfType is either 'overlap' or 'sweep'
    verbose            =checkKwargs(kwargs,'verbose'            ,  True)    
    verbose            =checkKwargs(optionsDict1,'verbose'      , verbose ,verbose=verbose)             
    verbose            =checkKwargs(optionsDict2,'verbose'            , verbose, verbose=verbose )             
    saveFilePath       =checkKwargs(kwargs,'saveFilePath'       ,  False, verbose=verbose) 
    windowNum          =checkKwargs(kwargs,'windowNum'          ,  20, verbose=verbose) 
    axis1              =checkKwargs(kwargs,'axis1'              ,  'x', verbose=verbose) 
    axis2              =checkKwargs(kwargs,'axis2'              ,  'x', verbose=verbose) 
    slice1             =checkKwargs(kwargs,'slice1'             ,  0, verbose=verbose) 
    slice2             =checkKwargs(kwargs,'slice2'             ,  0, verbose=verbose)
    binaryFilter1      =checkKwargs(kwargs,'binaryFilter1'             ,  None, verbose=verbose) 
    binaryFilter2      =checkKwargs(kwargs, 'binaryFilter2'             ,  None, verbose=verbose)
    forceWindowLen     =checkKwargs(kwargs,'forceWindowLen'     ,  20, verbose=verbose) 
    microCrop1         =checkKwargs(kwargs,'microCrop1'         ,  False, verbose=verbose) 
    microCrop2         =checkKwargs(kwargs,'microCrop2'         ,  False, verbose=verbose) 
    distMultiplier     =checkKwargs(kwargs,'distMultiplier'     ,  1, verbose=verbose)     
    centerOnRandGrain  =checkKwargs(kwargs,'centerOnRandGrain'  ,  False, verbose=verbose) 
    replaceInf         =checkKwargs(kwargs,'replaceInf'         ,  False, verbose=verbose) 
    ignoreBoundaryEdges=checkKwargs(kwargs,'ignoreBoundaryEdges',  True, verbose=verbose)
    sdfDistMin         =checkKwargs(kwargs,'sdfDistMin'         ,  1, verbose=verbose) 
    sdfNormalize       =checkKwargs(kwargs,'sdfNormalize'       ,  False, verbose=verbose)
    sdfReverse         =checkKwargs(kwargs,'sdfReverse'         ,  False, verbose=verbose)
    sdfReverseSlice         =checkKwargs(kwargs,'sdfReverseSlice'         ,  False, verbose=verbose)
    sdfOnlyMass        =checkKwargs(kwargs,'sdfOnlyMass'        ,  False, verbose=verbose)
    sdfRoot            =checkKwargs(kwargs,'sdfRoot'            ,  False, verbose=verbose)
    sdfFloorNorm       =checkKwargs(kwargs,'sdfFloorNorm'       ,  True, verbose=verbose)
    sdfThresh          =checkKwargs(kwargs,'sdfThresh'          ,  0, verbose=verbose)
    sdfType            =checkKwargs(kwargs,'sdfType'            ,  'overlap', verbose=verbose)
    forceCellLength    =checkKwargs(kwargs,'forceCellLength'    ,  False, verbose=verbose)
    dream3dContainer   =checkKwargs(kwargs,'dream3dContainer'   ,  False, verbose=verbose)
    multiproc   =checkKwargs(kwargs,'multiproc'   ,  False, verbose=verbose)
    maxTimeSec   =checkKwargs(kwargs,'maxTimeSec'   ,  36000, verbose=verbose)
    availProcs   =checkKwargs(kwargs,'availProcs'   ,  5, verbose=verbose)
    delayCallTime   =checkKwargs(kwargs,'delayCallTime'   ,  5, verbose=verbose)
    
    
    #relativeScale               =checkKwargs(kwargs,' relativeScale'             ,  True) #Depreciated, both windows same size.
    #totalAreaHold               =checkKwargs(kwargs,' totalAreaHold'             ,  False) #Depreciated, cell measurements don't matter.
    #noGrainInWindow             =checkKwargs(kwargs,' noGrainInWindow'           ,  3) #Depreciated, window size set in pixels
    #winSizeDetermine            =checkKwargs(kwargs,' winSizeDetermine'          ,  'max') #Depreciated, both windows same size
    #distType                    =checkKwargs(kwargs,' distType'                  ,  'inverse',) #Depreciated, only use SDF
    #ignoreBoundaryTripleKagome  =checkKwargs(kwargs,' ignoreBoundaryTripleKagome',  False,)     #Depreciated, only use Kagome
    #sdfPenaltyType              =checkKwargs(kwargs,' sdfPenaltyType'            ,  'constant',) #Depreciated, Penalty is not used anymore.

    #set options
    saveFilePath       =checkKwargs(optionsDict1,'saveFilePath'       , saveFilePath,verbose=verbose)     
    windowNum          =checkKwargs(optionsDict1,'windowNum'          , windowNum ,verbose=verbose)         
    axis1              =checkKwargs(optionsDict1,'axis'              , axis1,verbose=verbose)              
    axis1              =checkKwargs(optionsDict1,'axis1'              , axis1,verbose=verbose)              
    slice1             =checkKwargs(optionsDict1,'slice'             , slice1,verbose=verbose)             
    slice1             =checkKwargs(optionsDict1,'slice1'             , slice1,verbose=verbose)             
    microCrop1         =checkKwargs(optionsDict1,'microCrop'         , microCrop1,verbose=verbose)         
    microCrop1         =checkKwargs(optionsDict1,'microCrop1'         , microCrop1,verbose=verbose)   
    binaryFilter1         =checkKwargs(optionsDict1,'binaryFilter'      , binaryFilter1,verbose=verbose)   
    binaryFilter1         =checkKwargs(optionsDict1,'binaryFilter1'     , binaryFilter1,verbose=verbose)   
    forceWindowLen     =checkKwargs(optionsDict1,'forceWindowLen'     , forceWindowLen,verbose=verbose)     
    distMultiplier     =checkKwargs(optionsDict1,'distMultiplier'     , distMultiplier,verbose=verbose)     
    centerOnRandGrain  =checkKwargs(optionsDict1,'centerOnRandGrain'  , centerOnRandGrain ,verbose=verbose) 
    replaceInf         =checkKwargs(optionsDict1,'replaceInf'         , replaceInf ,verbose=verbose)        
    ignoreBoundaryEdges=checkKwargs(optionsDict1,'ignoreBoundaryEdges', ignoreBoundaryEdges,verbose=verbose)
    sdfDistMin         =checkKwargs(optionsDict1,'sdfDistMin'         , sdfDistMin ,verbose=verbose)        
    sdfNormalize       =checkKwargs(optionsDict1,'sdfNormalize'       , sdfNormalize ,verbose=verbose)      
    sdfReverse         =checkKwargs(optionsDict1,'sdfReverse'         , sdfReverse ,verbose=verbose)        
    sdfReverseSlice         =checkKwargs(optionsDict1,'sdfReverseSlice'         , sdfReverseSlice ,verbose=verbose)        
    sdfOnlyMass        =checkKwargs(optionsDict1,'sdfOnlyMass'        , sdfOnlyMass ,verbose=verbose)       
    sdfRoot            =checkKwargs(optionsDict1,'sdfRoot'            , sdfRoot,verbose=verbose)           
    sdfFloorNorm       =checkKwargs(optionsDict1,'sdfFloorNorm'       , sdfFloorNorm,verbose=verbose)     
    sdfThresh          =checkKwargs(optionsDict1,'sdfThresh'          , sdfThresh,verbose=verbose)          
    sdfType            =checkKwargs(optionsDict1,'sdfType'            , sdfType,verbose=verbose)           
    forceCellLength    =checkKwargs(optionsDict1,'forceCellLength'    , forceCellLength,verbose=verbose)
    dream3dContainer    =checkKwargs(optionsDict1,'dream3dContainer'  ,dream3dContainer,verbose=verbose)
    multiproc   =checkKwargs(optionsDict1,'multiproc'   ,  False, verbose=verbose)
    maxTimeSec   =checkKwargs(optionsDict1,'maxTimeSec'   ,  36000, verbose=verbose)
    availProcs   =checkKwargs(optionsDict1,'availProcs'   ,  5, verbose=verbose)
    delayCallTime   =checkKwargs(optionsDict1,'delayCallTime'   ,  5, verbose=verbose)
    
    saveFilePath       =checkKwargs(optionsDict2,'saveFilePath'       , saveFilePath,verbose=verbose)     
    windowNum          =checkKwargs(optionsDict2,'windowNum'          , windowNum ,verbose=verbose)         
    axis2              =checkKwargs(optionsDict2,'axis'              , axis2,verbose=verbose)              
    slice2             =checkKwargs(optionsDict2,'slice'             , slice2,verbose=verbose)             
    microCrop2         =checkKwargs(optionsDict2,'microCrop'         , microCrop2,verbose=verbose)         
    axis2              =checkKwargs(optionsDict2,'axis2'              , axis2,verbose=verbose)              
    slice2             =checkKwargs(optionsDict2,'slice2'             , slice2,verbose=verbose)             
    microCrop2         =checkKwargs(optionsDict2,'microCrop2'         , microCrop2,verbose=verbose)         
    binaryFilter2         =checkKwargs(optionsDict1,'binaryFilter'      , binaryFilter1,verbose=verbose)   
    binaryFilter2         =checkKwargs(optionsDict1,'binaryFilter2'     , binaryFilter1,verbose=verbose)   
    forceWindowLen     =checkKwargs(optionsDict2,'forceWindowLen'     , forceWindowLen,verbose=verbose)     
    distMultiplier     =checkKwargs(optionsDict2,'distMultiplier'     , distMultiplier,verbose=verbose)     
    centerOnRandGrain  =checkKwargs(optionsDict2,'centerOnRandGrain'  , centerOnRandGrain ,verbose=verbose) 
    replaceInf         =checkKwargs(optionsDict2,'replaceInf'         , replaceInf ,verbose=verbose)        
    ignoreBoundaryEdges=checkKwargs(optionsDict2,'ignoreBoundaryEdges', ignoreBoundaryEdges,verbose=verbose)
    sdfDistMin         =checkKwargs(optionsDict2,'sdfDistMin'         , sdfDistMin ,verbose=verbose)        
    sdfNormalize       =checkKwargs(optionsDict2,'sdfNormalize'       , sdfNormalize ,verbose=verbose)      
    sdfReverse         =checkKwargs(optionsDict2,'sdfReverse'         , sdfReverse ,verbose=verbose)        
    sdfReverseSlice         =checkKwargs(optionsDict2,'sdfReverseSlice'         , sdfReverseSlice ,verbose=verbose)        
    sdfOnlyMass        =checkKwargs(optionsDict2,'sdfOnlyMass'        , sdfOnlyMass ,verbose=verbose)       
    sdfRoot            =checkKwargs(optionsDict2,'sdfRoot'            , sdfRoot,verbose=verbose)           
    sdfFloorNorm       =checkKwargs(optionsDict2,'sdfFloorNorm'       , sdfFloorNorm,verbose=verbose)     
    sdfThresh          =checkKwargs(optionsDict2,'sdfThresh'          , sdfThresh,verbose=verbose)          
    sdfType            =checkKwargs(optionsDict2,'sdfType'            , sdfType,verbose=verbose)           
    forceCellLength    =checkKwargs(optionsDict2,'forceCellLength'    , forceCellLength,verbose=verbose) 
    dream3dContainer    =checkKwargs(optionsDict2,'dream3dContainer'  , dream3dContainer,verbose=verbose) 
    multiproc   =checkKwargs(optionsDict2,'multiproc'   ,  False, verbose=verbose)
    maxTimeSec   =checkKwargs(optionsDict2,'maxTimeSec'   ,  36000, verbose=verbose)
    availProcs   =checkKwargs(optionsDict2,'availProcs'   ,  5, verbose=verbose)
    delayCallTime   =checkKwargs(optionsDict2,'delayCallTime'   ,  5, verbose=verbose)

    if saveFilePath == False:
        saveWindowData = False
    else:
        saveWindowData = True
    if verbose:
        print('Target: ', inFilePath1)
        print('Compare:', inFilePath2)
    if verbose:
        print('Save: ', saveWindowData, 'AND', saveFilePath)
        #print('Relative Scale: {}, Equiv. Grain Diameter Window Size: {}, Distance Multipler = {}'.format(relativeScale, noGrainInWindow, distMultiplier))

    #=============Get Sample Slices================
    if isinstance(inFilePath1, str) and inFilePath1.endswith('dream3d'):
        try:
            f1d3 = h5py.File(inFilePath1, 'r')
        except OSError:
            print("Unable to open file: {}".format(inFilePath1))
            quit()
        targetSliceIds1, perCellLength1 = getSlice(inFilePath1, axis1, slice1, dream3dContainer=dream3dContainer) 
        if not isinstance(targetSliceIds1, list):
            targetSliceIds1 = [targetSliceIds1]
        xsize1, ysize1 = targetSliceIds1[0].shape
    elif isinstance(inFilePath1, str) and inFilePath1.endswith('mat'):
        try:
            dataDict = sio.loadmat(inFilePath1)
        except FileNotFoundError:
            print("Unable to open file: {}".format(inFilePath1))
            quit()
        targetSliceIds1 = dataDict['dataMat']
        xsize1, ysize1 = targetSliceIds1.shape
        perCellLength1 = 1/dataDict['density']
    elif isinstance(inFilePath1, str) and inFilePath1.endswith('png'):
        targetSliceIds1 = getImage(inFilePath1)
        perCellLengh1 = 1
        xsize1, ysize1 = targetSliceIds1.shape
    elif isinstance(inFilePath1, np.ndarray):
        targetSliceIds1 = [inFilePath1]
        perCellLength1 = 1
        xsize1, ysize1 = targetSliceIds1[0].shape
    else:
        print("Can't Open {}. Need either a .dream3d or .mat file".format(inFilePath1))
        quit()

    if isinstance(inFilePath2, str) and inFilePath2.endswith('dream3d'):
        try:
            f2d3 = h5py.File(inFilePath2, 'r')
        except OSError:
            print("Unable to open file: {}".format(inFilePath2))
            quit()
        targetSliceIds2, perCellLength2 = getSlice(inFilePath2, axis2, slice2, dream3dContainer=dream3dContainer) 
        if not isinstance(targetSliceIds2, list):
            targetSliceIds2 = [targetSliceIds2]
        xsize2, ysize2 = targetSliceIds2[0].shape
    elif isinstance(inFilePath2, str) and andinFilePath2.endswith('mat'):
        try:
            dataDict = sio.loadmat(inFilePath2)
        except FileNotFoundError:
            print("Unable to open file: {}".format(inFilePath2))
            quit()
        targetSliceIds2 = dataDict['dataMat']
        xsize2, ysize2 = targetSliceIds2.shape
        perCellLength2 = 1/dataDict['density']
    elif isinstance(inFilePath2, str) and inFilePath2.endswith('png'):
        targetSliceIds2 = getImage(inFilePath2)
        perCellLengh2 = 1
        xsize2, ysize2 = targetSliceIds2.shape
    elif isinstance(inFilePath2, np.ndarray):
        targetSliceIds2 = [inFilePath2]
        perCellLength2 = 1
        xsize2, ysize2 = targetSliceIds2[0].shape
    else:
        print("Can't Open {}. Need either a .dream3d or .mat file".format(inFilePath1))
        quit()

    if forceCellLength:
        perCellLength2 = 1
        perCellLength1 = 1
    elif not isinstance(forceCellLength, bool):
        perCellLength2 = forceCellLength
        perCellLength1 = forceCellLength
    else:
        pass
        
    perCellLength1 = perCellLength1*distMultiplier
    perCellLength2 = perCellLength2*distMultiplier

    #================ Pre-Cut the microstructures. If it's too large, assumes center. ============================
    print('{} | No. Slices: {}, Slice Shape: {}'.format(inFilePath1, len(targetSliceIds1), targetSliceIds1[0].shape))
    print('{} | No. Slices: {}, Slice Shape: {}'.format(inFilePath2, len(targetSliceIds2), targetSliceIds2[0].shape))
    microShape = targetSliceIds1[0].shape
    xcenter = int(microShape[0]/2) 
    ycenter = int(microShape[1]/2)
    for slind1 in range(len(targetSliceIds1)):
        if microCrop1 == False:
            break
        elif isinstance(microCrop1, int):
            highPointx1 = xcenter + int(microCrop1/2)
            lowPointx1 = xcenter - int(microCrop1/2)
            highPointy1 = ycenter + int(microCrop1/2)
            lowPointy1 = ycenter - int(microCrop1/2)
        elif isinstance(microCrop1, tuple) or isinstance(microCrop1, list) or isinstance(microCrop1, np.ndarray) and len(microCrop1) == 2:
            highPointx1 = xcenter + int(microCrop1[0]/2)
            lowPointx1 = xcenter - int(microCrop1[0]/2)
            highPointy1 = ycenter + int(microCrop1[1]/2)
            lowPointy1 = ycenter - int(microCrop1[1]/2)
        else:
            break
        if highPointx1 >= microShape[0]:
            highPointx1 == microShape[0]
        if lowPointx1 < 0:
            lowPointx1 == 0
        if highPointy1 >= microShape[1]:
            highPointy1 == microShape[1]
        if lowPointy1 < 0:
            lowPointy1 == 0
        targetSliceIds1[slind1] = targetSliceIds1[slind1][lowPointx1:highPointx1, lowPointy1:highPointy1]
        xsize1, ysize1 = targetSliceIds1[slind1].shape
    microShape = targetSliceIds2[0].shape
    xcenter = int(microShape[0]/2) 
    ycenter = int(microShape[1]/2)
    for slind2 in range(len(targetSliceIds2)):
        if microCrop2 == False:
            break
        elif isinstance(microCrop2, int):
            highPointx2 = xcenter + int(microCrop2/2)
            lowPointx2 = xcenter - int(microCrop2/2)
            highPointy2 = ycenter + int(microCrop2/2)
            lowPointy2 = ycenter - int(microCrop2/2)
        elif isinstance(microCrop1, tuple) or isinstance(microCrop2, list) or isinstance(microCrop2, np.ndarray) and len(microCrop2) == 2:
            highPointx2 = xcenter + int(microCrop2[0]/2)
            lowPointx2 = xcenter - int(microCrop2[0]/2)
            highPointy2 = ycenter + int(microCrop2[1]/2)
            lowPointy2 = ycenter - int(microCrop2[1]/2)
        else:
            break
        if highPointx2 >= microShape[0]:
            highPointx2 == microShape[0]
        if lowPointx2 < 0:
            lowPointx2 == 0
        if highPointy2 >= microShape[1]:
            highPointy2 == microShape[1]
        if lowPointy2 < 0:
            lowPointy2 == 0
        targetSliceIds2[slind2] = targetSliceIds2[slind2][lowPointx2:highPointx2, lowPointy2:highPointy2]
        xsize2, ysize2 = targetSliceIds2[slind2].shape
        

    #============== 3. Make the windows ===============

    uniTuple, pixTuple, minTuple = ms.windowAnalysis(targetSliceIds1[0], targetSliceIds2[0], perCellLength1, perCellLength2, relativeScale = False, winSizeDetermine = 'max', verbose = verbose, numSamples = windowNum, noGrainInWindow = 4)

    uniqueIds1 = np.unique(targetSliceIds1[0])
    uniqueIds2 = np.unique(targetSliceIds2[0])

    #All in units of pixels
    
    winlen1 = int(forceWindowLen)
    winlen2 = int(forceWindowLen)
    
    avgAreaUni1 = uniTuple[3]
    avgAreaUni2 = uniTuple[4]
    avgAreaPix1 = pixTuple[3]
    avgAreaPix2 = pixTuple[4]
    
    stdAreaUni1 = uniTuple[5]
    stdAreaUni2 = uniTuple[6]
    stdAreaPix1 = pixTuple[5]
    stdAreaPix2 = pixTuple[6]
    esdUni1 = uniTuple[7]
    esdUni2 = uniTuple[8]

    #sf is the lengthwise scaling factor for the compare grain. Boxes are scaled down to simulate loss of grain data
    #but then cellLength must be multiplied by sf to compensate (since it scales both box and grain data) 
    #get the random windows, cut them out. and salt the earth.
    
    
    cellArrayD1sdf = np.zeros(windowNum, np.object)
    cellArrayD2sdf = np.zeros(windowNum, np.object)
    cellArrayP1sdf = np.zeros(windowNum, np.object)
    cellArrayP2sdf = np.zeros(windowNum, np.object)
    uv1SDF= np.zeros(windowNum, np.object)
    uv2SDF= np.zeros(windowNum, np.object)
    CSDF  = np.zeros(windowNum, np.object)
    pena1SDF= np.zeros(windowNum, np.object)
    pena2SDF= np.zeros(windowNum, np.object)
    
    windowPosMat1 = np.zeros([windowNum,2]) 
    windowPosMat2 = np.zeros([windowNum,2])
    windowDataArray1 = np.zeros(windowNum, np.object)
    windowDataArray2 = np.zeros(windowNum, np.object)

    #precalculate slices and sdf slices
    sl1=np.random.randint(0, len(targetSliceIds1), windowNum)
    sl2=np.random.randint(0, len(targetSliceIds2), windowNum)
    sl1unique = np.unique(sl1)
    sl2unique = np.unique(sl2)
   
    maxWinVal = 0
    
    if multiproc:
        if mpUp==False:
            startMP(availProcs)
        global dataMan
        global jobQueue
        global completeQueue
        global dataListShared
        processes = []
    
    sdfSlices1 = {}
    sdfSlices2 = {}
    copySliceSet = False
    print('slices 1:', sl1unique) 
    print('slices 2:', sl2unique)
    if ignoreBoundaryEdges:
        for i in range(len(sl1unique)):
                slind1 = sl1unique[i]
                if multiproc:
                    jobQueue.put((1, slind1))
                else:
                    print('Precalcing {}-1. {} of {} slices'.format(slind1, i+1, len(sl1unique)))
                    if sdfType == 'overlap':
                        sdfSlices1[slind1]=ms.overlapSDF(targetSliceIds1[slind1], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1') #Still a bug in here with ignoreBoundaryEdges = True
                    elif sdfType == 'sweep':
                        sdfSlices1[slind1]=ms.fastSweepSDF(targetSliceIds1[slind1], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1', binaryFilter=binaryFilter1) 

    if np.any(inFilePath1 != inFilePath2) or np.any(optionsDict1 != optionsDict2) or len(sl1unique) > 1:

        if ignoreBoundaryEdges:
            for i in range(len(sl2unique)):
                    slind2 = sl2unique[i]
                    if multiproc:
                        jobQueue.put((2, slind2))
                    else:
                        print('Precalcing {}-2. {} of {} slices'.format(slind2, i+1, len(sl2unique))) 
                        if sdfType == 'overlap':
                            sdfSlices2[slind2]=ms.overlapSDF(targetSliceIds2[slind2], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1')
                        elif sdfType == 'sweep':
                            sdfSlices2[slind2]=ms.fastSweepSDF(targetSliceIds2[slind2], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1', binaryFilter=binaryFilter2) 
    else:
        copySliceSet = True
    
    if multiproc:
        for i in range(availProcs): jobQueue.put("END")
        if verbose:
            print("Spawning Processes")
        #clear shared datalist
        for i in range(len(dataListShared)):
            dataListShared.pop()
        dataListShared.extend([sdfType, targetSliceIds1, targetSliceIds2, binaryFilter1, binaryFilter2])
        for i in range(availProcs):
            p=mp.Process(target=queueSDFPrecalc, args=(jobQueue, completeQueue, dataListShared), kwargs={'verbose':verbose})
            processes.append(p)
            p.start()
            if verbose:
                print('{}) Spawning {}, PID: {}, Alive: {}'.format(i, p.name, p.pid, p.is_alive()))
        print('Joining')

        endTime=time.time() + maxTimeSec
        try:
            for p in processes:
                joinSecs = max(0.0, min(endTime-time.time(), maxTimeSec))
                p.join(maxTimeSec)
                if verbose:
                    print('Joining {}, PID: {}, Alive: {}'.format(p.name, p.pid, p.is_alive()))
        except KeyboardInterrupt:
            for p in processes:
                p.terminate()
        termNum=0
        failNum=0
        liveprocs = list(processes)
        callTime = 0
        while liveprocs:
            try:
                p=processes.pop()
                if p.is_alive():
                    p.terminate()
                    if verbose:
                        print('Terminating {}, PID: {}, Alive: {}'.format(p.name, p.pid, p.is_alive()))
                    termNum+=1
                else:
                    exitcode = p.exitcode
                    if exitcode:
                        failNum+=1
            except:
                if time.time() > callTime+delayCallTime:
                    print(time.asctime(), liveprocs)
                    callTime=time.time()
                pass
                
            liveprocs = [p for p in liveprocs if p.is_alive()]
        while True:
            try:
                jobQueue.get(block=False)
            except queue.Empty:
                break

        while True:
            try:
                setInd, sliceInd, sdfSlice = completeQueue.get(block=False)
                if setInd == 1:
                    sdfSlices1[sliceInd] = sdfSlice
                if setInd == 2:
                    sdfSlices2[sliceInd] = sdfSlice
            except queue.Empty:
                break


    if copySliceSet:
        print('Reusing First File')
        sdfSlices2=sdfSlices1
        sl2 = sl1
        sl2unique = sl1unique
        xsize2 = xsize1
        ysize2 = ysize1
    
    if ignoreBoundaryEdges and sdfReverseSlice:
        for sli in range(len(sdfSlice1)):
            sdfSlice1[sli] = np.abs(sdfSlice1[sli] - np.max(sdfSlice1[sli])) 
        for sli in range(len(sdfSlice2)):
            sdfSlice2[sli] = np.abs(sdfSlice2[sli] - np.max(sdfSlice2[sli])) 

    for i in range(0, windowNum):
        if verbose:
            print(i+1, 'of', windowNum)
        if centerOnRandGrain==True:
            centerId1 = uniqueIds1[r.randint(0, len(uniqueIds1)-1)]
            coordList1, boundCoords = ms.getPixelCoords(targetSliceIds1[0], centerId1)
            xCenter = abs(boundCoords[1] + boundCoords[0])/2
            yCenter = abs(boundCoords[3] + boundCoords[2])/2
            lowPointx1 = xCenter - int(winlen1/2)
            lowPointy1 = yCenter - int(winlen2/2)
            highPointx1 = xCenter + int(winlen1/2) 
            highPointy1 = yCenter + int(winlen2/2)
            print(centerId1, boundCoords, xCenter, yCenter)
            
            centerId2 = uniqueIds2[r.randint(0, len(uniqueIds2)-1)]
            coordList2, boundCoords = ms.getPixelCoords(targetSliceIds2, centerId2)
            xCenter = abs(boundCoords[1] + boundCoords[0])/2
            yCenter = abs(boundCoords[3] + boundCoords[2])/2
            lowPointx2 = xCenter - int(winlen2/2)
            lowPointy2 = yCenter - int(winlen2/2)
            highPointx2 = xCenter + int(winlen2/2) 
            highPointy2 = yCenter + int(winlen2/2)

             
        elif centerOnRandGrain == 'center':
            xcenter1 = int(xsize1 / 2)
            ycenter1 = int(ysize1 / 2)
            lowPointx1 = xcenter1 - (winlen1/2)
            lowPointy1 = ycenter1 - (winlen1/2)
            highPointx1 = int(lowPointx1 + winlen1)
            highPointy1 = int(lowPointy1 + winlen1)
            
            xcenter2 = int(xsize2 / 2)
            ycenter2 = int(ysize2 / 2)
            lowPointx2 = xcenter2 - (winlen2/2)
            lowPointy2 = ycenter2 - (winlen2/2)
            highPointx2 = int(lowPointx2 + winlen2)
            highPointy2 = int(lowPointy2 + winlen2)
            
        else:
            lowPointx1 = r.randint(0, xsize1 - 1 - winlen1)
            lowPointy1 = r.randint(0, ysize1 - 1 - winlen1)
            highPointx1 = int(lowPointx1 + winlen1)
            highPointy1 = int(lowPointy1 + winlen1)
            
            lowPointx2 = r.randint(0, xsize2 - 1 - winlen2)
            lowPointy2 = r.randint(0, ysize2 - 1 - winlen2)
            highPointx2 = int(lowPointx2 + winlen2)
            highPointy2 = int(lowPointy2 + winlen2)
        if lowPointx1 < 0:
            highPointx1 = highPointx1 + abs(lowPointx1)
            lowPointx1 = 0
        elif lowPointx1 <= 0 and highPointx1 > xsize1 -1:
            lowPointx1 = 0
            highPointx1 = xsize1-1
        elif highPointx1 > xsize1-1:
            lowPointx1 = lowPointx1 - abs(highPointx1-xsize1-1)
            highPointx1 = xsize1-1

        if lowPointy1 < 0:
            highPointy1 = highPointy1 + abs(lowPointy1)
            lowPointy1 = 0
        elif lowPointy1 <= 0 and highPointy1 > ysize1 -1:
            lowPointy1 = 0
            highPointy1 = ysize1-1
        elif highPointy1 > ysize1-1:
            lowPointy1 = lowPointy1 - abs(highPointy1-ysize1-1)
            highPointy1 = xsize1-1

        if lowPointx2 < 0:
            highPointx2 = highPointx2 + abs(lowPointx2)
            lowPointx2 = 0
        elif lowPointx2 <= 0 and highPointx2 > xsize2 -1:
            lowPointx2 = 0
            highPointx2 = xsize1-1
        elif highPointx2 > xsize2-1:
            lowPointx2 = lowPointx2 - abs(highPointx2-xsize2-1)
            highPointx2 = xsize2-1

        if lowPointy2 < 0:
            highPointy2 = highPointy2 + abs(lowPointy2)
            lowPointy2 = 0
        elif lowPointy2 <= 0 and highPointy2 > ysize2 -1:
            lowPointy2 = 0
            highPointy2 = ysize1-1
        elif highPointy2 > ysize2-1:
            lowPointy2 = lowPointy2 - abs(highPointy2-ysize2-1)
            highPointy2 = xsize2-1
        
        highPointx1 = int(highPointx1)
        lowPointx1 = int(lowPointx1)
        highPointy1 = int(highPointy1)
        lowPointy1 = int(lowPointy1)
        highPointx2 = int(highPointx2)
        lowPointx2 = int(lowPointx2)
        highPointy2 = int(highPointy2)
        lowPointy2 = int(lowPointy2)
        if verbose:
            print(lowPointx1,'-', highPointx1, ':', lowPointy1,'-',highPointy1)
            print(lowPointx2,'-', highPointx2, ':', lowPointy2,'-',highPointy2)
    
        windowPosMat1[i, :] = np.array([(lowPointx1 + highPointx1)/2, (lowPointy1 + highPointy1)/2])
        windowPosMat2[i, :] = np.array([(lowPointx2 + highPointx2)/2, (lowPointy2 + highPointy2)/2])
        windowDataArray1[i] = targetSliceIds1[sl1[i]][lowPointx1:highPointx1,lowPointy1:highPointy1]
        windowDataArray2[i]= targetSliceIds2[sl2[i]][lowPointx2:highPointx2,lowPointy2:highPointy2]

        #For skipping purposes, if windows are the exact same.
        sameCoords = highPointx1 == highPointx2 and lowPointx1 == lowPointx2 and highPointy1 == highPointy2 and lowPointy1 == lowPointy2

        #====SIGNED DISTANCE FUNCTION (SDF)=====    

        #SDF Flow:
        #Precalculate Slice if ignoreBoundaryEdges.
        #U1, U2 = overlap or fastsweep SDF or cutout.
        #sdfRoot
        #sdfThresh
        #sdfPenalties (for unbalanced, depreciated)
        #sdfNormalize (note. Calling the run function autonormalizes it, but this is for testing normalization for graphing and such.)

        #U1 = ms.fastSweepSDF(targetSliceIds1[sl1[i]][lowPointx1:highPointx1,lowPointy1:highPointy1], 'all', 1, ignoreBoundaryEdges=ignoreBoundaryEdges, uFunc = 'L1')
        #U2 = ms.fastSweepSDF(targetSliceIds2[sl2[i]][lowPointx2:highPointx2,lowPointy2:highPointy2], 'all', 1, ignoreBoundaryEdges=ignoreBoundaryEdges, uFunc = 'L1')
        
        if ignoreBoundaryEdges: #From precalculated slices
            #print('sl1', sl1) 
            #print('sl2', sl2) 
            #print('sdfSlices: ', len(sdfSlices1), len(sdfSlices2))
            U1 = sdfSlices1[sl1[i]][lowPointx1:highPointx1,lowPointy1:highPointy1]
            U2 = sdfSlices2[sl2[i]][lowPointx2:highPointx2,lowPointy2:highPointy2]
        else: #Calculate the windows
            if sdfType=='overlap':
                U1 = ms.overlapSDF(targetSliceIds1[sl1[i]][lowPointx1:highPointx1,lowPointy1:highPointy1], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1')
                if np.any(inFilePath1 != inFilePath2) or np.any(optionsDict1 != optionsDict2) or not sameCoords:
                    U2 = ms.overlapSDF(targetSliceIds2[sl2[i]][lowPointx2:highPointx2,lowPointy2:highPointy2], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1')
                else:
                    print('Reusing U1, same sample and same window')
                    U2 = U1
            elif sdfType == 'sweep':
                U1 = ms.fastSweepSDF(targetSliceIds1[sl1[i]][lowPointx1:highPointx1,lowPointy1:highPointy1], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1', binaryFilter=binaryFilter1)
                if np.any(inFilePath1 != inFilePath2) or np.any(optionsDict1 != optionsDict2) or not sameCoords:
                    U2 = ms.fastSweepSDF(targetSliceIds2[sl2[i]][lowPointx2:highPointx2,lowPointy2:highPointy2], 'all', 1, ignoreBoundaryEdges=False, uFunc = 'L1', binaryFilter=binaryFilter2)
                else:
                    print('Reusing U1, same sample and same window')
                    U2 = U1
        #print('1:',lowPointx1, highPointx1, lowPointy1, highPointy1)
        #print('2:',lowPointx2, highPointx2, lowPointy2, highPointy2)
        #print('U1 shape:', U1.shape, sdfSlices1[sl1[i]].shape, 'U2 shape:', U2.shape, sdfSlices2[sl2[i]].shape, sdfSlices1[sl1[i]][lowPointx1:highPointx1,lowPointy1:highPointy1].shape, sdfSlices2[sl2[i]][lowPointx2:highPointx2,lowPointy2:highPointy2].shape)

        if sdfRoot:
            U1 = np.sqrt(U1)
            U2 = np.sqrt(U2)
        if sdfReverse:
            U1 = np.abs(U1 - np.max(U1))
            U2 = np.abs(U2 - np.max(U2))
        
        uniqueU1 = np.unique(U1)
        uniqueU2 = np.unique(U2) #sorted low to high.
        
        if sdfThresh < 0:
            threshU1 = np.nonzero(np.logical_not(np.isin(U1, uniqueU1[sdfThresh:])))
            threshU2 = np.nonzero(np.logical_not(np.isin(U2, uniqueU2[sdfThresh:])))
            U1[threshU1] = 0
            U2[threshU2] = 0
        elif sdfThresh > 0:
            threshU1 = np.nonzero(np.logical_not(np.isin(U1, uniqueU1[:sdfThresh-1])))
            threshU2 = np.nonzero(np.logical_not(np.isin(U2, uniqueU2[:sdfThresh-1])))
            U1[threshU1] = 0
            U2[threshU2] = 0


        cellArrayD1sdf[i] = U1
        cellArrayD2sdf[i] = U2
        n1, n2 = U1.shape #assumes U1 and U2 are the same size.
        uv1 = (np.reshape(U1, n1*n2)*distMultiplier)+sdfDistMin #distMultiplier should be ~10
        uv2 = (np.reshape(U2, n1*n2)*distMultiplier)+sdfDistMin
        if np.sum(uv1) > maxWinVal:
            maxWinVal = np.sum(uv1)
        if np.sum(uv2) > maxWinVal:
            maxWinVal = np.sum(uv2)
        #umax = max(max(uv1), max(uv2))
        #uv1 = uv1 / np.max(uv1) #for some reason, if you normalize by the max of each, it matters which is being placed first
        #uv2 = uv2 / np.max(uv2)
        if sdfNormalize:
            uv1 = uv1/np.sum(uv1)
            uv2 = uv2/np.sum(uv2)
        if sdfOnlyMass:
            C = np.zeros(1)
        else:
            C = np.zeros((n1*n2, n1*n2))
        if sdfOnlyMass:
            l1 = np.ones(1)
            l2 = np.ones(1)
        elif sdfPenaltyType == 'constant':
            l1 = np.ones(n1*n2)
            l2 = np.ones(n1*n2)
        elif sdfPenaltyType == 'constmax':
            l1 = np.ones(n1*n2)*np.max(U1)
            l2 = np.ones(n1*n2)*np.max(U2)
        elif sdfPenaltyType == 'constesd':
            l1 = np.ones(n1*n2)*esdUni1
            l2 = np.ones(n1*n2)*esdUni2
        elif sdfPenaltyType == 'overlay':
            l1 = uv1
            l2 = uv2
        elif sdfPenaltyType == 'progressive':
            l1 = np.zeros(n1*n2)
            l2 = l1
        elif sdfPenaltyType == '4xWinSize':
            l1 = np.ones(n1*n2)*4*np.max([n1, n2])
            l2 = np.ones(n1*n2)*4*np.max([n1, n2])

        for b in range(0, n2):
            for a in range(0, n1):
                vx = np.abs(np.arange(0, n1) -a)
                vy = np.abs(np.arange(0, n2)-b)
                xx, yy = np.meshgrid(vx, vy)
                if not sdfOnlyMass:
                    C[b*n2+ a, :] = np.reshape(xx+yy, (n1*n2))
                    if sdfPenaltyType == 'progressive':
                        l1[b*n2 + a] = min(min(a+1, n1-a), min(b+1, n2-b)) #make this a function of the ESD
                        l2[b*n2 + a] = min(min(a+1, n1-a), min(b+1, n2-b)) #make this a function of the ESD
        uv1SDF[i] = uv1
        uv2SDF[i] = uv2
        CSDF[i] = C
        pena1SDF[i] = l1
        pena2SDF[i] = l2
    if sdfFloorNorm:
        for i in range(windowNum):
            cellArrayD1sdf[i]=cellArrayD1sdf[i] + int((maxWinVal)/(n1*n2)) - sdfDistMin
            uv1SDF[i]=uv1SDF[i] + int(maxWinVal/(n1*n2)) 
            cellArrayD2sdf[i]=cellArrayD2sdf[i] + int((maxWinVal)/(n1*n2)) - sdfDistMin
            uv2SDF[i]=uv2SDF[i] + int(maxWinVal/(n1*n2))
    #'grainData1': targetSliceIds1, #Full Slice 
    #'grainData2': targetSliceIds2, 
    saveDict = {'slice1': slice1, # Which Slice
            'slice2': slice2, 
            'avgGrainAreaUni1': avgAreaUni1, #units
            'avgGrainAreaUni2': avgAreaUni2, #units
            'avgGrainAreaPix1': avgAreaPix1, #units
            'avgGrainAreaPix2': avgAreaPix2, #units
            'stdGrainAreaUni1': stdAreaUni1, #units
            'stdGrainAreaUni2': stdAreaUni2, #units
            'stdGrainAreaPix1': stdAreaPix1, #units
            'stdGrainAreaPix2': stdAreaPix2, #units
            'esdUni1':esdUni1,
            'esdUni2':esdUni2,
            'windowLength1':winlen1,
            'windowLength2':winlen2,
            'windowPos1': windowPosMat1, #Corners of each window
            'windowPos2': windowPosMat2, 
            'windowDataArray1':windowDataArray1, #The data of each cutout
            'windowDataArray2':windowDataArray2}
    winDataDict = {
            'distanceCellArraySDF1': cellArrayD1sdf, #U1, 
            'distanceCellArraySDF2': cellArrayD2sdf,  #U2
            'uv1SDF': uv1SDF,
            'uv2SDF': uv2SDF,
            'CostMatSDF':  CSDF,
            'penaEdge1SDF': pena1SDF,
            'penaEdge2SDF': pena2SDF
            }
    saveDict.update(winDataDict)
    if saveWindowData:
        if verbose:
            print('Saving to:', saveFilePath)
        ms.saveMatFile(saveFilePath, saveDict)

    return saveDict
#Doxygen, ATLAS, LAPACK

def queueWindowSDFWass(jobQueue, completeQueue, dataList, timeout=5, verbose=True, roundMult=1e6, addNorm=False):
        
    #SUPPORT FUNCTION FOR RUN WINDOWS SDF WASS

    while True:
        try:
            if jobQueue.empty():
                break
            else:
                coords = jobQueue.get(block=True, timeout=5)#wait at most 5 to recieve an item before terminating itself.
        except queue.Empty:
            print(mp.current_process().name, "Queue Empty")
            continue
        if coords == 'END':
            print(mp.current_process().name, "ENDING")
            break
        else:
            uv1s = dataList[0]
            uv2s = dataList[1]
            l1s = dataList[2]
            l2s = dataList[3]
            C = dataList[4]
            try:
                lmult=dataList[5]
            except:
                lmult = 1
            try:
                usePenaltyFinal=dataList[6]
            except:
                usePenaltyFinal=False
            try:
                endlmult=dataList[7]
            except:
                endlmult=1
            try:
                hardW=dataList[8]
            except:
                hardW=False

            i,j = coords
            uv1 = uv1s[i]
            uv2 = uv2s[j]
            l1 = l1s[i]
            l2 = l2s[j]
            if verbose:
                print('Starting {}, {} {}'.format(i, j, mp.current_process().name))
            if isinstance(hardW, bool) and hardW:
                dist, gamma = gwWass(C, uv1, uv2) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                Csum = dist
                Psum1 = 0
                Psum2 = 0

            elif isinstance(hardW, str) and hardW == 'massdiff':
                dist = np.abs(np.sum(uv1) - np.sum(uv2)) 
                gamma = np.zeros((5,5))
                Csum = 0 
                Psum1 = dist
                Psum2 = 0
                print('Mass Diff:', dist)
            elif isinstance(hardW, str) and hardW == '2dhist':
                dist= gwF.solver2d(uv1, uv2, libPath=libDirSolver, idStr=mp.current_process().name, roundMult = roundMult, addNorm=addNorm) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
               
                gamma = np.zeros((5,5))
                Csum = dist
                Psum1 = 0
                Psum2 = 0
            else:
                dist, gamma, Csum, Psum1, Psum2 = softW(C, uv1, uv2, l1, l2, lmult = lmult, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult)
            if verbose:
                print('Finished {}, {} : {}, {} at {}'.format(i, j, dist, mp.current_process().name, time.asctime()))
            returnTuple = (coords, dist, Csum, Psum1, Psum2)
            completeQueue.put(returnTuple)

    return True
def runWindowsSDFWass (sdfCells1, sdfCells2, Ccells, sdfPenalties1 = None, sdfPenalties2 = None, Cscale = 1, lmult = 1, usePenaltyFinal=False, hardW='2dhist', endlmult=1, startWindows = 10, stepWindows = 10, verbose = True, gwDeltaThresh = 0.5, availProcs = 5, multiproc = False, maxTimeSec=36000, wMatMax=1e6, delayCallTime=5, idStr=0, forceWinSym=False, scoreMult=1, roundMult = 1e6, addNorm=False):

    #sdfCells1 and sdfCells2 are numpy object vectors containing the flattened SDF matrix (uv1 and uv2)
    #multicore is a bool for multiprocessing
    #forceWinSym forces the window-score matrix to be symmetrical, cutting time in half.
    try:
        ncells1 = sdfCells1.size
        ncells2 = sdfCells2.size
    except:
        print('cell data is missing')
        return

    if isinstance(sdfPenalties1, type(None)):
        sdfPenalties1 = np.zeros((sdfCells1[0].shape)) + 1 
    if isinstance(sdfPenalties2, type(None)):
        sdfPenalties2 = np.zeros((sdfCells2[0].shape)) + 1 

    wMat = np.zeros([ncells1, ncells2])+wMatMax
    CMat = np.zeros([ncells1, ncells2])+wMatMax #Geometric factors
    PMat = np.zeros([ncells1, ncells2]) #Penalty or Mass factors
    #First, create the amount of runs
    runEndNums1 = list(range(startWindows, ncells1, stepWindows))
    runEndNums1.append(ncells1)

    runEndNums2 = list(range(startWindows, ncells2, stepWindows))
    runEndNums2.append(ncells2)

    maxRuns = max(len(runEndNums1), len(runEndNums2))
    prevEndNum1 = 0
    prevEndNum2 = 0

    prev_gwScore = 100000 #Arbitrarily big.
    deltas = []
    scores = []
    
    C = Ccells[0]*Cscale
    print(ncells1, ncells2)
    if hardW=='2dhist':
        sdfCells1, sdfCells2 = gwF.solver2dPrewrite(sdfCells1, sdfCells2, idStr=idStr, roundMult = roundMult, addNorm = addNorm)


    if multiproc:
        if mpUp==False:
            startMP(availProcs)
        global dataMan
        global jobQueue
        global completeQueue
        global dataListShared
        processes = []
        #set up all jobs
        if verbose:
            print('Setting Up Jobs')
        for i in range(0, ncells1):
            if forceWinSym:
                for j in range(i, ncells2):
                    jobQueue.put((i,j))
            else:
                for j in range(0,ncells2):
                    jobQueue.put((i,j))
        for i in range(availProcs):
            jobQueue.put("END")
        #set up processes
        if verbose:
            print('Spawning Processes')
            print('This part may take a while for large nxn runs. Stay calm if nothing appears on slurm cheers.')

        for i in range(len(dataListShared)):
            dataListShared.pop() #make sure that the list is clear
        dataListShared.extend([sdfCells1, sdfCells2, sdfPenalties1, sdfPenalties2, C, lmult, usePenaltyFinal, endlmult, hardW])
        for i in range(availProcs):
            p= mp.Process(target=queueWindowSDFWass, args=(jobQueue, completeQueue, dataListShared), kwargs={'verbose':verbose, 'addNorm':addNorm, 'roundMult':roundMult}) 
            processes.append(p)
            p.start()
            if verbose:
                print('{}) Spawning {}, PID: {}, Alive: {}'.format(i, p.name, p.pid, p.is_alive()))
        #Doing stuff here.

        print('Joining')
        
        endTime = time.time() + maxTimeSec
        try:
            for p in processes:
                joinSecs=max(0.0, min(endTime-time.time(), maxTimeSec))
                p.join(maxTimeSec)
                if verbose:
                    print('Joining {}, PID: {}, Alive: {}'.format(p.name, p.pid, p.is_alive()))
        except KeyboardInterrupt:
            for p in processes:
                p.terminate()
        #terminate the hanging ones:
        termNum=0
        failNum=0
        liveprocs = list(processes)
        callTime = 0
        while liveprocs:
            try:
                p=processes.pop()
                if p.is_alive():
                    p.terminate()
                    if verbose:
                        print('Terminating {}, PID: {}, Alive: {}'.format(p.name, p.pid, p.is_alive()))
                    termNum+=1
                else:
                    exitcode = p.exitcode
                    if exitcode:
                        failNum+=1
            except:
                if time.time() > callTime+delayCallTime:
                    print(time.asctime(), liveprocs)
                    callTime=time.time()
                pass
                
            liveprocs = [p for p in liveprocs if p.is_alive()]
        
        while True:
            try:
                jobQueue.get(block=False)
            except queue.Empty:
                break

        while True:
            try:
                resTuple = completeQueue.get(block=False)
                i,j = resTuple[0]
                dist = resTuple[1]
                Csum = resTuple[2]
                Psum1 = resTuple[3]
                Psum2 = resTuple[4]
                wMat[i,j] = dist
                CMat[i,j] = Csum
                PMat[i,j] = Psum1 + Psum2
                if forceWinSym:
                    wMat[j,i] = dist
                    CMat[j,i] = Csum
                    PMat[j,i] = Psum1 + Psum2
            except queue.Empty:
                break

        #for p in processes:
        #    p.terminate()
        #    if verbose:
        #        print('Terminating {}, PID: {}, Alive: {}'.format(p.name, p.pid, p.is_alive()))
        

        #while not completeQueue.empty():

        G = np.zeros(ncells1 * ncells2, order = 'F')
        gwScore, gwGamma = gwWass(wMat, ncells1, ncells2) #gwWass(wMat, ncells1, ncells2, G)
        #cScore, cGamma = gwWass(CMat, ncells1, ncells2)#gwWass(CMat, ncells1, ncells2, G)
        #pScore, pGamma =gwWass(PMat, ncells1, ncells2) #gwWass(PMat, ncells1, ncells2, G)
        
        #np.set_printoptions(threshold=sys.maxsize, linewidth=np.inf, precision=4)
        #print(wMat)
        #np.set_printoptions(threshold=1000)
            
        #gwScore, gwGamma = gwF.hardW(wMat, ncells1, ncells2, G)
        #cScore, cGamma = gwF.hardW(CMat,   ncells1, ncells2, G)
        #pScore, pGamma = gwF.hardW(PMat,   ncells1, ncells2, G)
        cScore = gwScore
        cGamma = gwGamma
        pScore = 0
        pGamma = np.zeros((1,1))

        if isinstance(hardW, str) and hardW=='2dhist':
            gwF.solver2dCleanup(sdfCells1, sdfCells2)
           
        gwScore = gwScore*scoreMult
        return(gwScore, gwGamma, wMat, CMat, PMat, cScore, pScore)
    
    else:

        for runNum in range(maxRuns):
            #Block 1, All x from prev1endNum to runEndNum1[runNum] but only from 0 to the previous range of ys
            if runNum <= len(runEndNums1)-1:
                curEndNum1 = runEndNums1[runNum]
            if runNum <= len(runEndNums2)-1:
                curEndNum2 = runEndNums2[runNum]
            if verbose:
                print('x endings:', runEndNums1)
                print('y endings:', runEndNums2)
                print('Running Block {} to {} by {} to {}.'.format(prevEndNum1, curEndNum1, prevEndNum2, curEndNum2))
            for i in range(prevEndNum1, curEndNum1):
                for j in range(0, prevEndNum2):
                    uv1 = sdfCells1[i]
                    uv2 = sdfCells2[j]
                    if isinstance(sdfPenalties1, np.ndarray):
                        l1 = sdfPenalties1[i]
                    else:
                        l1 = sdfPenalties1
                    if isinstance(sdfPenalties2, np.ndarray):
                        l2 = sdfPenalties2[j]
                    else:
                        l2 = sdfPenalties2
                    if isinstance(hardW, bool) and hardW:
                        dist, gamma = gwWass(C, uv1, uv2) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                        Csum = dist
                        Psum1 = 0
                        Psum2 = 0
                    elif isinstance(hardW, str) and hardW == 'massdiff':
                        dist = np.abs(np.sum(uv1) - np.sum(uv2)) 
                        gamma = np.zeros((5,5))
                        Csum = 0 
                        Psum1 = dist
                        Psum2 = 0
                        print('Mass Diff:', dist)
                    elif isinstance(hardW, str) and hardW == '2dhist':
                        dist = gwF.solver2d(uv1, uv2, libPath=libDirSolver, idStr= idStr, roundMult = roundMult, addNorm = addNorm) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                        gamma = np.zeros((5,5))
                        Csum = dist
                        Psum1 = 0
                        Psum2 = 0
                    else:
                        dist, gamma, Csum, Psum1, Psum2= softW(C, uv1, uv2, l1, l2, lmult = lmult, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult) 
                    if verbose:
                        print('{}, {} : {}'.format(i, j, dist))
                    wMat[i, j] = dist
                    CMat[i,j] = Csum
                    PMat[i,j] = Psum1+Psum2
                    if forceWinSym:
                        wMat[j,i] = dist
                        CMat[j,i] = Csum
                        PMat[j,i] = Psum1+Psum2

            if not forceWinSym:
                for j in range(prevEndNum2, curEndNum2):
                    for i in range(0, prevEndNum1):
                        uv1 = sdfCells1[i]
                        uv2 = sdfCells2[j]
                        if isinstance(sdfPenalties1, np.ndarray):
                            l1 = sdfPenalties1[i]
                        else:
                            l1 = sdfPenalties1
                        if isinstance(sdfPenalties2, np.ndarray):
                            l2 = sdfPenalties2[j]
                        else:
                            l2 = sdfPenalties2
                        if isinstance(hardW, bool) and hardW:
                            dist, gamma = gwWass(C, uv1, uv2) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                            Csum = dist
                            Psum1 = 0
                            Psum2 = 0
                        elif isinstance(hardW, str) and hardW == 'massdiff':
                            dist = np.abs(np.sum(uv1) - np.sum(uv2)) 
                            gamma = np.zeros((5,5))
                            Csum = 0 
                            Psum1 = dist
                            Psum2 = 0
                            print('Mass Diff:', dist)
                        elif isinstance(hardW, str) and hardW == '2dhist':
                            dist= gwF.solver2d(uv1, uv2, libPath=libDirSolver, idStr= idStr, roundMult = roundMult, addNorm = addNorm) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                            gamma = np.zeros((5,5))
                            Csum = dist
                            Psum1 = 0
                            Psum2 = 0
                        else:
                            dist, gamma, Csum, Psum1, Psum2 = softW(C, uv1, uv2, l1, l2, lmult = lmult, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult ) 
                        if verbose:
                            print('{}, {} : {}'.format(i, j, dist))
                        wMat[i, j] = dist
                        CMat[i,j] = Csum
                        PMat[i,j] = Psum1+Psum2

            for i in range(prevEndNum1, curEndNum1):
                if forceWinSym:
                    jStartNum = prevEndNum2 + (i - prevEndNum1)
                else:
                    jStartNum = prevEndNum2
                for j in range(jStartNum, curEndNum2):
                    uv1 = sdfCells1[i]
                    uv2 = sdfCells2[j]
                    if isinstance(sdfPenalties1, np.ndarray):
                        l1 = sdfPenalties1[i]
                    else:
                        l1 = sdfPenalties1
                    if isinstance(sdfPenalties2, np.ndarray):
                        l2 = sdfPenalties2[j]
                    else:
                        
                        l2 = sdfPenalties2
                    if isinstance(hardW, bool) and hardW:
                        dist, gamma = gwWass(C, uv1, uv2) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                        Csum = dist
                        Psum1 = 0
                        Psum2 = 0
                    elif isinstance(hardW, str) and hardW == 'massdiff':
                        dist = np.abs(np.sum(uv1) - np.sum(uv2)) 
                        gamma = np.zeros((5,5))
                        Csum = 0 
                        Psum1 = dist
                        Psum2 = 0
                        print('Mass Diff:', dist)
                    elif isinstance(hardW, str) and hardW == '2dhist':
                        dist= gwF.solver2d(uv1, uv2, libPath=libDirSolver, idStr= idStr, roundMult = roundMult, addNorm = addNorm) #IN THIS CASE, WINDOWS (UV1, UV2) NEED TO BE NORMALIZED BEFORE HAND.
                        gamma = np.zeros((5,5))
                        Csum = dist
                        Psum1 = 0
                        Psum2 = 0
                    else:
                        dist, gamma, Csum, Psum1, Psum2 = softW(C, uv1, uv2, l1, l2, lmult = lmult, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult) 
                    if verbose:
                        print('{}, {} : {}'.format(i, j, dist))
                    wMat[i, j] = dist
                    CMat[i,j] = Csum
                    PMat[i,j] = Psum1+Psum2
                    if forceWinSym:
                        wMat[j,i] = dist
                        CMat[j,i] = Csum
                        PMat[j,i] = Psum1+Psum2


            G = np.zeros(curEndNum1*curEndNum2, order = 'F')
            gwScore, gwGamma = gwWass(wMat[0:curEndNum1, 0:curEndNum2], curEndNum1, curEndNum2, G)
            #cScore, cGamma = gwWass(CMat[0:curEndNum1, 0:curEndNum2], ncells1, ncells2, G)
            #pScore, pGamma = gwWass(PMat[0:curEndNum1, 0:curEndNum2], ncells1, ncells2, G)
            #np.set_printoptions(threshold=sys.maxsize, linewidth=np.inf, precision=4)
            #print(wMat[0:curEndNum1, 0:curEndNum2])
            #np.set_printoptions(threshold=1000)
            #gwScore, gwGamma = gwF.hardW(wMat[0:curEndNum1, 0:curEndNum2], curEndNum1, curEndNum2, G)
            #cScore, cGamma = gwF.hardW(CMat[0:curEndNum1, 0:curEndNum2], curEndNum1, curEndNum2, G)
            #pScore, pGamma = gwF.hardW(PMat[0:curEndNum1, 0:curEndNum2],  curEndNum1, curEndNum2, G)
            #For use with 2dhist since that's the standard now.
            cScore = gwScore
            cGamma = gwGamma
            pScore = 0
            pGamma = np.zeros((1,1))


            #Get stats, deltas, and low/high

            minScore = wMat.min()
            maxScore = wMat.max()
            minIndTup = np.nonzero(wMat == minScore)
            maxIndTup = np.nonzero(wMat == maxScore)
            minInd = (minIndTup[0][0], minIndTup[1][0])
            maxInd = (maxIndTup[0][0], maxIndTup[1][0])

            gwDelta = abs(gwScore - prev_gwScore)
            deltas.append(gwDelta)
            scores.append(gwScore)
            print('GW Score:', scores)   
            print('GW Deltas', deltas[1:])
            if gwDeltaThresh != False and gwDelta < gwDeltaThresh:
                break
            else:
                prev_gwScore = gwScore
                prevEndNum1 = curEndNum1
                prevEndNum2 = curEndNum2
        
        if isinstance(hardW, str) and hardW=='2dhist':
            gwF.solver2dCleanup(sdfCells1, sdfCells2)
        
        gwScore = gwScore*scoreMult
        return (gwScore, gwGamma, wMat, CMat, PMat, cScore, pScore, scores, deltas[1:], minInd, maxInd, minScore, maxScore)            

def resRegen(refSample, altSample, altRegenJsonFP, altRegenDream3dFP, noGrainInWindow = 4, searchWindowLen = 64, windowSize = 64, regenSizeMult=1, axis='x', sliceNum = 'half', distMultiplier=1, sdfDistMin=1, forceType = 'avgmass', forceLen = None, forceMass = None, regenStats = False):
    #Where refSample is the reference structure
    #altRegenJsonFP is the json filepath
    #altRegenDream3dFP is the dream3d filepath
    #noGrainInWindows is the expected number of grain diameters per window
    #sampleWindowLen is the search area for the statistics, square from the center.
    #windowSize is the expected number 
    #Regenerate a microstructure depending on its microstructure and reference.
    #run ref and alt through SDF and get the total amount of mass for each.
    #How much smaller/bigger do the pixels have to be in order for the reference and alt to match each other in the same window size
    #noGrainInWindow and sampleWindowLen are for the size of the windows used by the wass program.
    from marchingSquare import fastSweepSDF
    if isinstance(refSample, str) and refSample.endswith('dream3d'):
        refSampleSlice, refslen = getSlice(refSample, axis, sliceNum)
    else:
        refSampleSlice = refSample
        refslen = 1
    if isinstance(refSampleSlice, list):
        refSampleSlice= refSampleSlice[0]
    if 1 in refSampleSlice.shape or len(refSampleSlice.shape) == 1:
        refSampleSlice = refSampleSlice.flatten()
        refSampleSlice = refSampleSlice.reshape((int(np.sqrt(refSampleSlice.size)), int(np.sqrt(refSampleSlice.size))))

    
    if isinstance(altSample, str) and altSample.endswith('dream3d'):
        altSampleSlice, altslen = getSlice(altSample, axis, sliceNum)
    else:
        altSampleSlice = altSample
        altslen = 1
    if isinstance(altSampleSlice, list):
        altSampleSlice= altSampleSlice[0]
    if 1 in altSampleSlice.shape or len(altSampleSlice.shape) == 1:
        altSampleSlice = altSampleSlice.flatten()
        altSampleSlice = altSampleSlice.reshape((int(np.sqrt(altSampleSlice.size)), int(np.sqrt(altSampleSlice.size))))
    
    xlo = int((refSampleSlice.shape[0]/2) - (windowSize/2))
    xhi = xlo + windowSize 
    ylo = int((refSampleSlice.shape[1]/2) - (windowSize/2))
    yhi = ylo + windowSize
    refSampleSlice = refSampleSlice[xlo:xhi, ylo:yhi]
    
    xlo = int((altSampleSlice.shape[0]/2) - (windowSize/2))
    xhi = xlo + windowSize 
    ylo = int((altSampleSlice.shape[1]/2) - (windowSize/2))
    yhi = ylo + windowSize
    altSampleSlice = altSampleSlice[xlo:xhi, ylo:yhi]
    
    refSampleSliceMass = ms.overlapSDF(refSampleSlice, 'all', 1, ignoreBoundaryEdges = False, uFunc = 'L1')
    altSampleSliceMass = ms.overlapSDF(altSampleSlice, 'all', 1, ignoreBoundaryEdges = False, uFunc = 'L1')

    #newAltsLen = np.sqrt((np.sum(altSampleSlice)*refslen**2)/np.sum(refSampleSlice)) #Think about this as concentration
    M1= np.sum((refSampleSliceMass*distMultiplier)+sdfDistMin) #pix^3
    A1 = refSampleSliceMass.shape[0]*refSampleSliceMass.shape[1] #pix^2
    N1 = np.unique(refSampleSlice).size
    M2 = np.sum((altSampleSliceMass*distMultiplier)+sdfDistMin) #pix^3
    A2 = altSampleSliceMass.shape[0]*altSampleSliceMass.shape[1] #pix^2
    N2 = np.unique(altSampleSlice).size
    avM1 = M1/N1
    avM2 = M2/N2
    print(forceType)
    print(refSample, 'origlen:',  refslen, 'searchWinLen:', searchWindowLen, 'M:', M1, 'A:', A1, 'N:', N1, 'avM:', avM1)
    print('vs')
    print(altSample, 'origlen:',  altslen,   'searchWinLen:', searchWindowLen, 'M:', M2, 'A:', A2, 'N:', N2, 'avM:', avM2)
    if forceType == 'setmass':
        newAltsLen = altslen*(M2/forceMass)
    elif forceType == 'setlen':
        newAltsLen = forceLen
    elif forceType == 'totmass':
        newAltsLen = altslen*(M2/M1)
    elif forceType == 'avgmass':
        newAltsLen = altslen*(avM2/avM1)**(1/3)
    elif forceType == 'stats': #ends it early.
        newAltsLen = altslen
        return (newAltsLen, M1, M2, A1, A2, N1, N2)
    elif forceType == 'resize': #Uses the exact same length as the input
        newAltsLen = altslen
    else:
        if not isinstance(forceType, str):
            raise TypeError('forceType is required to be a string')
        else:
            raise ValueError('forceType is an unknown value. Given: {}'.format(forceType))
    print('newlen:', newAltsLen)
    #print(refSampleSliceMass.shape, altSampleSliceMass.shape)
    #newAltsLen = 5*(noGrainInWindow)*(M2/(A2*N2)) 

    altPipelineDict = d3f.getPipeline(altSample)
    for key in altPipelineDict.keys():
        if 'Resolution' in altPipelineDict[key].keys():
            if not forceType == 'resize':
                altPipelineDict[key]['Resolution']['x']=newAltsLen
                altPipelineDict[key]['Resolution']['y']=newAltsLen
                altPipelineDict[key]['Resolution']['z']=newAltsLen
    
        if 'Dimensions' in altPipelineDict[key].keys():
            altPipelineDict[key]['Dimensions']['x']=int(regenSizeMult*windowSize)
            altPipelineDict[key]['Dimensions']['y']=int(regenSizeMult*windowSize)
            altPipelineDict[key]['Dimensions']['z']=int(regenSizeMult*windowSize) #Dimensions = pixels per side
    
        if 'OutputFile' in altPipelineDict[key].keys():
            altPipelineDict[key]['OutputFile']=os.path.abspath(altRegenDream3dFP) #dream3d
        if 'OutputFilePath' in altPipelineDict[key].keys():
            altPipelineDict[key]['OutputFilePath']=os.path.abspath(altRegenDream3dFP.replace('.dream3d', '.csv')) #statscsv

    d3f.writePipeFromDict(altRegenJsonFP, altPipelineDict)
    d3f.runDream3d(altRegenJsonFP)
    if regenStats:
        regenSampleSlice, regenslen = getSlice(altRegenDream3dFP, axis, sliceNum)
        if isinstance(regenSampleSlice, list):
            regenSampleSlice= regenSampleSlice[0]
        if 1 in regenSampleSlice.shape or len(regenSampleSlice.shape) == 1:
            regenSampleSlice = regenSampleSlice.flatten()
            regenSampleSlice = regenSampleSlice.reshape((int(np.sqrt(regenSampleSlice.size)), int(np.sqrt(regenSampleSlice.size))))
        regenSampleSliceMass = fastSweepSDF(regenSampleSlice, 'all', 1, ignoreBoundaryEdges = False, uFunc = 'L1')
        Mr= np.sum((regenSampleSliceMass*distMultiplier)+sdfDistMin) #pix^3
        Ar = regenSampleSliceMass.shape[0]*regenSampleSliceMass.shape[1] #pix^2
        Nr = np.unique(regenSampleSlice).size
        print(forceType)
        print(refSample, 'origlen:',  refslen, 'searchWinLen:', searchWindowLen, 'M:', M1, 'A:', A1, 'N:', N1, 'avM:', avM1)
        print('vs')
        print(altSample, 'origlen:',  altslen,   'searchWinLen:', searchWindowLen, 'M:', M2, 'A:', A2, 'N:', N2, 'avM:', avM2)
        print('newlen:', newAltsLen)
        print('Regen Stats:')
        print(altRegenDream3dFP, 'searchWinLen:', searchWindowLen, 'M:', Mr, 'A:', Ar, 'N:', Nr, 'avM:', Mr/Nr)
        return (newAltsLen, M1, M2, A1, A2, N1, N2, Mr, Ar, Nr)
    else:
        return (newAltsLen, M1, M2, A1, A2, N1, N2)


