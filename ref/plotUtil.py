import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pixelMethod as pm
import marchingSquare as ms

def getImage(imgfp, fig=None, ax=None):
    if isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    if isinstance(imgfp, str):
        img=mpimg.imread(imgfp)
        ax.imshow(img)
    elif isinstance(imgfp, type(None)):
        pass
    else:
        ax.imshow(img)
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    ax.tick_params(axis='y', which='both', left=False, right=False, labelleft=False)
    return fig, ax

def rasterPlot(sliceIds, axs=False, keepValues = True, cmap='viridis',vmin=None,vmax=None, plot3d=False):
    #sliceIds is a nxm of grain ids. numpuwa array
    #axs may be predetermined to slot it in.
    #otherwise it will return a new figure
    #vmin and vmax define colorbar scales.
    #plot3d requiers a 3d axis
    uni = np.unique(sliceIds)
    nslice = sliceIds.copy()
    if keepValues == False:
        for i in range(uni.size):
            nslice[sliceIds == uni[i]] = i
    if keepValues == 'random':
        idRange = np.arange(len(uni))
        np.random.shuffle(idRange)
        for i in range(uni.size):
            nslice[sliceIds == uni[i]] = idRange[i]

    if isinstance(axs, bool) and axs == False:
        im = plt.pcolor(nslice, edgecolors = 'none', cmap=cmap, vmin=vmin, vmax=vmax)
    else:
        if plot3d:
            print(axs)
            X = np.arange(nslice.shape[0])
            Y = np.arange(nslice.shape[1])
            X,Y=np.meshgrid(X,Y)
            im = axs.plot_surface(X,Y,nslice,cmap=cmap,vmin=vmin,vmax=vmax, rstride=1, cstride=1, linewidth=0,antialiased=True,shade=False)
        else:
            im = axs.pcolor(nslice, edgecolors = 'none', cmap=cmap, vmin=vmin, vmax=vmax)

    return axs, im

def crossPlot(ccmatrix, titles=None, celltitles = None, plotTitle=None, fig = None, ax = None, titleKwarg = {}, textKwarg={}, cmap='viridis', colorbar=False):
        #celltitles is a matrix the same size as ccmatrix
        ccmatrix = ccmatrix.copy()
        if len(ccmatrix.shape) < 2:
            ccmatrix = ccmatrix.reshape((1, len(ccmatrix)))
            print(ccmatrix.shape)
        nv = ccmatrix.shape[0]
        nh = ccmatrix.shape[1]
        if isinstance(ax, type(None)) and isinstance(fig, type(None)):
            fig, ax = plt.subplots()

        im = ax.imshow(ccmatrix, cmap=cmap)

        if not isinstance(titles, type(None)):
            ax.set_xticks(np.arange(nh))
            ax.set_yticks(np.arange(nv))
            ax.set_xticklabels(titles, **titleKwarg)# fontsize = 4)
            ax.set_yticklabels(titles, **titleKwarg)# fontsize = 4)
        else:
            ax.tick_params(axis='x', which = 'both', bottom=False, top = False, labelbottom=False)
            ax.tick_params(axis='y', which = 'both', left=False, right= False, labelleft=False)
        plt.setp(ax.get_xticklabels(), rotation = 45, ha = "right", rotation_mode = "anchor")
        if isinstance(plotTitle, str):
            ax.set_title(plotTitle)# fontsize=4)
        print(celltitles)
        for i in range(nv): #Might need to swap these around?)
            for j in range(nh):
                print(i,j, nh, nv, ccmatrix.shape)
                if ccmatrix[i,j] > (np.max(ccmatrix)-np.min(ccmatrix))/2 + np.min(ccmatrix):
                    col = 'k'
                else:
                    col = 'w'
                if isinstance(celltitles, type(None)):
                    instr = '{:.4}'.format(ccmatrix[i,j])
                else:
                    instr = celltitles[i,j]
                print(instr, i, j)
                text = ax.text(j, i, instr, ha = "center", va = "center", color = col, **textKwarg)# fontsize=4)
        fig.tight_layout()
        if colorbar:
            fig.colorbar(im)
        return fig, ax

def linPlot(scoreMats, varRange=None, varName = '',yaxislabel="Score", titles=[], plotTitle=None, plotStyles= [], multiStyle = 'all', dataReplaceDict={}, fig=None, ax=None):
        #Variation metric is (Difference in Mean Average Score) / Average of STD = Dimensionless Quantity
        isMulti=True
            
        print(scoreMats)
        if not isinstance(scoreMats, list):
            scoreMats = [scoreMats]
            isMulti=False
        if isinstance(ax, type(None)) and isinstance(fig, type(None)):
            fig, ax = plt.subplots()
        if multiStyle == 'minmax':
            yaxislabel = "Score (minmax)"
            nx, ny = scoreMats[0].shape
            if isinstance(varRange, type(None)):
                varRange = np.arange(ny)
            nk = len(scoreMats)
            avgVals = np.zeros(ny)
            maxVals = np.zeros(ny)
            minVals = np.zeros(ny)
            for i in range(nx):
                vals = np.zeros((nk, ny))
                mi = np.zeros((nk, ny))
                ma = np.zeros((nk, ny))
                for k in range(nk):
                    vals[k, :] = scoreMats[k][i, :]
                avgVals[:] = np.mean(vals, 0)
                maxVals[:] = np.max(vals, 0)
                minVals[:] = np.min(vals, 0)
                minmaxVals = np.zeros((2, vals.shape[1]))
                minmaxVals[0,:] = avgVals - minVals
                minmaxVals[1,:]= maxVals - avgVals
                if len(plotStyles) == 0:
                    A = ax.errorbar(varRange, avgVals, yerr=minmaxVals)
                else:
                    if isinstance(plotStyles[i], str):
                        A =ax.errorbar(varRange, avgVals, yerr=minmaxVals, fmt=plotStyles[i])
                    if isinstance(plotStyles[i], dict):
                        A = ax.errorbar(varRange, avgVals, *plotStyles[i], yerr=minmaxVals)
                A.set_label(titles[i])


                ax.fill_between(varRange, minVals, maxVals, alpha=0.4)
              
        elif multiStyle == 'std':
            yaxislabel = "Score"#(std)"
            nx, ny = scoreMats[0].shape
            if isinstance(varRange, type(None)):
                varRange = np.arange(ny)
            nk = len(scoreMats)
            avgVals = np.zeros(ny)
            stdVals = np.zeros(ny)
            for i in range(nx):
                vals = np.zeros((nk, ny))
                mi = np.zeros((nk, ny))
                ma = np.zeros((nk, ny))
                for k in range(nk):
                    print(scoreMats[k].shape)
                    vals[k, :] = scoreMats[k][i, :]
                avgVals[:] = np.mean(vals, 0)
                stdVals[:] = np.std(vals, 0)
                

                if len(plotStyles) == 0:
                    A = ax.errorbar(varRange, avgVals, yerr=stdVals)
                else:
                    if isinstance(plotStyles[i], str):
                        A =ax.errorbar(varRange, avgVals, yerr=stdVals, fmt=plotStyles[i])
                    if isinstance(plotStyles[i], dict):
                        A = ax.errorbar(varRange, avgVals, yerr=stdVals, *plotStyles[i])

                A.set_label(titles[i])
                ax.fill_between(varRange, avgVals-stdVals, avgVals+stdVals, alpha=0.4)


        elif multiStyle == 'varMetric':
            yaxislabel = "mean(|vals - avg|) / std"
            nx, ny = scoreMats[0].shape #List of Arrays
            #nx -> x axis, force window length
            #ny -> run number
            #Rolled vs. Rolled
            #Run 1: 1,2,3,4
            #Run 2: 1,2,3,4
            #Run 3: 1,2,3,4
            
            #Rolled vs. Rolled
            #Run 1: 1,2,3,4
            #Run 2: 1,2,3,4
            #Run 3: 1,2,3,4
            if isinstance(varRange, type(None)):
                varRange = np.arange(ny)
            nk = len(scoreMats)
            avgVals = np.zeros(ny)
            stdVals = np.zeros(ny)
            relVals = np.zeros(ny)
            for i in range(nx):
                vals = np.zeros((nk, ny))
                mi = np.zeros((nk, ny))
                ma = np.zeros((nk, ny))
                for k in range(nk):
                    vals[k, :] = scoreMats[k][i, :]
                avgVals[:] = np.mean(vals, 0)
                stdVals[:] = np.std(vals, 0)
                relVals[:] = np.mean(np.abs(vals - avgVals), 0) / stdVals
                print('vals', vals)
                print('avgs', avgVals)
                print('stds', stdVals)
                print(vals-avgVals)
                print(np.mean(np.abs(vals-avgVals), 0))
                print(relVals)
                if len(plotStyles) == 0:
                    A =ax.plot(varRange, relVals)
                else:
                    if isinstance(plotStyles[i], str):
                        A = ax.plot(varRange, relVals, plotStyles[i])
                    if isinstance(plotStyles[i], dict):
                        A = ax.plot(varRange, relVals, *plotStyles[i])

                A.set_label(titles[i])
                #ax.fill_between(varRange, avgVals-stdVals, avgVals+stdVals, alpha=0.2)

        elif multiStyle == 'grad':
            yaxislabel = "(d(score)/dx) (std)"
            nx, ny = scoreMats[0].shape
            if isinstance(varRange, type(None)):
                varRange = np.arange(ny)
            nk = len(scoreMats)
            avgVals = np.zeros(ny)
            stdVals = np.zeros(ny)
            for i in range(nx):
                vals = np.zeros((nk, ny))
                mi = np.zeros((nk, ny))
                ma = np.zeros((nk, ny))
                for k in range(nk):
                    vals[k, :] = scoreMats[k][i, :]
                relVals = np.gradient(vals, axis=1)
                print(relVals)
                avgVals[:] = np.mean(relVals, 0) 
                stdVals[:] = np.std(vals, 0)
                print('vals', vals)
                print('avgs', avgVals)
                print('stds', stdVals)
                print(vals-avgVals)
                print(np.mean(np.abs(vals-avgVals), 0))
                print(relVals)
                if len(plotStyles) == 0:
                    A = ax.plot(varRange, relVals)
                else:
                    if isinstance(plotStyles[i], str):
                        A = ax.plot(varRange, avgVals, plotStyles[i])
                    if isinstance(plotStyles[i], dict):
                        A = ax.plot(varRange, avgVals, *plotStyles[i])
                
                A.set_label(titles[i])
                ax.fill_between(varRange, avgVals-stdVals, avgVals+stdVals, alpha=0.4)


        else:
            for scoreMat in scoreMats:
                nx, ny = scoreMat.shape
                if isinstance(varRange, type(None)):
                    varRange = np.arange(ny)
                for i in range(nx):
                    if len(plotStyles) == 0:
                        A=ax.plot(varRange, scoreMat[i,:], label=titles[i])
                    if isinstance(plotStyles[i], str):
                        A=ax.plot(varRange, scoreMat[i,:], plotStyles[i], label=titles[i])
                    if isinstance(plotStyles[i], dict):
                        A=ax.plot(varRange, scoreMat[i,:], *plotStyles[i], label=titles[i])

            
        if isinstance(plotTitle, str):
            plt.title(plotTitle.format(**dataReplaceDict), fontsize=8)
        if len(titles) != 0:
            ax.legend(fontsize=6)
        ax.set_xlabel(varName)
        ax.set_ylabel(yaxislabel)
        fig.tight_layout()
        return fig, ax

def landPlot(scoreDict, xlabel='', ylabel='', selfRefPoint=False, selfRefStr='', colorbar=True, fig=None, ax=None, xyrel=lambda x,y: y):
    if isinstance(ax, type(None)):
        fig, ax = plt.subplots(1,1)
    C = [] 
    xpts = [] 
    ypts = [] 
    yptsconv = []
    xmin=np.inf
    xmax=-np.inf
    ymin=np.inf
    ymax=-np.inf
    ngrid = 100

    for i,j in scoreDict.keys():
        val = scoreDict[(i,j)][2]
        x = scoreDict[(i,j)][0][0]
        y = scoreDict[(i,j)][0][1]
        
        C.append(val)
        xpts.append(x)
        ypts.append(y)
        yptsconv.append(xyrel(x,y))
        if xmin > x: xmin=x
        if xmax < x: xmax=x
        if ymin > y: ymin=y
        if ymax < y: ymax=y
    #xgrid = np.linspace(xmin, xmax, ngrid)
    #ygrid = np.linspace(ymin, ymax, ngrid)
    C=np.array(C)
    xpts=np.array(xpts)
    ypts = np.array(ypts)
    yptsconv=np.array(yptsconv)


    fig,axs = plt.subplots(1,2)
    im = axs[0].tricontourf(xpts, yptsconv, C, levels=30)
    axs[0].tricontour(xpts, yptsconv, C, levels=30, colors='k', linewidths=0.5,linestyles='dashed')
    if isinstance(selfRefPoint, tuple):
        axs[0].scatter(selfRefPoint[0],selfRefPoint[1], color='white', marker='x')
        axs[0].annotate('Target\n{}: {}'.format(selfRefPoint,selfRefStr), selfRefPoint, color='white', fontsize='x-small')
    axs[0].set_xlabel(xlabel)
    axs[0].set_ylabel(ylabel)

    fig.colorbar(im) 

    return fig, ax


def plotMicroStruct(idList, titles = False, subplotStyle = 'square',keepValues = True, axis='x', sliceNum='half', noGrainInWindow='all', forceWindowLen=False, sdf=False, axs = None, cmap='viridis', sharedcm=False, optionsDict=False, plot3d=False, zbounds=None, verbose=True):
    """given a list of 2d or a n^2 vector plot thing.
    subplotStyle is eithe 'square', 'row', 'col', or a tuple.copy()
    axs has to be a numpy array of axis, flattened.
    if sharedcm is True, all plots will have the same normalized colormapings.
    putting in an options dict overwrites it all"""
    #vbounds sets the colorbar range (min, max)
    if isinstance(optionsDict, dict) and 'dream3dContainer' in optionsDict.keys():
        dream3dContainer = optionsDict['dream3dContainer']
    else:
        dream3dContainer = None
    microList = []
    xmax = 0
    ymax = 0
    vmin = 0
    vmax = 0
    if isinstance(idList, list):
        for i in range(len(idList)):
            sample = idList[i]
            print("Plotting: {} of {}".format(i+1, len(idList)))
            if isinstance(axis, list):
                plotaxis = axis[i]
            elif isinstance(optionsDict, dict):
                try:
                    plotaxis = optionsDict['axis']
                except KeyError:
                    plotaxis = axis
            else:
                plotaxis = axis
            if isinstance(sliceNum, list):
                plotSliceNum=sliceNum[i]
            else:
                plotSliceNum=sliceNum
            if isinstance(sdf, list):
                doSDF = sdf[i]
            else:
                doSDF = sdf
            if verbose and isinstance(sample, str):
                print(sample)
            

            if isinstance(sample, str) and sample.endswith('dream3d'):#for filepaths to dream3d
                sample, slen = pm.getSlice(sample, plotaxis, plotSliceNum, dream3dContainer=dream3dContainer)
            
            if isinstance(sample, list):
                sample = sample[0]
            if 1 in sample.shape or len(sample.shape) == 1:
                sample = sample.flatten()
                sample = sample.reshape((int(np.sqrt(sample.size)), int(np.sqrt(sample.size))))
            xsize1, ysize1 = sample.shape


            if isinstance(optionsDict, dict):
                optionsDict = optionsDict.copy()
                optionsDict['windowNum']=1
                dataDict=pm.generateWindowData(sample, sample, optionsDict1=optionsDict, optionsDict2=optionsDict)
                if doSDF:
                    sample = dataDict['uv1SDF'][0]
                    if len(sample.shape) == 1:
                        newSize = int(np.sqrt(sample.shape[0]))
                        sample = np.reshape(sample, (newSize, newSize))

                else:
                    sample = dataDict['windowDataArray1'][0]

            elif noGrainInWindow != 'all': 
                #uniTuple, pixTuple, minTuple = ms.windowAnalysis(sample, sample, 1, 1, relativeScale = False, winSizeDetermine = 'max', verbose = False, numSamples = 1, noGrainInWindow = noGrainInWindow)
                #winlen1 = pixTuple[0]
                xcenter1 = int(xsize1 / 2)
                ycenter1 = int(ysize1 / 2)
                lowPointx1 = int(xcenter1 - (winlen1/2))
                lowPointy1 = int(ycenter1 - (winlen1/2))
                highPointx1 = int(lowPointx1 + winlen1)
                highPointy1 = int(lowPointy1 + winlen1)
                sample=sample[lowPointx1:highPointx1,lowPointy1:highPointy1]
            elif not isinstance(forceWindowLen, bool) and isinstance(forceWindowLen, int):
                xcenter1 = int(xsize1 / 2)
                ycenter1 = int(ysize1 / 2)
                lowPointx1 = int(xcenter1 - forceWindowLen/2) 
                lowPointy1 = int(ycenter1 - forceWindowLen/2)
                highPointx1 = int(lowPointx1 + forceWindowLen) 
                highPointy1 = int(lowPointy1 + forceWindowLen)
                sample=sample[lowPointx1:highPointx1,lowPointy1:highPointy1]

            if doSDF and not isinstance(optionsDict, dict):
                sample = ms.fastSweepSDF(sample, 'all', 1, ignoreBoundaryEdges=False, uFunc="L1")
            x, y = sample.shape
            if xmax < x:
                xmax = x
            if ymax < y:
                ymax = y
            if vmin < np.min(sample):
                vmin = np.min(sample)
            if vmax < np.max(sample):
                vmax = np.max(sample)
            microList.append(sample)
         
    else:
        sample = idList
        if isinstance(sample, str) and sample.endswith('dream3d'):#for filepaths to dream3d
            sample, slen = pm.getSlice(sample, axis, sliceNum)

        if 1 in sample.shape or len(sample.shape) == 1:
            sample = sample.flatten()
            sample = sample.reshape((int(np.sqrt(sample.size)), int(np.sqrt(sample.size))))
        if sdf:
            sample = ms.overlapSDF(sample, 'all', 1, ignoreBoundaryEdges=False, uFunc="L1")
        x, y = sample.shape
        if xmax < x:
            xmax = x
        if ymax < y:
            ymax = y
        microList = [sample]
    
    
    

    if subplotStyle == 'square':
        row = int(np.ceil(np.sqrt(len(idList))))
        col = int(np.ceil(np.sqrt(len(idList))))
    elif subplotStyle == 'row':
        row = 1 
        col = int(np.ceil(len(idList)))
    elif subplotStyle == 'col':
        row = int(np.ceil(len(idList)))
        col = 1 
    elif isinstance(subplotStyle, tuple):
        row = subplotStyle[0]
        col = subplotStyle[1]
 
    if isinstance(axs, type(None)):
        if plot3d:
            fig = plt.figure()
            axs = np.ndarray((row, col), dtype=np.object)
            print(axs)
            for i in range(row):
                for j in range(col):
                    axs[i,j] = fig.add_subplot(row,col, int(i*row) + j+1, projection='3d')
        else:
            fig, axs = plt.subplots(row, col)
        if isinstance(axs, np.ndarray):
            axs = axs.reshape(row*col)
        else:
            axs = [axs]
    else:
        fig = None

    if not sharedcm:
        vmax = None
        vmin = None
    if not isinstance(zbounds, type(None)):
        zmin, zmax = zbounds
    else:
        zmin = vmin
        zmax = vmax
    for i in range(len(microList)):
        a, im = rasterPlot(microList[i], keepValues = keepValues, axs = axs[i], cmap=cmap, vmax = vmax, vmin=vmin, plot3d=plot3d)
        if plot3d:
            if sharedcm:
                axs[i].set_zlim(zmin, zmax)
        else:
            axs[i].axis('equal')
        axs[i].set_xlim(0, xmax)
        axs[i].set_ylim(0, ymax)
        plt.colorbar(im, ax =axs[i])
        if titles != False:
            axs[i].set_title(titles[i])
    
    return fig, axs

