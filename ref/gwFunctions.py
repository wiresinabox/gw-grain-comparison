#From Jeremy Mason's example. All rights to him.
#Wrappers and such.


import ctypes as ct
import numpy as np
import numpy.ctypeslib as ctl
import time
import sys
import os
import subprocess as sub
np.seterr(divide='raise')

class gw_gromov:
    #wrapper for gw_gromov, a vanilla version of gw_wass.
    #Approximates for the Gromov-Wasserstein Function, which is the difference between 
    #2 probability distributions on SEPERATE metric spaces (effectively 2 unconnected graphs).
    #I believe uses sinkhorn?
    autoReg = False
    def __init__(self, libname, libdir, autoReg = False):
        self.c_gw_gromov = ctl.load_library(libname, libdir).gw_gromov
        self.c_gw_gromov.restype = ct.c_double
        self.autoReg = autoReg

    
    def __call__(self, m1in, m2in, D1in, D2in, initial = 'o', t1=False, t2=False, printTime = False, regularM=True):
        #Assume all variables are untranslated to proper types 
        #m1in, m2in are numpy arrays (vectors) of size m and n respectively.
        #THEY MUST ALSO BE A NX1 OR MX1 ARRAY, NOT A VECTOR.
        #D1in and D2in are numpy arrays (matricies) dense matrix of mxm and nxn
        #t1 and t2 control typing, they need to be in dtype = np.float32, order = 'F'
        #regularM controls whether the marginal measures m1in and m2in are summed to 1.
        #Preferably, do the conversions before hand cause it can get a bit slow. 
        #Issue! Can we set our entropy regularized constant?
        
        
        #print(m1in)
        #print(m2in)
        #print(D1in)
        #print(D2in)
        #time.sleep(4)
        #Get sizes
        if printTime:
            startTime = time.time()
        if type(m1in) == type([]):
            m1in = np.array(m1in)
        if type(m2in) == type([]):
            m2in = np.array(m2in)


        n1t = m1in.shape
        n2t = m2in.shape
        n1 = n1t[0]
        n2 = n2t[0]

        if len(n1t) < 2:
            m1in = np.expand_dims(m1in, axis = 1)
        if len(n2t) < 2:
            m2in = np.expand_dims(m2in, axis = 1)

        #check types
        if t1 == False:
            t1 = np.zeros(n1, dtype = np.uint32, order = 'F')
        if t2 == False:
            t2 = np.zeros(n2, dtype = np.uint32, order = 'F')

        #Check if m1 and m2 are regularized
        m1Sum = np.sum(m1in)
        m2Sum = np.sum(m2in)
        if m1Sum != 1 and regularM:
            m1 = np.array(m1in/m1Sum, dtype = np.float64, order = 'F')
        else:
            m1 = np.array(m1in, dtype = np.float64, order = 'F')
        if m2Sum != 1 and regularM:
            m2 = np.array(m2in/m1Sum, dtype = np.float64, order = 'F')
        else:
            m2 = np.array(m2in, dtype = np.float64, order = 'F')
        
        #Convert D1 and D2
        D1 = np.array(D1in, dtype = np.float64, order = 'F')
        D2 = np.array(D2in, dtype = np.float64, order = 'F')
        #G is then gamma, the optimal coupling

        G = np.empty((n1, n2), order = 'F')
        
        if printTime:
            convTime= time.time()
        dist = self.c_gw_gromov(
            G.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m1.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m2.ctypes.data_as(ct.POINTER(ct.c_double)), \
            t1.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            t2.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            D1.ctypes.data_as(ct.POINTER(ct.c_double)), \
            D2.ctypes.data_as(ct.POINTER(ct.c_double)), \
            ct.c_uint32(n1),        \
            ct.c_uint32(n2),        \
            ord(initial))
        if printTime:
            endTime = time.time()
            print('Conversion Time: {}, Execution Time: {}'.format(convTime - startTime, endTime - convTime))
        return (dist, G)
        


class gw_wass:
    #Wrapper for gw_wasserstein
    #Solves for the Wasserstein Distance, which is a measure of probability on the SAME metric space.
    #Otherwise known as the Earth Mover's Distance (EMD)
    #May 
    def __init__(self, libname, libdir):
        print(libname)
        print(libdir)
        self.c_gw_wasser = ctl.load_library(libname, libdir).gw_wasser
        self.c_gw_wasser.restype = ct.c_double

    def __call__(self, Cin, m1in, m2in, Gin = None, t1 = False, t2 = False, regularM=True):

        #THIS NEEDS TO BE NORMALIZED. S.T THE MASS STAYS EQUAL TO 1
        if isinstance(m1in, int):
            m1 = np.ones(m1in, dtype = np.float64, order = 'F')
        else:
            m1 = m1in
        if isinstance(m2in, int):
            m2 = np.ones(m2in, dtype = np.float64, order = 'F')
        else:
            m2 = m2in
        if regularM:
            m1 = np.array(m1/np.sum(m1), dtype = np.float64, order = 'F')
            m2 = np.array(m2/np.sum(m2), dtype = np.float64, order = 'F')
        else:
            m1 = np.array(m1, dtype = np.float64, order = 'F')
            m2 = np.array(m2, dtype = np.float64, order = 'F')
            
        n1 = m1.size
        n2 = m2.size
        
        #print('m1in sum:', np.sum(m1in), 'm1 sum:', np.sum(m1))

        if t1 == False:
            t1 = np.zeros(n1, dtype = np.float64, order = 'F')
        if t2 == False:
            t2 = np.zeros(n2, dtype = np.float64, order = 'F')
        C = np.array(Cin.flatten(), dtype = np.float64, order = 'F')
        if isinstance(Gin, type(None)):
            G = np.zeros(m1.size * m2.size, order = 'F')
        else:
            G = Gin
        
        #print(m1)
        #print(m2)
        #print(C)

        dist = self.c_gw_wasser(
            G.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m1.ctypes.data_as(ct.POINTER(ct.c_double)), \
            m2.ctypes.data_as(ct.POINTER(ct.c_double)), \
            t1.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            t2.ctypes.data_as(ct.POINTER(ct.c_uint32)), \
            C.ctypes.data_as(ct.POINTER(ct.c_double)), \
            ct.c_uint32(n1),        \
            ct.c_uint32(n2))

        G = G.reshape((m1.size, m2.size))
        return (dist, G)

def rtsafe(x, WACC=0.01, MAXIT=100, SMALL_NUM=2e-16):
    #Finds the maximum values of x s.t. phi(w|x) = x(1-x^{-w})-wlog(x) is not less than zero
    #Described in A. Thibault. L.Chizat, C. Dossal, and N.Papadakis, Overrelaxed
    #Sinkhorn--Knopp Algorithem for Regularixed Optimal Transport
    #Via standard newton's method.
    #This one is a modified version based on pg. 366 of W.H. Press S. A. Teukolsky, W. T. Vetterling, B. P. Flannery Numerical Recipies in C. Second Ed.
    #Originally implimented by Jeremy Mason in wasserstein3.m

    #Definitions

    if x >=1.:
        w=2.
        return w

    logx = np.log(x)
    #Initial Search Interval
    wl = 2.
    wh = 1.

    #Initial Guess
    w = 1. + 1./np.sqrt(1.-np.log(x))
    dtold=1.
    dt=dtold

    xow=np.power(x, 1.-w)
    f=x-xow-w*logx
    df=(xow-1.)*logx


    for a in range(0, MAXIT):
        if ((w-wh)*df-f)*((w-wl)*df-f)>0. or np.abs(2.*f) > np.abs(dtold*df):
            dtold=dt
            dt=(wh-wl)/2.
            w=wl+dt
        else:
            dtold=dt
            dt=f/df
            w=w-dt
        if np.abs(dt) < WACC:
            return w
        xow=np.power(x, 1.-w)
        f=x-xow-w*logx
        df=(xow-1.)*logx
        if f < 0.:
            wl=w
        else:
            wh=w
    raise RuntimeError("rtsafe: failed to converge")


def newton(x, WACC = 0.01, MAXIT = 100, SMALL_NUM=2e-16):
    #Originally implemented by Jeremy Mason in newton.c
    if np.any(not np.isfinite(x)):
        raise RuntimeError('newton: argument not finite')
    if x >= 1. - SMALL_NUM:
        return 2.
    log_x = np.log(x)
    #initial search interval
    wl = 1.
    wh = 2.
    #initial guess
    w = 1. + 1./ np.sqrt(1. - log_x)
    xow = np.power(x, 1.-w)
    f = x - xow - w*log_x
    df = (xow-1.)*log_x
    dw_old = 1.
    dw = -f / df
    
    for a in range(0, MAXIT):
        if (wh-w) < dw or dw < (wl - w) or np.abs(dw) > np.abs(dw_old/2.):
            dw = (wh - wl)/2. - w
        w= w + dw
        if (np.abs(dw) < WACC):
            return w
        xow = np.power(x, 1.-w)
        f = x - xow - w * log_x
        df = (xow-1.) * log_x

        dw_old = dw
        dw = -f/df

        if dw > 0.:
            wl = w
        else:
            wh = w
    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=8)
    print('x:\n', x)
    raise RuntimeError('newton: failed to converge')


def hardW(C, m1, m2, G = None, t1=False, t2=False, normGamma=False, normMass=True, etaFMult = 1e-2):
    if isinstance(m1, int):
        m1 = np.ones(m1, dtype = np.float64, order = 'F')
    if isinstance(m2, int):
        m2 = np.ones(m2, dtype = np.float64, order = 'F')

    if normMass:
        m1 = m1/np.sum(m1)
        m2 = m2/np.sum(m2)

    #NORMALIZE m1, and m2


    tau = 1e3
    delc = 0.02
    MAX_IT = 1e4
    theta = 1.75
    etaF = etaFMult*min(np.min(m1) / m1.size, np.min(m2) / m2.size)
    etaS = max(np.median(C), 4*etaF)
    tolF = 1e-4* etaF

    #print('etaF:', etaF, 'etaS:', etaS, 'tolF:', tolF) 
    n1 = m1.size
    n2 = m2.size

    if isinstance(G, type(None)):
        G = np.zeros((n1, n2))
    
    if isinstance(t1, type(False)) and t1 == False:
        t1 = np.zeros(n1)
    if isinstance(t2, type(False)) and t2 == False:
        t2 = np.zeros(n2)
    
    mask = np.zeros(C.shape)
    for a in range(0, n1):
        for b in range(0, n2):
            if t1[a] != t2[b]:
                mask[a,b] = -3.403e+38
    
    R = np.exp(mask) *np.matmul(m1, m2.transpose())
    u = np.ones(n1)
    v = np.ones(n2)
    Rv = np.sum(R, 1)
    while max(abs(u*Rv-m1)) > tolF:
        u=m1/Rv
        v=m2/np.matmul(R.transpose(), u)
        Rv = np.matmul(R, v)
    R = np.matmul(np.matmul(np.diag(u), R), np.diag(v))
    
    etaRange = etaS/etaF
    k=int(np.ceil(np.log(etaRange)/np.log(4.)))
    l=etaRange**(-1./k)
    
    E = np.append(etaS* np.power(l, np.arange(0, k)), etaF)
    T = np.append(np.fmax(E[0:k] / 100., tolF), tolF)
    T=T*min(n1, n2)
    
    alph = np.zeros(n1)
    beta=np.zeros(n2)
    K = -C
    for a in range(0, k+1):
        iterc=0
        not_converged=True
        while not_converged:
            #print('NEXT')
            u=np.ones(n1)
            v=np.ones(n2)
            #print('C:', C[380, :])
            K=np.round(np.exp(K/E[a] + mask), 8) * R
            #print('K:', K[380, :], np.sum(K[380,:]), K[380,240], C[380, 240])
            Kv=np.sum(K,1)
            while np.max(np.abs(u)) < tau and np.max(np.abs(v)) < tau and not_converged:
                try:
                    ur = u / (m1 / Kv) #Newton will fail to converge if divide by zero is encountered here.
                except FloatingPointError as e:
                    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=5)
                    print('C:\n', C)
                    print('Kv:\n', Kv)
                    print('K:\n', K)
                    print('m1:\n', m1)
                    print('u:\n', u)
                    #print('m1/Kv:', m1/Kv)
                    infInd = np.nonzero(Kv == 0)
                    print('Zero at:', infInd)
                    print('Kv at zero:', Kv[infInd])
                    print('m1 at zero:', m1[infInd])
                    print('K at zero:',  K[infInd, :])
                    print('C at zero:', C [infInd, :])
                    #print(Kv[380])
                    print(e)
                    quit()
                w = np.fmin(np.fmax(1., newton(np.min(ur)) - delc), theta)
                u = u / (ur**w)

                try:
                    vr = v / (m2 / np.matmul(K.transpose(), u))
                except FloatingPointError as e:
                    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=8)
                    #print('C:\n', C)
                    print('K:\n', K)
                    print('m2:\n', m2)
                    print('v:\n', v)
                    print('u:\n', u)
                    print(e)
                    quit()
                w = np.fmin(np.fmax(1., newton(np.min(vr)) - delc), theta)
                v = v / (vr**w)

                Kv = np.matmul(K, v)
                iterc += 1
                not_converged = np.max(np.abs(u * Kv - m1)) > T[a] and iterc < MAX_IT
                
            alph = alph + E[a]*np.log(u)
            beta = beta + E[a]*np.log(v)
            
            for b in range(0,n2):
                K[:, b] = -C[:, b] + alph + beta[b]


    G = np.round(np.exp((K/etaF) + mask), 8) * R
    
    #Normalize Gamma
    if normGamma:
        G = G/n1

    
    score = np.sum(G*C)
        
    #print(G)
    #print('Sum G x:', np.sum(G,0))
    #print('Sum G y:', np.sum(G,1))
    #print('Score:', score)
    return score, G
                



def softW(c,m1,m2,l1in,l2in,lmult = 1,K0=False, t1=False,t2=False, usePenaltyFinal = False, endlmult=1, swapMass = True, safeC=True):
    #pass
    #usePenaltyFinal is for the final calculation. If it is false, the final one will be set to 1 else a number.
    #constants
    #lmult multiplies at the beginning, lmult multiplies at the end.
    #safeC scales the cost matrix down before scaling back up the score at the end. Ensure that underflow doesn't happen.
    n1 = m1.size
    n2 = m2.size
    eps = np.finfo(np.float64).eps
    realmax = np.finfo(np.float64).max
    stime= time.time()

    #Making things default
    if isinstance(K0, type(False)) and K0 == False:
        #K0 = (np.ones((n1,n2)) / (n1*n2)) * np.mean([sum(m1), sum(m2)]) 
        K0 = np.outer(m1, m2) / np.mean([sum(m1), sum(m2)]) #np.ones((n1,n2)) / (n1 * n2)
    if isinstance(t1, type(False)) and t1 == False:
        t1 = np.ones(n1)
    if isinstance(t2, type(False)) and t2 == False:
        t2 = np.ones(n2)

    #Setting lmult to a window dependant constant
        
        

    l1 = l1in * lmult 
    l2 = l2in * lmult 

    #It seems like if m2 has a HIGHER MAXIMUM VALUE, then it reaches a lower minimum because
    #it seems like rs1-rs0 converges first always.
    if swapMass:
        if np.max(m2) > np.max(m1):
            c = c.transpose()
            tempm = m1
            templ = l1
            m1 = m2
            l1 = l2
            m2 = tempm
            l2 =templ

    if safeC: #keeps the coupling score dominant
        print('SAFEING C')
        maxC = np.max(c)
        c = c/maxC

    #print('m1:', m1)
    #print('m2:', m2)
    #print('m1:\n', m1.reshape((int(np.sqrt(m1.size)), int(np.sqrt(m1.size))))*(1/np.min(m1)), m1.shape)
    #print('m2:\n', m2.reshape((int(np.sqrt(m2.size)), int(np.sqrt(m2.size))))*(1/np.min(m2)), m2.shape)
    #print('l1:', l1, l1.shape)
    #print('l2:', l2, l2.shape)
    #print('usePenaltyFinal:', usePenaltyFinal) 

    #Adjustable
    EPS_F = 1e-4 #CHANGE THIS VALUE IF THINGS START BREAKING
    TOL_F = 1e-8
    #TOL_D = 100. * eps
    THRES = 1000.
    M_MAX_IT = 1e4
    #K_MAX_IT = max(n1, n2);
    
    # Segregates different types of materials
    mask = np.zeros([n1,n2])
    for a in range(0, n1):
        for b in range(0, n2):
            if t1[a] != t2[b]:
                mask[a,b] = -realmax

    #K0 = K0 * np.exp(mask)
    arg = -c #Make C approximately one

    rs1 = np.zeros([n1])
    cs1 = np.zeros([n2])

    u = np.zeros([n1])
    v = np.zeros([n2])

    eps_s = np.fmax(np.max(c), 4. * EPS_F)
    eps_range = eps_s / EPS_F
    #print(np.ceil(np.log(eps_range)/np.log(4.)))



    k = int(np.ceil(np.log(eps_range) / np.log(4.)))
    l = eps_range**(-1. / k)
    E = np.append(eps_s * np.power(l, np.arange(0, k)), EPS_F)
    T = np.append(np.fmax(E[0:k] / 100., TOL_F), TOL_F)
   
    iterList = [0,0,0,0,0]

    for a in range(E.size):
        m_iter = 0
        m_not_converged = True
        while m_not_converged:
            rm = np.ones([n1,1]) #a
            cm = np.ones([n2,1]) #b
            K1 = np.exp(arg / E[a])
            Kc = np.sum(K1, 1) #column sums
            Kr = np.sum(K1, 0) #row sums
            while m_not_converged and (np.abs(rm) < THRES).all() and (np.abs(cm) < THRES).all():
                #HERE, corresponds with line 298 to 351 c code.
                #54 to 74, matlab
                
                rm = m1 / Kc #row multiplier
                rmr = np.exp(-(l1 + u) / E[a]) 
                rm = np.fmax(rm, rmr)
                rmr = np.exp((l1 - u) / E[a])
                #print(np.max(rmr), np.min(rmr), 'fmin:', np.min(rm))
                #if np.max(rmr) == np.inf:
                #    quit()
                #lambda Total Variation, proxdivF(s, u, e)
                rm = np.fmin(rm, rmr)
                #Kr = K1.transpose().dot(rm)
                Kr = np.matmul(K1.transpose(), rm)

                #rs0 = rs1
                #cs0 = cs1
                #print('rm:',rm)
                #print('Kc:',Kc)
                #rs0 = rm * Kc
                #cs0 = cm * Kr

                cm = m2 / Kr
                cmr = np.exp(-(l2 + v) / E[a])
                cm = np.fmax(cm, cmr)
                cmr = np.exp((l2 - v) / E[a])
                cm = np.fmin(cm, cmr)
                Kc = np.matmul(K1,cm)
                #Kc = K1.dot(cm)

                #NOTES: Imbalance on the final coupling matrix comes from the algorithem scaling
                #the matrix overall by a constant to which ever mass it hits last, aka m2.

                rs0 = rs1
                cs0 = cs1
                #print('rm:',rm)
                #print('Kc:',Kc)
                rs1 = rm * Kc
                cs1 = cm * Kr
                #print(np.abs(rs1 - rs0), np.abs(cs1-cs0), T[a]) 
                threshConv1 =(np.abs(rs1 - rs0) > T[a] * n2).any()
                threshConv2=(np.abs(cs1-cs0) > T[a] * n1).any()
                iterConv=(m_iter < M_MAX_IT)
                threshCheck1 = (np.abs(rm) < THRES).all()
                threshCheck2 = (np.abs(cm) < THRES).all()
                #m_not_converged = ((rs1 - rs0) > T[a] * n2).any() or (np.abs(cs1-cs0) > T[a] * n1).any() and (m_iter < M_MAX_IT)
                m_not_converged=(threshConv1 or threshConv2) and iterConv
                #TO HERE
                m_iter += 1
                #print('E:', E[a])
                #print('iter:', m_iter)
                #print('rm:',rm) 
                #print('cm:',cm) 
                #print('u:',u) 
                #print('v:',v) 
            #print(threshConv1, threshConv2, iterConv, m_not_converged, '|', threshCheck1, threshCheck2)
            iterList[0] += int(not threshConv1)#Checks for convergence to an answer
            iterList[1] += int(not threshConv2)#Checks for convergence to an answer.
            iterList[2] += int(not iterConv) #Sets maximum iteration number
            iterList[3] += int(not threshCheck1) #Checks if row multipliers are too large
            iterList[4] += int(not threshCheck2) #Checks if col multipliers are too large

            u = u + E[a] * np.log(rm)
            v = v + E[a] * np.log(cm)
            
            for b in range(0, n2):
                arg[:, b] = -c[:, b] + u.transpose() + v[b]# + mask[:, b]
        #print(K1)

    K1 = np.exp(arg / E[a])
    K = K1
    #print(m_iter)
    Csum =  np.sum(K*c)
    if safeC:
        Csum = Csum*maxC
    if not isinstance(usePenaltyFinal, bool) and np.isreal(usePenaltyFinal):
        l2 = usePenaltyFinal
        l1 = usePenaltyFinal
    elif isinstance(usePenaltyFinal, bool) and usePenaltyFinal == False:
        l2 = 1 
        l1 = 1 
    elif isinstance(usePenaltyFinal, str) and usePenaltyFinal == 'frac': #fraction of the original
        l1 = l1in*endlmult
        l2 = l2in*endlmult
    else:
        l1 = l1*endlmult
        l2 = l2*endlmult

    Psum2 = np.sum(l2*np.abs(np.sum(K,0).transpose() - m2)) 
    Psum1 = np.sum(l1 * np.abs(np.sum(K,1) - m1))
    etime = time.time()

    #print(K)
    #print(iterList)
    print('lmult: {}, swapMass: {}, usePenaltyFinal: {}, endlmult: {}, Time: {:.3f} s'.format(lmult, swapMass, usePenaltyFinal, endlmult, (etime-stime))) 
    print('Coupling Term : ', Csum)
    print('Penalty Term 1: ', Psum1)
    print('Penalty Term 2: ', Psum2)
    #print('l1:', l1)
    #print('l2:', l2)
    print('(M_MAX_IT:{}, THRES:{})\nEnd Reasons: convergence=[uv1:{}, uv2:{}, m_iter:{}], mult_scale=[uv1:{}, uv2:{}]'.format(M_MAX_IT, THRES, iterList[0], iterList[1], iterList[2], iterList[3], iterList[4]))

    D = Csum + Psum2 + Psum1
    return D, K, Csum, Psum1, Psum2

seed = 1
def solver2dPrewrite(m1s, m2s, tempDir='../dottemp', idStr='', roundMult=1e6, addNorm=False):
    rng1 = np.random.RandomState(seed)
    rng2 = np.random.RandomState(seed)
    #takes in a list of windows, does the mass blanacing, and then writes them all to a location
    print('Prewriting 2dhist windows to', tempDir)
    print('Normalizing by Addition:', addNorm)
    try:
        os.mkdir(tempDir)
    except FileExistsError:
        pass
    fpList1=[]
    fpList2=[]
    t = '{}-{}-{}'.format(time.localtime()[4], time.localtime()[5], time.localtime()[6])
    for j in range(len(m1s)):
        m1=m1s[j]
        m2=m2s[j]
        if len(m1s.shape) == 1:
            m1=m1.reshape((int(np.ceil(np.sqrt(m1.size))),int(np.ceil(np.sqrt(m1.size)))))
        if len(m2.shape) == 1:
            m2=m2.reshape((int(np.ceil(np.sqrt(m2.size))),int(np.ceil(np.sqrt(m2.size))))) #1d to 2d, assumes square
        #print(m1, m1.shape)
        if np.sum(m1) == 0:
            m1sum = 1
        else:
            m1sum = np.sum(m1)
        if np.sum(m2) == 0:
            m2sum = 1
        else:
            m2sum = np.sum(m2)
        if addNorm:
            m1 = m1 + np.ceil((roundMult - m1sum)/m1.size)
            m2 = m2 + np.ceil((roundMult - m2sum)/m2.size) #make sure roundMult is larger than m1sum
        else:
            m1 = np.ceil(m1*(roundMult/m1sum))
            m2 = np.ceil(m2*(roundMult/m2sum)) #need to be exactly the same mass, including overflow
        m1over = int(np.sum(m1) - roundMult)
        m2over = int(np.sum(m2) - roundMult)
        #print('Overmass:', m1over, m2over)
        #Scatters mass balancing noise to get exact mass ratio. This means there is a slight stochastic noise!
        for i in np.arange(0, abs(m1over)):
            if m1over >0:
                m1[rng1.randint(0, m1.shape[0]), rng1.randint(0, m1.shape[1])]-=1
            else: 
                m1[rng1.randint(0, m1.shape[0]), rng1.randint(0, m1.shape[1])]+=1
        for i in np.arange(0, abs(m2over)):
            if m2over >0:
                m2[rng2.randint(0, m2.shape[0]), rng2.randint(0, m2.shape[1])]-=1
            else:
                m2[rng2.randint(0, m2.shape[0]), rng2.randint(0, m2.shape[1])]+=1

        fp1 = '{}/win1-{}-{}-{}.csv'.format(tempDir,t, idStr, j)
        fp2 = '{}/win2-{}-{}-{}.csv'.format(tempDir,t, idStr, j)
        fpList1.append(fp1)
        fpList2.append(fp2)
        np.savetxt(fp1, np.ceil(m1), delimiter=',', fmt='%1i')
        np.savetxt(fp2, np.ceil(m2), delimiter=',', fmt='%1i')
    return fpList1, fpList2 
        
def solver2dCleanup(fpList1, fpList2):
    #Removes windows after a run
    for i in range(len(fpList1)):
        try:
            os.remove(fpList1[i])
        except FileNotFoundError:
            print('File not Found for Deletion:', fpList1[i])
            pass
        try:
            os.remove(fpList2[i])
        except FileNotFoundError:
            print('File not Found for Deletion:', fpList2[i])
            pass

def solver2d(m1, m2, tempDir='../dottemp', idStr='', libPath = '/usr/local/solver.so', roundMult=1e6, timeout=120, addNorm = False):
    #Calls the Discrete Optimal Transport (DOT) library for 2D histograms
    #Bassetti F., Gualandi S., Veneroni M. On the Computation of Kantorovich-Wasserstein Distances between 2D-Histograms by Uncapacitated Minimum Cost Flows. Available on arXiv. Submitted on April, 2nd, 2018.

    #m1 and m2 are either 2d histograms or not. They will get balanced and multiplied by roundMult
    #Since they need to be the same mass and integers.
    #Increadibly hacky method.
    try:
        os.mkdir(tempDir)
    except FileExistsError:
        pass
    t = '{}-{}-{}'.format(time.localtime()[4], time.localtime()[5], time.localtime()[6])
    if isinstance(m1, str):
        fp1 = m1
        csv1 = open(fp1, 'r')
        n1 = len(csv1.readline().split(','))
        csv1.close()
    else:
        if len(m1.shape) == 1:
            m1=m1.reshape((int(np.ceil(np.sqrt(m1.size))),int(np.ceil(np.sqrt(m1.size)))))
        m1 = np.ceil(m1*(roundMult/np.sum(m1)))
        n1 = m1.shape[0]
        m1over = int(np.sum(m1) - roundMult)
        for i in np.arange(0, abs(m1over)):
            if m1over >0:
                m1[np.random.randint(0, m1.shape[0]), np.random.randint(0, m1.shape[1])]-=1
            else: 
                m1[np.random.randint(0, m1.shape[0]), np.random.randint(0, m1.shape[1])]+=1

        i=0
        fileExists=True
        while fileExists:
            fp1 = '{}/win1-{}-{}-{}.csv'.format(tempDir,t, idStr, i)
            if i > 1:
                print('Searching for:', fp1)
            fileExists = os.path.isfile(fp1)
        np.savetxt(fp1, np.ceil(m1), delimiter=',', fmt='%1i')

    if isinstance(m2, str):
        fp2 = m2
        csv2 = open(fp2, 'r')
        n2 = len(csv2.readline().split(','))
        csv2.close()
    else:
        if len(m2.shape) == 1:
            m2=m2.reshape((int(np.ceil(np.sqrt(m2.size))),int(np.ceil(np.sqrt(m2.size))))) #1d to 2d, assumes square
            #m1=m1*(1/min(m1[np.nonzero(m1!=0)])) #If m1 is regularized, this turns it back into integers
            #m2=m2*(1/min(m2[np.nonzero(m2!=0)])) #If m1 is regularized, this turns it back into integers
            #m1 = m1*roundMult
            #m2 = m2*roundMult
            n2 = m2.shape[0]
            m2 = np.ceil(m2*(roundMult/np.sum(m2))) #need to be exactly the same mass, including overflow
            m2over = int(np.sum(m2) - roundMult)
            #Scatters mass balancing noise to get exact mass ratio. This means there is a slight stochastic noise!
            for i in np.arange(0, abs(m2over)):
                if m2over >0:
                    m2[np.random.randint(0, m2.shape[0]), np.random.randint(0, m2.shape[1])]-=1
                else:
                    m2[np.random.randint(0, m2.shape[0]), np.random.randint(0, m2.shape[1])]+=1

        i=0
        fileExists=True
        while fileExists:
            fp1 = '{}/win1-{}-{}-{}.csv'.format(tempDir,t, idStr, i)
            fp2 = '{}/win2-{}-{}-{}.csv'.format(tempDir,t, idStr, i)
            if i > 1:
                print('Searching for:', fp2)
            fileExists = os.path.isfile(fp2)
        np.savetxt(fp2, np.ceil(m2), delimiter=',', fmt='%1i')

    try:
        out = sub.run([libPath, '-sa', '--h1={}'.format(fp1), '--h2={}'.format(fp2)],stdout=sub.PIPE, timeout=timeout)
        dist = int(out.stdout.split()[19].decode().strip(','))
    except sub.TimeoutExpired:
        print('Reached Timeout of {}s'.format(timeout))
        dist = 0
    
    #massM1 = np.sum(np.loadtxt(fp1, delimiter=','))
    #massM2 = np.sum(np.loadtxt(fp2, delimiter=','))

    if not isinstance(m1, str):
        os.remove(fp1)
    if not isinstance(m2,str):
        os.remove(fp2) #cleanup

    #return dist/roundMult*1e2
    #print('WINDOWS LMAO:', n1, dist/(roundMult*n1))
    if addNorm:
        return dist/(n1*n2)
    else:
        return dist/(roundMult)



##DEPRECIATED
def softGW(m1,m2,d1,d2,l1,l2,lmult = 1,K0=False, t1=False,t2=False):
    #Transliteration of a matlab function gromov_wasser written by Jeremy Mason into python. Plus a bit of other stuff.

    #Where K0 is the initial kernal. If K0 is nothing, then I ASSUME it's a n1xn2 of ones
    #where m1, m2 are the measures (masses). numpy array, vector.
    #where t1, t2 are the types of each node. should all be 1 for our purposes.
    #where d1, d2 are the distances of their respective graphs. Matrix of n1xn1 and n2xn2 by all purposes
    #where l1, l2 are the penality measures. They are n1 and n2 size respectively
    #If elements of l1 and l2 goes to infinity, then this problem becomes the normal GW optimization.
    #lmult is the multiplier on the penalties.
    #Solves for the unbalanced GW problem:
    #GW = min( (1/2) sum ii'(sum jj'(|d1(i,i') - d2(j,j')|*mu(i,j)*mu(i',j'))) + sum i (l1*|m1(i)-sum j(mu(i,j))) + sum j (l2*|m2(j) - sum i(mu(i,j)))
    
    #print('d1:', d1)
    #print('d2:', d2)
    #print('m1:', m1)
    #print('m2:', m2)
    #print('l1:', l1)
    #print('l2:', l2)
    
    #constants
    n1 = m1.size
    n2 = m2.size
    eps = np.finfo(np.float64).eps
    realmax = np.finfo(np.float64).max

    #Making things default
    if isinstance(K0, type(False)) and K0 == False:
        K0 = np.ones((n1,n2)) / (n1 * n2)
    if isinstance(t1, type(False)) and t1 == False:
        t1 = np.ones(n1)
    if isinstance(t2, type(False)) and t2 == False:
        t2 = np.ones(n2)

    l1 = l1 * lmult
    l2 = l2 * lmult


    #Experimental, normalization
    #m1 = m1 / np.sum(m1)
    #m2 = m2 / np.sum(m2)

    #Adjustable
    EPS_F = 1e-6 #CHANGE THIS VALUE IF THINGS START BREAKING
    TOL_F = 1e-8
    TOL_D = 100. * eps
    THRES = 1000.
    M_MAX_IT = 1e5
    K_MAX_IT = max(n1, n2);
    
    # Segregates different types of materials
    mask = np.zeros([n1,n2])
    for a in range(0, n1):
        for b in range(0, n2):
            if t1[a] != t2[b]:
                mask[a,b] = -realmax

    #Cost Matrix
    C = np.zeros([n1 *n2, n1*n2])

    for a in range(0, n2):
        for b in range(0, n2):
            blocka = np.arange(0,n1) + a * n1
            blockb = np.arange(0,n1) + b * n1
            C[blocka[0]:blocka[-1]+1, blockb[0]:blockb[-1]+1] = np.abs(d1 - d2[a,b])

    K0 = K0 * np.exp(mask)
    c = np.reshape(np.matmul(C, np.reshape(K0, -1)), (n1, n2)) / 2.
    arg = -c + mask

    cols = np.zeros([n2])

    allmIter =[]
    k_iter = 0
    while True:
        #UNBALANCED WASSERSTEIN
        #c is distance matrix
        #m1, m2, masses.
        rs1 = np.zeros([n1])
        cs1 = np.zeros([n2])

        u = np.zeros([n1])
        v = np.zeros([n2])

        eps_s = np.fmax(np.max(c), 4. * EPS_F)
        eps_range = eps_s / EPS_F
        #print(np.ceil(np.log(eps_range)/np.log(4.)))


        k = int(np.ceil(np.log(eps_range) / np.log(4.)))
        l = eps_range**(-1. / k)
        E = np.append(eps_s * np.power(l, np.arange(0, k)), EPS_F)
        T = np.append(np.fmax(E[0:k] / 100., TOL_F), TOL_F)
        
        for a in range(E.size):
            m_iter = 0
            m_not_converged = True
            while m_not_converged:
                rm = np.ones([n1,1]) #u
                cm = np.ones([n2,1]) #v
                K1 = np.exp(arg / E[a])
                Kc = np.sum(K1, 1)

                while m_not_converged and (np.abs(rm) < THRES).all() and (np.abs(cm) < THRES).all():
                    #HERE, corresponds with line 298 to 351 c code.
                    #54 to 74, matlab
                    rm = m1 / Kc
                    rmr = np.exp(-(l1 + u) / E[a]) 
                    rm = np.fmax(rm, rmr)
                    rmr = np.exp((l1 - u) / E[a])
                    #lambda Total Variation, proxdivF(s, u, e)
                    rm = np.fmin(rm, rmr)
                    Kr = K1.transpose().dot(rm)

                    cm = m2 / Kr
                    cmr = np.exp(-(l2 + v) / E[a])
                    cm = np.fmax(cm, cmr)
                    cmr = np.exp((l2 - v) / E[a])
                    cm = np.fmin(cm, cmr)
                    Kc = K1.dot(cm)

                    rs0 = rs1
                    cs0 = cs1
                    #print('rm:',rm)
                    #print('Kc:',Kc)
                    rs1 = rm * Kc
                    cs1 = cm * Kr
                   
                    threshConv1 =((rs1 - rs0) > T[a] * n2).any()
                    threshConv2=(np.abs(cs1-cs0) > T[a] * n1).any()
                    iterConv=(m_iter < M_MAX_IT)
                    
                    m_not_converged=(threshConv1 or threshConv2) and (iterConv)
                    #m_not_converged = ((rs1 - rs0) > T[a] * n2).any() or (np.abs(cs1-cs0) > T[a] * n1).any() and (m_iter < M_MAX_IT)
                    #TO HERE
                    m_iter += 1
                
                u = u + E[a] * np.log(rm)
                v = v + E[a] * np.log(cm)

                for b in range(0, n2):
                    arg[:, b] = -c[:, b] + u.transpose() + v[b] + mask[:, b]
            allmIter.append(m_iter)
        #END HERE
        #any updates to K and c are from the gw
        k_iter += 1

        K1 = np.exp(arg / EPS_F)
        if (np.abs(K1[:] - K0[:]) < TOL_F).all() or (k_iter >= K_MAX_IT):
            break
        K0 = K1

        c = np.reshape(C.dot(K1.reshape(-1)), (n1,n2))/2

    K = K1
    D = K1.reshape(-1).dot(C).dot(K1.reshape(-1)) / 2. + np.sum(l2*np.abs(np.sum(K1,0).transpose() - m2)) +np.sum(l1 * np.abs(np.sum(K1,1) - m1))

    print(np.mean(np.array(allmIter)), np.std(np.array(allmIter)))

    return D, K

def plusFunc(x):
    if x > 0:
        return x
    else:
        return 0

def gaussian(x, mu, sigma):
    return (1/(sigma*np.sqrt(2*np.pi)))*np.exp(-np.power(x - mu, 2.) / (2 * np.power(sigma, 2.)))

import string
def parsenprepr(fn):
    reprFile = open(fn, 'r')
    data = []
    lines = reprFile.readlines()
    for line in lines:
        l = line.replace(']', '').replace('[', '')
        lineData = [float(val.strip()) for val in l.split() if set(val.replace('.', '')).issubset(set(string.digits))]
        if len(lineData) > 0:
            data.append(lineData)
    returnArray = np.zeros((len(data), len(data[0])))
    for i in range(len(data)):
        returnArray[i,:] = np.array(data[i])
    return returnArray

if __name__ == '__main__':
    np.set_printoptions(linewidth = np.inf, threshold=sys.maxsize, precision=4)
    gw_wasser_fun = gw_wass('libgwdist.so', '/usr/local/lib/');
    gw_gromov_fun = gw_gromov('libgwdist.so', '/usr/local/lib/');
    # Create the python function object.

    try: n1 = int(sys.argv[1])
    except: n1 = 5
    n2 = n1
    print('Running: {} x {}'.format(n1, n2))
    #C = np.random.random((n1,n2))*2+3
    #C = np.zeros((n1, n2))
    #C[:, 4] = np.inf
    C = parsenprepr('C2.txt')
    #C = C[n1:-1, n2:-1]
    print('C:\n',C.shape, np.min(C), np.max(C))
    try: score, gamma= hardW(C, C.shape[0], C.shape[1], etaFMult=1)
    except: score = 'failed'
    print('alter')
    try: scorenorm, gammanorm= hardW(C*100, C.shape[0], C.shape[1], normGamma = False, normMass=True)
    except: scorenorm = 'failed'
    scorec, gammac= gw_wasser_fun(C, C.shape[0], C.shape[1])
    
    #print('gamma:\n', gamma)
    print('score:', score, score, scorenorm, scorec)

#    import matplotlib.pyplot as plt
#    #and softGW!
#    #Get input
#    try:
#        measureLen1 = int(sys.argv[1])
#    except:
#        measureLen1 = 5
#    
#    try:
#        measureLen2 = int(sys.argv[2])
#    except:
#        measureLen2 = 5
#    print('Running a {ml1} by {ml2} GW test.'.format(ml1 = measureLen1, ml2 = measureLen2))
#    fig, ax = plt.subplots(2,4)
#    ax = ax.flatten()
#    
#
#    hardScores = []
#    hardpyScores = []
#    softScores = []
#    transRatio1 = []
#    transRatio2 = []
#    hardTimeList = []
#    softTimeList = []
#    hardTimePyList = []
#    measureLin = np.array([20]) 
#    #measureLin = np.linspace(5,20, 15)
#    #measureLin = np.linspace(0.25, 20)
#    for ni in range(measureLin.size):
#        measureLen1 = int(measureLin[ni])
#        measureLen2 = int(measureLin[ni])
#        #measureLen1 = 100
#        #measureLen2 = 100
#        mmult = 1 #measureLin[ni]
#        print('ni:', ni, '-', measureLen1, 'by', measureLen2)
#        optionsDict = {
#                'windowNum':1,
#                'axis':'y',
#                'slice':'half',
#                'relativeScale':False,
#                'totalAreaHold' : 1,
#                'noGrainInWindow':4,
#                'forceWindowLen':measureLen1, #Overrides noGrainInWindow
#                'winSizeDetermine':'max',
#                'distMultiplier':1,
#                'centerOnRandGrain': 'center',
#                'distType' : 'sdf',
#                'sdfDistMin': 1,
#                'sdfPenaltyType':'constesd',
#                'ignoreBoundaryEdges':False,
#                'verbose':False,
#                'forceCellLength':True,
#                'sdfNormalize':True #NOTE! This forces each window to have the same mass, intended to be used with the balanced/hard wasserstein
#        }
#
#
#        testType = 'sample'
#        if testType == 'sample':
#            import pixelMethod as pm
#            import plotUtil as pu
#            inSample1 = '../dream3d-samples/Mon-Jan-4-2xEquiaxed-regen.dream3d'
#            inSample2 = '../dream3d-samples/Mon-Jan-4-Equiaxed-regen.dream3d'
#            #inSample2 = '../dream3d-samples/eq-bimodal-05x-matrix.dream3d'
#            #inSample1 = '../dream3d-samples/Mon-Jan-4-Equiaxed-regen.dream3d'
#            dataDictsList = pm.generateWindowData(inSample1, inSample2, optionsDict1 = optionsDict, optionsDict2 = optionsDict)
#            Cwass = dataDictsList['CostMatSDF'][0]
#            m1 = dataDictsList['uv1SDF'][0]
#            m2 = dataDictsList['uv2SDF'][0]
#            l1 =  dataDictsList['penaEdge1SDF'][0]
#            l2 = dataDictsList['penaEdge2SDF'][0]
#            pu.plotMicroStruct([m1, m2], axs=ax[4:])
#        elif testType == 'offset':
#            import pixelMethod as pm
#            import sampleGenerator as sg
#            import plotUtil as pu
#            
#            inSample1 = sg.offsetSquare(measureLen1+3, grainSize = 6, xoffset=0, yoffset=0)
#            inSample2 = sg.offsetSquare(measureLen1+3, grainSize = 6, xoffset=0, yoffset=0)
#            dataDictsList = pm.generateWindowData(inSample1, inSample2, optionsDict1 = optionsDict, optionsDict2 = optionsDict)
#            Cwass = dataDictsList['CostMatSDF'][0]
#            m1 = dataDictsList['uv1SDF'][0]
#            m2 = dataDictsList['uv2SDF'][0]
#            l1 =  dataDictsList['penaEdge1SDF'][0]
#            l2 = dataDictsList['penaEdge2SDF'][0]
#            pu.plotMicroStruct([inSample1, inSample2, np.resize(m1*np.sum(inSample1), (measureLen1,measureLen2)), np.resize(m2*np.sum(inSample2), (measureLen1, measureLen2))], axs=ax[4:])
#            #plt.show()
#            #quit()
#
#        else:
#            # Measures for the spaces.
#            m1 = np.zeros((measureLen1), dtype = np.float64, order = 'F')
#            m2 = np.zeros((measureLen2), dtype = np.float64, order = 'F')
#            xvals1 = np.linspace(0, 1, measureLen1)
#            xvals2 = np.linspace(0, 1, measureLen2)
#            for i in range(0, measureLen1):
#                m1[i] = (gaussian(xvals1[i], 0.3, 0.2)+1)*mmult
#            for i in range(0, measureLen2):
#                m2[i] = (gaussian(xvals2[i], 0.7, 0.2)+1)*mmult
#
#            #maxm = max(np.max(m1), np.max(m2))
#            #m1 = np.array(m1/maxm, dtype = np.float64, order = 'F')
#            #m2 = np.array(m2/maxm, dtype = np.float64, order = 'F')
#            
#            m1 = np.array(m1, dtype = np.float64, order = 'F')
#            m2 = np.array(m2, dtype = np.float64, order = 'F')
#
#            ax[4].plot(xvals1, m1)
#            ax[5].plot(xvals2, m2)
#            # Types of the points.
#            t1 = np.zeros(measureLen1, dtype = np.uint32, order = 'F');
#            t2 = np.zeros(measureLen2, dtype = np.uint32, order = 'F');
#            
#            t1[0] = 1;
#            t2[len(t2)-1] = 1;
#
#            # Transportation costs.
#            Cwass = np.zeros((measureLen1, measureLen2), dtype = np.float64, order = 'F')
#            for i in range(0, measureLen1):
#                for j in range(0, measureLen2):
#                    Cwass[i,j] = abs(i-j)
#            #C1 = np.zeros((measureLen1, measureLen1), dtype = np.float64, order = 'F')
#            #for i in range(0, measureLen1):
#            #    for j in range(0, measureLen1):
#            #        C1[i,j] = abs(i-j)
#            #C2 = np.zeros((measureLen2, measureLen2), dtype = np.float64, order = 'F')
#            #for i in range(0, measureLen2):
#            #    for j in range(0, measureLen2):
#            #        C2[i,j] = abs(i-j)
#
#            Cwass = Cwass
#            
#            G = np.zeros(measureLen1*measureLen2, order = 'F')
#
#            #soft l1,l2
#            l1 = np.zeros((measureLen1), dtype = np.float64, order = 'F')+1
#            l2 = np.zeros((measureLen2), dtype = np.float64, order = 'F')+1
#
#        #transRatio1.append(np.max(m1)/np.max(Cwass))
#        #transRatio2.append(np.min(m1)/np.max(Cwass))
#
#        #print('m1:')
#        #print(m1, 'sum:', np.sum(m1))
#        #print('m2:')
#        #print(m2, 'sum:', np.sum(m2))
#        #print('Cwass:')
#        #print(Cwass)
#        #print('l1:')
#        #print(l1)
#        #print('l2:')
#        #print(l2)
#        #print('C1:')
#        #print(C1)
#        #print('C2:')
#        #print(C2)
#        #print('t1:')
#        #print(t1)
#        #print('t2:')
#        #print(t2)
#
#        # Actually performs the calculation.
#        convergeTest = False
#
#        if convergeTest:
#            notConverged=True
#            mmult=1
#            prevmmult = 1
#            totalmmult = 0
#            m1t = m1.copy()
#            m2t = m2.copy()
#            while notConverged:
#                baseline = np.min(m1)
#                m1 = (m1-baseline)*(mmult/prevmmult) + baseline
#                baseline = np.min(m2)
#                m2 = (m2-baseline)*(mmult/prevmmult) + baseline
#                
#                m2 = m2 * (np.sum(m1)/np.sum(m2)) #Reguarlize s.t they have same mass.
#
#                stime = time.time() 
#                wassDist, wassGamma = gw_wasser_fun(Cwass, m1, m2, regularM=False)
#                hardtime = time.time() - stime
#                #print(wassDist)
#                wassGamma = wassGamma
#                #lmultLin = np.linspace(0.5, 2000, 20)
#                lmultLin = np.array([100]) 
#                distLin = np.zeros(lmultLin.size)
#                CsumLin = np.zeros(lmultLin.size)
#                Psum1Lin = np.zeros(lmultLin.size)
#                Psum2Lin = np.zeros(lmultLin.size)
#
#                #m1 = m1 / np.sum(m1)
#                #m2 = m2 / np.sum(m2)
#                stime = time.time()
#                wasspyDist, wasspyGamma = hardW(Cwass, m1, m2)
#                hardpytime = time.time() - stime
#                
#                lmult = lmultLin[0]
#                stime = time.time()
#                dist, gamma, Csum, Psum1, Psum2 = softW(Cwass, m1, m2, l1, l2 ,lmult = lmult, swapMass = False)
#                softtime = time.time() - stime
#                distLin[0] = dist
#                CsumLin[0] = Csum
#                Psum1Lin[0] = Psum1
#                Psum2Lin[0] = Psum2
#                print(measureLen1, ':', mmult, ':', dist, wassDist, wasspyDist, dist/wassDist - 1)
#                notConverged= np.abs((dist/wassDist)-1) > 0.1 #Wait until they converge
#                        
#                prevmmult = mmult
#                mmult+=2
#                #print(wassGamma)
#                #print(np.sum(wassGamma, 1))
#                #print('soft:')
#                #print(gamma.transpose())
#                #print(np.sum(gamma.transpose(), 1))
#            
#            baseline = np.min(m1t)
#            m1t = (m1t-baseline)*mmult + baseline
#            baseline = np.min(m2t)
#            m2t = (m2t-baseline)*mmult + baseline
#            #m2t = m2t * (np.sum(m1t)/np.sum(m2t)) #Reguarlize s.t they have same mass.
#            print('hello', np.max(m1t-m1), np.max(m2t-m2))
#            print(m1)
#            print(m1t)
#            #dist, gamma, Csum, Psum1, Psum2 = softW(Cwass, m1t, m2t, l1, l2 ,lmult = lmult, swapMass = False)
#            #distLin[0] = dist
#            #CsumLin[0] = Csum
#            #Psum1Lin[0] = Psum1
#            #Psum2Lin[0] = Psum2
#
#
#            hardTimeList.append(hardtime)
#            hardTimePyList.append(hardpytime)
#            softTimeList.append(softtime)
#            print('Times:', hardtime, hardpytime, softtime)
#            hardScores.append(wassDist)
#            hardpyScores.append(wasspyDist)
#            softScores.append(dist)
#            transRatio1.append(np.max(m1)/np.max(Cwass))
#            transRatio2.append(mmult)
#
#        else:
#            stime = time.time() 
#            wassDist, wassGamma = gw_wasser_fun(Cwass, m1, m2, regularM=False)
#            hardtime = time.time() - stime
#            hardTimeList.append(hardtime)
#            print('Hard Dist:', wassDist)
#            wassGamma = wassGamma
#            #lmultLin = np.linspace(0.5, 10000, 20) #LMULT NEEDS TO INCREASE WITH THE MAX DISTANCE TO ENSURE 
#            lmultLin = np.array([4*measureLen1]) 
#            distLin = np.zeros(lmultLin.size)
#            CsumLin = np.zeros(lmultLin.size)
#            Psum1Lin = np.zeros(lmultLin.size)
#            Psum2Lin = np.zeros(lmultLin.size)
#            print('sum:', np.sum(m1))
#            #m1 = m1 / np.sum(m1)
#            #m2 = m2 / np.sum(m2)
#            stime = time.time()
#            wasspyDist, wasspyGamma = hardW(Cwass, m1, m2)
#            print('Hard Py Score:', wasspyDist) 
#            #wasspyDist, gamma, Csum, Psum1, Psum2 = softW(Cwass, m1, m2, l1, l2 ,lmult = lmultLin[0], swapMass = False)
#            softtime = time.time() - stime
#            
#            hardpytime = time.time() - stime
#            hardTimePyList.append(hardpytime)
#            for i in range(lmultLin.size):
#                lmult = lmultLin[i]
#                stime = time.time()
#                #maxC = np.max(Cwass)
#                #maxC = 1.
#                dist, gamma, Csum, Psum1, Psum2 = softW(Cwass, m1, m2, l1, l2 ,lmult = lmult, swapMass = False)
#                softtime = time.time() - stime
#                distLin[i] = dist
#                CsumLin[i] = Csum
#                Psum1Lin[i] = Psum1
#                Psum2Lin[i] = Psum2
#            softTimeList.append(softtime)
#            print('Times:', hardtime, hardpytime, softtime)
#            hardScores.append(wassDist)
#            hardpyScores.append(wasspyDist)
#            softScores.append(dist)
#            #print(wassGamma)
#            #print(np.sum(wassGamma, 1))
#            #print('soft:')
#            #print(gamma.transpose())
#            #print(np.sum(gamma.transpose(), 1))
#    hardScores = np.array(hardScores)
#    hardpyScores = np.array(hardpyScores)
#    softScores = np.array(softScores)
#
#    testFile = open('testFile.txt', 'w')
#    np.savetxt(testFile, gamma, fmt='%.2e')
#    testFile.close()
#    a=ax[0].plot(lmultLin, distLin, marker='o')
#    a[0].set_label('Total Score')
#    a=ax[0].plot(lmultLin, CsumLin, marker = 'o')
#    a[0].set_label('Coupling Score')
#    a=ax[0].plot(lmultLin, Psum1Lin, marker = 'o')
#    a[0].set_label('Penalty 1 Score')
#    a=ax[0].plot(lmultLin, Psum2Lin, marker='o')
#    a[0].set_label('Penalty 2 Score')
#    a=ax[0].axhline(wassDist, color = 'k')
#    ax[0].set_xlabel('lmult')
#    ax[0].set_ylabel('score')
#    ax[0].legend()
#    ax[1].pcolor(wassGamma)
#    ax[2].pcolor(gamma)
#    
#    a=ax[3].plot(measureLin, hardScores, marker='o')
#    a[0].set_label('Bal. C')
#    a=ax[3].plot(measureLin, softScores, marker='o')
#    a[0].set_label('Unbal. Py')
#    a=ax[3].plot(measureLin, hardpyScores, marker='o')
#    a[0].set_label('Bal. Py')
#    ax[3].set_xlabel('windowSize')
#    ax[3].set_ylabel('score')
#    #a=ax[7].plot(measureLin, transRatio1)
#    #a[0].set_label('max(m1)/max(C)')
#    #ax2 = ax[7].twinx()
#    #a=ax2.plot(measureLin, transRatio2, color='tab:orange')
#    #a[0].set_label('mmult')
#    #ax[7].set_xlabel('windowSize')
#    ax[3].legend()
#    #ax[7].legend()
#
#    a=ax[6].plot(measureLin, hardTimeList, marker='o')
#    a[0].set_label('Bal. C')
#    a=ax[6].plot(measureLin, softTimeList, marker='o')
#    a[0].set_label('Unbal. Py')
#    a=ax[6].plot(measureLin, hardTimePyList, marker='o')
#    a[0].set_label('Bal. Py')
#    ax[6].set_xlabel('windowSize')
#    ax[6].set_ylabel('time (s)')
#
#    ax[6].legend()
#    plt.show()
#    
#    #wassDist1, wassGamma1 = softW(Cwass, m1, m2, l1, l2 ,lmult = 1, swapMass = False)
#    #wassDist2, wassGamma2a = softW(Cwass, m1, m2, l1, l2, lmult = 1000, swapMass= False)#, K0 = wassGamma1) 
#    #print(wassDist1, wassDist2)
#    #wassDist1, wassGamma1 = softW(Cwass, m2, m1, l2, l1, lmult = 1, swapMass = False)
#    #wassDist2, wassGamma2b = softW(Cwass, m2, m1, l2, l1, lmult = 1000, swapMass = False)#, K0 = wassGamma1) 
#    #print(wassDist1, wassDist2)
#    #print(wassGamma2a, '\n')
#    
#    #mults = np.linspace(1, 1000, 30)
#    #dists1 = np.zeros(mults.size)
#    #dists2 = np.zeros(mults.size)
#    #for i in range(mults.size):
#    #    mult = mults[i]
#    #    wassDist2a, wassGamma2a = softW(Cwass, m1, m2, l1, l2, lmult = mult, swapMass=False)#, K0 = wassGamma1) 
#    #    wassDist2b, wassGamma2b = softW(Cwass, m2, m1, l2, l1, lmult = mult, swapMass=False)#, K0 = wassGamma1) 
#    #    dists1[i] = wassDist2a
#    #    dists2[i] = wassDist2b
#    #print(mults)
#    #print(dists1)
#    #print(dists2)
#    #print(np.min(dists1), mults[np.nonzero((dists1 == np.min(dists1)))[0][0]])
#    #print(np.min(dists1), mults[np.nonzero((dists2 == np.min(dists2)))[0][0]])
#    
#    #DISTANCES EXPLODE WHEN PENALTY REACHES N*2.5 for a C of NxN Size 
#
#    #print(wassGamma)
#    #softDist, softKernal = softGW(m1,m2,C1,C2,l1,l2)
#    #gwDist, gwKernal = gw_gromov_fun(m1,m2,C1,C2)
#
#    #print(softDist, gwDist)
#    

