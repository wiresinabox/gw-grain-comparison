import sys
import numpy as np
fp = sys.argv[1]
fn = open(fp, 'r')

dataLinesRaw = fn.readlines()
dataLines = []
for i in range(1, len(dataLinesRaw)-1):
    dataLines.append(dataLinesRaw[i].strip().split(','))
titles = dataLinesRaw[0].strip().split(',')
data = np.array(dataLines[1:]).astype(np.float)

for i in range(data.shape[1]):
    print('{} | avg: {}, std: {}'.format(titles[i], np.mean(data[:, i]), np.std(data[:, i])))
    

