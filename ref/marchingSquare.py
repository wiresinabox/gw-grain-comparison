#Marching Squares was here once. Now more of a support module. Rename this later. 
import numpy as np
import math as m
import scipy.io as sio #For saving/load
import numbers
from scipy import stats 
def sliceAnalysis(G1in, l1, forceCellLength=False, volume=False):
    #A smaller version of windowAnalysis that will get the initial statistics for the slice.
    if isinstance(G1in, list):
        G1 = np.array(G1in).squeeze()
    elif isinstance(G1in, np.ndarray):
        G1 = G1in
    else:
        raise TypeError('G1 needs to be either a double nested list or a 2D numpy array')

    if forceCellLength:
        l1 = 1
    elif not isinstance(forceCellLength, bool):
        l1 = forceCellLength
    else:
        pass
    #print('l1: ',l1)
    
    uniqueIds1, grainAreasPix1 = np.unique(G1, return_counts=True)
    if volume:
        esds = (6*(grainAreasPix1*(l1**3))/m.pi)**(1/3)
    else:
        esds = (4*(grainAreasPix1*(l1**2))/m.pi)**(1/2)
    avgAreaPix1 = np.mean(grainAreasPix1)
    stdAreaPix1 = np.std(grainAreasPix1)
    avgAreaUni1 = avgAreaPix1 * (l1**2) 
    stdAreaUni1 = stdAreaPix1 * (l1**2)
    esdUni1 = np.sqrt((4*avgAreaUni1)/m.pi)
    esdstdUni1 = np.sqrt((4*stdAreaUni1)/m.pi)
    shape, loc, scale = stats.lognorm.fit(esds, floc=0) #hold position at zero
    #print(shape, loc, scale)
    mu = np.log(scale)
    sigma = shape 
    #Pix, Uni, Ids & Areas
    returnDict = {'areaPixel':(avgAreaPix1, stdAreaPix1),
                  'areaUnits':(avgAreaUni1, stdAreaUni1),
                  'ESD': (esdUni1, esdstdUni1), 
                  'parameters': (mu, sigma), 
                  'uniqueIds': uniqueIds1, 
                  'grainAreas': grainAreasPix1
                  }
    return returnDict


def windowAnalysis(G1in, G2in, l1, l2, relativeScale = True, noGrainInWindow = 3, winSizeDetermine = 'max', verbose = True, numSamples = 100):
    #A revision of the earlier window analysis, with careful attention paid to area and unbalancing.
    #G1, G2, are either numpy arrays of nxm or a list of lists of a 2D slice of the microstructure.
    #With each element being the ID of the grain at that position.
    #l1, l2, are the scale of the side of each pixel (assumes equiaxed pixels) with units of units / pixel side.
    #relativeScale determines whether the scaling for the second microstructure is changed s.t. the window 
    #also captures 3 grain sizes. This compares geometric differences more over size differences.
    #However, you then need to factor in the size as a seperate degree of comparison.
    #noGrainInWindow is how many ESD of the microstructure the window size will scale to.
    #numSamples is used for determining if the microstructure is large enough.
    
    if isinstance(G1in, list):
        G1 = np.array(G1).squeeze()
    elif isinstance(G1in, np.ndarray):
        G1 = G1in
    else:
        raise TypeError('G1 needs to be either a double nested list or a 2D numpy array')
    if isinstance(G2in, list):
        G2 = np.array(G2).squeeze()
    elif isinstance(G2in, np.ndarray):
        G2 = G2in
    else:
        raise TypeError('G2 needs to be either a double nested list or a 2D numpy array')
    
    #First get the average and standard deviation of the grain sizes in pixels
    uniqueIds1, grainAreasPix1 = np.unique(G1, return_counts=True)
    uniqueIds2, grainAreasPix2 = np.unique(G2, return_counts=True)

    avgAreaPix1 = np.mean(grainAreasPix1)
    avgAreaPix2 = np.mean(grainAreasPix2)
    stdAreaPix1 = np.std(grainAreasPix1)
    stdAreaPix2 = np.std(grainAreasPix2)

    #Convert to length units
    #These are effectively mu and sigma for the dream3d algorithem
    avgAreaUni1 = avgAreaPix1 * (l1**2) 
    avgAreaUni2 = avgAreaPix2 * (l2**2)
    stdAreaUni1 = stdAreaPix1 * (l1**2)
    stdAreaUni2 = stdAreaPix2 * (l2**2)

    if relativeScale:
        #Makes it s.t. each window has noGrainInWindow grain sizes in comparison
        
        #first get the ESD Diameters in units
        esdUni1 = m.sqrt(4*avgAreaUni1/m.pi)
        esdUni2 = m.sqrt(4*avgAreaUni2/m.pi)

        
        #Window Sizes:
        winLenUni1 = noGrainInWindow*esdUni1
        winLenUni2 = noGrainInWindow*esdUni2

        #Now in Pixels
        winLenPix1 = m.ceil(winLenUni1/l1)
        winLenPix2 = m.ceil(winLenUni2/l2)

        #We can then scale the distances within the window areas s.t. they contain an equal amount. 
        #wl1 = wl2 * sF
        sF2Uni = winLenUni1/winLenUni2
        sF2Pix = winLenPix1/winLenPix2
        #In this case, the one for scaling the distance matrix is in Uni AFTER it gets converted from pixels -> units

    else:
        esdUni1 = m.sqrt(4*avgAreaUni1/m.pi)
        esdUni2 = m.sqrt(4*avgAreaUni2/m.pi)

        if winSizeDetermine == 'first' or winSizeDetermine == 1:
            winLenUni1 = noGrainInWindow*esdUni1
            winLenUni2 = noGrainInWindow*esdUni1
        if winSizeDetermine == 'second' or winSizeDetermine == 2:
            winLenUni1 = noGrainInWindow*esdUni2
            winLenUni2 = noGrainInWindow*esdUni2
        if winSizeDetermine == 'max':
            winLenUni1 = noGrainInWindow*max(esdUni1, esdUni2)
            winLenUni2 = noGrainInWindow*max(esdUni1, esdUni2)
        if winSizeDetermine == 'min':
            winLenUni1 = noGrainInWindow*min(esdUni1, esdUni2)
            winLenUni2 = noGrainInWindow*min(esdUni1, esdUni2)

        print(esdUni1, esdUni2, winLenUni1, winLenUni2)

        #Now in Pixels
        winLenPix1 = m.ceil(winLenUni1/l1)
        winLenPix2 = m.ceil(winLenUni2/l1)

        sF2Uni = winLenUni1/winLenUni2
        sF2Pix = winLenPix1/winLenPix2

    uniTuple = (winLenUni1, winLenUni2, sF2Uni, avgAreaUni1, avgAreaUni2, stdAreaUni1, stdAreaUni2, esdUni1, esdUni2)
    pixTuple = (winLenPix1, winLenPix2, sF2Pix, avgAreaPix1, avgAreaPix2, stdAreaPix1, stdAreaPix2)

    minMicroArea1 = 4*numSamples*(noGrainInWindow**2)*avgAreaUni1
    minMicroLengthPix1 = m.ceil(m.sqrt(minMicroArea1))/l1
    minMicroArea2 = 4*numSamples*(noGrainInWindow**2)*avgAreaUni2
    minMicroLengthPix2 = m.ceil(m.sqrt(minMicroArea2))/l2
    x1, y1 = G1.shape
    x2, y2 = G2.shape
    sl1 = m.ceil(np.sqrt(x1*y1))
    sl2 = m.ceil(np.sqrt(x2*y2))
    if sl1 <= minMicroLengthPix1:
        s1OK = "FALSE"
    else:
        s1OK = "TRUE"

    if sl2 <= minMicroLengthPix2:
        s2OK = "FALSE"
    else:
        s2OK = "TRUE"

    if verbose == True:
        
        print("""\n=== RUN INFO ===
SAMPLE 1 CELL LENGTH : {} units/pixel length
SAMPLE 2 CELL LENGTH : {} units/pixel length

SAMPLE 1 SHAPE       : {} by {}  px  - {} by {} units
SAMPLE 2 SHAPE       : {} by {}  px  - {} by {} units

GRAINS PER WINDOW: {} \n""".format(l1, l2, x1, y1, x1*l1, y1*l1, x2, y2, x2*l2, y2*l2, noGrainInWindow))

        print("Number of Samples      : {}".format(numSamples))
        print("Minimum Sample 1 Length: {} px - Sample Length : {} px - Ok? {}".format(minMicroLengthPix1, sl1, s1OK))
        print("Minimum Sample 2 Length: {} px - Sample Length : {} px - Ok? {}\n".format(minMicroLengthPix2, sl2, s2OK))
        
        print("""=== STATISTICS READOUT (UNITS), Sample 1 & Sample 2 ===
AVERAGE GRAIN SIZE : {:.3f}  &  {:.3f}  
STD GRAIN SIZE     : {:.3f}  &  {:.3f}
Window Length      : {:.3f}  &  {:.3f}
Scaling Factor     : {:.3f}

Scaling Check Area : {:.3f}  =  {:.3f}\n""".format(avgAreaUni1, avgAreaUni2, stdAreaUni1, stdAreaUni2, winLenUni1, winLenUni2, sF2Uni, winLenUni1**2, (winLenUni2**2)*(sF2Uni**2)))

        print("""=== STATISTICS READOUT (PIXELS), Sample 1 & Sample 2 ===
AVERAGE GRAIN SIZE : {:.3f}  &  {:.3f}
STD GRAIN SIZE     : {}  &  {}
Window Length      : {}  &  {}
Scaling Factor     : {}
Scaling Check Area : {}  =  {}\n""".format(avgAreaPix1, avgAreaPix2, stdAreaPix1, stdAreaPix2, winLenPix1, winLenPix2, sF2Pix, winLenPix1**2, (winLenPix2**2)*(sF2Pix**2)))

        

        
    return (uniTuple, pixTuple, (minMicroArea1, minMicroArea2))

    
def dijkstra (graph, source, replaceInf=False):
    #replaceInf replaces Infinites with a specified number
    gsizex, gsizey = graph.shape
    #1. Create the unvisited Set
    unvisitedSet = set(range(0, gsizex))
    #2. Assign every node a tenative distance. 0 for our source. Inf for everything else.
    dist = np.ones(gsizex)*np.inf
    dist[source] = 0

    #2b. Set source node as current.
    curNode = source
    minVal = 0
    #3. For the current node. Consider the unvisited neighbors
    while (len(unvisitedSet) > 0) and (minVal != np.inf):
        #First get the neighbors.
        curDist = graph[curNode, :]
        neighborNodes = np.nonzero(curDist != 0)[0]
        for neighNode in neighborNodes:
            #Make the neighbor is unvisited.
            if neighNode in unvisitedSet:
                #Check if the path to the neighbor node would make a shorter distance that the current calculated distance for the current node.
                alt = dist[curNode] + curDist[neighNode]
                if alt < dist[neighNode]:
                    #If the new path is shorter for the neighbor,
                    #Then place it in.
                    dist[neighNode] = alt
                    #Otherwise keep it the same
        #Then once we're done, remove the current node from unvisited.
        unvisitedSet.remove(curNode)

        #Then, choose the smallest calculated distance of unvisited nodes.
        if len(unvisitedSet) == 0:
            break
        unvisitList = list(unvisitedSet)
        unvisitedDist = dist[unvisitList]
        #Chose the minimum of them, set them to the current node
        minVal = unvisitedDist.min()
        unvisitInd = np.nonzero(unvisitedDist == minVal)[0][0]
        curNode = unvisitList[unvisitInd]

        #If either the minVal or the unvisitedSet is empty, stop.

    if isinstance(replaceInf, numbers.Number):
        dist[dist==np.inf] = replaceInf


    return dist
            

def dijkstraFull (graph, replaceInf = False):
    gsizex, gsizey = graph.shape
    distMat = np.zeros([gsizex, gsizey])
    for source in range(0, gsizex):
        distMat[source, :] = dijkstra(graph, source, replaceInf)
    return distMat

def saveMatFile (matName, valName, value=None):
    if type(valName) == type(dict()):
        sio.savemat(matName, valName)
    else:
        sio.savemat(matName, {valName:value})

def uFuncL1(a, b, fij, h):
        return min(a,b) + fij*h

def uFuncL2(a, b, fij, h):
    #where a is the minimum of the x neighbor and b is the minimum of the y neighbor
    if np.abs(a - b) > fij*h:
        return min(a,b) + fij*h #if u/d/l/r motion is shorter
    else:
        return (a + b + np.sqrt(2*(fij**2)*(h**2) - (a-b)**2))/2 #if diagnols are shorter


    
def fastSweepSDF(X, grainId = 'all', h = 1, f = lambda i,j: 1, dataArray = None, maxIter = 1000, convThresh = 1e-2, onlyId = True,ignoreBoundaryEdges = False, uFunc = 'L2', binaryFilter=None, verbose=True, verboseDivisions=1000):
    #where X is a numpy array of grain ids
    #h is the length of one side of the cell s.t. the area is (h^2). 
    #grainId is the id of the grain you want a SDF for. if grainId = 'all', will calculate all boundaries
    #f is a function or numpy array reliant on i and j. Default f = 1;
    #onlyId means that ids outside return zero.
    #ignore boundary treats the boundary as not an edge.
    #if binaryFilter is an integer, then it treats pixels of that value as a boundary 

    if uFunc == 'L1':
        uFunc = uFuncL1
    elif uFunc == 'L2':
        uFunc = uFuncL2
    else:
        pass

    U = np.zeros(X.shape) + 1e4 
    
    xsize = X.shape[0] 
    ysize = X.shape[1]
    #First check if cellIdArray needs to be transformed into a numpy array.
    if dataArray == None:
        dataArray = np.zeros([xsize+2, ysize+2], int)
        #X = np.array(X).squeeze()
        dataArray[1:xsize+1, 1:ysize+1] = X 
    
    #1. Marching Squares to find Boundary (gamma)
    #2. Set boundary s.t. U(gamma) = 0
    #3. Loop four sweeps
    #   1:n, 1:m
    #   n:1, m:1
    #   n:1, 1:m
    #   1:n, m:1
    #   3a) At each i, j:
    #       get a and b, the minimums of the ones l/r, u/d. If at edge, take the one that's avaliable.
    #       Caclulate U[i,j] from that function
    #4. Stop once the thing converges, which I guess is the maximum difference from the previous iteration below thresh.
    if isinstance(binaryFilter, int): #For a binary image of boundaries
        edgePixels = np.array(np.nonzero(dataArray==binaryFilter)).transpose() #nx2 array of x and y.
        #List of list [ [x, y], [x, y]], of the edges
        grainPixels = np.array(np.nonzero(dataArray!=binaryFilter)).transpose() #nx2 array of x and y.
        

    elif grainId != 'all': #For a specific grain.
        tfmat = dataArray == grainId
        edgePixels = [] #List of list [ [x, y], [x, y]], of the edges
        grainPixels = np.array(np.nonzero(tfmat)).transpose() #nx2 array of x and y.
        if ignoreBoundaryEdges:
            tfmat[0:xsize+1, [0,ysize+1]] = True
            tfmat[[0,xsize+1], 0:ysize+1] = True
        for i in range(0, grainPixels.shape[0]):
            #check if edge
            gx = grainPixels[i, 0]
            gy = grainPixels[i, 1]
            isInternal = np.all([tfmat[gx+1, gy],tfmat[gx-1, gy],tfmat[gx, gy+1],tfmat[gx, gy-1]])
            if not isInternal:
                edgePixels.append([gx, gy])
        
    else:
        edgePixels = [] #List of list [ [x, y], [x, y]], of the edges
        grainPixels = np.array(np.nonzero(dataArray != 0)).transpose() #nx2 array of x and y.
        for gx in range(1, xsize+1):
            for gy in range(1, ysize + 1):
                dij = dataArray[gx, gy]
                if ignoreBoundaryEdges:
                    tf1 = dataArray[gx+1, gy] == dij or dataArray[gx+1, gy] == 0
                    tf2 = dataArray[gx-1, gy] == dij or dataArray[gx-1, gy] == 0
                    tf3 = dataArray[gx, gy+1] == dij or dataArray[gx, gy+1] == 0
                    tf4 = dataArray[gx, gy-1] == dij or dataArray[gx, gy-1] == 0
                else:
                    tf1 = dataArray[gx+1, gy] == dij 
                    tf2 = dataArray[gx-1, gy] == dij 
                    tf3 = dataArray[gx, gy+1] == dij 
                    tf4 = dataArray[gx, gy-1] == dij 

                isInternal = np.all(np.array([tf1, tf2, tf3, tf4]))

                if not isInternal:
                    edgePixels.append([gx, gy])

    edgePixels = np.array(edgePixels)-1
    grainPixels = grainPixels-1
    #print(U.shape, edgePixels.shape)
    U[edgePixels[:, 0], edgePixels[:, 1]] = 0 #Note this crashes here when there's only one grain in the window.

    xranges = np.array([[0, xsize, 1], [xsize-1, -1, -1], [xsize-1, -1, -1], [0, xsize, 1]])
    yranges = np.array([[0, ysize, 1], [0, ysize, 1], [ysize-1, -1, -1], [ysize-1, -1, -1]])
    notConverged = True
    iterNum = 0
    prevU = U
    
    div = int(maxIter/verboseDivisions)
    if div == 0:
        div = 1
    while notConverged:
        #print(iterNum)
        for k in range(0,len(xranges)):
            for i in range(xranges[k, 0],xranges[k, 1], yranges[k, 2]):
                for j in range(yranges[k,0], yranges[k, 1], yranges[k,2]):
                    #print(U[i+1, j], U[i-1, j], U[i, j+1], U[i, j-1])
                    if not np.any(np.all(edgePixels == [i,j], axis = 1)):
                        if i <= 0:
                            a = U[i+1, j]
                        elif i >= xsize-1:
                            a = U[i-1, j]
                        else:
                            a = min(U[i+1, j], U[i-1, j])

                        if j <= 0:
                            b = U[i, j+1]
                        elif j >= ysize -1:
                            b = U[i, j-1]
                        else:
                            b = min(U[i, j+1], U[i, j-1])

                        if isinstance(f, np.ndarray):
                            fij = f[i,j]
                        else:
                            fij = f(i,j)
                        
                        U[i,j] = uFunc(a, b, fij, h)
        #check convergence
        convCheck = np.abs(U-prevU)
        notConverged = iterNum <= maxIter and not np.all(convCheck <= convThresh)
        if verbose:
            if iterNum % div == 0:
                print('{} of {} - {:.2%}. Convergence: {} > {}'.format(iterNum, maxIter, iterNum/maxIter, np.max(convCheck), convThresh))
        prevU = U
        iterNum +=1

    if onlyId:
        ret = np.zeros(U.shape)
        ret[grainPixels[:, 0], grainPixels[:, 1]] = U[grainPixels[:, 0], grainPixels[:, 1]]
        return ret
    else:
        return U
    
def outerEdgeSDF(X, grainId, grainBuff = 1, h = 1, f = lambda i,j: 1, dataArray = None, maxIter = 1000, convThresh = 1e-2, onlyId = True, ignoreBoundaryEdges = False, uFunc = 'L2'):
    #where X is a numpy array of grain ids
    #grainId is the specific grain to cut out and get the SDF for.
    #grainBuff is the size of the buffer zone used to make the SDF since some grains are tiny.
    
    #general proceedure, 'paintbucket' in from edges of minima boundaries this becomes a 'zero mask'. get SDF boundaries. Apply zero mask from before.
    xsize, ysize = X.shape
    grainXcoords, grainYcoords = np.nonzero(X == grainId)
    xmin = np.min(grainXcoords)
    xmax = np.max(grainXcoords)
    ymin = np.min(grainYcoords)
    ymax = np.max(grainYcoords)
    grainX = np.zeros((xmax-xmin+1+grainBuff+grainBuff, ymax-ymin+1+grainBuff+grainBuff))
    grainX[grainBuff:grainBuff + (xmax+1-xmin), grainBuff:grainBuff + (ymax+1-ymin)] = X[xmin:xmax+1, ymin:ymax+1]
    #print(grainX)
    #if ymin == ymax:
    #    grainX = grainX[:, None] #Adds singleton dimensions. Only issue in y dimension for some reason
    #    print('add y singleton')
    grainX[grainX != grainId] = 2 
    grainX[grainX == grainId] = 1
    gxmax, gymax = grainX.shape
    gxmax = gxmax - 1
    gymax = gymax - 1
    #first set window edges that aren't the same as the grain id to zero.
    grainX[0, np.nonzero(grainX[0, :] == 2)[0]] = 0
    grainX[gxmax, np.nonzero(grainX[gxmax, :] == 2)[0]] = 0
    grainX[np.nonzero(grainX[:, 0] == 2)[0], 0] = 0
    grainX[np.nonzero(grainX[:, gymax] == 2)[0], gymax] = 0
    for ci in range(1, np.min([gxmax, gymax])-2):
        for i in range(ci, gxmax-ci+1):
            for j in [ci, gymax-ci]:# Squeezes in from the side.
                #print(i, j, '|', ci, gxmax-ci, '|', ci, gymax - ci, grainX.shape)
                
                uid = grainX[i+1, j]
                did = grainX[i-1, j]
                lrd = grainX[i, j+1]
                rid = grainX[i, j-1]
                quadTF = np.any(np.array([uid, did, lrd, rid])==0)
                if quadTF and grainX[i,j] != 1:
                    grainX[i, j] = 0
        for j in range(ci, gymax-ci+1):
            for i in [ci, gxmax-ci]:# Squeezes in from the side.
                #print(i, j, '|', ci, gxmax-ci, '|', ci, gymax - ci, grainX.shape)
                
                uid = grainX[i+1, j]
                did = grainX[i-1, j]
                lrd = grainX[i, j+1]
                rid = grainX[i, j-1]
                quadTF = np.any(np.array([uid, did, lrd, rid])==0)
                if quadTF and grainX[i,j] != 1:
                    grainX[i, j] = 0
        #print(ci)
        #print(grainX)
    grainX[grainX == 2] = 1 #Fill in the insides, this makes it the mask
    #print(grainX)
    grainXSDF = fastSweepSDF(grainX, grainId = 'all', h = 1, f = lambda i,j: 1, dataArray = None, maxIter = maxIter, convThresh = convThresh, onlyId = onlyId, ignoreBoundaryEdges = ignoreBoundaryEdges, uFunc = uFunc, verbose=False)
    #print(grainXSDF)
    grainXSDF = grainXSDF*grainX #Masking
    #print(grainXSDF)
    #print(grainX)
    #quit()
    returnX = np.zeros((xsize, ysize))
    returnX[xmin:xmax+1, ymin:ymax+1] = grainXSDF[grainBuff: grainBuff+(xmax+1-xmin), grainBuff: grainBuff+(ymax+1-ymin)]
    return returnX, grainXSDF, grainX

def overlapSDF(X, grainId = 'all', h = 1, f = lambda i,j: 1, dataArray = None, maxIter = 1000, convThresh = 1e-2, onlyId = True,ignoreBoundaryEdges = False, uFunc = 'L2', verboseDivisions=5, verbose=True):

    if grainId == 'all':
        uniqueIds = np.unique(X)
    else:
        uniqueIds = [grainId]
    returnX = np.zeros(X.shape)
    div = int(len(uniqueIds)/verboseDivisions)
    if div == 0:
        div = 1
    for ind in range(len(uniqueIds)):
        grainid = uniqueIds[ind]
        if ind % div == 0:
            if verbose:
                print('{} of {} - {:.2%}'.format(ind, len(uniqueIds), ind/len(uniqueIds)))
        if np.any(X == grainid):
            #print(grainid)
            grainXFull, grainSDF, grainMask = outerEdgeSDF(X, grainId = grainid, h = 1, f = lambda i,j: 1, dataArray = None, maxIter = maxIter, convThresh = convThresh, onlyId = onlyId, ignoreBoundaryEdges = ignoreBoundaryEdges, uFunc = uFunc)
            #print(grainid)
            #print(grainXFull) 
            returnX = returnX + grainXFull
    return returnX
