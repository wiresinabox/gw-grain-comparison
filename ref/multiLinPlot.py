import numpy as np
import sys
import os
import string
import plotUtil as pltu
import matplotlib.pyplot as plt

#python3 multiLinPlot <file1> <file2> <file3>

def valConv(val):
    if all([l in string.digits for l in val.strip()]):
        val = int(val)
    elif all([l in string.digits + '.' for l in val.strip()]):
        val = float(val)
    elif val.strip() == 'True':
        val = True
    elif val.strip() == 'False':
        val = False
    else:
        val = val.strip()
    return val

def parseOutput(fn):
    inFile = open(fn, 'r')
    scoreArray = []
    varName = ''
    varRange = []
    sampleNameList = []
    stylesList=[] 
    sampleFPList = [] #tuples
    runOptionsDict = dict()
    runInfoDict = dict()
    isDone = False
    runType = ''
    curSection = ''
    while not isDone:
        line = inFile.readline().strip()
        print(line)
        if line.strip() == '':
            continue
        #print(curSection, line)
        if (line.endswith('>') and line.startswith('<')) or line.isupper():
            curSection = line.replace('>', '').replace('<','')
            continue

        if curSection == 'WINDOW SCORES':
            isDone = True
            break
        elif curSection == 'SCORES':
            scoreArray.append(np.fromstring(line, sep=',', dtype=float))
        elif curSection == 'SCORES GEO CONTRIB':
            runType = 'cross'
        elif curSection == 'SCORES MASS CONTRIB':
            runType = 'cross'
        elif curSection == 'SCORES MASS':
            runType = 'cross'
        elif curSection == 'VARNAME':
            runType = 'lin'
            varName = line
        elif curSection == 'VARRANGE':
            varRange = np.fromstring(line, sep=',', dtype=float)
        elif curSection == 'SAMPLES':
            sampleIter,_1, tempLine = line.partition(')')
            sampleName,_1, tempLine = tempLine.partition(':')
            try:
                fn1, fn2 = tempLine.strip().split()
            except ValueError:
                fn1 = tempLine.strip()
                fn2 = ''
            sampleNameList.append(sampleName.strip())
            sampleFPList.append((fn1.strip(), fn2.strip()))
        elif curSection == 'STYLES':
            sampleIter,_1, tempLine = line.partition(')')
            sampleName,_1, styleStr = tempLine.partition('|')
            stylesList.append(styleStr.strip())


        elif curSection == 'RUN OPTIONS':
            if ':' in line:
                part = ':'
            else:
                part = '='
            var,_1, val = line.partition(part)
            val = valConv(val)
            runOptionsDict[var.strip()] = val
        elif curSection == 'RUN INFO':
            if ':' in line:
                part = ':'
            else:
                part = '='
            var,_1, val = line.partition(part)
            val = valConv(val)
            runInfoDict[var.strip()] = val
        else:
            print('Unknown Section:', curSection)
            pass
    scoreArray = np.array(scoreArray)
    returnDict = dict()
    #print('runType'        , runType         ) 
    #print('scoreArray'     , scoreArray )
    #print('varName'        , varName )
    #print('varRange'       , varRange )
    #print('sampleNameList' , sampleNameList )
    #print('sampleFPList'   , sampleFPList )
    #print('runOptionsDict' , runOptionsDict )
    #print('runInfoDict'    , runInfoDict )
    returnDict['runType']        = runType
    returnDict['scoreArray']     = scoreArray 
    returnDict['varName']        = varName 
    returnDict['varRange']       = varRange 
    returnDict['sampleNameList'] = sampleNameList 
    returnDict['sampleFPList']   = sampleFPList
    returnDict['runOptionsDict'] = runOptionsDict
    returnDict['runInfoDict']    = runInfoDict
    returnDict['stylesList']    = stylesList
    return returnDict

if __name__ == '__main__':
    fnList = sys.argv[1:]
    scoreArrayList = []
    multiStyle = 'std' #'minmax
    #multiStyle = 'minmax'
    #multiStyle = 'std'
    for fn in fnList:
        print(fn)
        parseDict = parseOutput(fn)
        scoreArrayList.append(parseDict['scoreArray'])
        
        runType = parseDict['runType']
    if runType == 'lin':
        titles = parseDict['sampleNameList']
        titles = [
                'R vs. R',
                'R vs. BII',
                'R vs. EII',
                'R vs. EI',
                'R vs. BI'
                ]
        #titles= [
        #        'Experimental vs. Experimental',
        #        'Experimental vs. Reconstruct 1',
        #        'Experimental vs. Reconstruct 2'
        #        ]

        varName = parseDict['varName']
        if varName == 'windowNum':
            varName = 'Number of Windows'
        elif varName == 'forceWindowLen':
            varName = 'Window Size (px)'
        varRange = parseDict['varRange']
        #fig, axs = plt.subplots(1,1, figsize=(6,4.5))
        fig, axs = plt.subplots(1,1, figsize=(4.5,4.5))
        if not isinstance(axs, np.ndarray):
            axs = np.array([axs])
        if 'stylesList' in parseDict.keys():
            plotStyles = parseDict['stylesList']
        else:
            plotStyles = []
        plotTitle = ''#'{}:variable'.format(varName)
        #for val, var in parseDict['runOptionsDict'].items():
        #    if not val == varName:
        #        plotTitle = plotTitle + ' - {}:{}'.format(val,var)
        for li in scoreArrayList:
            print(li)
        plt.suptitle(plotTitle, fontsize = 'xx-small', wrap=True) 
        pltu.linPlot(scoreArrayList, varRange = varRange, varName = varName, titles = titles, plotStyles = plotStyles, multiStyle = 'std', fig=fig, ax = axs[0])
        #pltu.linPlot(scoreArrayList, varRange = varRange, varName = varName, titles = titles, plotStyles = plotStyles, multiStyle = multiStyle, fig = fig, ax = axs[1])
    savefp = fnList[0].replace('.txt', '-combined-{}.png'.format(len(fnList)))
    print('Saving to:', savefp)
    fig.savefig(savefp, dpi=600)
    plt.show()
