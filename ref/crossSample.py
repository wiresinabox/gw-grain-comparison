import sys
import dream3dFunctions as d3f
import time
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pixelMethod as pm
import sampleGenerator as sg
import optimization as opt
from plotUtil import rasterPlot, crossPlot, linPlot, plotMicroStruct, landPlot
from pixelMethod import resRegen
import resource
sys.path.append('/home/esuwandi/bin/python-libs')
#make the cross correlation matrix for n samples
def setMemLimit(gig):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    print('Setting Memory Limit to {} GB, {} b'.format(gig, gig*1e9))
    resource.setrlimit(resource.RLIMIT_AS, (int(gig*1e9), hard))

def linearCompare(sampleList1, sampleList2, varName, varRange, windowOptionsDict = {}, titles=[], plotTitle=False, lmult = 1, Cscale = 0.1, usePenaltyFinal=False, endlmult=1, doPlot = True, plotStyles=[], multiproc = False, cores = 4, partialScores = False, hardW = False, forceWinSym=False, roundMult = 1e6, addNorm = False, verbose=True):
    #Partial Scores means it will also plot the csc and psc. 
    #compares varying one variable, eg. window size.
    if partialScores:
        scoreMat = np.zeros((int(len(sampleList1)*3), len(varRange)))
        newTitles = []
        for title in titles:
            newTitles.append(title+' - full score')
            newTitles.append(title+' - coupling score')
            newTitles.append(title+' - penalty score')
        titles = newTitles
    else: 
        scoreMat = np.zeros((len(sampleList1), len(varRange)))
    microList = []
    dataList = []
    maxval = 0 
    for i in range(len(varRange)):
        varValue = varRange[i]
        microList.append([])
        for j in range(len(sampleList1)):
            inSample1 = sampleList1[j]
            inSample2 = sampleList2[j]
            if isinstance(windowOptionsDict, list):
                inOptions = windowOptionsDict[j]
            else:
                inOptions = windowOptionsDict

            print(varName, varValue)
            inOptions[varName] = varValue
            dataDicts = pm.generateWindowData(inSample1, inSample2, optionsDict1 = inOptions, optionsDict2 = inOptions)
            ptype=inOptions['sdfPenaltyType']
            esdUni1=dataDicts['esdUni1']
            esdUni2=dataDicts['esdUni2']
            winlen1=dataDicts['windowLength1']
            winlen2=dataDicts['windowLength2']
            if partialScores:
                titles[int(j*3)] = titles[int(j*3)].format(ESD1=esdUni1, ESD2=esdUni2, WIN1=winlen1, WIN2=winlen2, PTYPE=ptype)
                titles[int(j*3+1)] = titles[int(j*3+1)].format(ESD1=esdUni1, ESD2=esdUni2, WIN1=winlen1, WIN2=winlen2, PTYPE=ptype)
                titles[int(j*3+2)] = titles[int(j*3+2)].format(ESD1=esdUni1, ESD2=esdUni2, WIN1=winlen1, WIN2=winlen2, PTYPE=ptype)
            else:
                titles[j] = titles[j].format(ESD1=esdUni1, ESD2=esdUni2, WIN1=winlen1, WIN2=winlen2, PTYPE=ptype)
            
            if len(titles) != 0:
                if partialScores:
                    sampleStr = "{} of {}, {}: {}, {}".format(i+1, len(varRange), varName, varValue, titles[int(j*3)])
                else:
                    sampleStr = "{} of {}, {}: {}, {}".format(i+1, len(varRange), varName, varValue, titles[j])
            else:
                sampleStr = "{} of {}, {}: {}, {}".format(i+1, len(varRange), varName, varValue, sample)
            print(sampleStr)
            U1s = dataDicts['distanceCellArraySDF1']
            U2s = dataDicts['distanceCellArraySDF2']
            Cs = dataDicts['CostMatSDF']
            uv1s = dataDicts['uv1SDF']
            uv2s = dataDicts['uv2SDF']
            l1 =  dataDicts['penaEdge1SDF']
            l2 = dataDicts['penaEdge2SDF']
            if varName == 'lmult':
                lmult = varValue
            if varName == 'Cscale':
                Cscale = varValue
            if varName == 'endlmult':
                endlmult = varValue
            stime = time.time()
            
            scoreTup = pm.runWindowsSDFWass(uv1s, uv2s, Cs, l1, l2, lmult = lmult, Cscale = Cscale, multiproc = multiproc, availProcs = cores, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult, hardW=hardW, idStr= jobNumber, forceWinSym=forceWinSym, verbose=verbose, roundMult = roundMult, addNorm = addNorm)
            dataList.append([(i,j), sampleStr, scoreTup[0], scoreTup[1], scoreTup[2], scoreTup[5], scoreTup[6]])
            microList[i].append(U2s[0])
            if np.max(U1s[0]) > maxval:
                maxval = np.max(U1s[0])
            if np.max(U2s[0]) > maxval:
                maxval = np.max(U2s[0])
            if partialScores: 
                scoreMat[int(j*3), i] = scoreTup[0] #full score
                scoreMat[int(j*3+1), i] = scoreTup[5] #csc
                scoreMat[int(j*3+2), i] = scoreTup[6] #psc
            else:
                scoreMat[j, i] = scoreTup[0]
            etime = time.time()
            print('Overall Score: ', scoreTup[0])
            print('Time Taken: {:.3f} min'.format((etime-stime)/60))
    figList= []
    if doPlot:
        fig = plt.figure(figsize=(16,9))
        gs = gridspec.GridSpec(len(sampleList1),3)
        axs = np.ndarray((len(sampleList1),3), dtype = np.object)
        ax = fig.add_subplot(gs[:, 0])
        axs[0,0] = ax
        dataReplaceDict={'ESD1':esdUni1, 'ESD2':esdUni2, 'WIN1':winlen1, 'WIN2':winlen2}
        linPlot(scoreMat, varRange=varRange, varName=varName,titles=titles, plotTitle=plotTitle, plotStyles=plotStyles, dataReplaceDict = dataReplaceDict, fig=fig, ax=ax)
        
        for i in range(len(microList[0])):
            ax = fig.add_subplot(gs[i, 1])
            axs[i,1] = ax
            ax.pcolor(microList[0][i], edgecolors='none', vmin=0, vmax=maxval)
            ax.axis('equal')
        for i in range(len(microList[0])):
            ax = fig.add_subplot(gs[i, 2])
            axs[i,2] = ax
            im = ax.pcolor(microList[-1][i], edgecolors='none', vmin=0, vmax=maxval)
            ax.axis('equal')

        fig.colorbar(im, ax = axs[:, 2])
        
        figList.append(fig)
    return scoreMat, figList, dataList

def crossMatrix(sampleList, windowOptionsList = [], titles = [], usePenaltyFinal=False, endlmult=1,hardW=False, identityShortcut = True, triAxis = False, doMatrixPlot = True, doMicroPlot = False, doWindowPlot = False, plotTitle=False, lmult = 1, Cscale = 1, multiproc = False, cores = 4, massVals = None, lenVals = None, partialScores=False, forceWinSym=False, roundMult = 1e6, addNorm = False, verbose=True):
    #where sampleList is a list of either filepaths to a dream3d file or matricies.
    #where windowOptionsList is a list of dictionaries keyed to options
    #identity shortcut will use the value of [i, j] for [j, i]
    #triAxis will take the sum of the three slices as the measurement
    #step 1: generate dataDicts for all samples.
    #order: sweep left to right. 1,1 1,2 1,3
    ccmatrix = np.zeros((len(sampleList), len(sampleList)))
    geoccmatrix = np.zeros((len(sampleList), len(sampleList)))
    massccmatrix = np.zeros((len(sampleList), len(sampleList)))
    massmatrix = np.zeros((len(sampleList), len(sampleList)))
    figureList = []
    dataList = []

    if isinstance(sampleList, np.ndarray):
        irange, jrange, krange = sampleList.shape #Preset matchings of object ndarray
    else:
        irange = len(sampleList)
        jrange = len(sampleList)

    for i in range(irange):
        if identityShortcut:
            start = i
        else:
            start = 0
        for j in range(start, jrange):
            print('Window: ', i, j)
            if isinstance(sampleList, np.ndarray):
                inSample1 = sampleList[i, j, 0]
                inSample2 = sampleList[i, j, 1]
            else:
                inSample1 = sampleList[i]
                inSample2 = sampleList[j]
            
            if len(titles) != 0:
                sampleStr = "{} vs. {}".format(titles[i], titles[j])
            else:
                sampleStr = "{} vs. {}".format(inSample1[i], inSample2[j])
            print(sampleStr)
            if not isinstance(windowOptionsList, list) and len(windowOptionsList) == 0:
                inOptions1 = []
                inOptions2 = []
            else:
                inOptions1 = windowOptionsList[i]
                inOptions2 = windowOptionsList[j]
            if triAxis:
                inOptions1.update({'axis' :'x'})
                inOptions2.update({'axis' :'x'})
                dictx = pm.generateWindowData(inSample1, inSample2, optionsDict1 = inOption1, optionsDict2 = inOptions2)
                inOptions1.update({'axis' :'y'})
                inOptions2.update({'axis' :'y'})
                dicty = pm.generateWindowData(inSample1, inSample2, optionsDict1 = inOption1, optionsDict2 = inOptions2)
                inOptions1.update({'axis' :'z'})
                inOptions2.update({'axis' :'z'})
                dictz = pm.generateWindowData(inSample1, inSample2, optionsDict1 = inOption1, optionsDict2 = inOptions2)
                dataDictsList= [dictx, dicty, dictz]
            else:
                dataDictsList = pm.generateWindowData(inSample1, inSample2, optionsDict1 = inOptions1, optionsDict2 = inOptions2)
    #step 2: run through everything?
            if triAxis:
                Cs = dataDictsList[0]['CostMatSDF']
                uv1s = dataDictsList[0]['uv1SDF']
                uv2s = dataDictsList[0]['uv2SDF']
                l1 =  dataDictsList[0]['penaEdge1SDF']
                l2 = dataDictsList[0]['penaEdge2SDF']
                scoreTup = pm.runWindowsSDFWass(uv1s, uv2s, Cs, l1, l2, lmult = lmult, Cscale = Cscale, usePenaltyFinal = usePenaltyFinal, endlmult=endlmult, hardW=hardW, multiproc = multiproc, availProcs = cores, idStr= jobNumber, forceWinSym=forceWinSym, verbose=verbose, roundMult = roundMult, addNorm = addNorm  )
                gwScorex = scoreTup[0]
                geoScorex = scoreTup[5]
                massScorex = scoreTup[6]
                massx = np.sum(scoreTup[4])
                dataList.append([(i,j,'x'), sampleStr, scoreTup[0], scoreTup[1], scoreTup[2], scoreTup[3], scoreTup[4]])

                Cs = dataDictsList[1]['CostMatSDF']
                uv1s = dataDictsList[1]['uv1SDF']
                uv2s = dataDictsList[1]['uv2SDF']
                l1 =  dataDictsList[1]['penaEdge1SDF']
                l2 = dataDictsList[1]['penaEdge2SDF']
                scoreTup = pm.runWindowsSDFWass(uv1s, uv2s, Cs, l1, l2, lmult = lmult, Cscale = Cscale, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult,hardW=hardW,  multiproc = multiproc, availProcs = cores, idStr= jobNumber, forceWinSym=forceWinSym, verbose=verbose, roundMult = roundMult, addNorm = addNorm)
                gwScorey = scoreTup[0]
                massy = np.sum(scoreTup[4])
                geoScorey = scoreTup[5]
                massScorey = scoreTup[6]
                dataList.append([(i,j,'y'), sampleStr, scoreTup[0], scoreTup[1], scoreTup[2], scoreTup[3], scoreTup[4]])
                
                Cs = dataDictsList[2]['CostMatSDF']
                uv1s = dataDictsList[2]['uv1SDF']
                uv2s = dataDictsList[2]['uv2SDF']
                l1 =  dataDictsList[2]['penaEdge1SDF']
                l2 = dataDictsList[2]['penaEdge2SDF']
                scoreTupz = pm.runWindowsSDFWass(uv1s, uv2s, Cs, l1, l2, lmult = lmult, Cscale = Cscale, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult, hardW=hardW,multiproc = multiproc, availProcs = cores, idStr= jobNumber, forceWinSym=forceWinSym, verbose=verbose, roundMult = roundMult, addNorm = addNorm)
                gwScore = scoreTup[0]
                geoScorez = scoreTup[5]
                massScorez = scoreTup[6]
                massz = np.sum(scoreTup[4])
                dataList.append([(i,j,'z'), sampleStr, scoreTup[0], scoreTup[1], scoreTup[2], scoreTup[3], scoreTup[4]])
                
                ccmatrix[i,j]=gwScorex+gwScorey+gwScorez
                geoccmatrix[i,j]=geoScorex+geoScorey+geoScorez
                massccmatrix[i,j]=massScorex+massScorey+massScorez
                massmatrix[i,j] = massx + massy + massz 

            else:
                Cs = dataDictsList['CostMatSDF']
                uv1s = dataDictsList['uv1SDF']
                uv2s = dataDictsList['uv2SDF']
                l1 =  dataDictsList['penaEdge1SDF']
                l2 = dataDictsList['penaEdge2SDF']
                
                esdUni1=dataDictsList['esdUni1']
                esdUni2=dataDictsList['esdUni2']
                winlen1=dataDictsList['windowLength1']
                winlen2=dataDictsList['windowLength2']
                titles[i] = titles[i].format(ESD1=esdUni1, ESD2=esdUni2, WIN1=winlen1, WIN2=winlen2)
                
                scoreTup = pm.runWindowsSDFWass(uv1s, uv2s, Cs, l1, l2, lmult = lmult, Cscale = Cscale, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult,hardW=hardW, multiproc = multiproc, availProcs = cores, idStr= jobNumber, forceWinSym=forceWinSym, verbose=verbose, roundMult = roundMult, addNorm = addNorm)
                ccmatrix[i,j] = scoreTup[0]
                geoccmatrix[i,j]=scoreTup[5]
                massccmatrix[i,j]=scoreTup[6]
                massmatrix[i,j] = np.sum(scoreTup[4])
                print('Overall Score: ', scoreTup[0])
                dataList.append([(i,j), sampleStr, scoreTup[0], scoreTup[1], scoreTup[2], scoreTup[3], scoreTup[4]])
                if identityShortcut:
                    ccmatrix[j,i] = scoreTup[0]
                    geoccmatrix[j,i]=scoreTup[5]
                    massccmatrix[j,i]=scoreTup[6]
                    massmatrix[j,i] = np.sum(scoreTup[4])
                    print('Overall Score: ', scoreTup[0])
                    dataList.append([(j,i), sampleStr, scoreTup[0], scoreTup[1], scoreTup[2], scoreTup[3], scoreTup[4]])

                print(ccmatrix)
    if doMatrixPlot:
        if partialScores:
            fig, axs = plt.subplots(2,2, figsize=(15,15))
            axs = axs.flatten()
            strMatrix1 = np.ndarray((irange, jrange), dtype = np.object)
            strMatrix2 = np.ndarray((irange, jrange), dtype = np.object)
            strMatrix3 = np.ndarray((irange, jrange), dtype = np.object)
            strMatrix4 = np.ndarray((irange, jrange), dtype = np.object)
            for i in range(irange):
                for j in range(jrange):
                    strMatrix1[i,j]='wsc:{:.7}'.format(ccmatrix[i,j]) #wasserstein score
                    strMatrix2[i,j]='csc:{:.7}'.format(geoccmatrix[i,j]) #geometric score
                    strMatrix4[i,j]='locm:{:.7}'.format(massmatrix[i,j]) #local mass, penalty score
                    strMatrix3[i,j]='psc:{:.7}'.format(massccmatrix[i,j]) #local mass, penalty score
                    if not isinstance(massVals, type(None)):
                        strMatrix4[i,j] = strMatrix4[i,j] + '\nM: {:.7}'.format(massVals[i,j])
                    if not isinstance(lenVals, type(None)):
                        strMatrix4[i,j] = strMatrix4[i,j] + '\nLi/Lj: {:.7}'.format(lenVals[i,j])

            crossPlot(ccmatrix, titles, celltitles= strMatrix1, fig = fig, ax = axs[0])
            axs[0].set_title('Full Score', fontsize=4)
            crossPlot(geoccmatrix, titles, celltitles= strMatrix2,fig = fig, ax = axs[1])
            axs[1].set_title('Geo (Coupling) Score', fontsize=4)
            crossPlot(massccmatrix, titles, celltitles= strMatrix3, fig = fig, ax = axs[2])
            axs[2].set_title('Mass (Penalty) Score', fontsize=4)
            crossPlot(massmatrix, titles, celltitles= strMatrix4, fig = fig, ax = axs[3])
            axs[3].set_title('Sum Penalty', fontsize=4)
            figureList.append((fig, axs))
        else:
            fig, axs = plt.subplots(1,1, figsize=(15,15))
            strMatrix1 = np.ndarray((irange, jrange), dtype = np.object)
            for i in range(irange):
                for j in range(jrange):
                    strMatrix1[i,j]='wsc:{:.7}'.format(ccmatrix[i,j]) #wasserstein score
            crossPlot(ccmatrix, titles, celltitles= strMatrix1, fig = fig, ax = axs)
            axs.set_title('Full Score', fontsize=4)
            figureList.append((fig, axs))

        #fig, ax = plt.subplots()
        #im = ax.imshow(ccmatrix)
        #ax.set_xticks(np.arange(len(sampleList)))
        #ax.set_yticks(np.arange(len(sampleList)))
        #ax.set_xticklabels(titles)
        #ax.set_yticklabels(titles)
        #plt.setp(ax.get_xticklabels(), rotation = 45, ha = "right", rotation_mode = "anchor")
        #if isinstance(plotTitle, str):
        #    ax.set_title(plotTitle.format(ESD1=esdUni1, ESD2=esdUni2, WIN1=winlen1, WIN2=winlen2))

        #for i in range(len(sampleList)):
        #    for j in range(len(sampleList)):
        #        text = ax.text(j, i, float('{:.4f}'.format(ccmatrix[i,j])), ha = "center", va = "center", color = 'w', fontsize=6)
        #fig.tight_layout()
        #figureList.append(fig)
        
    
    return ccmatrix, figureList, dataList, geoccmatrix, massccmatrix, massmatrix

def crossCompareMain():
    print('===STARTING: CROSS COMPARE===') 
    writeOutFile = True

    startascTime = time.asctime()
    startTime = time.time()
    sampleList = []
    nameList = []
    
    optionsDict= {
            'windowNum':500,
            'axis':'y',
            'slice': 'all', #np.random.randint(0, 128, 20),
            #'microCrop1':160,
            #'microCrop2': 160,
            'relativeScale':False,
            'totalAreaHold' : 1,
            'noGrainInWindow':4,
            'forceWindowLen':20, #Overrides noGrainInWindow
            'winSizeDetermine':'max',
            'distMultiplier':1,
            'centerOnRandGrain': False,
            'distType' : 'sdf',
            'sdfDistMin': 0,
            'sdfPenaltyType':'constesd',
            'ignoreBoundaryEdges':True,
            'verbose':False,
            'forceCellLength':True,
            'sdfNormalize':False, #Forces each window to have the same amount of mass
            'sdfReverse': True,
            'sdfRoot':False,
            'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
            'sdfFloorNorm':False,
            'sdfThresh':0,
            'sdfType': 'overlap',
            'forceWinSym': True,
            'roundMult':5e4,
            'addNorm':True,
            'availProcs':numNodes,
            'multiproc':multiproc,
    }

    if smallTest:
        optionsDict['windowNum'] = 50
        optionsDict['slice'] = 'half'

    if sampleSet == 'tile':
        print('SAMPLE SET: TILE')
        sampleList.append(sg.brickTile(64, 12, 12, offset=(0,-5),tileType = 'straight')) #straight
        nameList.append('Bricks, 8x8 pixels')
        sampleList.append(sg.brickTile(64, 12, 12, offset=(1,-3), tileType = 'straight')) #straight
        nameList.append('Bricks, 8x8 pixels, Offset')
        sampleList.append(sg.brickTile(72, 8, 18, tileType = 'straight')) #straight
        nameList.append('Bricks, 6x10 pixels')
        sampleList.append(sg.brickTile(72, 8, 18, tileType = 'herringbone')[14:71,10:71]) #straight
        nameList.append('Bricks, 6x10 pixels')
        sampleList.append(sg.hexagonTile(64, 7)) #straight
        nameList.append('Hexagons, 7 pixel sides')
        optionsDict['centerOnRandGrain'] = 'center'
        optionsDict['windowNum'] = 1
        optionsDict['forceWindowLen'] = 30
    elif sampleSet == 'exp': 
        print('SAMPLE SET: EXPERIMENTAL')
        sampleList.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Final.dream3d')
        nameList.append('IN100, Exp. EBSD')
        sampleList.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic.dream3d')
        nameList.append('Synthetic IN100')
        sampleList.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic-2.dream3d')
        nameList.append('Synthetic IN100, Same Gen.')
        optionsDict['sdfType'] = 'sweep'
        optionsDict['microCrop1'] = 160
        optionsDict['microCrop2'] = 160
        optionsDict['axis'] = 'x'
    else:
        print('SAMPLE SET: SYNTHETIC')
        sampleList.append(            '../dream3d-samples/eq1-matrix-res2x.dream3d') #equiaxed
        nameList.append('Equiaxed, 2x Mag')
        sampleList.append(           '../dream3d-samples/eq2x-matrix-res2x.dream3d') #twice as big
        nameList.append('Equiaxed, 2x ESD, 2x Mag')
        sampleList.append(  '../dream3d-samples/eq-bimodal-2x-matrix-res2x.dream3d') #two sizes
        nameList.append('Bimodal, 0.5 vol. frac. 2x ESD, 2x Mag')
        sampleList.append( '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
        nameList.append('Bimodal, 0.5 vol. frac. 0.5x ESD, 2x Mag')
        sampleList.append('../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
        nameList.append('Rolled 2:1:0.5, 2x Mag')
    

    hardW = '2dhist' #Forces the Balanced wasserstein to be used for each window instead. If so sdfNormalize must be True
    titleList = nameList.copy()
    regen = False
    refRegen = False #This regens each reference sample for stuff like balancing masses and what not.
    relativeRegen = False #This regens each sample relative to the row.
    sampleMassArray = np.zeros((len(sampleList), len(sampleList))) 
    sampleLenArray = np.zeros((len(sampleList), len(sampleList))) 
    sampleMassDiffArray = np.zeros((len(sampleList), len(sampleList)))
    i=0
    if regen:
        setMass = 4e4
        charLenList = []
        charMassList = []
        sampleArray = np.ndarray((len(sampleList), len(sampleList), 2), dtype=np.object)
        for i in range(len(sampleList)):
            sample1 = sampleList[i] 
            altfp = sampleList[i] 
            if refRegen and isinstance(sample1, str):
                altRegenJson = '../dream3d-samples/regen/{}-{}-{}-{}-regen.json'.format(jobNumber, i, i, nameList[i].replace(' ', '').replace('.','').replace(':','-'))
                altRegenD3 = '../dream3d-samples/regen/{}-{}-{}-{}-regen.dream3d'.format(jobNumber, i, i,nameList[i].replace(' ', '').replace('.','').replace(':','-'))
                regenDataTup = resRegen(sample1, altfp, altRegenJson, altRegenD3, axis=optionsDict['axis'], sliceNum=optionsDict['slice'], searchWindowLen=128, windowSize=128, distMultiplier=optionsDict['distMultiplier'], sdfDistMin=optionsDict['distMultiplier'], forceType = 'setmass', forceMass = setMass, regenStats=True)
                sampleArray[i, i, 0] = altRegenD3 
                sampleArray[i, i, 1] = altRegenD3 
                regenMass = regenDataTup[7]
                #charLenList.append(regenDataTup[0])
                #charMassList.append(regenDataTup[7])
                #sampleMassArray[i, i] = charMassList.append(regenDataTup[7])
                #titleList[i] = nameList[i] + '\nL: {:4}\nM: {:.6}'.format(charLenList[i], charMassList[i])
            else:
                regenDataTup = resRegen(sample1, altfp, '', '', axis=optionsDict['axis'], sliceNum=optionsDict['slice'], searchWindowLen=128, windowSize=128, distMultiplier=optionsDict['distMultiplier'], sdfDistMin=optionsDict['distMultiplier'], forceType = 'stats', forceMass = setMass, regenStats=True)
                sampleArray[i, i, 0] = sampleList[i] 
                sampleArray[i, i, 1] = sampleList[i] 
                regenMass = regenDataTup[2]
            charLenList.append(regenDataTup[0])
            charMassList.append(regenMass)
            sampleMassArray[i, i] = regenMass
            sampleLenArray[i,i] = charLenList[i]/regenDataTup[0]
            sampleMassDiffArray[i, i] = charMassList[i] - regenMass 
            titleList[i] = nameList[i] + '\nL: {:4}\nM: {:.6}'.format(charLenList[i], charMassList[i])

        for i in range(len(sampleList)):
            for j in range(len(sampleList)):
                if j != i:
                    sample1 = sampleArray[i,i,0] 

                    if relativeRegen:
                        altRegenJson = '../dream3d-samples/regen/{}-{}-{}-{}-regen.json'.format(jobNumber, i, j, nameList[i].replace(' ', '').replace('.','').replace(':','-'))
                        altRegenD3 = '../dream3d-samples/regen/{}-{}-{}-{}-regen.dream3d'.format(jobNumber, i, j,nameList[i].replace(' ', '').replace('.','').replace(':','-'))
                        altfp = sampleList[j] 
                        regenDataTup = resRegen(sample1, altfp, altRegenJson, altRegenD3, axis=optionsDict['axis'], sliceNum=optionsDict['slice'], searchWindowLen=128, windowSize=128, distMultiplier=optionsDict['distMultiplier'], sdfDistMin=optionsDict['distMultiplier'], forceType = 'setlen', forceLen=charLenList[i], regenStats=True)
                        sample2 = altRegenD3
                        regenMass = regenDataTup[7]
                        #nameList[i] = nameList[i] + '- regen'
                        sampleArray[i, j, 0] = sample1 
                        sampleArray[i, j, 1] = sample2
                    else:
                        #Use the reference samples
                        altfp = sampleArray[j,j,0] 
                        regenDataTup = resRegen(sample1, altfp, '', '', axis=optionsDict['axis'], sliceNum=optionsDict['slice'], searchWindowLen=128, windowSize=128, distMultiplier=optionsDict['distMultiplier'], sdfDistMin=optionsDict['distMultiplier'], forceType = 'stats', forceLen=charLenList[i], regenStats=True)
                        sample2 = sampleArray[j,j,0]
                        origMass = regenDataTup[1]
                        regenMass = regenDataTup[2]
                        #nameList[i] = nameList[i] + '- regen'
                        sampleArray[i, j, 0] = sample1 
                        sampleArray[i, j, 1] = sample2
                    sampleLenArray[i,j] = charLenList[i]/regenDataTup[0]
                    sampleMassArray[i, j] = regenMass 
                    sampleMassDiffArray[i, j] = charMassList[i] - regenMass 
                    #print('MASS DIFF:', sampleMassDiffArray[i,j], charMassList[i], regenMass, i,j)
                    #print('charMassList:', charMassList)
                    #print('charLenList:', charLenList)
                else:
                    continue
                    #sample1 = sampleList[i]
                    #sample2 = sampleList[j]
                        
                #sampleArray[i, j, 0] = sample1 
                #sampleArray[i, j, 1] = sample2
        sampleList = sampleArray
    #lmult = 10
    lmult = optionsDict['forceWindowLen'] + optionsDict['forceWindowLen'] #AT LEAST THE DIAGNOL, MAX DISTANCE IN TAXICAB
    Cscale = 1
    endlmult =1
    #fig, axs = plotMicroStruct(sampleList, keepValues='random', axis = optionsDict['axis'], sliceNum=optionsDict['slice'])
    if isinstance(optionsDict['slice'], list):
        sl = optionsDict['slice'][0]
    elif isinstance(optionsDict['slice'], str):
        sl = 'half'
    else:
        sl = optionsDict['slice']

    doMicroPlot = True
    if doMicroPlot:
        if regen and relativeRegen:
            for i in range(sampleList.shape[0]):
                fig, axs = plotMicroStruct(list(sampleList[i, :, 1]), axis = optionsDict['axis'], sliceNum=sl, noGrainInWindow=optionsDict['noGrainInWindow'], forceWindowLen=optionsDict['forceWindowLen'], subplotStyle = 'row', sdf=True, sharedcm=True, optionsDict=optionsDict.copy())
        elif regen and not relativeRegen:
            fig, axs = plotMicroStruct(list(sampleList[0, :, 1]), axis = optionsDict['axis'], sliceNum=sl, noGrainInWindow=optionsDict['noGrainInWindow'], forceWindowLen=optionsDict['forceWindowLen'], subplotStyle = 'row', sdf=True, sharedcm=True, optionsDict=optionsDict.copy())
        else:
            fig, axs = plotMicroStruct(sampleList, axis = optionsDict['axis'], sliceNum=sl, noGrainInWindow=optionsDict['noGrainInWindow'], forceWindowLen=optionsDict['forceWindowLen'], sdf=True, sharedcm=True, optionsDict=optionsDict.copy())
        plt.ioff()
        plt.savefig('{}/{}-{}-micro.png'.format(outDir, jobNumber, i), dpi = 300, bbox_inches='tight')
        plt.close(fig)

    print(sampleList)
    print(nameList)
    print(optionsDict)
    print('Nodes = {}'.format(numNodes))
    optionsList = [] 
    for i in range(len(sampleList)):
        optionsList.append(optionsDict)
    
    #ccmat = np.zeros((5,5))
    print('Nodes = {}'.format(numNodes))
    if isinstance(optionsDict, list):
        winNum=optionsDict[0]['windowNum']
    else:
        winNum=optionsDict['windowNum']
    
    if isinstance(optionsDict, list):
        winSize=optionsDict[0]['windowNum']
    else:
        winSize=optionsDict['windowNum']
        
    
    
    if 'forceWinSym' in optionsDict: forceWinSym = optionsDict['forceWinSym']
    else: forceWinSym = False
    
    if 'centerOnRandGrain' in optionsDict: centerOnRandGrain = optionsDict['centerOnRandGrain']
    else: centerOnRandGrain = False
    
    if 'forceWinSym' in optionsDict: forceWinSym = optionsDict['forceWinSym']
    else: forceWinSym = False
    
    if 'microCrop1' in optionsDict: microCrop1 = optionsDict['microCrop1']
    else: microCrop1 = None
    
    if 'microCrop2' in optionsDict: microCrop2 = optionsDict['microCrop2']
    else: microCrop2 = None
    
    if 'sdfType' in optionsDict: sdfType = optionsDict['sdfType']
    else: sdfType = 'overlap'
    
    if 'roundMult' in optionsDict: roundMult = optionsDict['roundMult']
    else: roundMult = 1e6
    
    if 'addNorm' in optionsDict: addNorm = optionsDict['addNorm']
    else: addNorm = False
    

    #plotTitle='Windows {} - {} ESD - Wass Score Penalty: {} - lmults: {} & {} - Cscale: {}'.format(winNum, winSizeStr, usePenaltyFinal, lmult, endlmult, Cscale) 
    plotTitle='{} Windows - {} px - Sym Pairwise: {} - SDF: {} - Grain Centering: {} - Crop: {} px & {} px'.format(winNum, winSize, forceWinSym, sdfType, centerOnRandGrain, microCrop1, microCrop2) 
    print(plotTitle)

    ccmat, figList, dataList, geoccmat, massccmat, massmat= crossMatrix(sampleList, optionsList, titleList, identityShortcut = True, lmult = lmult, Cscale = Cscale, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult, hardW=hardW, doMatrixPlot = True, multiproc = multiproc, cores = numNodes, massVals = sampleMassDiffArray, lenVals = sampleLenArray, roundMult = roundMult, addNorm = addNorm, verbose=False)
    if len(figList) != 0:
        print(figList)
        fig = figList[0][0]
        ax=figList[0][1]
        plt.suptitle(plotTitle)
        plt.ioff()
        plt.savefig('{}/{}-ccmat.png'.format(outDir, jobNumber), dpi = 300, bbox_inches='tight')
        plt.close(fig)
        
    print(sampleList)
    print(nameList)
    print(optionsDict)
    print('Nodes = {}'.format(numNodes))
    print(ccmat)
        

    if writeOutFile:
        outFile = open("{}/{}-ccmat.txt".format(outDir, jobNumber), 'w')
        outFile.write("<SCORES>\n")
        outFile.write(np.array2string(ccmat,separator=',').replace('[','').replace('],','').replace(']]','').replace(' ', '')+'\n')
        outFile.write("<SCORES GEO CONTRIB>\n")
        outFile.write(np.array2string(geoccmat,separator=',').replace('[','').replace('],','').replace(']]','').replace(' ', '')+'\n')
        outFile.write("<SCORES MASS CONTRIB>\n")
        outFile.write(np.array2string(massccmat,separator=',').replace('[','').replace('],','').replace(']]','').replace(' ', '')+'\n')
        outFile.write("<SCORES MASS>\n")
        outFile.write(np.array2string(massmat,separator=',').replace('[','').replace('],','').replace(']]','').replace(' ', '')+'\n')
        outFile.write("<SAMPLES>\n")
        for i in range(len(sampleList)):
            outFile.write("{}) {} : {}\n".format(i, nameList[i], sampleList[i]))
        outFile.write("<RUN OPTIONS>\n")
        for items in optionsDict.items():
            outFile.write("{} : {}\n".format(items[0], items[1]))
        outFile.write("<RUN INFO>\n")
        outFile.write('\nProcesses Used : {}\n'.format(numNodes))

        endascTime = time.asctime()
        endTime = time.time()
        outFile.write('Start : {}\n'.format(startascTime))
        outFile.write('End : {}\n'.format(endascTime))
        outFile.write('Run Time : {} min\n'.format((endTime-startTime)/60))
        
        outFile.write("<WINDOW SCORES>\n")
        np.set_printoptions(precision=3)
        for i in range(len(dataList)):
            outFile.write("{}: {}\n".format(dataList[i][0], dataList[i][1]))
            outFile.write("Score: {}\n".format(dataList[i][2]))
            outFile.write("Coupling (Gamma):\n{}\n".format(dataList[i][3]))
            outFile.write("Window Scores:\n{}\n".format(dataList[i][4]))
        outFile.close()
        np.set_printoptions(precision=8)

    quit()
    

#[[ 139.60762601  359.52760737  193.29013139  201.30363988  163.38687784  141.20962674]
# [1944.41999225 3297.52863765 2269.58607338 1644.47080485 2009.20404517 1536.50602805]
# [ 187.43915598  404.83253879  171.91968008  245.61199955  194.10987789  185.30930691]
# [ 108.1522363   185.07041256  117.93920786   34.61205892  106.68195909  113.17770899]
# [ 273.74955842  707.92871669  350.07658712  366.04634501  252.274858    285.64568944]
# [ 407.99643235 2169.88644953  781.44733716  510.69157174  432.22368371  188.27346661]]

def linearMain(preset='main'):
    writeOutFile = True

    startascTime = time.asctime()
    startTime = time.time()
    sampleList1 = []
    sampleList2 = []
    nameList = []
    stylesList = []

    
    if preset == 'windowNum': 
        print('===STARTING: WINDOW NUMBER===') 
    
        optionsDictBase = {
                'windowNum':5,
                'axis':'y',
                'slice': 'all', #np.random.randint(0, 128, 5),
                'relativeScale':False,
                'totalAreaHold' : 1,
                'noGrainInWindow':4,
                'forceWindowLen':20, #Overrides noGrainInWindow
                'winSizeDetermine':'max',
                'distMultiplier':1,
                'centerOnRandGrain': False,
                'distType' : 'sdf',
                'sdfDistMin': 0,
                'sdfPenaltyType':'constesd',
                'ignoreBoundaryEdges':True,
                'verbose':False,
                'forceCellLength':True,
                'sdfNormalize':False, #Forces each window to have the same amount of mass
                'sdfReverse': True,
                'sdfRoot':False,
                'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
                'sdfFloorNorm':False,
                'sdfThresh':0,
                'forceWinSym':True,
                'roundMult':5e4,
                'addNorm':True,
                'availProcs':numNodes,
                'multiproc':multiproc,
        }
        s2nameList = []
        lastBatchNum = 0
        optionsDict = optionsDictBase
    

        #Notes. -regen are 128x128x128 samples set to an average mass of 4e4 
        #Notes. -regenL are 180x180x180 samples set to an average mass of 4e4 and thus have REALLY SMALL GRAINS. Works well though.
        #Notes. -regenL2 are 180x180x180 samples set to an average mass of (4e4/128^3)*180^3
        #Notes. -res2x are 256x256x256 samples with double magnification
        
        if sampleSet == 'tile':
            print('SAMPLE SET: TILE')
            sampleList2.append(sg.brickTile(256, 12, 12, tileType = 'straight')) #straight
            s2nameList.append('Bricks, 8x8 pixels')
            stylesList.append('rs-')
            sampleList2.append(sg.brickTile(256, 8, 18, tileType = 'straight')) #straight
            s2nameList.append('Bricks, 6x10 pixels')
            stylesList.append('bo-')
            sampleList2.append(sg.brickTile(256, 8, 18, tileType = 'herringbone')) #straight
            s2nameList.append('Bricks, 6x10 pixels, herringbone')
            stylesList.append('bP-')
            sampleList2.append(sg.hexagonTile(256, 7)) #straight
            s2nameList.append('Hexagons, 7 pixel sides')
            stylesList.append('bh-')
        elif sampleSet =='exp':
            print('SAMPLE SET: EXPERIMENTAL')
            sampleList2.append('../experimental/ni-alloy-groeber-ebsd/SmallIN100_Experimental.dream3d')
            s2nameList.append('Experimental IN100, ')
            stylesList.append('b--')
            sampleList2.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic-2.dream3d')
            s2nameList.append('Synthetic IN100, Same Gen.')
            stylesList.append('go-')
            sampleList2.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic.dream3d')
            s2nameList.append('Synthetic IN100')
            stylesList.append('g-')
            optionsDictBase['sdfType'] = 'sweep'
            optionsDictBase['microCrop1'] = 160
            optionsDictBase['microCrop2'] = 160
            optionsDictBase['axis'] = 'x'
        else:
            print('SAMPLE SET: SYNTHETIC')

            sampleList2.append('../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
            s2nameList.append('Rolled, 2x Mag')
            stylesList.append('k-')
            sampleList2.append(  '../dream3d-samples/eq-bimodal-2x-matrix-res2x.dream3d') #two sizes, but small
            s2nameList.append('Bimodal 2x, 2x Mag')
            stylesList.append('ko:')
            sampleList2.append(           '../dream3d-samples/eq2x-matrix-res2x.dream3d') #twice as big
            s2nameList.append('Equiaxed, 2x ESD, 2x Mag')
            stylesList.append('ko--')
            sampleList2.append(            '../dream3d-samples/eq1-matrix-res2x.dream3d') #equiaxed
            s2nameList.append('Equiaxed, 2x Mag')
            stylesList.append('k--')
            sampleList2.append( '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
            s2nameList.append('Bimodal 0.5x, 2x Mag')
            stylesList.append('k:')

        
        var = 'windowNum'
        varRange = np.int_(np.linspace(50, 500, 6))
        #varRange = np.int_(np.linspace(50, 200, 4))
        
        if smallTest:
            optionsDictBase['slice']='half'
            varRange = np.int_(np.linspace(50, 200, 3))

        print(varRange) 
        s1fp = sampleList2[0]
        s1name = s2nameList[0]
        for i in range(0, len(sampleList2)): 
            sampleList1.append(s1fp)
            s2name = s2nameList[i]
            nameList.append(s1name + ' v. ' + s2name)
        
        for i in range(len(sampleList1)):
            print(nameList[i], '-', sampleList1[i], 'vs.', sampleList2[i])

        regen = False
        refRegen= False 
        
    if preset == 'windowSize': 
        
        optionsDict = []
        #THIS OPTION SET WORKS.
        #optionsDictBase = {
        #        'windowNum':250,
        #        'axis':'y',
        #        'slice': 'all', #np.random.randint(0, 128, 20),
        #        'relativeScale':False,
        #        'totalAreaHold' : 1,
        #        'noGrainInWindow':4,
        #        'forceWindowLen':40, #Overrides noGrainInWindow
        #        'winSizeDetermine':'max',
        #        'distMultiplier':1,
        #        'centerOnRandGrain': False,
        #        'distType' : 'sdf',
        #        'sdfDistMin': 0,
        #        'sdfPenaltyType':'constesd',
        #        'ignoreBoundaryEdges':True,
        #        'verbose':False,
        #        'forceCellLength':True,
        #        'sdfNormalize':False, #Forces each window to have the same amount of mass
        #        'sdfReverse': True,
        #        'sdfRoot':False,
        #        'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
        #        'sdfFloorNorm':False,
        #        'sdfThresh':0
        #}
        #SYNTHETIC SAMPLES
        #optionsDictBase = {
        #        'windowNum':500,
        #        'axis':'y',
        #        'slice': 'all', #'half',  #np.random.randint(0, 117, 5),
        #        'relativeScale':False,
        #        #'microCrop1':160,
        #        #'microCrop2':160,
        #        'totalAreaHold' : 1,
        #        'noGrainInWindow':4,
        #        'forceWindowLen':20, #Overrides noGrainInWindow
        #        'winSizeDetermine':'max',
        #        'distMultiplier':1,
        #        'centerOnRandGrain': False,
        #        'distType' : 'sdf',
        #        'sdfDistMin': 0,
        #        'sdfPenaltyType':'constesd',
        #        'ignoreBoundaryEdges':True,
        #        'verbose':False,
        #        'forceCellLength':True,
        #        'sdfNormalize':False, #Forces each window to have the same amount of mass
        #        'sdfReverse': True,
        #        'sdfRoot':False,
        #        'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
        #        'sdfFloorNorm':False,
        #        'sdfThresh':0,
        #        'sdfType':'overlap',
        #        'forceWinSym':True
        #}
        #EXPERIMENTAL SAMPLES
        optionsDictBase = {
                'windowNum':300,
                'axis':'y',
                'slice': 'all', #np.random.randint(0, 128, 5),
                'relativeScale':False,
                'totalAreaHold' : 1,
                'noGrainInWindow':4,
                'forceWindowLen':20, #Overrides noGrainInWindow
                #'microCrop1':160,
                #'microCrop2':160,
                'winSizeDetermine':'max',
                'distMultiplier':1,
                'centerOnRandGrain': False,
                'distType' : 'sdf',
                'sdfDistMin': 0,
                'sdfPenaltyType':'constesd',
                'ignoreBoundaryEdges':True,
                'verbose':False,
                'forceCellLength':True,
                'sdfNormalize':False, #Forces each window to have the same amount of mass. Only matters for plotting. 
                'sdfReverse': True,
                'sdfRoot': False,
                'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
                'sdfFloorNorm':False,
                'sdfThresh':0,
                'sdfType':'overlap',
                'forceWinSym':True,
                'roundMult':5e4,
                'addNorm':True,
                'availProcs':numNodes,
                'multiproc':multiproc,
        }
        s2nameList = []
        lastBatchNum = 0
       
        #var = 'noGrainInWindow'
        #varRange = np.linspace(6, 1, 10)
        var = 'forceWindowLen'
        varRange = np.linspace(60, 5, 6)
        
        if smallTest:
            optionsDictBase['slice']='half'
            optionsDictBase['windowNum']=25
            varRange = np.linspace(40, 10, 3)

        
        optionsDictBase['sdfPenaltyType'] = 'constesd'
        print(optionsDictBase)
        #Notes. -regen are 128x128x128 samples set to an average mass of 4e4 
        #Notes. -regenL are 180x180x180 samples set to an average mass of 4e4 and thus have REALLY SMALL GRAINS. Works well though.
        #Notes. -regenL2 are 180x180x180 samples set to an average mass of (4e4/128^3)*180^3
        #Notes. -res2x are 256x256x256 samples with double magnification
        if sampleSet == 'tile':
            print('SAMPLE SET: TILE')
            sampleList2.append(sg.brickTile(256, 12, 12, tileType = 'straight')) #straight
            s2nameList.append('Bricks, 8x8 pixels')
            stylesList.append('rs-')
            sampleList2.append(sg.brickTile(256, 8, 18, tileType = 'straight')) #straight
            s2nameList.append('Bricks, 6x10 pixels')
            stylesList.append('bo-')
            sampleList2.append(sg.brickTile(256, 8, 18, tileType = 'herringbone')) #straight
            s2nameList.append('Bricks, 6x10 pixels, herringbone')
            stylesList.append('bP-')
            sampleList2.append(sg.hexagonTile(256, 7)) #straight
            s2nameList.append('Hexagons, 7 pixel sides')
            stylesList.append('bh-')
        elif sampleSet =='exp':
            print('SAMPLE SET: EXPERIMENTAL')
            sampleList2.append('../experimental/ni-alloy-groeber-ebsd/SmallIN100_Experimental.dream3d')
            s2nameList.append('Experimental IN100, ')
            stylesList.append('b--')
            sampleList2.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic-2.dream3d')
            s2nameList.append('Synthetic IN100, Same Gen.')
            stylesList.append('go-')
            sampleList2.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic.dream3d')
            s2nameList.append('Synthetic IN100')
            stylesList.append('g-')
            optionsDictBase['sdfType'] = 'sweep'
            optionsDictBase['microCrop1'] = 160
            optionsDictBase['microCrop2'] = 160
            optionsDictBase['axis'] = 'x'
        else:
            print('SAMPLE SET: SYNTHETIC')

            sampleList2.append('../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
            s2nameList.append('Rolled, 2x Mag')
            stylesList.append('k-')
            sampleList2.append(  '../dream3d-samples/eq-bimodal-2x-matrix-res2x.dream3d') #two sizes, but small
            s2nameList.append('Bimodal 2x, 2x Mag')
            stylesList.append('ko:')
            sampleList2.append(           '../dream3d-samples/eq2x-matrix-res2x.dream3d') #twice as big
            s2nameList.append('Equiaxed, 2x ESD, 2x Mag')
            stylesList.append('ko--')
            sampleList2.append(            '../dream3d-samples/eq1-matrix-res2x.dream3d') #equiaxed
            s2nameList.append('Equiaxed, 2x Mag')
            stylesList.append('k--')
            sampleList2.append( '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
            s2nameList.append('Bimodal 0.5x, 2x Mag')
            stylesList.append('k:')

        #sampleList2.append('../experimental/steel3ebsd.dream3d') 
        #s2nameList.append('Steel EBSD, Small')
        #stylesList.append('r:')
        #sampleList2.append('../experimental/steel1ebsd.dream3d') 
        #s2nameList.append('Steel EBSD, Large')
        #stylesList.append('ro-')
        #sampleList2.append('../experimental/steel2ebsd.dream3d') 
        #s2nameList.append('Steel EBSD, Med')
        #stylesList.append('r--')
        #sampleList2.append('../experimental/ni-alloy-groeber-ebsd/SmallIN100_Experimental.dream3d')

        
        #sampleList2.append('../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
        #s2nameList.append('Rolled, 2x Mag')
        #stylesList.append('k-')
        #sampleList2.append(  '../dream3d-samples/eq-bimodal-2x-matrix-res2x.dream3d') #two sizes, but small
        #s2nameList.append('Bimodal 2x, 2x Mag')
        #stylesList.append('ko:')
        #sampleList2.append(           '../dream3d-samples/eq2x-matrix-res2x.dream3d') #twice as big
        #s2nameList.append('Equiaxed, 2x ESD, 2x Mag')
        #stylesList.append('ko--')
        #sampleList2.append(            '../dream3d-samples/eq1-matrix-res2x.dream3d') #equiaxed
        #s2nameList.append('Equiaxed, 2x Mag')
        #stylesList.append('k--')
        #sampleList2.append( '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
        #s2nameList.append('Bimodal 0.5x, 2x Mag')
        #stylesList.append('k:')
        
        
        
        for i in range(len(sampleList2) - lastBatchNum):
            optionsDict.append(optionsDictBase.copy())
        lastBatchNum = len(sampleList2)

        
      
        #THIS IS AN IMPORTANT OPTION!
        regen = False
        refRegen= False #Only works if forceType is 'setmass' since then the referecne structure is not used
        regenWindowSize = 256
        setMass = 4e4 #It seems like 4e4 mass for 128^3 volume works decently well. Scale to that.
        forceType = 'resize' #'setmass'
            
        #REFERENCE SAMPLE 
        s1name=s2nameList[0] #Changeable
        s1fp =sampleList2[0]
        #sample1 regen
        if refRegen:
            altRegenJson = '../dream3d-samples/regen/{}-{}-regen.json'.format(jobNumber, s1name.replace(' ', '').replace('.','').replace(':','-'))
            altRegenD3 = '../dream3d-samples/regen/{}-{}-regen.dream3d'.format(jobNumber,s1name.replace(' ', '').replace('.','').replace(':','-'))
            regenDataTup = resRegen(s1fp, s1fp, altRegenJson, altRegenD3, axis=optionsDictBase['axis'], sliceNum=optionsDictBase['slice'], searchWindowLen=regenWindowSize, windowSize=regenWindowSize, distMultiplier=optionsDictBase['distMultiplier'], sdfDistMin=optionsDictBase['distMultiplier'], forceType=forceType, forceMass=setMass, regenStats=True)
            s1name=s1name + ' - Regen' #Changeable
            s1fp = altRegenD3


        for i in range(0, len(sampleList2)): 
            #sampleList1.append(sg.brickTile(110, 5,10, tileType = 'straight')) #straight
            #sampleList1.append('../dream3d-samples/eq-bimodal-05x-matrix.dream3d') #two sizes, but small
            #sampleList1.append('../dream3d-samples/r-2-1-05-orient-matrix.dream3d') #rolled
            #s1name = 'Rolled 2:1:0.5'
            #sampleList1.append(sampleList2[0])
            #s1name = 'Rolled 2:1:0.5 Regen'
            #sampleList1.append('../dream3d-samples/eq1-matrix.dream3d') #equiaxed
            #sampleList1.append(sampleList2[0])
            
            sampleList1.append(s1fp)

            s2name = s2nameList[i]
            if regen and isinstance(sampleList2[i], str):
                altfp = sampleList2[i] 
                altRegenJson = '../dream3d-samples/regen/{}-{}-regen.json'.format(jobNumber, s2name.replace(' ', '').replace('.','').replace(':','-'))
                altRegenD3 = '../dream3d-samples/regen/{}-{}-regen.dream3d'.format(jobNumber,s2name.replace(' ', '').replace('.','').replace(':','-'))
                regenDataTup = resRegen(s1fp, altfp, altRegenJson, altRegenD3, axis=optionsDictBase['axis'], sliceNum=optionsDictBase['slice'], searchWindowLen=regenWindowSize, windowSize=regenWindowSize, distMultiplier=optionsDictBase['distMultiplier'], sdfDistMin=optionsDictBase['distMultiplier'], forceType=forceType, forceMass=setMass, regenStats=True)
                sampleList2[i] = altRegenD3
                s2name = s2name + '- regen'


            nameList.append(s1name + ' v. ' + s2name + ' (ESD: {ESD1:.2f}, {ESD2:.2f}) - {PTYPE}')
        for i in range(len(sampleList1)):
            print(nameList[i], '-', sampleList1[i], 'vs.', sampleList2[i])
        

        
        #fig, axs = plotMicroStruct([sampleList1[0]] + sampleList2[1:], keepValues='random', subplotStyle='row', axis = optionsDictBase['axis'], sliceNum=optionsDictBase['slice'])
        #plt.ioff()
        #plt.savefig('{}/{}-micro.png'.format(outDir, jobNumber, var), dpi = 300, bbox_inches='tight')
        #plt.close(fig)

    if var != 'lmult': lmult = 100
    else: lmult = 'variable'
    
    if var != 'Cscale': Cscale = 1
    else: Cscale = 'variable'
    
    if var != 'endlmult': endlmult = 1
    else: endlmult = 'variable'

    if var == 'noGrainInWindow': winSizeStr = 'variable'
    else: winSizeStr = optionsDictBase['noGrainInWindow']

    hardW = '2dhist' #massdiff, 2dhist, true or false

    print('Preset = {}'.format(preset))
    print('Nodes = {}'.format(numNodes))
    
    if var == 'windowNum': winNum='Variable'
    else:
        if isinstance(optionsDictBase, list):
            winNum=optionsDictBase[0]['windowNum']
        else:
            winNum=optionsDictBase['windowNum']

    if var == 'windowSize': winSize='Variable'
    else: winSize = optionsDictBase['forceWindowLen']
    
    if 'forceWinSym' in optionsDictBase: forceWinSym = optionsDictBase['forceWinSym']
    else: forceWinSym = False
    
    if 'centerOnRandGrain' in optionsDictBase: centerOnRandGrain = optionsDictBase['centerOnRandGrain']
    else: centerOnRandGrain = False
    
    if 'forceWinSym' in optionsDictBase: forceWinSym = optionsDictBase['forceWinSym']
    else: forceWinSym = False
    
    if 'microCrop1' in optionsDictBase: microCrop1 = optionsDictBase['microCrop1']
    else: microCrop1 = None
    
    if 'microCrop2' in optionsDictBase: microCrop2 = optionsDictBase['microCrop2']
    else: microCrop2 = None
    
    if 'sdfType' in optionsDictBase: sdfType = optionsDictBase['sdfType']
    else: sdfType = 'overlap'
    
    if 'slice' in optionsDictBase: sliceNum = optionsDictBase['slice']
    else: sliceNum = 'unknown'
    
    if 'roundMult' in optionsDictBase: roundMult = optionsDictBase['roundMult']
    else: roundMult = 1e6
    
    if 'addNorm' in optionsDictBase: addNorm = optionsDictBase['addNorm']
    else: addNorm = False
    
    
    #plotTitle='Windows {} - {} ESD - Wass Score Penalty: {} - lmults: {} & {} - Cscale: {}'.format(winNum, winSizeStr, usePenaltyFinal, lmult, endlmult, Cscale) 
    plotTitle='{} Windows - {} px - Sym Pairwise: {} - SDF: {} - Centering: {} - Crop: {} & {} px - slice: {} - +Norm: {} - roundMult: {}'.format(winNum, winSize, forceWinSym, sdfType, centerOnRandGrain, microCrop1, microCrop2, sliceNum, addNorm, roundMult) 
    print(plotTitle)
        
    scoreMat, figList, dataList = linearCompare(sampleList1, sampleList2, var, varRange, optionsDict, titles=nameList, lmult = lmult, Cscale = Cscale, multiproc = multiproc, cores = numNodes, doPlot = True, plotTitle=plotTitle, usePenaltyFinal=usePenaltyFinal, endlmult=endlmult, plotStyles=stylesList, partialScores=False, hardW=hardW, roundMult = roundMult, addNorm = addNorm, verbose=False)

    #ccmat = np.zeros((5,5))
    if len(figList) != 0:
        fig = figList[0]
    
    for i in range(len(sampleList1)):
        print(nameList[i], '-', sampleList1[i], 'vs.', sampleList2[i])
    print(optionsDict)
    print('Nodes = {}'.format(numNodes))
     
    if writeOutFile:
        outStr = ''
        outFile = open("{}/{}-linmat.txt".format(outDir, jobNumber), 'w')

        outStr = outStr + '<SCORES>\n'
        outStr = outStr + np.array2string(scoreMat,separator=',').replace('[','').replace('],','').replace(']]','').replace(' ', '')+'\n'
        outStr = outStr + '<VARNAME>\n'
        outStr = outStr + var + '\n' 
        outStr = outStr + '<VARRANGE>\n'
        outStr = outStr + np.array2string(varRange,separator=',').replace('[','').replace('],','').replace(']]','').replace(' ', '')+'\n'
        #outStr = outStr + '<NAMES>\n'
        #for name in nameList:
        #    outStr = outStr + name + '\n' 

        outStr = outStr+'\n'
        outStr=outStr+"<SAMPLES>\n"
        for i in range(len(sampleList1)):
            outStr=outStr+"{}) {}: {} {}\n".format(i, nameList[i], sampleList1[i], sampleList2[i])
        outStr=outStr+"<STYLES>\n"
        for i in range(len(stylesList)):
            outStr=outStr+"{}) {} | {}\n".format(i, nameList[i], stylesList[i])
        outStr=outStr+"<RUN OPTIONS>\n"
        for items in optionsDictBase.items():
            outStr=outStr+"{} : {}\n".format(items[0], items[1])
        
        outStr=outStr+"<RUN INFO>\n"
        outStr=outStr+'\nProcesses Used = {}\n'.format(numNodes)

        endascTime = time.asctime()
        endTime = time.time()
        outStr=outStr+'Start: {}\n'.format(startascTime)
        outStr=outStr+'End: {}\n'.format(endascTime)
        outStr=outStr+'Run Time: {} min\n'.format((endTime-startTime)/60)
        outStr = outStr+'\n'
        outStr=outStr+"<WINDOW SCORES>\n"
        np.set_printoptions(precision=3)
        for i in range(len(dataList)):
            outStr=outStr+"{}: {}\n".format(dataList[i][0], dataList[i][1])
            outStr=outStr+"Score: {}\n".format(dataList[i][2])
            outStr=outStr+"Coupling (Gamma):\n"
            outStr = outStr + np.array2string(dataList[i][3],separator=',').replace('[','').replace('],','').replace(']]','')+'\n'
            outStr=outStr+"Window Scores:\n"
            outStr = outStr + np.array2string(dataList[i][4],separator=',').replace('[','').replace('],','').replace(']]','')+'\n'
            outStr = outStr+'\n'
        np.set_printoptions(precision=8)

        outFile.write(outStr)
        outFile.close()
    
    print(scoreMat)
    plt.ioff()
    #plt.show()
    plt.savefig('{}/{}-lincompare.png'.format(outDir, jobNumber, var), dpi = 300, bbox_inches='tight')
    plt.close(fig)
   


def simplexMain():
    #variables
    startascTime = time.asctime()
    startTime = time.time()

    dataFolder = '../simplex-data'
    runFolder = '{}/{}'.format(dataFolder, jobNumber)
    runDataFolder = '{}/data-dream3d'.format(runFolder, jobNumber)
    runParaFolder = '{}/parameters-json'.format(runFolder, jobNumber)
    runPicFolder = '{}/micro-png'.format(runFolder, jobNumber)
    try:
        os.mkdir(dataFolder)
    except:
        pass
    try:
        os.mkdir(runFolder)
    except:
        print("Heads up! Run folder already exists!")
        pass
    try:
        os.mkdir(runDataFolder)
    except:
        print("Heads up! Run Data folder already exists!")
        pass
    try:
        os.mkdir(runParaFolder)
    except:
        print("Heads up! Run Parameters folder already exists!")
        pass
    try:
        os.mkdir(runPicFolder)
    except:
        print("Heads up! Run Pics folder already exists!")
        pass
    comparePath = '../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d'
    d3path='Statistics/1'
    odfPath = None

    #comparePath ='../experimental/ni-alloy-groeber-ebsd/SmallIN100_Experimental.dream3d'
    #odfPath ='../experimental/ni-alloy-groeber-ebsd/SmallIN100_Experimental-odf_Phase_1.csv'
    #d3path = 'Statistics/1' #Where fitted statistics are
    #comparePath ='../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic.dream3d'
    #odfPath ='../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic-odf_Phase_1.csv'
    #d3path = 'Statistics/1' #Where fitted statistics are
    optionsDict = {
            'windowNum':400,
            'windowNumMin':100, #Synthetic Annealing specific
            'axis':'y',
            'slice': 'all', #DON'T FORGET TO CHANGE THIS.
            'relativeScale':False,
            'totalAreaHold' : 1,
            'noGrainInWindow':4,
            #'microCrop1':120,
            #'microCrop2':120,
            'forceWindowLen':20, #Overrides noGrainInWindow
            'winSizeDetermine':'max',
            'distMultiplier':1,
            'centerOnRandGrain': False,
            'distType' : 'sdf',
            'sdfDistMin': 0,
            'sdfPenaltyType':'constesd',
            'ignoreBoundaryEdges':True,
            'verbose':False, #This is for the window creation verbosity
            'forceCellLength':True,
            'sdfNormalize':False, #Forces each window to have the same amount of mass
            'sdfReverse': True,
            'sdfRoot':False,
            'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
            'sdfFloorNorm':False,
            'sdfThresh':0,
            'sdfType':'overlap', #sweep or overalp, use sweep for experimental,
            'availProcs':numNodes,
            'multiproc':multiproc,

    }
    lmult = 10
    Cscale = 1
    endlmult =1
    
    #FOR TESTING
    if smallTest:
        print('RUNNING A SMALLER TEST')
        optionsDict['windowNum'] = 50
        optionsDict['windowNumMin']=50
        #optionsDict['slice'] = 'half' #np.random.randint(0,115,10)
        optionsDict['slice'] = np.random.randint(0,255,3)

    sliceStats, d3Stats, d3DerivedStats = pm.getInitialStats(comparePath, sliceNumX=optionsDict['slice'], sliceNumY=optionsDict['slice'], sliceNumZ=optionsDict['slice'], dream3dContainer=d3path)# forceCellLength=optionsDict['forceCellLength'])
    avMu = sliceStats['vol']['parameters'][0]
    avStd = sliceStats['vol']['parameters'][1]  
    d3mu = d3DerivedStats['mu']
    d3sigma = d3DerivedStats['sigma']
    d3minCut = d3DerivedStats['minCut']
    d3maxCut = d3DerivedStats['maxCut']
    d3binStep = d3DerivedStats['binStep']
    #print(d3DerivedStats)
    startInputs = { 
        'baRatio': 0.8,
        'caRatio': 0.8,
        #'bacaRatio': 1.5,
        #'mu':1.5,
        #'sigma':0.4,
    }
    #(delta, min, max). None if there are no bounds or default. #Note that for the rolled case, caRatio must be lower than baRatio.
    startParameters = {
        'baRatio': (-0.1, 0.1, 1),
        'caRatio': (-0.1, 0.1, 1),
        #'bacaRatio': (-0.3, 1, 15),
        #'mu': (0.1, 0.1, 100),
        #'sigma': (0.1, 0.05, 100),

    }


    #For Ni Experimental. Mu = 1.1418676, Std = 0.6267919, minCut = 1.240701, maxCut = 13.240701, 13 steps, binStep = 1
    staticInputs = {
        'mu': d3mu, #avMu, 
        'sigma': d3sigma,#avStd, # np.mean(statsTup[1]),
        'binStep': d3binStep, #1,
        'minCut': d3minCut,#np.log(d3Stats['BinNumber'][0]),
        'maxCut': d3maxCut,#np.log(d3Stats['BinNumber'][-2]),
        #'forceBinCount':13,
        'axisLength': 32, #int(np.ceil(128*resDict['y'])), #Dimensions = ceil(axisLength / resolution)
        'resolution': 0.125, #resDict['y'],
        'euler1':0,
        'euler2':0,
        'euler3':0,
        'eulerSigma':1,
        'weight':40000,
        'compareFilePath': comparePath, #rolled
        'saveFolder': runFolder,
        'jsonFolder': runParaFolder,
        'dream3dFolder': runDataFolder,
        'microPicFolder': runPicFolder,
        'startWindow': 5,
        'stepWindow': 5,
        'maxWindow': 5,
        'optionsDict': optionsDict,
        'lmult':lmult,
        'Cscale':Cscale,
        'endlmult':endlmult,
        'usePenaltyFinal':usePenaltyFinal,
        'multiproc': multiproc,
        'cores': numNodes,
        'microPlot':True,
        'nameList':['xx', 'yy', 'zz'],
        'avgNum':1,
        'regen': False,
        'ODF-Filepath':odfPath,
        'ODF-Compress':True,
        'ODF-Thresh':10,
        'forceWinSym':True,
        'scoreMult':1, #I want to see if this affects pdfo.
        'roundMult': 5e4,
        'addNorm':True,
        'verbose':False #This is for the pairwise sdf window verbosity.
    }
    #remove odd values
    d3Stats.pop('FeatureSize Distribution')
    d3Stats.pop('BoundaryArea')
    d3Stats.pop('Name')
    d3Stats.pop('BinNumber')
    d3Stats.pop("FeatureSize Vs B Over A Distributions")
    d3Stats.pop("FeatureSize Vs C Over A Distributions")
    d3Stats.pop("FeatureSize Vs Neighbors Distributions")
    d3Stats.pop("FeatureSize Vs Omega3 Distributions")
    d3Stats.pop("ODF")

    #staticInputs.update(d3Stats) #NOTE. This will overwrite the ODF/MDf values w/ target file's. Comment this out if unwanted.
    #defaults
    startDelta = 1 
    maxLimit = None
    minLimit = None
    #SCALING FACTORS
    reflS = 1 #Alpha, scaling factor, a > 0
    expaS = 1.2 #Gamma, Expansion, gamma > 1
    contS = 0.1 #Rho, Contraction, 0 < rho < 0.5
    shrkS = 0.7 #Shrink
    
    evalParamSetup = opt.evalParamClass(startInputs, startParameters, staticInputs = staticInputs)
    #Self-Reference
    baseScore = 0
    doSelfRef = False
    if doSelfRef:
        #Base (Self v Self) Score:
        baseScore = evalParamSetup.selfEvaluate(setBaseScore=True)
        print('Base Score:', baseScore)

    #===SIMPLEX===
    if sampleSet == 'simplex':
        pointsInput, currentScores = opt.simplexGeometric(startInputs, staticInputs = staticInputs, startParameters = startParameters, startDelta = startDelta, maxLimit = maxLimit, minLimit = minLimit, evalFunc = opt.evaluateParameters, stopScoreThresh = 0.01, stopInputThresh = 0.05, reflS = reflS, expaS = expaS, contS = contS, shrkS = shrkS)
    

    #===SYNTH ANNEAL===
    elif sampleSet == 'anneal':
        Tsteps = [10, 7, 4, 1, 0]
        stateSteps = [40, 25, 15, 10, 10]
        passThresh=10
        if smallTest:
            Tsteps = [10, 5, 0]
            stateSteps = [10, 5, 3]
            passThresh=4
        pointsInput, currentScores, inputDictFinal, bestScore, inputDictBest = opt.simAnneal(
                                                                    startInputs,
                                                                    bounds=startParameters,
                                                                    #evaluateFunc = opt.randomEval,
                                                                    staticDict = staticInputs,
                                                                    paramFunc = opt.windowAnnealParamGen,
                                                                    tempUpdateFunc = opt.windowAnnealTempUpdate,
                                                                    Tmax = 10,
                                                                    Tsteps=Tsteps,
                                                                    stateSteps = stateSteps, 
                                                                    passThresh = passThresh, #how many before the iteration cuts early
                                                                    passBreak=False #if no better ones are found, algorithm will end early if True
                                                                    )

    #===FORCE LANDSCAPE===
    elif sampleSet == 'landscape':
        deltaNum = 10
        inputRangeDict = {varName:(varTup[1], varTup[2], deltaNum) for varName, varTup in startParameters.items()}
        dependencyDict={}
        #dependencyDict = {'caRatio':('baRatio', 'le')}
        #scoreArray, scoreDict
        pointsInput, currentScores, varNameList, rangeList= opt.bruteLandscape(inputRangeDict, staticDict=staticInputs, dependencyDict=dependencyDict) 
        fig, ax = plt.subplots(1,1, figsize=(5,5))
        fig, ax = landPlot(currentScores, xlabel='B/A', ylabel='C/A', selfRefPoint = (0.5, 0.25), selfRefStr=baseScore)
        plt.savefig('{}/{}-landscape.png'.format(runFolder, jobNumber), dpi=300)

    else: #PDFO is default 
        print('INITIAL VALUES:', evalParamSetup.getInitialValues())
        print('BOUNDS:', evalParamSetup.getBounds())
        pointsInput, res= opt.pdfoGeometric(evalParamSetup.getInitialValues(), evalFunc = evalParamSetup.evaluate, bounds= evalParamSetup.getBounds())
        currentScores = res.fun
        print('Current Score:', currentScores)
        print('Final Inputs', pointsInput)

    writeOutFile = True
    if writeOutFile:
        np.set_printoptions(threshold=sys.maxsize)
        outFile = open("{}/{}-simplex.txt".format(runFolder, jobNumber), 'w')
        outFile.write("<FINAL INPUTS>:\n")
        outFile.write(str(pointsInput)+"\n")
        outFile.write("<SCORES>\n")
        outFile.write(str(currentScores)+"\n")
        
        outFile.write("<INPUT VARS>:\n")
        for items in startInputs.items():
            varName = items[0]
            if varName in startParameters:
                sd, minv, maxv = startParameters[varName]
            else:
                sd = startDelta
                minv = minLimit
                maxv = maxLimit
            outFile.write("{} : {}, delta: {}, min: {}, max: {}\n".format(items[0], items[1], sd, minv, maxv))
        
        outFile.write("<STATIC INPUTS>\n")
        for items in staticInputs.items():
            if items[0] != 'optionsDict':
                outFile.write("{} : {}\n".format(items[0], items[1]))

        outFile.write("<RUN OPTIONS>\n")
        for items in optionsDict.items():
            outFile.write("{} : {}\n".format(items[0], items[1]))
        outFile.write('\nProcesses Used = {}\n'.format(numNodes))

        endascTime = time.asctime()
        endTime = time.time()
        outFile.write('Start : {}\n'.format(startascTime))
        outFile.write('End : {}\n'.format(endascTime))
        outFile.write('Run Time : {} min\n'.format((endTime-startTime)/60))
        outFile.close()
        #np.set_printoptions(threshold=1000)



def resMain(reffpList = None, optionsDict = None, loopType='micro', plot3d=False, sdf=True, sharedcm=True, overwrite=False):
    import marchingSquare as ms
    #generalized scratch area.
    #loopType is allmicro, micro, regen
    if isinstance(reffpList, type(None)):
        reffpList = []
        #reffpList.append(             '../dream3d-samples/eq1-matrix.dream3d') #equiaxed
        #reffpList.append( '../dream3d-samples/1040-AR-40x.dream3d') #rolled

        #reffpList.append( '../experimental/optical/test.dream3d') #rolled
        #reffpList.append( '../experimental/steelebsd/steel1ebsd.dream3d') 
        #reffpList.append( '../experimental/steelebsd/steel2ebsd.dream3d') 
        #reffpList.append( '../experimental/steelebsd/steel3ebsd.dream3d') 
        #reffpList.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Final.dream3d')
        #reffpList.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic.dream3d')
        #reffpList.append( '../experimental/ni-alloy-groeber-ebsd/SmallIN100_Synthetic-2.dream3d')
        #reffpList.append( '../experimental/al-15-mingwei/Al15-AN-2-Specimen-1-Site-2-Map-Data-6.dream3d')
        #reffpList.append( '../experimental/al-15-mingwei/Al15-AR2-Specimen-1-Site-3-Map-Data-4.dream3d')
        #reffpList.append( '../dream3d-samples/r-2-1-05-orient-matrix.dream3d') #rolled
        #reffpList.append( '../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
        #reffpList.append( '../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
        #reffpList.append( '../dream3d-samples/r-2-1-05-orient-matrix-res2x.dream3d') #rolled
        #reffpList.append(  '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
        #reffpList.append(  '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
        #reffpList.append(  '../dream3d-samples/eq-bimodal-05x-matrix-res2x.dream3d') #two sizes, but small
        reffpList.append(   '../dream3d-samples/eq-bimodal-2x-matrix-res2x.dream3d') #two sizes, but small
        #reffpList.append(            '../dream3d-samples/eq2x-matrix.dream3d') #two sizes, but small
        #reffpList.append(sg.brickTile(256, 12, 12, tileType = 'straight')) #straight
        #reffpList.append(sg.brickTile(256, 8, 18, tileType = 'straight')) #straight
        #reffpList.append(sg.brickTile(256, 8, 18, tileType = 'herringbone')) #straight
        #reffpList.append(sg.hexagonTile(256, 7)) #straight
    if isinstance(optionsDict, type(None)):
        optionsDict = {
                'windowNum':500,
                'axis':'y',
                'slice': 'half', #1,#'all', #DON'T FORGET TO CHANGE THIS.
                'forceWindowLen':40, #Overrides noGrainInWindow
                #'microCrop1':300,
                #'microCrop2':300,
                'distMultiplier':1,
                'centerOnRandGrain':'center',
                'sdfDistMin': 0,
                'ignoreBoundaryEdges':True, #This also prevents the entire microstructure slice from being calculated. Turn off for complex samples.
                'verbose':False,
                'forceCellLength':True,
                'sdfNormalize':False, #Forces each window to have the same amount of mass
                'sdfReverse': True,
                'sdfRoot':False,
                'sdfOnlyMass':True, #Does not calculate C and l if you're not using it for memory management.
                'sdfFloorNorm':False,
                'sdfThresh':0,
                'sdfType':'overlap' #sweep or overlap
        }
    #loopType='allmicro' #allmicro, micro, regen



    for refind in range(len(reffpList)):
        reffp = reffpList[refind]
        #refSampleSlice, refslen = pm.getSlice(reffp, 'y', 'half')
        #refSlice = refSampleSlice
        #altRegenJson = '../dream3d-samples/regen/eq1-rolled-regen.json'
        #altRegenD3 = '../dream3d-samples/regen/eq1-rolled-regen.dream3d'
        #resRegen(altfp, altRegenJson, altRegenD3, axis='y')

        #Replacing Values to regen
        if isinstance(reffp, str):
            regenfp = os.path.abspath(reffp.replace('.dream3d', '-res2x.dream3d'))
        if loopType == 'regen':
            regenjsonfp = regenfp.replace('.dream3d', '.json')
            
            print('Original:', reffp, '\nNew:', regenfp)
            jsonPipeDict = d3f.getPipeline(reffp)
            for key in jsonPipeDict.keys():
                if 'Filter_Human_Label' not in jsonPipeDict[key]:
                    continue

                if jsonPipeDict[key]['Filter_Human_Label'] == 'Initialize Synthetic Volume':
                    print('Changing Synthetic Volume')
                    resolution = 0.125
                    dimension = 256 
                    axisLength = int(np.ceil(dimension*resolution))
                    jsonPipeDict[key] = d3f.genSyntheticVolumeModule(axisLength=axisLength, resolution=resolution)

                if jsonPipeDict[key]['Filter_Human_Label'] == 'Write DREAM.3D Data File':
                    print('Replacing Dream3d Output Name!')
                    jsonPipeDict[key]['OutputFile'] = regenfp

                if jsonPipeDict[key]['Filter_Human_Label'] == 'Export ASCII Data':
                    print('Replacing Stats Output Name!')
                    jsonPipeDict[key]['OutputFilePath'] = regenfp.replace('.dream3d', '.csv') 
                    jsonPipeDict[key]['OutputPath'] = regenfp 

            for val, var in jsonPipeDict.items():
                print(val, type(val))
                print(var)
                print('\n')
            d3f.writePipeFromDict(regenjsonfp, jsonPipeDict)
            d3f.runDream3d(regenjsonfp)
            os.remove(regenjsonfp)
            fig, axs = plotMicroStruct([reffp, regenfp], subplotStyle = 'row', cmap='viridis')
            plt.show()
        elif loopType=='micro':
            
            if isinstance(reffp, str):
                savefn = '{}/{}'.format(os.getcwd(), reffp.rpartition('/')[2].replace('.dream3d', '.png'))
            else:
                savefn = '{}/{}-window.png'.format(os.getcwd(), refind)
            if plot3d:
                savefn= savefn.replace('.png', '-plot3d.png')
            if sdf:
                savefn= savefn.replace('.png', '-sdf.png')
            if not overwrite:
                if os.path.exists(savefn):
                    fileiter=0
                    notExists=True
                    while notExists:
                        fileiter+=1
                        testfn=savefn.replace('.png', '-{}.png'.format(fileiter))
                        notExists=os.path.exists(testfn)
                        print(notExists, testfn)
                    savefn=savefn.replace('.png', '-{}.png'.format(fileiter))
            print('savefn:', savefn)
            fig, axs = plotMicroStruct([reffp], subplotStyle = 'row', cmap='viridis', plot3d=plot3d, sdf=sdf, optionsDict=optionsDict, zbounds=(0, 10))
            #fig, axs = plotMicroStruct([reffp, regenfp], subplotStyle = 'row', cmap='viridis')
            plt.savefig(savefn, dpi = 300, bbox_inches='tight')
            #plt.show()
            plt.close(fig)
        #print('Standard')
        #t1 = time.time()
        #refSDF = ms.fastSweepSDF(refSlice, ignoreBoundaryEdges = False, uFunc = 'L1')
        #t2 = time.time()
        #print('Overlap')
        #t3 = time.time()
        #refOverSDF = ms.overlapSDF(refSlice, ignoreBoundaryEdges = False, uFunc = 'L1')
        #t4 = time.time()
        #print('standard:', t2-t1, 'over:', t4-t3)
    if loopType == 'allmicro':
        savefn = 'allmicro.png'
        if plot3d:
            savefn= savefn.replace('.png', '-plot3d.png')
        if sdf:
            savefn= savefn.replace('.png', '-sdf.png')
        fig, axs = plotMicroStruct(reffpList, subplotStyle = 'row', cmap='viridis', forceWindowLen = None, plot3d=plot3d, sdf=sdf, sharedcm=sharedcm, optionsDict=optionsDict, zbounds=(0,0.001))
        plt.savefig(savefn, dpi = 300, bbox_inches='tight')
        plt.show()
        

def scratchMain():
    #For testing out things.
    evalFunc = pm.sphereFunc
    startDict = {'x':62, 'y':78, 'z':-76}
    startParameters = {'x':(1, 5, 30), 'y':(1, 2, 100)}
    evalParamSetup = pm.evalParamClass(startDict, startParameters = startParameters, evalFunc=evalFunc)
    print(evalParamSetup.evaluate(5,5,5))
    print(evalParamSetup.getBounds())
    param, res= pm.pdfoGeometric(evalParamSetup.getInitialValues(), evalFunc = evalParamSetup.evaluate, bounds= evalParamSetup.getBounds())
    print(param)
    print(res)

if __name__ == "__main__":

    if '-n' in sys.argv:
        numNodes = int(sys.argv[sys.argv.index('-n')+1])
        if numNodes > 1:
            multiproc = True
        else:
            multiproc = True
    else:
        multiproc = False
        numNodes = 1

    if '-j' in sys.argv:
        jobNumber = sys.argv[sys.argv.index('-j')+1]
    else:
        jobNumber = time.asctime().replace(' ', '-').replace(':', '-')
    
    if '-o' in sys.argv:
        outDir = sys.argv[sys.argv.index('-o')+1]
    else:
        outDir = os.getcwd()
    
    if '-m' in sys.argv:
        setMemLimit(int(sys.argv[sys.argv.index('-m')+1]))
    
    if '-upf' in sys.argv:
        upf = sys.argv[sys.argv.index('-upf')+1]
        if upf.lower() == 'true':
            usePenaltyFinal=True
        elif upf.lower() == 'false':
            usePenaltyFinal=False
        else:
            try:
                usePenaltyFinal = float(upf)
            except:
                usePenaltyFinal=upf
    else:
            usePenaltyFinal=False
    
    if '-s' in sys.argv: #sampleset
        sampleSet = sys.argv[sys.argv.index('-s')+1]
    else:
        sampleSet = None

    if '--small' in sys.argv:
        smallTest = True
    else:
        smallTest = False

    if '-t' in sys.argv:
        runType = sys.argv[sys.argv.index('-t')+1]
        if runType == 'cross':
            crossCompareMain()
        if runType == 'penaltyMult':
            linearMain(preset='penaltyMult')
        if runType == 'windowSize':
            linearMain(preset='windowSize')
        if runType == 'windowNum':
            linearMain(preset='windowNum')
        if runType == 'simplex':
            simplexMain()
        if runType == 'res':
            resMain()
        if runType == 'scratch':
            scratchMain()
    else:
        resMain()
        #crossCompareMain()
        #linearMain(preset='penaltyMult')
        #linearMain(preset='windowSize')
        #linearMain(preset='windowNum')
        #simplexMain()

