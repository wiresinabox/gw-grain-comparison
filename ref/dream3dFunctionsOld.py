#A module for specifically dream3d manipulation and generation of pipelines
import subprocess
import json
import numbers
import random
import math
import os
import h5py
from scipy.stats import lognorm
import string
import numpy as np

pathToPipelineRunner = '~/bin/dream3d/bin/PipelineRunner'

#Each pipeline module is keyed as 0 : {Module}

def writePipelines(outFilePath, modulesList):
    #Write a pipeline file
    name = outFilePath.split('/')[-1].split('.')[0]
    jsonDict = {"PipelineBuilder": {"Name": name, "Number_Filters": len(modulesList), "Version":6}}
    for i in range(len(modulesList)):
        module = modulesList[i]
        #print(type(module), module)
        jsonDict[str(i)] = module
    jsonStr = json.dumps(jsonDict, sort_keys=True, indent = 4)
    jsonFile = open(outFilePath, 'w')
    jsonFile.write(jsonStr)
    jsonFile.close()

def genBetaConstants(aspectRatio = 'equiaxed', addAlphRand = True, addBetaRand = True, alphA = 1.1, alphB = 28.9, alphMult = 1, betaA = 30, betaB = 28.9, betaMult = -1, alphRandMult = 1, betaRandMult = 1):
    #Creates the Beta Constants based on this formula:
    #const = (A + Mult*(B* 1/aspectRatio) + randMult*rand())
    if aspectRatio == 'equiaxed':
        alphA = 15
        alphB = 0
        alphMult = 0
        alphRandMult = 1
        betaA = 1.25
        betaB = 0
        betaMult = 0
        betaRandMult = 0.5
        aspectRatio = 1
    elif aspectRatio == 'omega3':
        alphA = 10
        alphB = 0
        alphMult = 0
        alphRandMult = 1
        betaA = 1.5
        betaB = 0
        betaMult = 0
        betaRandMult = 0.5
        aspectRatio = 1

    elif isinstance(aspectRatio, numbers.Number):
        if aspectRatio == 0:
            return
    else:
        return


    if addAlphRand == True:
        alphRand = alphRandMult*random.random()
    elif isinstance(addAlphRand, numbers.Number):
        alphRand = addAlphRand
    else:
        alphRand = 0
    
    alpha = alphA + alphMult*(alphB*(1/aspectRatio)) + alphRand
    
    if addBetaRand == True:
        betaRand = betaRandMult*random.random()
    elif isinstance(addBetaRand, numbers.Number):
        betaRand = addBetaRand
    else:
        betaRand = 0
    
    beta = betaA + betaMult*(betaB*(1/aspectRatio)) + betaRand
    return (alpha, beta)

def genNeighborConst(maxBins, A = 14.0, B = 2.0, C = 0.3):
    #Generates mu and sigma from the number of bins
    #mu = ln(A + B * (binNum - middleBin)
    #sigma = 0.3 + ((middleBin - binNum) / (middleBin * 10)
    if A == 'equiaxed':
        A = 14.0
        B = 2.0
        C = 0.3
    elif A == 'rolled':
        A = 8.0
        B = 1
        C = 0.3
    middleBin = maxBins / 2
    mus = []
    sigmas = []
    for i in range(maxBins):
        try:
            #print(A, B, A+B*(i-middleBin))
            mus.append(math.log(A + B * (i - middleBin)))
        except:
            mus.append('nan')
        sigmas.append(float(C + ((middleBin - i) / (middleBin * 10))))
    return (mus, sigmas)

def logNorm (x, mu, sigma):
    #I hope this is correct
    val = (1 / (x*sigma*math.sqrt(2*math.pi)))*math.exp(-1*((math.log(x) - mu)**2)/(2*(sigma**2)))
    return val

def genBinStats(mu, sigma, maxCut = 5, minCut = 5, binStep = 0.5):
     
    #Get the Cutoffs
    minCutx = math.exp(mu - minCut*sigma)
    maxCutx = math.exp(mu + maxCut*sigma)
    #Create equally spaced bins. For n bins, n + 1 points.
    numOfBins = math.floor(((maxCutx - minCutx) / binStep) + 1);
    #print(mu, sigma, maxCut, minCut, maxCutx, minCutx)
    #The center point is going to be half the binStep
    binNums = [0 for x in range(numOfBins)]
    for i in range(numOfBins):
        binNums[i] = minCutx + (i *binStep) #logNorm(minCutx + (i * binStep), mu, sigma)
        #print(i, minCutx + (i* binStep), binNums[i])

    return binNums

def genAxisODFweight(euler1, euler2, euler3, sigma=1, weight=50000):
    #All values are either values or lists
    if not isinstance(euler1, list):
        euler1 = [euler1]
    if not isinstance(euler2, list):
        euler2 = [euler2]
    if not isinstance(euler3, list):
        euler3 = [euler3]
    if not isinstance(sigma, list):
        sigma = [sigma]
    if not isinstance(weight, list):
        weight = [weight]

    odfDict = {'Euler 1':euler1,
            'Euler 2':euler2,
            'Euler 3':euler3,
            'Sigma':sigma,
            'weight':weight}
    return odfDict


def genStatsData(binMu = 1, binSigma = 0.1, A = 1, B = 1, C = 1, PhaseType = "Primary", Name = "Primary Phase", PhaseFraction = 1, maxCut = 5, minCut = 5, binStep = 0.5, omegaRand = True, baRand = True, caRand = True, euler1 = None, euler2 = None, euler3 = None, eulerSigma=None, weight=None):
    #Generates a single primary phase StatsData. We'll deal with percipitates later.

    #AxisODF-Weights {Euler 1, Euler 2, Euler 3, Sigma, Weight}. How aligned are the grains?
    #Bin Count
    #BinNumber, aka the ESD of each bin.
    #BoundaryArea = 0
    #Crystal Symmetry = 1
    #FeatureSize Distribution {Average aka Mu, Stndard Deviation aka Sigma}
    #FeatureSize Vs B Over A Distribution { Alpha: {}, Beta: {}

    #Step One, Bin Sizes.
    if not isinstance(euler1, type(None)) and not isinstance(euler2, type(None)) and not isinstance(euler3, type(None)) and not isinstance(eulerSigma, type(None)) and not isinstance(weight, type(None)):
        axisODFweights = genAxisODFweight(euler1, euler2, euler3, eulerSigma, weight)
    else:
        axisODFweights = {}

    binNums = genBinStats(binMu, binSigma, maxCut, minCut, binStep); #binNumber
    binCount = len(binNums) #Bin Count
    BoundaryArea = 0
    CrystalSymmetry = 1 #Crystal Symmetry
    FeatureSizeDistrib = {"Average":binMu, "Standard Deviation":binSigma}
    #Create B over A
    if A == B == C:
        baAspectRatio = 'equiaxed'
        caAspectRatio = 'equiaxed'
        neighborType = 'equiaxed'
    else:
        baAspectRatio = A / B
        caAspectRatio = A / C
        neighborType = 'rolled'
    baAlpha = []
    baBeta = []
    caAlpha = []
    caBeta = []
    omega3Alpha = []
    omega3Beta = []
     #Same for both equiaxed and rolled
    for i in range(binCount):
        baTuple = genBetaConstants(baAspectRatio, addAlphRand = baRand, addBetaRand = baRand)
        #print(baTuple[0], baTuple[1])
        baAlpha.append(baTuple[0])
        baBeta.append(baTuple[1])
        caTuple = genBetaConstants(caAspectRatio, addAlphRand = caRand, addBetaRand = caRand)
        caAlpha.append(caTuple[0])
        caBeta.append(caTuple[1])
        omega3Tuple = genBetaConstants('omega3', addAlphRand = baRand, addBetaRand = caRand)
        omega3Alpha.append(omega3Tuple[0])
        omega3Beta.append(omega3Tuple[1])
    FeatureSizeOmega3 = {"Alpha":omega3Alpha, "Beta":omega3Beta, "Distribution Type":"Beta Distribution"} #FeatureSize Vs B Over A Distributions
    FeatureSizeBoA = {"Alpha":baAlpha, "Beta":baBeta, "Distribution Type":"Beta Distribution"} #FeatureSize Vs B Over A Distributions
    FeatureSizeCoA = {"Alpha":caAlpha, "Beta":caBeta, "Distribution Type": "Beta Distribution"} #FeatureSize Vs B Over A Distributions
    
    neighborTuple = genNeighborConst(binCount, neighborType) 
    FeatureSizeNeighbor = {"Average":neighborTuple[0], "Standard Deviation":neighborTuple[1], "Distribution Type": "Log Normal Distribution"}
     

    #Feature Diameter Info is [stepSize, LargestBinNum, SmallestBinNum]
    FeatureDiameterInfo = [binStep, max(binNums), min(binNums)]
    MDFweights = {}
    ODFweights = {}

    StatsDataArray ={ "1":{"AxisODF-Weights": axisODFweights,
            "Bin Count": binCount,
            "BinNumber": binNums,
            "BoundaryArea": BoundaryArea,
            "Crystal Symmetry": CrystalSymmetry,
            "FeatureSize Distribution": FeatureSizeDistrib,
            "FeatureSize Vs B Over A Distributions": FeatureSizeBoA,
            "FeatureSize Vs C Over A Distributions": FeatureSizeCoA,
            "FeatureSize Vs Neighbors Distributions": FeatureSizeNeighbor,
            "FeatureSize Vs Omega3 Distributions": FeatureSizeOmega3,
            "Feature_Diameter_Info": FeatureDiameterInfo,
            "MDF-Weights": MDFweights,
            "Name": Name,
            "ODF-Weights": ODFweights,
            "PhaseFraction": PhaseFraction,
            "PhaseType": PhaseType},
            "Name" : "Statistics",
            "Phase Count": 2 
            }
    return StatsDataArray
    

def genStatsGeneratorModule (binMu = 1, binSigma = 0.1, binStep = 0.5, ratioA = 1, ratioB = 1, ratioC = 1, maxCut = 5, minCut = 5, omegaRand = True, baRand = True, caRand = True, euler1 = None, euler2 = None, euler3=None, eulerSigma = None, weight=None):

    CellEnsembleAttributeMatrixName = "CellEnsembleData"
    CrystalStructureArrayName = "CrystalStructures"
    Filter_Human_Label = "StatsGenerator"
    PhaseNamesArrayName = "PhaseName" 
    PhaseTypesArrayName = "PhaseTypes" 
    StatsDataArrayName ="Statistics" 
    StatsGeneratorDataContainerName = "StatsGeneratorDataContainer"
    Filter_Enabled = True
    Filter_Name = "StatsGeneratorFilter"
    Filter_Uuid = "{f642e217-4722-5dd8-9df9-cee71e7b26ba}"
    #StatsDataArray={1:{...}, 2:{...}} of each phase
    #Also needs Phase Count
    StatsDataArray = genStatsData(binMu = binMu, binSigma = binSigma, binStep = binStep, A = ratioA, B = ratioB, C = ratioC, maxCut = maxCut, minCut = minCut, omegaRand = omegaRand, baRand = baRand, caRand = caRand, euler1 = euler1, euler2=euler2, euler3=euler3, eulerSigma=eulerSigma, weight=weight)

    StatsGenArray = {"CellEnsembleAttributeMatrixName": CellEnsembleAttributeMatrixName, "CrystalStructureArrayName":CrystalStructureArrayName, "Filter_Enabled":Filter_Enabled, "Filter_Human_Label":Filter_Human_Label, "Filter_Name":Filter_Name, "Filter_Uuid":Filter_Uuid, "PhaseNamesArrayName":PhaseNamesArrayName, "PhaseTypesArrayName": PhaseTypesArrayName, "StatsDataArrayName":StatsDataArrayName, "StatsGeneratorDataContainerName":StatsGeneratorDataContainerName, "StatsDataArray":StatsDataArray}

    return StatsGenArray
    #Just for nowS

    #A module designed to rederive and calculate the statsgenerator module (or at least some of it) in python
    #Step 1: Bin Sizes based on a Log Normal Distribution
    #mu is the log(ESD), where ESD is the equivalent sphere diameter (in voxels)
    #sigma is the standard deviation
    #cut off values are calculated by minCutoff = exp(mu - min*sigma) and maxCutoff = exp(mu + max *sigma)
    #Bin Size is calculated by: numBins = (maxCutoff - minCutoff / binStepSize) + 1
    #Aspect Ratio of B/A see SyntheticBuildingFilters/Presets/PrimaryRolledPresets.cpp
    #Under initializeBOverATableModel
    #Effectively, it is a beta distribution with
    #Equiaxed:
    #alpha = 15 + randDouble
    #beta = 1.25 + (0.5 * randDouble)
    #Rolled:
    #alpha = (1.1 + (28.9 * (1/AspectRatio))) + randDouble (random.cpp)
    #beta = (30 - (28.9 * (1/AspectRatio))) + randDouble (random.cpp)
    #With the aspect ratio = A/B or A/C, A >= B >= C, and randDouble from [0, 1)

    #Omega3 values for both use the equillized axis equation

    #Neighbors work on a similar log-normal distribution
    #Equiaxed:
    #In a loop by i, and middlebin being the num-of-bins / 2
    #mu = ln(14.0 + (2.0 * (i - middlebin))
    #sigma = 0.3 + ((middlebin - i) / (middlebin * 10))
    #Rolled:
    #mu = ln(8.0 + (1.0 * (i - middlebin))
    #sigma = 0.3 + ((middlebin - i) / (middlebin * 10))


def genSyntheticVolumeModule(axisLength = 32, resolution = 0.25):
    dimLength = math.ceil(axisLength/resolution)
    trueLength = dimLength*resolution
    
    boxDimensions = "X Range: 0 to {xdel} (Delta: {xdel})\nY Range: 0 to {ydel} (Delta: {ydel})\nZ Range: 0 to {zdel} (Delta: {zdel})".format(xdel = trueLength, ydel = trueLength, zdel = trueLength) 
    CellAttributeMatrixName = "CellData"
    DataContainerName = "SyntheticVolumeDataContainer"
    Dimensions = {'x': dimLength, 'y': dimLength, 'z': dimLength}
    EnsambleAttributeMatrixName = 'CellEnsembleData'
    EstimateNumberOfFeatures = 0
    EstimatedPrimaryFeatures = ''
    FilterVersion = '6.5.128'
    Filter_Enabled = True
    Filter_Human_Label = "Initialize Synthetic Volume"
    Filter_Name = "InitializeSyntheticVolume"
    Filter_Uuid = "{c2ae366b-251f-5dbd-9d70-d790376c0c0d}"
    InputPhaseTypesArrayPath = {
        "Attribute Matrix Name": "CellEnsembleData",
        "Data Array Name": "PhaseTypes",
        "Data Container Name": "StatsGeneratorDataContainer"
    }
    InputStatsArrayPath = {
        "Attribute Matrix Name": "CellEnsembleData",
        "Data Array Name": "Statistics",
        "Data Container Name": "StatsGeneratorDataContainer"
    }
    Origin = {'x': 0, 'y': 0, 'z': 0}
    Resolution = {'x': resolution, 'y': resolution, 'z' : resolution}

    synthVolData = {
            "boxDimensions" : boxDimensions,
            "CellAttributeMatrixName" : CellAttributeMatrixName,
            "DataContainerName" : DataContainerName,
            "Dimensions" : Dimensions,
            "EnsambleAttributeMatrixName" :EnsambleAttributeMatrixName,
            "EstimateNumberOfFeatures" :EstimateNumberOfFeatures,
            "EstimatedPrimaryFeatures" :EstimatedPrimaryFeatures,
            "FilterVersion" : FilterVersion,
            "Filter_Enabled" : Filter_Enabled,
            "Filter_Human_Label" : Filter_Human_Label,
            "Filter_Name" : Filter_Name,
            "Filter_Uuid" : Filter_Uuid,
            "InputPhaseTypesArrayPath" : InputPhaseTypesArrayPath,
            "InputStatsArrayPath" : InputStatsArrayPath,
            "Origin" : Origin,
            "Resolution" : Resolution
            }
    return synthVolData

def genEstablishShapeTypesModule():
    estShapeData={
    "FilterVersion": "6.5.128",
    "Filter_Enabled": True,
    "Filter_Human_Label": "Establish Shape Types",
    "Filter_Name": "EstablishShapeTypes",
    "Filter_Uuid": "{4edbbd35-a96b-5ff1-984a-153d733e2abb}",
    "InputPhaseTypesArrayPath": {
        "Attribute Matrix Name": "CellEnsembleData",
        "Data Array Name": "PhaseTypes",
        "Data Container Name": "StatsGeneratorDataContainer"
    },
    "ShapeTypeData": [
        999,
        0
    ],
    "ShapeTypesArrayName": "ShapeTypes"
    }
    return estShapeData

def genPrimaryPackingModule():
    

    packingModuleData = {
        "CellPhasesArrayName": "Phases",
        "FeatureGeneration": 0,
        "FeatureIdsArrayName": "FeatureIds",
        "FeatureInputFile": "/home/ethan",
        "FeaturePhasesArrayName": "Phases",
        "FilterVersion": "6.5.128",
        "Filter_Enabled": True,
        "Filter_Human_Label": "Pack Primary Phases",
        "Filter_Name": "PackPrimaryPhases",
        "Filter_Uuid": "{84305312-0d10-50ca-b89a-fda17a353cc9}",
        "InputPhaseNamesArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "PhaseName",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "InputPhaseTypesArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "PhaseTypes",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "InputShapeTypesArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "ShapeTypes",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "InputStatsArrayPath": {
            "Attribute Matrix Name": "CellEnsembleData",
            "Data Array Name": "Statistics",
            "Data Container Name": "StatsGeneratorDataContainer"
        },
        "MaskArrayPath": {
            "Attribute Matrix Name": "CellData",
            "Data Array Name": "Mask",
            "Data Container Name": "ImageDataContainer"
        },
        "NewAttributeMatrixPath": {
            "Attribute Matrix Name": "Synthetic Shape Parameters (Primary Phase)",
            "Data Array Name": "",
            "Data Container Name": "SyntheticVolumeDataContainer"
        },
        "NumFeaturesArrayName": "NumFeatures",
        "OutputCellAttributeMatrixPath": {
            "Attribute Matrix Name": "CellData",
            "Data Array Name": "",
            "Data Container Name": "SyntheticVolumeDataContainer"
        },
        "OutputCellEnsembleAttributeMatrixName": "CellEnsembleData",
        "OutputCellFeatureAttributeMatrixName": "CellFeatureData",
        "PeriodicBoundaries": 0,
        "SaveGeometricDescriptions": 0,
        "SelectedAttributeMatrixPath": {
            "Attribute Matrix Name": "",
            "Data Array Name": "",
            "Data Container Name": ""
        },
        "UseMask": 0
    }
    return packingModuleData

def genWriteModule(saveFilePath):
    cwd = os.getcwd()
    sd,part,fn = saveFilePath.rpartition('/')
    os.chdir(sd)
    fullfn = os.getcwd() + part + fn
    os.chdir(cwd)
    writeModuleData =  {
        "FilterVersion": "1.2.809",
        "Filter_Enabled": True,
        "Filter_Human_Label": "Write DREAM.3D Data File",
        "Filter_Name": "DataContainerWriter",
        "Filter_Uuid": "{3fcd4c43-9d75-5b86-aad4-4441bc914f37}",
        "OutputFile": fullfn,
        "WriteTimeSeries": 0,
        "WriteXdmfFile": 1
    }
    return writeModuleData

def runDream3d(jsonFilePath):
    cwd = os.getcwd()
    sd,part,fn = jsonFilePath.rpartition('/')
    os.chdir(sd)
    fullfn = os.getcwd() + part + fn
    os.chdir(cwd)
    subprocess.run("{} -p {}".format(pathToPipelineRunner, fullfn), shell = True)

def runSynthPipeline(jsonFilePath, dream3dFilePath, mu, sigma, baRatio, caRatio, binStep=0.5, minCut=5, maxCut=5, axisLength = 32, resolution = 0.25, euler1 = None, euler2 = None, euler3= None, eulerSigma = None, weight = None):
    print('RUNNING SYNTH PIPELINE')
    print('mu:', mu)
    print('sigma:', sigma)
    print('baRatio:', baRatio)
    print('caRatio:', caRatio)
    print('binStep:', binStep)
    print('minCut:', minCut)
    print('maxCut:', maxCut)
    print('axisLength:', axisLength)
    print('resolution:', resolution)
    pipelineList = []
    pipelineList.append(genStatsGeneratorModule(binMu = mu, binSigma = sigma, binStep= binStep, minCut = minCut, maxCut = maxCut, ratioA = 1, ratioB = baRatio, ratioC = caRatio, euler1 = euler1, euler2=euler2, euler3=euler3, eulerSigma=eulerSigma, weight=weight))
    pipelineList.append(genSyntheticVolumeModule(axisLength = axisLength, resolution = resolution))
    pipelineList.append(genEstablishShapeTypesModule())
    pipelineList.append(genPrimaryPackingModule())
    pipelineList.append(genWriteModule(dream3dFilePath))
    writePipelines(jsonFilePath, pipelineList)
   

    print(jsonFilePath)
    print(dream3dFilePath)
    print('Running Dream3d?')
    runDream3d(jsonFilePath)
    
    #presetModel is either equiaxed or rolled, or custom?

#homeDir = '/home/ethan/Desktop/gw-research'
#runSynthPipeline( homeDir + '/synthetictest.json', homeDir + '/synthetictest.dream3d')


#SIMPLEX ALGORITHEM
#Numerical Recipies in C (second edition), check this. Don't use it, they will sue you.
#in n dimensions, we have n+1 points.
#10 choices of all 9 parameters.
#Start small, it will expand itself.

def getPipeline(d3filePath):
    #returns a dictionary of the exported dream3d pipeline. Use json.dumps to write it to json string again.
    f1d3 = h5py.File(d3filePath, 'r')
    f1d3Pipe = f1d3['Pipeline']['Pipeline']
    dest = np.array((1,1), dtype = f1d3Pipe.dtype)
    f1d3Pipe.read_direct(dest)
    d3pipestr = str(dest[0]) 
    lines = d3pipestr.strip().split("\\n")
    d3formatstr = ''
    d3jsonstr = ''
    for line in lines:
        d3formatstr = d3formatstr + line.replace('\\', '",')+'\n'
        d3jsonstr = d3jsonstr + line.strip().replace('\\', ',')
    #print(d3formatstr)
    d3jsonstr = d3jsonstr[1:].strip("'").strip()
    #outfile = open('test.txt', 'w')
    #outfile.write(d3formatstr)
    #outfile.close()
    jsonPipeDict = json.loads(d3jsonstr)
    return(jsonPipeDict)

def writePipeFromDict(outFilePath, jsonDict):
    moduleList = []
    #print(jsonDict.keys())
    #for i in range(len(jsonDict)-1):
    for key in sorted(list(jsonDict.keys())):
        if any(num in list(string.digits) for num in list(key)):
            moduleList.append(jsonDict[key])
    writePipelines(outFilePath, moduleList)
