# Wasserstein Grain Comparison

This repository contains several python scripts used to approximate
the underlying microstructure distributions for single phase polycrystalline
microstructures using the Wasserstein metric. 

The included linearcompare.py also generates the representative samples used
to benchmark the method.

hardW() and softW() is transliterated from MATLAB code by Jeremy Mason.

ref contains the original project code. src contains the cleaned up version,
tests contain two examples that showcase several aspects of the program.

## Dependencies

Python 3.6+

Python Libraries:

 * [Numpy][numpy]
 * [h5py][h5py]
 * [Matplotlib][matplotlib]

In addition, several other repositories are needed.

 * [Dream3D][Dream3D] (optional, used to generate synthetic samples on the fly)
 * [gw_dist][gw_dist] (optional, used to calculate the microstructure scores from the pairwise window distances)
 * [Discrete Optimal Transport Library][dotlib] (optional but HIGHLY recommended, used to calculate pairwise window scores)

## Usage

There are three environmental variables that can be set. These can also be set through python functions at runtime.

 * GW_LIB_DIR - /path/to/lib directory where libgwdist.so is located. (optional)
 * SOLVER2D_LIB_DIR - /path/to/dotlib-master directory where solver.so is located. (optional but HIGHLY recommended)
 * D3_PIPELINE_DIR - /path/to/dream3d/bin/PipelineRunner (optional)


## License
This project is licensed under the [MIT License](https://opensource.org/licenses/MIT)

## Contributors

Ethan Suwandi (etsuwandi@ucdavis.edu)

Jeremy Mason (jkmason@ucdavis.edu)

[numpy]:https://numpy.org/
[h5py]:https://www.h5py.org/
[matplotlib]:https://matplotlib.org/
[Dream3D]:http://dream3d.bluequartz.net/
[gw_dist]:
[dotlib]:https://github.com/stegua/dotlib:
