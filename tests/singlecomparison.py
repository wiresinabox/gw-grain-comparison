import sys
sys.path.append('../src')
import sampleGenerator as sg
import microcompare as mc
import gwFunctions as gwf
import dream3dFunctions as d3f

#Single comparison between an equiaxed DREAM3D synthetic microstructure and a hexagonal pattern.

#These can also be set as environmental variables
gwf.set_GW_LIB_DIR('/usr/local/lib') 
gwf.set_SOLVER2D_LIB_DIR('../../dotlib-master')
d3f.set_D3_PIPELINE_DIR('~/bin/dream3d/bin/PipelineRunner')

#Generate equiaxed sructure from parameters:
sample1 = 'eq.dream3d'
d3parameters = {
    'axisLength': 16, 
    'resolution': 0.125, 
        }
phase1 = {
    'Name': 'Primary',
    'PhaseType': 'Primary',
    'PhaseFraction': 0.75,
    'mu': 1, 
    'sigma': 0.1, 
    'binStep': 0.5,
    'minCut': 5,
    'maxCut': 5,
    'euler1':0,
    'euler2':0,
    'euler3':0,
    'eulerSigma':0,
    'weight':0,
    }

phase2 = { #percipitate
    'Name': 'Particles',
    'PhaseType': 'Precipitate',
    'PhaseFraction': 0.25,
    'mu': 0.3, 
    'sigma': 0.1, 
    'binStep': 0.5,
    'minCut': 5,
    'maxCut': 5,
    'euler1':0,
    'euler2':0,
    'euler3':0,
    'eulerSigma':0,
    'weight':0,

    }
#d3f.runSynthPipeline(jsonFilePath='eq.json', dream3dFilePath=sample1, verbose=False, **d3parameters)
d3f.runSynthPipeline(jsonFilePath='eq.json', dream3dFilePath=sample1, verbose=True, phaseKwargs=[phase1, phase2], **d3parameters)

#Generate a single 2D hexagonal slice.
sample2 = sg.hexagonTile(128, sideLen = 5)

#Generate windows used for comparison from microstructures.
windowList1, windowList2 = mc.generateWindows(sample1, sample2, windowSize=20, windowNum=20, sliceSDF=True)

#Run comparison.
msScore, scoreMat, G = mc.compareWindows(windowList1, windowList2)
print('Pairwise Scores:')
print(scoreMat)
print('Gamma:')
print(G)
print('Microstructure Score:')
print(msScore)
