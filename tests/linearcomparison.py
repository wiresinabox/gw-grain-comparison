import sys
import os
sys.path.append('../src')

import matplotlib.pyplot as plt
import mputil as mpu
import numpy as np

import sampleGenerator as sg
import microcompare as mc
import gwFunctions as gwf
import dream3dFunctions as d3f

#Generation and comparison of representative samples as well as a demonstration of multiprocessing usage

#How many children processes will be spawned
numSubprocesses = 3 

#File paths to dependencies. These can also be set as environmental variables.
gwf.set_GW_LIB_DIR('/usr/local/lib') 
gwf.set_SOLVER2D_LIB_DIR('../../dotlib-master')
d3f.set_D3_PIPELINE_DIR('~/bin/dream3d/bin/PipelineRunner')

#Generate representative samples 
generateSamples=True #If True, will regenerate samples.

sampleDir = 'd3samples'
try: os.mkdir(sampleDir)
except FileExistsError: pass
d3parameters = {
    'axisLength': 16, 
    'resolution': 0.125, 
        }

sampleList = []

sample = sampleDir+'/'+'EI.dream3d'
sampleList.append(sample)
phase1 = {
    'mu': 1, 
    'sigma': 0.1, 
    }
if generateSamples: d3f.runSynthPipeline(jsonFilePath=sample.replace('.dream3d', '.json'), dream3dFilePath=sample, verbose=False, phaseKwargs=phase1, **d3parameters)

sample = sampleDir+'/'+'EII.dream3d'
sampleList.append(sample)
phase1 = {
    'mu': 1.68, 
    'sigma': 0.1, 
    }
if generateSamples: d3f.runSynthPipeline(jsonFilePath=sample.replace('.dream3d', '.json'), dream3dFilePath=sample, verbose=False, phaseKwargs=phase1, **d3parameters)

sample = sampleDir+'/'+'BI.dream3d'
sampleList.append(sample)
phase1 = {
    'mu': 1, 
    'sigma': 0.1,
    'PhaseFraction': 0.75
    }
phase2 = { 
    'Name': 'Particles',
    'PhaseType': 'Precipitate',
    'PhaseFraction': 0.25,
    'mu': 0.3, 
    'sigma': 0.1, 
    }
 
if generateSamples: d3f.runSynthPipeline(jsonFilePath=sample.replace('.dream3d', '.json'), dream3dFilePath=sample, verbose=False, phaseKwargs=[phase1, phase2], **d3parameters)

sample = sampleDir+'/'+'BII.dream3d'
sampleList.append(sample)
phase1 = {
    'mu': 1, 
    'sigma': 0.1,
    'PhaseFraction': 0.75
    }
phase2 = { 
    'Name': 'Particles',
    'PhaseType': 'Precipitate',
    'PhaseFraction': 0.25,
    'mu': 1.68, 
    'sigma': 0.1, 
    }
if generateSamples: d3f.runSynthPipeline(jsonFilePath=sample.replace('.dream3d', '.json'), dream3dFilePath=sample, verbose=False, phaseKwargs=[phase1, phase2], **d3parameters)

sample = sampleDir+'/'+'RI.dream3d'
sampleList.append(sample)
phase1 = {
    'mu': 1, 
    'sigma': 0.1,
    'baRatio':0.5,
    'caRatio':0.25,
    'euler1':0,
    'euler2':0,
    'euler3':0,
    'eulerSigma':1,
    'weight':40000,
    }
if generateSamples: d3f.runSynthPipeline(jsonFilePath=sample.replace('.dream3d', '.json'), dream3dFilePath=sample, verbose=False, phaseKwargs=phase1, **d3parameters)


#Make comparisons
refSample = sampleList[4] #RI
windowSizes = np.arange(60, 9, -10) 
scoresArray = np.zeros((windowSizes.size, len(sampleList)))
sliceNum=np.random.randint(0, int(d3parameters['axisLength']/d3parameters['resolution']), 10) #Selects 10 random slices to compare to avoid sampling the same region multiple times. 

for i in range(windowSizes.size):
    windowSize = windowSizes[i]
    for j in range(len(sampleList)):
        sample = sampleList[j]

        print('{} px: {} vs {}'.format(windowSize, refSample, sample))
        #If the multiprocessing module isn't running in the background yet, the mc.generateWindows() and mc.compareWindows() will automatically start it. It can also be explicitly be started with mpu.startMP(cores). 
        windowList1, windowList2 = mc.generateWindows(refSample, sample, 
                windowSize=windowSize, 
                windowNum=100, 
                axis='y', 
                sliceNum=sliceNum, 
                sliceSDF=True, 
                sdfReverse=True,
                multiprocessing=True, 
                cores=numSubprocesses)
        
        #The jobid arguement is useful to avoid collisions if you have multiple runs writing to the same window temp directory (dottemp by default). 
        msScore, scoreMat, G = mc.compareWindows(windowList1, windowList2, jobid='linear', multiprocessing=True, cores=numSubprocesses)
        scoresArray[i,j] = msScore


mpu.endMP() #If multiprocessing is utilized, this must be called otherwise the program will hang at the end.
print('Remember to clean up your temp file directory if you previously interrupted!')

#Plotting
fig, ax = plt.subplots()

for j in range(len(sampleList)):
    sample = sampleList[j]
    label='{} vs {}'.format(refSample.replace(sampleDir+'/', '').replace('.dream3d', ''), sample.replace(sampleDir+'/', '').replace('.dream3d', ''))
    ax.plot(windowSizes, scoresArray[:, j], label=label)


ax.set_xlabel('Window Size (px)')
ax.set_ylabel('Microstructure Score')
ax.legend()
plt.show()
